add_executable(${Target})

find_package(libstd REQUIRED)

include(${CMAKE_CURRENT_LIST_DIR}/../os_config.cmake)
