#!/bin/python3

def handler_wrapper(name, errcode=False):
  print(".globl handler_"+name+"_wrapper")
  print("handler_"+name+"_wrapper:")
  if errcode:
    print("\txor %r15, (%rsp) # swap(errcode,r15)")
    print("\txor (%rsp), %r15")
    print("\txor %r15, (%rsp)")
  else:
    print("\tpush %r15")
  print("\tpush %r14")
  print("\tpush %r13")
  print("\tpush %r12")
  print("\tpush %r11")
  print("\tpush %r10")
  print("\tpush %r9")
  print("\tpush %r8")
  print("\tpush %rdi")
  print("\tpush %rsi")
  print("\tpush %rdx")
  print("\tpush %rcx")
  print("\tpush %rbx")
  print("\tpush %rax")
  print("\tpush %rbp")

  print("\tmov %rsp, %rdi")
  if errcode:
    print("\tmov %r15, %rsi")
  print("\tmovabsq $handler_"+name+", %rax")
  print("\tcall *%rax")

  print("\tpop %rbp")
  print("\tpop %rax")
  print("\tpop %rbx")
  print("\tpop %rcx")
  print("\tpop %rdx")
  print("\tpop %rsi")
  print("\tpop %rdi")
  print("\tpop %r8")
  print("\tpop %r9")
  print("\tpop %r10")
  print("\tpop %r11")
  print("\tpop %r12")
  print("\tpop %r13")
  print("\tpop %r14")
  print("\tpop %r15")
  print("\tiretq")

handler_wrapper("de")
handler_wrapper("bp")
handler_wrapper("of")
handler_wrapper("db")
handler_wrapper("br")
handler_wrapper("ud")
handler_wrapper("nm")
handler_wrapper("df", True)
handler_wrapper("ts", True)
handler_wrapper("np", True)
handler_wrapper("ss", True)
handler_wrapper("gp", True)
handler_wrapper("pf", True)
handler_wrapper("mf")
handler_wrapper("ac", True)
handler_wrapper("mc")
handler_wrapper("xm")
handler_wrapper("ve")
handler_wrapper("cp", True)
handler_wrapper("hv")
handler_wrapper("vc", True)
handler_wrapper("sx", True)

handler_wrapper("default")
handler_wrapper("pf_lazy", True)
handler_wrapper("cmos")
handler_wrapper("timer")
handler_wrapper("kbd")
handler_wrapper("syscall")
