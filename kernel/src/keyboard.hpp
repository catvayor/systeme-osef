#pragma once

#include <stdint.h>
#include <array>
#include <keyboard_specials>
#include <kqueue>

namespace Keyboard {
  void init();
  void kbd_input(uint8_t code); // Kbd event handler - could be used to simulate key presses
  bool haschar();
  int16_t getchar();

  class KbdState {
   public:
    void init();
    bool pressed(uint8_t c, bool ext = false) const; // key code (not ascii)
    bool alt_pressed() const;
    bool altgr_pressed() const;
    bool shift_pressed() const;
    bool ctrl_pressed() const;
    bool mod4_pressed() const;
    int16_t key_code(uint8_t c); // changes state as if c was received from keyboard

   private:
    std::array<std::array<bool, 128>, 2> states; // [ext][code]
    bool got_ext = false;

    bool get(uint8_t c, bool ext) const;
    void set(uint8_t c, bool ext);
    void clear(uint8_t c, bool ext);
  };

  KbdState get_state();

  void switch_layout();
  unsigned get_layout();
  constexpr unsigned LAY_US = 0;
  constexpr unsigned LAY_FR = 1;
}
