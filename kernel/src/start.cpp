#include <array>
#include <ata>
#include <boot_info>
#include <interact>
#include <string>
#include <terminal>
#include <virtual_memory_base>
#include <x86>
#include "exceptions.hpp"
#include "gdt.hpp"
#include "keyboard.hpp"
#include <kmalloc>
#include "scheduler.hpp"
#include "null_file.hpp"

#include "fs.hpp"
#include "physical_mount.hpp"

constexpr uintptr_t stack_top = 0xFFFF808000000000;

void test_drive();

[[noreturn]] void main() {
  Files::root->create_dir<Files::PhysicalMount>(0, "mnt");
  auto dev = Files::root->new_dir("dev");
  dev->create_file<Files::NullFile>("null");

  ProcessPool::init();

  Keyboard::init();
  Terminal::print("Press enter to continue...");
  while (Keyboard::getchar() != '\n') x86::hlt();
  Terminal::newline();

  Interact::display_banner();

  ProcessPool::Scheduler::scheduling_loop();

  Interact::warning("Kernel end.");
  x86::freeze();
}

void free_bootloader() {
  // Require
  Interact::init();
  // Init
  Terminal::print("Free bootloader...");

  virtual_memory::free_bootloader(bootloader_size);
  virtual_memory::PML3_entry(nullptr) = 0;
  virtual_memory::reloadPaging();

  // Done
  Interact::print_done();
}

extern "C" void _init();

extern "C" [[noreturn]] void _start() {
  asm("movq %0, %%rsp" :: "i"(stack_top));
  Gdt::init((void*)stack_top);
  Interrupt::clear_int();
  load_boot_info();
  Interrupt::init();

  free_bootloader();
  Drive::init();
  _init();
  main();
}

void test_drive() {
  Drive::init();
  uint16_t buf[256];
  Drive::ide_read_sectors(0, // drive
                          0, // lba
                          1, // numsects
                          buf);
  for (int i = 0; i < 256; ++i) {
    Terminal::putc((char) buf[i]);
    Terminal::putc((char)(buf[i] >> 8));
  }
  Terminal::print("\n-----\n");
  buf[0] = 'X' + (((uint16_t)'X') << 8);
  Drive::ide_write_sectors(0, // drive
                           0, // lba
                           1, // numsects
                           buf);
  buf[0] = 'Y' + (((uint16_t)'Y') << 8);
  Drive::ide_read_sectors(0, // drive
                          0, // lba
                          1, // numsects
                          buf);
  for (int i = 0; i < 256; ++i) {
    Terminal::putc((char) buf[i]);
    Terminal::putc((char)(buf[i] >> 8));
  }
}
