#pragma once

#include "virtual_file.hpp"

namespace Files {
  class Pipe: public VirtualFile {
    kqueue<uint8_t> M_data;
    bool read_connected = false;
    bool write_connected = false;

   protected:
    void fopen_read() override;
    void fopen_write() override;
    void fclose_read() override;
    void fclose_write() override;

   public:
    Pipe(kstring const& name, VirtualDir* parent);

    void write(kqueue<uint8_t>& buf) override;
    kvector<uint8_t> read(uint64_t) override;
    bool writable() override;
    bool readable() override;
    int64_t size() override;
  };

  class FlushPipe : public VirtualFile {
    kqueue<kqueue<uint8_t>> M_data;
   public:
    FlushPipe(kstring const& name, VirtualDir* parent);
    void write(kqueue<uint8_t>& buf) override;
    kvector<uint8_t> read(uint64_t) override;
    bool writable() override;
    bool readable() override;
    int64_t size() override;
  };
}
