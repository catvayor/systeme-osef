#pragma once

#include "physical_dir.hpp"

namespace Files {

  class PhysicalMount : public PhysicalDir {
    fs::filesys_t fs;

   public:
    PhysicalMount(uint8_t drive, kstring const& name, VirtualDir* parent);
    void flush() override;
  };

}
