#include "pipe_files.hpp"

namespace Files {
  constexpr unsigned max_size = 0x1000;

  Pipe::Pipe(kstring const& name, VirtualDir* parent) : VirtualFile(name, parent) {}

  void Pipe::fopen_read() {
    read_connected = true;
    VirtualFile::fopen_read();
  }

  void Pipe::fopen_write() {
    write_connected = true;
    VirtualFile::fopen_write();
  }

  void Pipe::fclose_read() {
    read_connected = false;
    VirtualFile::fclose_read();
  }

  void Pipe::fclose_write() {
    write_connected = false;
    VirtualFile::fclose_write();
  }

  void Pipe::write(kqueue<uint8_t>& buf) {
    while (writable() && !buf.empty()) {
      M_data.push(buf.pop().value());
    }
  }

  kvector<uint8_t> Pipe::read(uint64_t max) {
    kvector<uint8_t> ret;
    for (uint64_t k = 0; k < max; k++) {
      if (M_data.empty()) break;
      ret.push_back(M_data.pop().value());
    }
    return ret;
  }

  bool Pipe::writable() {
    return M_data.size() < max_size;
  }

  bool Pipe::readable() {
    return !M_data.empty() || !write_connected; // !write_connected: eof
  }

  int64_t Pipe::size() {
    return M_data.size();
  }


  FlushPipe::FlushPipe(kstring const& name, VirtualDir* parent) : VirtualFile(name, parent) {}

  void FlushPipe::write(kqueue<uint8_t>& buf) {
    M_data.push(buf);
    buf.clear();
  }

  kvector<uint8_t> FlushPipe::read(uint64_t max) {
    kvector<uint8_t> ret;
    if (M_data.empty()) return ret;
    while (ret.size() < max && !M_data.front().empty()) {
      ret.push_back(M_data.front().pop().value());
    }
    if (M_data.front().empty()) M_data.pop();
    return ret;
  }

  bool FlushPipe::writable() {
    return true;
  }

  bool FlushPipe::readable() {
    return !M_data.empty();
  }

  int64_t FlushPipe::size() {
    return M_data.size();
  }
}
