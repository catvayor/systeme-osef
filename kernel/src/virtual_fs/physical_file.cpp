#include "physical_file.hpp"

namespace Files {

  PhysicalFile::PhysicalFile(fs::file_t* pfile, VirtualDir* parent)
    : CursorFile(ku16string_to_kstring(pfile->filename), parent), pfile(pfile) { }

  void PhysicalFile::fopen() {
    pfile->read_data();
    CursorFile::fopen();
  }

  void PhysicalFile::fclose() {
    flush();
    pfile->unread();
    CursorFile::fclose();
  }

  int64_t PhysicalFile::size() {
    if (pfile->data_read)
      return pfile->required_len();
    else
      return pfile->len;
  }

  void PhysicalFile::write_at(const uint8_t* str, uint64_t count, uint64_t pos) {
    pfile->changed = true;
    if (pos + count > pfile->fdata.size())
      pfile->fdata.resize(pos + count);
    std::memcpy(pfile->fdata.data() + pos, str, count);
  }

  void PhysicalFile::read_at(uint8_t* dest, uint64_t count, uint64_t pos) {
    //bound checking is already done
    std::memcpy(dest, pfile->fdata.data() + pos, count);
  }

  void* PhysicalFile::data() {
    return pfile->fdata.data();
  }

  bool PhysicalFile::has_raw_access() {
    return pfile->data_read;
  }

  void PhysicalFile::trunc() {
    pfile->fdata.clear();
  }

  bool PhysicalFile::truncable() {
    return true;
  }

  bool PhysicalFile::resizable() {
    return true;
  }

  void PhysicalFile::resize(uint64_t size) {
    pfile->fdata.resize(size);
  }
}
