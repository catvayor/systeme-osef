#pragma once

#include <kstring>
#include <kvector>
#include "path.hpp"

namespace Files {

  enum EntryType {
    et_null,
    et_dir,
    et_file,
  };

  struct DirEntry {
    EntryType type;
    kstring name;

    DirEntry() = default;
    DirEntry(EntryType t, kstring n);
  };

  class VirtualFile;

  class VirtualDir {
    VirtualDir* m_parent;
    kstring name;

    friend VirtualFile;

   protected:
    //public ??
    kvector<VirtualFile*> files;
    kvector<VirtualDir*> subdirs;
    virtual void flush();

   public:

    VirtualDir(kstring const& dirname, VirtualDir* parent);
    VirtualDir(VirtualDir const&) = delete;
    VirtualDir(VirtualDir&&) = delete;
    virtual ~VirtualDir();

    void forget_file(VirtualFile*);
    void forget_dir(VirtualDir*);
    kstring dirname();

    template<typename T, typename... Args>
    T* create_file(Args&&... args) {
      T* ret = new T(std::forward<Args>(args)..., this);
      files.push_back(ret);
      return ret;
    }

    template<typename T, typename... Args>
    T* create_dir(Args&&... args) {
      T* ret = new T(std::forward<Args>(args)..., this);
      subdirs.push_back(ret);
      return ret;
    }

    //default impl : virtual (in ram) cursor file
    //override for physical : real cursor file
    virtual VirtualFile* new_file(kstring const& filename);

    //default impl : this type
    //for physical : real directory
    virtual VirtualDir* new_dir(kstring const& dirname);

    VirtualFile* get_file(kstring const& filename, bool create = false);
    VirtualDir* get_dir(kstring const& dirname, bool create = false);
    VirtualFile* find_file(kstring const& path, bool create = false, bool make_p = false);
    VirtualDir* find_dir(kstring const& path, bool create = false, bool make_p = false);

    VirtualFile* find_file(Path const& path, bool create = false, bool make_p = false);
    VirtualDir* find_dir(Path const& path, bool create = false, bool make_p = false);

    kvector<DirEntry> dir_ls();
    VirtualDir* parent();
  };

  extern VirtualDir* root;

}
