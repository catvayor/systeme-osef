#include "cursor_file.hpp"
#include <algorithm>

namespace Files {

  CursorFile::CursorFile(kstring const& filename, VirtualDir* parent)
    : VirtualFile(filename, parent) { }

  bool CursorFile::seekable() {
    return true;
  }

  bool CursorFile::readable() {
    return true;
  }

  bool CursorFile::writable() {
    return true;
  }
};
