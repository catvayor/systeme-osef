#include "virtual_cursor_file.hpp"

namespace Files {

  VirtualCursorFile::VirtualCursorFile(kstring const& name, VirtualDir* parent)
    : CursorFile(name, parent) { }

  int64_t VirtualCursorFile::size() {
    return fdata.size();
  }

  void VirtualCursorFile::write_at(const uint8_t* str, uint64_t count, uint64_t pos) {
    if (pos + count > fdata.size())
      fdata.resize(pos + count);
    std::memcpy(fdata.data() + pos, str, count);
  }

  void VirtualCursorFile::read_at(uint8_t* dest, uint64_t count, uint64_t pos) {
    std::memcpy(dest, fdata.data() + pos, count);
  }

  void VirtualCursorFile::trunc() {
    fdata.clear();
  }

  bool VirtualCursorFile::truncable() {
    return true;
  }

  bool VirtualCursorFile::resizable() {
    return true;
  }

  void VirtualCursorFile::resize(uint64_t size) {
    fdata.resize(size);
  }
}
