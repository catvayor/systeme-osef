#pragma once

#include "virtual_file.hpp"

namespace Files {

  class CursorFile: public VirtualFile {
   public:
    CursorFile(kstring const& filename, VirtualDir* parent);

    bool seekable() override;
    bool readable() override;
    bool writable() override;
  };
}
