#pragma once

#include "cursor_file.hpp"
#include "fs.hpp"

namespace Files {

  class PhysicalFile : public CursorFile {
    fs::file_t* pfile;

   protected:
    void fopen() override;
    void fclose() override;
   public:
    PhysicalFile(fs::file_t* pfile, VirtualDir* parent);
    int64_t size() override;
    void write_at(const uint8_t* str, uint64_t count, uint64_t pos) override;
    void read_at(uint8_t* dest, uint64_t count, uint64_t pos) override;
    void* data() override;
    bool has_raw_access() override;
    void trunc() override;
    bool truncable() override;
    bool resizable() override;
    void resize(uint64_t) override;
  };
}
