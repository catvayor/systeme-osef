#pragma once

#include "cursor_file.hpp"

namespace Files {

  class VirtualCursorFile : public CursorFile {
    kvector<uint8_t> fdata;
   public:
    VirtualCursorFile(kstring const& name, VirtualDir* parent);
    int64_t size() override;
    void write_at(const uint8_t* str, uint64_t count, uint64_t pos) override;
    void read_at(uint8_t* dest, uint64_t count, uint64_t pos) override;
    void trunc() override;
    bool truncable() override;
    bool resizable() override;
    void resize(uint64_t) override;
  };

}
