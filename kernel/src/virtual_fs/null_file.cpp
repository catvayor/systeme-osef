#include "null_file.hpp"

namespace Files {

  NullFile::NullFile(kstring const& name, VirtualDir* parent)
    : VirtualFile(name, parent) { }

  void NullFile::write(kqueue<uint8_t>& buf) {
    buf.clear();
  }
  kvector<uint8_t> NullFile::read(uint64_t) {
    return kvector<uint8_t>();
  }

  bool NullFile::writable() {
    return true;
  }

  bool NullFile::readable() {
    return true;
  }

  int64_t NullFile::size() {
    return 0;
  }
}
