#pragma once
#include "virtual_file.hpp"

namespace Files {

  class NullFile : public VirtualFile {
   public:
    NullFile(kstring const& name, VirtualDir* parent);
    void write(kqueue<uint8_t>& buf) override;
    kvector<uint8_t> read(uint64_t max) override;
    bool writable() override;
    bool readable() override;
    int64_t size() override;

  };

}
