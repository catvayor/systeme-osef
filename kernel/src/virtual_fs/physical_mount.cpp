#include "physical_mount.hpp"

namespace Files {

  PhysicalMount::PhysicalMount(uint8_t drive, kstring const& name, VirtualDir* parent)
    : PhysicalDir(name, parent), fs(drive) {
    take_dir(fs.root);
  }

  void PhysicalMount::flush() {
    fs.flush();
    VirtualDir::flush();
  }

}
