#include "path.hpp"
#include <kvector>
namespace Files {

  Path::Path() : data("/") {}

  Path::Path(kstring s) : data(s) {
    compress();
  }

  Path Path::operator+(kstring const& s) const {
    Path ret = *this;
    ret += s;
    return ret;
  }

  Path& Path::operator+=(kstring const& s) {
    data += s;
    compress();
    return *this;
  }

  void Path::compress() {
    kvector<kstring> path;
    if (data.empty())
      return;
    bool absolute = data.front() == '/';
    path.push_back("");
    for (char c : data) {
      if (c == '/') {
        if (path.back() == "") {
          path.pop_back();
        } else if (path.back() == ".") {
          path.pop_back();
        } else if (path.back() == "..") {
          if (path.size() > 1 && path[path.size() - 2] != "..") {
            path.pop_back();
            path.pop_back();
          } else if (path.size() == 1 && absolute)
            path.pop_back();
        }
        path.push_back("");
      } else {
        path.back().push_back(c);
      }
    }
    data = "";
    for (kstring const& s : path) {
      data += "/";
      data += s;
    }
  }

  kstring Path::str() const {
    return data;
  }

}
