#pragma once

#include <kqueue>
#include <kvector>
#include <optional>
#include "virtual_dir.hpp"

namespace Files {
  class VirtualFile {
    VirtualDir* m_parent;
    kstring name;
    std::size_t open_cnt;
    std::size_t open_read_cnt;
    std::size_t open_write_cnt;

   protected:
    virtual void fopen();
    virtual void fopen_read();
    virtual void fopen_write();
    virtual void fclose_read();
    virtual void fclose_write();
    virtual void fclose();

   public:
    enum SeekMode {
      SEEK_SET,
      SEEK_CUR,
      SEEK_END
    };

    VirtualFile(kstring const& name, VirtualDir* parent);
    VirtualFile(const VirtualFile&) = delete;
    VirtualFile(VirtualFile&&) = delete;
    virtual ~VirtualFile();

    void open();
    void open_read();
    void open_write();
    void close_read();
    void close_write();
    void close();

    kstring filename();

    virtual void write(kqueue<uint8_t>& buf);
    virtual kvector<uint8_t> read(uint64_t max);
    virtual bool writable();
    virtual bool readable();
    virtual void* data();
    virtual bool has_raw_access();
    virtual void flush();
    virtual bool resizable();
    virtual void resize(uint64_t);
    virtual void trunc();
    virtual bool truncable();
    virtual int64_t size() = 0;

    virtual bool seekable();
    virtual void write_at(const uint8_t* str, uint64_t count, uint64_t pos);
    virtual void read_at(uint8_t* dest, uint64_t count, uint64_t pos);
  };
}
