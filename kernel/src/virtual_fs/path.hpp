#pragma once

#include <kstring>

namespace Files {

  class Path {
    kstring data;
    void compress();

   public:
    Path();
    explicit Path(kstring);
    Path operator+(kstring const&) const;
    Path& operator+=(kstring const&);
    kstring str() const;
  };

}
