#include "physical_dir.hpp"
#include "physical_file.hpp"

namespace Files {

  kstring dirname_reader(ku16string const& name) {
    kstring ret = ku16string_to_kstring(name);
    ret.pop_back();//remove ending '/'
    return ret;
  }

  PhysicalDir::PhysicalDir(kstring dirname, VirtualDir* parent)
    : VirtualDir(dirname, parent) {
  }

  PhysicalDir::PhysicalDir(fs::gen_dir* pdir, VirtualDir* parent)
    : PhysicalDir(dirname_reader(pdir->dirname()), parent) {
    take_dir(pdir);
  }

  void PhysicalDir::take_dir(fs::gen_dir* dir) {
    pdir = dir;
    pdir->read_data();
    for (fs::gen_dir* subd : pdir->subdirs())
      create_dir<PhysicalDir>(subd);
    for (fs::file_t* file : pdir->files())
      create_file<PhysicalFile>(file);
  }

  VirtualFile* PhysicalDir::new_file(kstring const& filename) {
    fs::file_t* npfile = new fs::file_t(pdir->filesys(), kstring_to_ku16string(filename));
    pdir->files().push_back(npfile);
    return create_file<PhysicalFile>(npfile);
  }

  VirtualDir* PhysicalDir::new_dir(kstring const& dirname) {
    fs::dir_t* npdir = new fs::dir_t(pdir->filesys(), kstring_to_ku16string(dirname));
    pdir->subdirs().push_back(npdir);
    return create_dir<PhysicalDir>(npdir);
  }

}
