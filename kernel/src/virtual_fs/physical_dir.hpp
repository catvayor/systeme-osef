#pragma once

#include "fs.hpp"
#include "virtual_dir.hpp"

namespace Files {

  class PhysicalDir : public VirtualDir {
    fs::gen_dir* pdir;

   protected:
    PhysicalDir(kstring dirname, VirtualDir* parent);
    void take_dir(fs::gen_dir* dir);

   public:
    PhysicalDir(fs::gen_dir* pdir, VirtualDir* parent);

    VirtualFile* new_file(kstring const& filename) override;
    VirtualDir* new_dir(kstring const& dirname) override;

  };

}
