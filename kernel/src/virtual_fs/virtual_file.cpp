#include "virtual_file.hpp"

#include <interact>

namespace Files {

  VirtualFile::VirtualFile(kstring const& filename, VirtualDir* parent)
    : m_parent(parent), name(filename), open_cnt(0), open_read_cnt(0), open_write_cnt(0) { }

  VirtualFile::~VirtualFile() {
    if (m_parent)
      m_parent->forget_file(this);
  }

  kstring VirtualFile::filename() {
    return name;
  }

  void VirtualFile::flush() {
    if (m_parent)
      m_parent->flush();
  }

  void VirtualFile::fopen() {}
  void VirtualFile::fopen_read() {}
  void VirtualFile::fopen_write() {}

  void VirtualFile::fclose_read() {}
  void VirtualFile::fclose_write() {}
  void VirtualFile::fclose() {}

  void VirtualFile::open() {
    if (!(open_cnt++))
      fopen();
  }
  void VirtualFile::open_read() {
    if (!(open_read_cnt++))
      fopen_read();
  }
  void VirtualFile::open_write() {
    if (!(open_write_cnt++))
      fopen_write();
  }

  void VirtualFile::close() {
    if (!--open_cnt)
      fclose();
  }
  void VirtualFile::close_read() {
    if (!--open_read_cnt)
      fclose_read();
  }
  void VirtualFile::close_write() {
    if (!--open_write_cnt)
      fclose_write();
  }

  bool VirtualFile::seekable() {
    return false;
  }

  void VirtualFile::write(kqueue<uint8_t>&) {
    Interact::error("write not implemented");
  }

  kvector<uint8_t> VirtualFile::read(uint64_t) {
    Interact::error("read not implemented");
  }

  bool VirtualFile::writable() {
    return false;
  }

  bool VirtualFile::readable() {
    return false;
  }

  void* VirtualFile::data() {
    Interact::error("Unaccessible raw data");
  }

  bool VirtualFile::has_raw_access() {
    return false;
  }

  void VirtualFile::trunc() {
    Interact::error("File not truncable");
  }

  bool VirtualFile::truncable() {
    return false;
  }

  bool VirtualFile::resizable() {
    return false;
  }

  void VirtualFile::resize(uint64_t) {
    Interact::error("File not resizable");
  }

  void VirtualFile::write_at(const uint8_t*, uint64_t, uint64_t) {
    Interact::error("Write_at not implemented");
  }

  void VirtualFile::read_at(uint8_t*, uint64_t, uint64_t) {
    Interact::error("Read_at not implemented");
  }

}
