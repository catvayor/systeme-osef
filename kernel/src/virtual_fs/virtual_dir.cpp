#include "virtual_dir.hpp"
#include "virtual_file.hpp"
#include "virtual_cursor_file.hpp"
#include <algorithm>

namespace Files {
  VirtualDir* root = new VirtualDir("/", nullptr);

  DirEntry::DirEntry(EntryType t, kstring n)
    : type(t), name(n) { }

  VirtualDir::VirtualDir(kstring const& dirname, VirtualDir* parent)
    : m_parent(parent), name(dirname)
  { }

  VirtualDir::~VirtualDir() {
    kvector<VirtualFile*> f_silent = files;
    files.clear();
    for (auto f : f_silent)
      delete f;
    kvector<VirtualDir*> d_silent = subdirs;
    subdirs.clear();
    for (auto d : d_silent)
      delete d;
    if (m_parent)
      m_parent->forget_dir(this);
  }

  void VirtualDir::flush() {
    if (m_parent)
      m_parent->flush();
  }

  void VirtualDir::forget_file(VirtualFile* f) {
    auto it = std::find(files.begin(), files.end(), f);
    if (it != files.end())
      files.erase(it);
  }

  void VirtualDir::forget_dir(VirtualDir* d) {
    auto it = std::find(subdirs.begin(), subdirs.end(), d);
    if (it != subdirs.end())
      subdirs.erase(it);
  }

  kstring VirtualDir::dirname() {
    return name;
  }

  VirtualDir* VirtualDir::parent() {
    if (m_parent)
      return m_parent;
    else
      return this;
  }

  VirtualFile* VirtualDir::new_file(kstring const& filename) {
    return create_file<VirtualCursorFile>(filename);
  }

  VirtualDir* VirtualDir::new_dir(kstring const& dirname) {
    return create_dir<VirtualDir>(dirname);
  }

  VirtualFile* VirtualDir::get_file(kstring const& filename, bool create) {
    for (VirtualFile* f : files)
      if (f->filename() == filename)
        return f;
    if (create)
      return new_file(filename);
    return nullptr;
  }

  VirtualDir* VirtualDir::get_dir(kstring const& dirname, bool create) {
    if (dirname == "")
      return this;
    if (dirname == ".")
      return this;
    if (dirname == "..")
      return parent();
    for (VirtualDir* d : subdirs)
      if (d->dirname() == dirname)
        return d;
    if (create)
      return new_dir(dirname);
    return nullptr;
  }

  VirtualFile* VirtualDir::find_file(kstring const& path, bool create, bool make_p) {
    auto beg = path.begin();
    auto end = path.end();
    auto endd = std::find(beg, end, '/');
    if (endd == end)
      return get_file(path, create);

    VirtualDir* dir = get_dir(kstring(beg, endd), make_p);
    if (dir)
      return dir->find_file(kstring(endd + 1, end), create, make_p);
    return nullptr;
  }

  VirtualDir* VirtualDir::find_dir(kstring const& path, bool create, bool make_p) {
    auto beg = path.begin();
    auto end = path.end();
    auto endd = std::find(beg, end, '/');
    if (endd == end)
      return get_dir(path, create);

    if (endd + 1 == end) // ending '/' detected
      return get_dir(kstring(beg, endd), create);

    VirtualDir* dir = get_dir(kstring(beg, endd), make_p);
    if (dir)
      return dir->find_dir(kstring(endd + 1, end), create, make_p);
    return nullptr;
  }

  VirtualFile* VirtualDir::find_file(Path const& path, bool create, bool make_p) {
    return find_file(path.str(), create, make_p);
  }
  VirtualDir* VirtualDir::find_dir(Path const& path, bool create, bool make_p) {
    return find_dir(path.str(), create, make_p);
  }

  kvector<DirEntry> VirtualDir::dir_ls() {
    kvector<DirEntry> ret;
    for (VirtualDir* d : subdirs)
      ret.push_back(DirEntry(et_dir, d->dirname() + "/"));
    for (VirtualFile* f : files)
      ret.push_back(DirEntry(et_file, f->filename()));
    return ret;
  }
}
