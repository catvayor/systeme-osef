#include "keyboard.hpp"

#include <interact>
#include <io_access>
#include <stdint.h>
#include "interrupt.hpp"
#include "pic.hpp"

namespace Keyboard {
  constexpr uint8_t LSHIFT = 0x2a;
  constexpr uint8_t RSHIFT = 0x36;
  constexpr uint8_t LCTRL  = 0x1d;
  constexpr uint8_t ALT   = 0x38;
  constexpr uint8_t LMOD4  = 0x5b;
  constexpr uint8_t RMOD4  = 0x5c;
  constexpr uint8_t ESC    = 0x81;

  kqueue<int16_t> buf;

  KbdState state;
  KbdState get_state() {
    return state;
  }

  struct KeyMap {
    const char *basic;
    const char *shift;
    const char *altgr;

    int16_t translate(int16_t code16) {
      if (code16 & Ext) return code16;
      else if (code16 & AltGr) return (code16 & 0xff00) | (uint8_t)altgr[code16 & 0xff];
      else if (code16 & Shift) return (code16 & 0xff00) | (uint8_t)shift[code16 & 0xff];
      else return (code16 & 0xff00) | (uint8_t)basic[code16 & 0xff];
    }
  };

  bool KbdState::pressed(uint8_t c, bool ext) const {
    if (c >= 128) Interact::error("Invalid keycode.");
    return get(c, ext);
  }

  bool KbdState::alt_pressed() const {
    return pressed(ALT);
  }

  bool KbdState::altgr_pressed() const {
    return pressed(ALT, true);
  }

  bool KbdState::shift_pressed() const {
    return pressed(LSHIFT) || pressed(RSHIFT);
  }

  bool KbdState::ctrl_pressed() const {
    return pressed(LCTRL);
  }

  bool KbdState::mod4_pressed() const {
    return pressed(LMOD4, true) || pressed(RMOD4, true);
  }

  int16_t KbdState::key_code(uint8_t c) {
    if (c == 0xe0) {
      got_ext = true;
      return -1;
    }
    if (c >= 128) {
      clear(c - 128, got_ext);
      got_ext = false;
      return -1;
    } else {
      set(c, got_ext);
      uint16_t code16 = c;
      switch (c) {
        case LSHIFT:
          code16 = Shift;
          break;
        case ALT:
          code16 = (got_ext ? AltGr : Alt);
          break;
        case LCTRL:
          code16 = Ctrl;
          break;
        case LMOD4:
          code16 = Mod4;
          break;
        default:
          if (shift_pressed()) code16 |= Shift;
          if (ctrl_pressed()) code16 |= Ctrl;
          if (alt_pressed()) code16 |= Alt;
          if (altgr_pressed()) code16 |= AltGr;
          if (mod4_pressed()) code16 |= Mod4;
          if (got_ext) code16 |= Ext;
      }
      got_ext = false;
      return code16;
    }
  }

  bool KbdState::get(uint8_t c, bool ext) const {
    return states[ext][c];
  }

  void KbdState::set(uint8_t c, bool ext) {
    states[ext][c] = true;
  }

  void KbdState::clear(uint8_t c, bool ext) {
    states[ext][c] = false;
  }

  KeyMap qwerty = {
    "\0\0331234567890-=\b\tqwertyuiop[]\n\0asdfghjkl;'`\0\\zxcvbnm,./\0*\0 \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
    "\0\033!@#$%^&*()_+\b\tQWERTYUIOP{}\n\0ASDFGHJKL:\"~\0|ZXCVBNM<>?\0\0\0 \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
    "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
  };

  KeyMap azerty = {
    "\0\033&\202\"'(-\212_\207\205)=\b\tazertyuiop^$\n\0qsdfghjklm\0\0\0*wxcvbn,;:!\0*\0 \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0<\0\0",
    "\0\0331234567890\370+\b\tAZERTYUIOP\0\0\n\0QSDFGHJKLM%~\0\0WXCVBN?./\0\0\0\0 \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0>\0\0",
    "\0\0\000~#{[|`\\^@]}\b\t\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
  };

  KeyMap layouts[2] = {qwerty, azerty};
  unsigned lay_id = 0;

  extern "C" void handler_kbd_wrapper();
  extern "C" void handler_kbd([[maybe_unused]] Interrupt::Context *ctxp) {
    kbd_input(inb(0x60));
    Pic::PIC_sendEOI(Pic::IRQ_KBD);
  }

  bool init_done = false;
  void init() {
    if (init_done) return;
    // Require
    Interact::init();
    Interrupt::init();
    Pic::init();
    // Init
    Interact::print_init("Keyboard");

    Interrupt::setup_handler(Pic::IRQ_INT + Pic::IRQ_KBD, handler_kbd_wrapper);

    Pic::IRQ_clear_mask(Pic::IRQ_KBD);

    init_done = true;
    Interact::print_done();
  }

  void switch_layout() {
    lay_id = 1 - lay_id;
  }

  unsigned get_layout() {
    return lay_id;
  }

  void kbd_input(uint8_t code) {
    int16_t code16 = state.key_code(code);
    if (code16 != -1) buf.push(layouts[lay_id].translate(code16));
  }

  bool haschar() {
    return !buf.empty();
  }

  int16_t getchar() {
    return *buf.pop();
  }
}
