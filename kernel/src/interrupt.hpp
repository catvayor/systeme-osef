#pragma once
#include <interrupt_base>

#include <stdint.h>

namespace Interrupt {
  void init();

  struct Context {
    uint64_t rbp, rax, rbx, rcx, rdx, rsi, rdi, r8, r9, r10, r11, r12, r13, r14, r15;
    uint64_t rip, cs, rflags, rsp, ss;

    bool is_user();
    void restore();
  };

  [[noreturn]] void fatal_error(Context*);
  [[noreturn]] void fatal_error_name(Context*, const char*);
  [[noreturn]] void fatal_error_name_errcode(Context*, const char*, uint64_t);
}
