#pragma once

#include <array>
#include <stdint.h>

namespace Gdt {
  struct TSS {
    uint32_t: 32;

    void* rsp0 = 0;
    void* rsp1 = 0;
    void* rsp2 = 0;

    uint64_t: 64;

    void* ist1 = 0;
    void* ist2 = 0;
    void* ist3 = 0;
    void* ist4 = 0;
    void* ist5 = 0;
    void* ist6 = 0;
    void* ist7 = 0;

    uint64_t: 64;

    uint16_t iopb = sizeof(TSS);
    uint16_t: 16;

    uint64_t lo_desc();
    uint64_t hi_desc();
  } __attribute__((packed));

  extern TSS tss;

  extern std::array<uint64_t, 6> gdt;

  constexpr uint16_t nul_idx = 0;
  constexpr uint16_t kcs_idx = 1;
  constexpr uint16_t ucs_idx = 2;
  constexpr uint16_t uds_idx = 3;
  constexpr uint16_t tss_idx = 4;

  constexpr uint16_t ring = 1;

  constexpr uint16_t kcs_sel = (kcs_idx*sizeof(uint64_t)) | (ring * 0);
  constexpr uint16_t ucs_sel = (ucs_idx*sizeof(uint64_t)) | (ring * 3);
  constexpr uint16_t uds_sel = (uds_idx*sizeof(uint64_t)) | (ring * 3);
  constexpr uint16_t tss_sel = tss_idx * sizeof(uint64_t);

  void init(); // do not call to init
  void init(void* rsp0);
}
