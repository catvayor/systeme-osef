#pragma once
#include <cstdint>
#include <cstring>
#include <fs_struct>
#include <kstring>
#include <kvector>

namespace fs {

  class filesys_t;

  struct fs_elem {
    filesys_t* fs;
    bool changed = false;
    bool data_read = false;
    bool entry_changed = false;

    fs_elem(filesys_t* filesys);
    fs_elem(const fs_elem&) = delete;
    fs_elem(fs_elem&&) = delete;
    virtual ~fs_elem() = default;

    virtual bool flush() = 0;
    virtual void read_data() = 0;
  };

  struct gen_file : public fs_elem {
    bool nofat;
    uint32_t first_clus;
    uint64_t len;
    uint64_t valid_len;
    bool removed = false;

    ku16string filename;
    uint16_t filename_hash;
    file_entry_t entry;

    gen_file(filesys_t* filesys, dir_entry_t* entries);
    gen_file(filesys_t* filesys, ku16string const& filename, uint16_t attrib);

    bool flush() override;
    void add_entries(kvector<dir_entry_t>& dir);

    virtual void remove();
    virtual uint64_t required_len() = 0;
    virtual char* data() = 0;
  };

  struct file_t : public gen_file {
    kvector<char> fdata;

    file_t(filesys_t* filesys, dir_entry_t* entries);
    file_t(filesys_t* filesys, ku16string const& filename);

    char* data() override;
    void read_data() override;
    uint64_t required_len() override;
    bool flush() override;
    void unread();
  };

  struct gen_dir {
    virtual ~gen_dir() = default;
    virtual kvector<file_t*>& files() = 0;
    virtual kvector<gen_dir*>& subdirs() = 0;
    virtual ku16string dirname() = 0;
    virtual void read_data() = 0;
    virtual filesys_t* filesys() = 0;
    virtual void remove();
    virtual bool flush() = 0;
  };

  struct dir_t : public gen_file, gen_dir {
    kvector<file_t*> m_files;
    kvector<gen_dir*> m_subdirs;
    kvector<dir_entry_t> entries;

    dir_t(filesys_t* filesys, dir_entry_t* entries);
    dir_t(filesys_t* filesys, ku16string const& dirname);

    ~dir_t();

    bool flush() override;
    char* data() override;
    void read_data() override;
    uint64_t required_len() override;

    kvector<file_t*>& files() override {
      return m_files;
    }
    kvector<gen_dir*>& subdirs() override {
      return m_subdirs;
    }
    ku16string dirname() override {
      ku16string ret = filename;
      ret.push_back(u'/');
      return ret;
    }
    void remove() override;

    filesys_t* filesys() override {
      return fs;
    }

  };

  struct root_dir : public fs_elem, gen_dir {
    kvector<file_t*> m_files;
    kvector<gen_dir*> m_subdirs;
    uint32_t first_clus;

    alloc_entry_t alloc_tbl_entry;
    cap_table_entry_t cap_table_entry;
    vol_lab_entry_t vol_labl_entry;

    root_dir(filesys_t* filesys);

    ~root_dir();

    bool flush() override;
    void read_data() override;

    kvector<file_t*>& files() override {
      return m_files;
    }
    kvector<gen_dir*>& subdirs() override {
      return m_subdirs;
    }
    ku16string dirname() override {
      return u"/";
    }
    filesys_t* filesys() override {
      return fs;
    }
  };

  struct filesys_t {
    uint8_t m_drive;
    bootsector_t bootsector;
    kvector<uint32_t> fat;
    kvector<uint8_t> alloc_bitmap;
    kvector<char16_t> cap_table;
    root_dir* root;

    filesys_t(uint8_t drive);
    filesys_t(const filesys_t&) = delete;
    filesys_t(filesys_t&&) = delete;
    ~filesys_t();

    uint32_t clus_byte_shift();
    uint64_t bytes_per_clus();
    uint64_t bytes_per_sect();
    std::size_t sect_aligned_size(std::size_t size);
    uint64_t clus_beg(uint32_t clus_nb);
    void rd_clus(void* dest, uint32_t clus_nb, uint64_t count);
    void rd_chained_clus(void* dest, uint32_t first_clus, uint64_t len, bool fat = true);
    void wt_clus(uint32_t clus_nb, void* data, uint64_t count);
    void wt_chained_clus(uint32_t first_clus, void* data, uint64_t len, bool fat = true);
    uint32_t& fat_nxt(uint32_t clus);
    uint32_t req_fat_nxt(uint32_t clus);
    bool used_clus(uint32_t clus_nb);
    uint32_t first_unused_clus();
    void mark_clus(uint32_t clus, bool used);
    ku16string capitalized(ku16string const& str);

    void flush();
  };

  uint32_t checksum32(const void* data, uint64_t len);
  uint16_t checksum16(const void* data, uint64_t len);

  uint32_t boot_region_checksum(const uint8_t* sectors, uint32_t sector_shift);
  uint16_t fileset_checksum(const void* data, uint32_t entry_count);

}
