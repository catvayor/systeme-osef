#include "fs.hpp"
#include <ata>
#include <interact>

namespace fs {

  fs_elem::fs_elem(filesys_t* filesys): fs(filesys) { }

  gen_file::gen_file(filesys_t* filesys, dir_entry_t* entries)
    : fs_elem(filesys), entry(*(file_entry_t*)entries) {
    uint8_t following = entry.secondary_count;
    kvector<filename_entry_t> names_ent;
    bool found_ext = false;
    uint8_t filename_size;
    for (auto i = entries + 1; i != entries + following + 1; ++i) {
      if (i->type == File_name)
        names_ent.push_back((filename_entry_t&)*i);
      else if (i->type == Stream_extension) {
        found_ext = true;
        stream_extension_entry_t& str_ext = (stream_extension_entry_t&) * i;
        nofat = str_ext.seconFlags & 2;
        filename_size = str_ext.nameLen;
        filename_hash = str_ext.nameHash;
        first_clus = str_ext.first_clus;
        len = str_ext.len;
        valid_len = str_ext.valid_data_len;
      } else
        Interact::error("unsupported sub_entry type");// << i->type;
    }
    if (!found_ext)
      Interact::error("stream extension field not found (mandatory)");
    if (filename_size > names_ent.size() * 15)
      Interact::error("not enough filename entry found");// << names_ent.size() << " found)" << std::endl;
    for (filename_entry_t const& ent : names_ent)
      for (char16_t c : ent.filename)
        filename.push_back(c);
    filename.resize(filename_size);
  }

  gen_file::gen_file(filesys_t* filesys, ku16string const& filename, uint16_t attrib)
    : fs_elem(filesys), filename(filename) {
    uint32_t nb_name_ent = (filename.size() + 14) / 15;
    entry.secondary_count = nb_name_ent + 1;
    entry.attrib = attrib;
    changed = true;
    entry_changed = true;
    first_clus = 0xFFFFFFFF;
    valid_len = len = 0;
    nofat = true;
    data_read = true;
  }

  bool gen_file::flush() {
    if (removed)
      return true;
    if (!changed)
      return entry_changed;
    if (first_clus == 0xFFFFFFFF) {
      first_clus = fs->first_unused_clus();
      fs->mark_clus(first_clus, true);
    }
    if (nofat) {
      uint32_t curr_nb_clus = (std::max<uint64_t>(len, 1) + fs->bytes_per_clus() - 1)
                              / fs->bytes_per_clus();
      uint32_t req_nb_clus = (std::max<uint64_t>(required_len(), 1) + fs->bytes_per_clus() - 1)
                             / fs->bytes_per_clus();
      if (req_nb_clus > curr_nb_clus) {
        uint32_t i = first_clus + curr_nb_clus;
        for (; i < (first_clus + req_nb_clus); ++i)
          if (fs->used_clus(i)) {
            nofat = false;
            for (uint32_t j = first_clus; j < i - 1; ++j)
              fs->fat[j] = j + 1;
            fs->fat[i - 1] = 0xFFFFFFFF;
            break;
          } else
            fs->mark_clus(i, true);
      } else {
        uint32_t i = first_clus + req_nb_clus;
        for (; i < (first_clus + curr_nb_clus); ++i)
          fs->mark_clus(i, false);
      }
    }

    if (required_len())
      fs->wt_chained_clus(first_clus, data(), required_len(), !nofat);
    else
      fs->wt_chained_clus(first_clus,
                          kvector<char>(fs->bytes_per_clus(), 0).data(),
                          fs->bytes_per_clus(), !nofat);
    if (len != required_len())
      entry_changed = true;
    len = required_len();
    if (valid_len != len)
      entry_changed = true;
    valid_len = len;
    changed = false;

    return entry_changed;
  }

  void gen_file::add_entries(kvector<dir_entry_t>& dir) {
    if (removed)
      return;
    kvector<dir_entry_t> file_ent;
    file_ent.push_back(entry);
    ku16string capt = fs->capitalized(filename);
    stream_extension_entry_t ent;
    ent.seconFlags = uint8_t(nofat ? 3 : 1);
    ent.nameLen = uint8_t(filename.size());
    ent.nameHash = checksum16(capt.data(), capt.size() * 2);
    ent.valid_data_len = valid_len;
    ent.first_clus = first_clus;
    ent.len = len;
    file_ent.push_back(ent);
    for (uint32_t i = 0; i < filename.size(); i += 15)
      file_ent.push_back(filename_entry_t(filename.data() + i,
                                          std::min<uint8_t>(filename.size() - i, 15)));
    entry.set_checksum = fileset_checksum(file_ent.data(), file_ent.size());
    dir.push_back(entry);
    for (auto i = file_ent.begin() + 1; i != file_ent.end(); ++i)
      dir.push_back(*i);
    entry_changed = false;
  }

  void gen_file::remove() {
    if (nofat) {
      uint32_t nb_clus = (len + fs->bytes_per_clus() - 1) / fs->bytes_per_clus();
      for (uint32_t i = first_clus; i < (first_clus + nb_clus); ++i)
        fs->mark_clus(i, false);
    } else
      fs->wt_chained_clus(first_clus, nullptr, 0, true);
    removed = true;
    changed = true;
    entry_changed = true;
  }

  file_t::file_t(filesys_t* filesys, dir_entry_t* entries)
    : gen_file(filesys, entries) { }

  file_t::file_t(filesys_t* filesys, ku16string const& filename)
    : gen_file(filesys, filename, 0x20) { }

  void file_t::read_data() {
    fdata.resize(fs->sect_aligned_size(len));
    fs->rd_chained_clus(fdata.data(), first_clus, valid_len, !nofat);
    fdata.resize(len);
    data_read = true;
  }

  uint64_t file_t::required_len() {
    return fdata.size();
  }

  char* file_t::data() {
    return fdata.data();
  }

  bool file_t::flush() {
    fdata.reserve(fs->sect_aligned_size(fdata.size()));
    return gen_file::flush();
  }

  void file_t::unread() {
    changed = false;
    fdata.clear();
    data_read = false;
  }

  void gen_dir::remove() {
    for (file_t* f : files()) {
      f->remove();
      delete f;
    }
    for (gen_dir* d : subdirs()) {
      d->remove();
      delete d;
    }
    files().clear();
    subdirs().clear();
  }

  dir_t::dir_t(filesys_t* filesys, dir_entry_t* entries)
    : gen_file(filesys, entries) { }

  dir_t::dir_t(filesys_t* filesys, ku16string const& dirname)
    : gen_file(filesys, dirname, 0x10) { }

  dir_t::~dir_t() {
    for (auto f : m_files)
      delete f;
    for (auto d : m_subdirs)
      delete d;
  }

  bool dir_t::flush() {
    if (!data_read)
      return false;
    entries.clear();
    for (gen_dir* subd : m_subdirs) {
      dir_t* d = (dir_t*)subd;//dynamic_cast<dir_t*>(subd);
      if (!d)
        continue;
      changed |= d->flush();
      d->entry_changed = false;
      d->add_entries(entries);
    }
    for (file_t* f : m_files) {
      changed |= f->flush();
      f->entry_changed = false;
      f->add_entries(entries);
    }
    uint64_t size = entries.size();
    entries.resize(fs->sect_aligned_size(size * sizeof(dir_entry_t)) / sizeof(dir_entry_t));
    entries.resize(size);

    return gen_file::flush();
  }

  void dir_t::read_data() {
    m_files.clear();
    m_subdirs.clear();
    kvector<dir_entry_t> entries;
    {
      std::size_t size = len / sizeof(dir_entry_t);
      kvector<dir_entry_t> rd_buf(fs->sect_aligned_size(len) / sizeof(dir_entry_t) + 1, dir_entry_t());
      fs->rd_chained_clus((char*)&rd_buf.front(), first_clus, len, !nofat);
      rd_buf.resize(size);
      for (auto const& entry : rd_buf)
        if (entry.is_active())
          entries.push_back(entry);
        else if (entry.is_null())
          break;
    }

    for (std::size_t i = 0; i < entries.size(); ++i) {
      dir_entry_t& entry = entries[i];
      switch (entry.type) {
        case Alloc:
          Interact::error("unexpected allocation table");
          break;
        case Cap_table:
          Interact::error("unexpected cap's table");
          break;
        case Volume_label:
          Interact::error("unexpected volume label");
          break;
        case File:
          if (((file_entry_t*)&entry)->attrib == 0x20)
            m_files.push_back(new file_t(fs, &entry));
          else if (((file_entry_t*)&entry)->attrib == 0x10)
            m_subdirs.push_back(new dir_t(fs, &entry));
          else
            Interact::error("file entry not supported");// << ((file_entry_t*)&entry)->attrib;
          break;
        case Stream_extension:
        case File_name:
          break;//skip (read by file objs)
        default:
          //wtf
          Interact::error("unknown entry type");// << entry.type << std::endl;
      }
    }
    data_read = true;
  }

  uint64_t dir_t::required_len() {
    return entries.size() * sizeof(dir_entry_t);
  }

  char* dir_t::data() {
    return (char*)entries.data();
  }

  void dir_t::remove() {
    if (!data_read)
      read_data();
    gen_dir::remove();
    gen_file::remove();
  }

  root_dir::root_dir(filesys_t* filesys)
    : fs_elem(filesys) { }

  root_dir::~root_dir() {
    for (auto f : m_files)
      delete f;
    for (auto d : m_subdirs)
      delete d;
  }

  bool root_dir::flush() {
    if (!data_read)
      return false;
    kvector<dir_entry_t> entries;

    entries.push_back(vol_labl_entry);
    entries.push_back(alloc_tbl_entry);
    entries.push_back(cap_table_entry);

    for (gen_dir* subd : m_subdirs) {
      dir_t* d = (dir_t*)subd;//dynamic_cast<dir_t*>(subd);
      if (!d)
        continue;
      changed |= d->flush();
      d->add_entries(entries);
    }
    for (file_t* f : m_files) {
      changed |= f->flush();
      f->add_entries(entries);
    }

    if (changed) {
      entries.resize(fs->sect_aligned_size(entries.size()*sizeof(dir_entry_t))
                     / sizeof(dir_entry_t));
      fs->wt_chained_clus(first_clus, (char*)entries.data(),
                          entries.size()*sizeof(dir_entry_t), true);
      changed = false;
      return true;
    }
    return false;
  }

  void root_dir::read_data() {
    first_clus = fs->bootsector.root;
    m_files.clear();
    m_subdirs.clear();
    kvector<dir_entry_t> entries;
    {
      kvector<dir_entry_t> rd_buf(fs->bytes_per_clus() / sizeof(dir_entry_t), dir_entry_t());
      for (uint32_t clus = first_clus; clus < 0xFFFFFFF8; clus = fs->fat_nxt(clus)) {
        fs->rd_clus((char*)&rd_buf.front(), clus, fs->bytes_per_clus());
        for (auto const& entry : rd_buf) {
          if (entry.is_active())
            entries.push_back(entry);
          else if (entry.is_null())
            break;
        }
      }
    }
    for (std::size_t i = 0; i < entries.size(); ++i) {
      dir_entry_t& entry = *&entries[i];
      switch (entry.type) {
        case Alloc:
          alloc_tbl_entry = entry;
          fs->alloc_bitmap.resize(fs->sect_aligned_size(alloc_tbl_entry.len));
          fs->rd_chained_clus((char*)fs->alloc_bitmap.data(), alloc_tbl_entry.first_clus, alloc_tbl_entry.len, true);
          break;
        case Cap_table:
          cap_table_entry = entry;
          fs->cap_table.resize(fs->sect_aligned_size(cap_table_entry.len) / 2);
          fs->rd_chained_clus(fs->cap_table.data(), cap_table_entry.first_clus, cap_table_entry.len);
          break;
        case Volume_label:
          vol_labl_entry = entry;
          break;//ignore it?
        case File:
          if (((file_entry_t*)&entry)->attrib == 0x20)
            m_files.push_back(new file_t(fs, &entry));
          else if (((file_entry_t*)&entry)->attrib == 0x10)
            m_subdirs.push_back(new dir_t(fs, &entry));
          else
            Interact::error("file entry not supported");// << ((file_entry_t*)&entry)->attrib;
          break;
        case Stream_extension:
        case File_name:
          break;//skip (read by file objs)
        default:
          //wtf
          Interact::error("unknown entry type");// << int(entry.type) << std::endl;
      }
    }
    data_read = true;
  }

  filesys_t::filesys_t(uint8_t drive): m_drive(drive) {
    Drive::ide_read_sectors(m_drive, // drive
                            0, // lba
                            1, // numsects
                            (uint16_t*)&bootsector );

    fat.resize((bootsector.fatLength << bootsector.sector_shift) / 4);
    Drive::read_sectors(m_drive, // drive
                        bootsector.fat_off, // lba
                        bootsector.fatLength, // numsects
                        (uint16_t*)&fat.front() );

//root dir
    root = new root_dir(this);
    root->read_data();
  }

  filesys_t::~filesys_t() {
    delete root;
  }

  uint32_t filesys_t::clus_byte_shift() {
    return bootsector.sector_shift + bootsector.cluster_shift;
  }

  uint64_t filesys_t::bytes_per_clus() {
    return 1ull << clus_byte_shift();
  }

  uint64_t filesys_t::bytes_per_sect() {
    return 1ull << bootsector.sector_shift;
  }

  std::size_t filesys_t::sect_aligned_size(std::size_t size) {
    return (size + bytes_per_sect() - 1) & ~(bytes_per_sect() - 1);
  }

  uint64_t filesys_t::clus_beg(uint32_t clus_nb) {
    return bootsector.clusterHead_off + (uint64_t(clus_nb - 2) << bootsector.cluster_shift);
  }

  uint32_t& filesys_t::fat_nxt(uint32_t clus) {
    return fat[clus];
  }

  uint32_t filesys_t::req_fat_nxt(uint32_t clus) {
    uint32_t nxt = fat_nxt(clus);
//     std::cout << "req nxt : " << clus << ", got : " << nxt;
    if (nxt < 0xFFFFFFF8u && nxt > 1) {
//         std::cout << " good" << std::endl;
      return nxt;
    }

    nxt = first_unused_clus();
    fat_nxt(clus) = nxt;
    fat_nxt(nxt) = 0xFFFFFFFF;
    mark_clus(nxt, true);
//     std::cout << " alloc " << nxt << std::endl;
    return nxt;
  }

  void filesys_t::rd_clus(void* dest, uint32_t clus_nb, uint64_t count) {
    Drive::read_sectors(m_drive, // drive
                        clus_beg(clus_nb), // lba
                        (count + bytes_per_sect() - 1) / bytes_per_sect(), // numsects
                        (uint16_t*)dest);
  }

  void filesys_t::rd_chained_clus(void* dest, uint32_t first_clus, uint64_t len, bool fat) {
    if (fat) {
      uint64_t nb_read = 0;
      for (uint32_t clus = first_clus; clus < 0xFFFFFFF8; clus = fat_nxt(clus)) {
        uint64_t reading = std::min(len - nb_read, bytes_per_clus());
        rd_clus((char*)dest + nb_read, clus, reading);
        nb_read += reading;
      }
    } else
      rd_clus(dest, first_clus, len);
  }

  void filesys_t::wt_clus(uint32_t clus_nb, void* data, uint64_t count) {
    Drive::write_sectors(m_drive, // drive
                         clus_beg(clus_nb), // lba
                         (count + bytes_per_sect() - 1) / bytes_per_sect(), // numsects
                         (uint16_t*)data);
  }

  void filesys_t::wt_chained_clus(uint32_t first_clus, void* data, uint64_t len, bool fat) { //if !fat, allocate fat as needed
    //std::cout << first_clus << " " << len << std::endl;
    if (fat) {
      uint64_t nb_wrote = 0;
      uint32_t clus = first_clus;
      for (; nb_wrote < len;) {
        uint64_t writting = std::min(len - nb_wrote, bytes_per_clus());
        wt_clus(clus, (char*)data + nb_wrote, writting);
        nb_wrote += bytes_per_clus();
        if (nb_wrote >= len)
          break;
        clus = req_fat_nxt(clus);
      }
      uint32_t tmp_clus = fat_nxt(clus);
      fat_nxt(clus) = 0xFFFFFFFF;
      while (tmp_clus < 0xFFFFFFF8) {
        mark_clus(tmp_clus, false);
        tmp_clus = fat_nxt(tmp_clus);
      }
    } else
      wt_clus(first_clus, data, len);
  }

  bool filesys_t::used_clus(uint32_t clus_nb) {
    if (clus_nb < 2)
      return true;
    return 1 & (alloc_bitmap[(clus_nb - 2) / 8] >> ((clus_nb - 2) & 7));
  }

  uint32_t filesys_t::first_unused_clus() {
    uint32_t i = 2;
    while (used_clus(i)) ++i;
    return i;
  }

  void filesys_t::flush() {
    if (root->flush()) {
      wt_chained_clus(root->alloc_tbl_entry.first_clus, alloc_bitmap.data(), alloc_bitmap.size(), true);
      Drive::write_sectors(m_drive, // drive
                           bootsector.fat_off, // lba
                           bootsector.fatLength, // numsects
                           (uint16_t*)fat.data());
    }
  }

  void filesys_t::mark_clus(uint32_t clus, bool used) {
    //std::cout << "marked clus " << clus << " " << used << std::endl;
    if (used)
      alloc_bitmap[(clus - 2) / 8] |= 1 << ((clus - 2) & 7);
    else
      alloc_bitmap[(clus - 2) / 8] &= ~(1 << ((clus - 2) & 7));
  }

  ku16string filesys_t::capitalized(ku16string const& str) {
    ku16string ret;
    for (char16_t c : str)
      ret.push_back(cap_table[uint16_t(c)]);
    return ret;
  }


  uint32_t checksum32(const void* data, uint64_t len) {
    uint8_t const* d = reinterpret_cast<uint8_t const*>(data);
    uint32_t checksum = 0;

    for (uint64_t i = 0; i < len; ++i)
      checksum = ((checksum << 31) | (checksum >> 1)) + d[i];
    return checksum;
  }

  uint16_t checksum16(const void* data, uint64_t len) {
    uint8_t const* d = reinterpret_cast<uint8_t const*>(data);
    uint16_t checksum = 0;

    for (uint64_t i = 0; i < len; ++i)
      checksum = ((checksum << 15) | (checksum >> 1)) + d[i];
    return checksum;
  }

  uint32_t boot_region_checksum(const uint8_t* sectors, uint32_t sector_shift) {
    uint32_t byte_count = (1 << sector_shift) * 11;
    uint32_t checksum = 0;

    for (uint64_t i = 0; i < byte_count; ++i)
      if ( !((i == 106) || (i == 107) || (i == 112)) )
        checksum = ((checksum << 31) | (checksum >> 1)) + sectors[i];
    return checksum;
  }

  uint16_t fileset_checksum(const void* data, uint32_t entry_count) {
    uint32_t byte_count = entry_count * sizeof(dir_entry_t);
    uint8_t const* d = reinterpret_cast<uint8_t const*>(data);
    uint16_t checksum = 0;

    for (uint64_t i = 0; i < byte_count; ++i)
      if ( !((i == 2) || (i == 3)) )
        checksum = ((checksum << 15) | (checksum >> 1)) + d[i];
    return checksum;
  }


}
