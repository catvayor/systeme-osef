#pragma once

#include <kstring>

kstring itos(uint64_t x) {
  kstring ret = "";
  do {
    kstring tmp = "";
    tmp.push_back(char('0' + (x % 10)));
    ret = tmp + ret;
    x /= 10;
  } while (x);
  return ret;
}
