#pragma once

#include <stdint.h>

namespace Pic {
  constexpr uint64_t IRQ_INT = 0x20;

  constexpr uint64_t IRQ_TIMER   = 0;
  constexpr uint64_t IRQ_KBD     = 1;
  constexpr uint64_t IRQ_CASCADE = 2; // VERY IMPORTANT for IRQs >= 8
  constexpr uint64_t IRQ_CMOS    = 8;
  constexpr uint64_t IRQ_DISK1   = 14;

  void init();
  void PIC_sendEOI(unsigned char irq);
  void IRQ_clear_mask(unsigned char IRQline);
  void IRQ_set_mask(unsigned char IRQline);
}
