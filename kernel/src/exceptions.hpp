#pragma once

#include <stdint.h>
#include "interrupt.hpp"

namespace Exceptions {
  using Interrupt::Context;

  void init();

  extern "C" void handler_pf(Context* ctxp, uint64_t errcode);

  enum : uint64_t {
    DE_INT = 0x00,
    DB_INT = 0x01,
    // Non-maskable Interrupt
    BP_INT = 0x03,
    OF_INT = 0x04,
    BR_INT = 0x05,
    UD_INT = 0x06,
    NM_INT = 0x07,
    DF_INT = 0x08,
    // Coprocessor Segment Overrun
    TS_INT = 0x0a,
    NP_INT = 0x0b,
    SS_INT = 0x0c,
    GP_INT = 0x0d,
    PF_INT = 0x0e,
    // Reserved
    MF_INT = 0x10,
    AC_INT = 0x11,
    MC_INT = 0x12,
    XM_INT = 0x13,
    VE_INT = 0x14,
    CP_INT = 0x15,
    // Reserved
    HV_INT = 0x1c,
    VC_INT = 0x1d,
    SX_INT = 0x1e,
    // Reserved
  };

  enum PF_bits : uint64_t {
    pf_present        = 1 << 0,
    pf_write          = 1 << 1,
    pf_user           = 1 << 2,
    pf_res_write      = 1 << 3,
    pf_instr_fetch    = 1 << 4,
    pf_protection_key = 1 << 5,
    pf_shadow_stack   = 1 << 6,
    pf_sgx_violation  = 1 << 15,
  };
}
