#include "time.hpp"

#include <interact>
#include <io_access>
#include <x86>
#include "interrupt.hpp"
#include "pic.hpp"

namespace Time {
  volatile bool time_changed = false;
  bool bcd;
  bool format12;

  RealTime real_time;

  uint8_t decode_bcd(uint8_t x) {
    if (bcd) return ((x / 16) * 10) + (x & 0xf);
    else return x;
  }

  uint8_t decode_hour(uint8_t x) {
    if (!format12) return decode_bcd(x);
    bool pm = x & 0x80;
    x = decode_bcd(x & 0x7f);
    if (x == 12) x = 0;
    if (pm) x += 12;
    return x;
  }

  extern "C" void handler_cmos_wrapper();
  extern "C" void handler_cmos([[maybe_unused]] Interrupt::Context *ctxp) {
    x86::cli();
    // all outb activate nmi
    outb(0x70, 0x00);
    real_time.sec = decode_bcd(inb(0x71));
    outb(0x70, 0x02);
    real_time.min = decode_bcd(inb(0x71));
    outb(0x70, 0x04);
    real_time.hour = decode_hour(inb(0x71));
    outb(0x70, 0x07);
    real_time.day = decode_bcd(inb(0x71));
    outb(0x70, 0x08);
    real_time.month = decode_bcd(inb(0x71));
    outb(0x70, 0x09);
    real_time.year_end = decode_bcd(inb(0x71));

    // cmos eoi
    outb(0x70, 0x0c);
    inb(0x71);

    time_changed = true;
    Pic::PIC_sendEOI(Pic::IRQ_CMOS);
    x86::sti();
  }

  void init() {
    static bool init_done = false;
    if (init_done) return;
    // Require
    Interact::init();
    Interrupt::init();
    Pic::init();
    // Init
    Interact::print_init("Time");

    x86::cli();
    outb(0x70, 0x8b); // disables nmi
    uint8_t regb = inb(0x71);
    outb(0x70, 0x8b);
    outb(0x71, regb | 0x40); // activates IRQ

    outb(0x70, 0x8a);
    uint8_t rega = inb(0x71);
    outb(0x70, 0x8a);
    outb(0x71, (rega & 0xf0) | 0x0f); // 0x0f == 2Hz
    x86::sti();

    bcd = regb & 2;
    format12 = regb & 4;

    Interrupt::setup_handler(Pic::IRQ_INT + Pic::IRQ_CMOS, handler_cmos_wrapper);

    Pic::IRQ_clear_mask(Pic::IRQ_CASCADE);
    Pic::IRQ_clear_mask(Pic::IRQ_CMOS);

    while (!time_changed) x86::hlt();

    // Done
    init_done = true;
    Interact::print_done();
  }

  bool time_updated() {
    if (time_changed) time_changed = false;
    else return false;
    return true;
  }

  RealTime get_time() {
    return real_time;
  }

  kstring u8_to_2kstr(uint8_t x) {
    kstring ret = "";
    ret.push_back(char('0' + (x / 10)));
    ret.push_back(char('0' + (x % 10)));
    return ret;
  }

  RealTime::operator kstring() {
    kstring ret = "20";
    ret += u8_to_2kstr(year_end);
    ret += "-";
    ret += u8_to_2kstr(month);
    ret += "-";
    ret += u8_to_2kstr(day);
    ret += " ";
    ret += u8_to_2kstr(hour);
    ret += ":";
    ret += u8_to_2kstr(min);
    ret += ":";
    ret += u8_to_2kstr(sec);
    return ret;
  }
}
