#include "exceptions.hpp"

#include <interact>
#include <terminal>
#include <virtual_memory_base>
#include <x86>
#include "lazy_memory.hpp"
#include "scheduler.hpp"

namespace Exceptions {
  using namespace Interrupt;

  extern "C" void handler_de_wrapper();
  extern "C" void handler_de(Context *ctxp) {
    fatal_error_name(ctxp, "Divide-by-zero Error");
  }

  extern "C" void handler_bp_wrapper();
  extern "C" void handler_bp(Context *ctxp) {
    fatal_error_name(ctxp, "Breakpoint");
  }

  extern "C" void handler_of_wrapper();
  extern "C" void handler_of(Context *ctxp) {
    fatal_error_name(ctxp, "Overflow");
  }

  extern "C" void handler_db_wrapper();
  extern "C" void handler_db(Context *ctxp) {
    fatal_error_name(ctxp, "Debug");
  }

  extern "C" void handler_br_wrapper();
  extern "C" void handler_br(Context *ctxp) {
    fatal_error_name(ctxp, "Bound Range Exceeded");
  }

  extern "C" void handler_ud_wrapper();
  extern "C" void handler_ud(Context *ctxp) {
    fatal_error_name(ctxp, "Invalid Opcode");
  }

  extern "C" void handler_nm_wrapper();
  extern "C" void handler_nm(Context *ctxp) {
    fatal_error_name(ctxp, "Device Not Available");
  }

  extern "C" void handler_df_wrapper();
  extern "C" void handler_df(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "Double Fault!", errcode);
  }

  extern "C" void handler_ts_wrapper();
  extern "C" void handler_ts(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "Invalid TSS", errcode);
  }

  extern "C" void handler_np_wrapper();
  extern "C" void handler_np(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "Segment Not Present", errcode);
  }

  extern "C" void handler_ss_wrapper();
  extern "C" void handler_ss(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "Stack-Segment Fault", errcode);
  }

  extern "C" void handler_gp_wrapper();
  extern "C" void handler_gp(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "General Protection Fault", errcode);
  }

  extern "C" void handler_pf_wrapper();
  extern "C" void handler_pf(Context *ctxp, uint64_t errcode) {
    if (!ctxp->is_user()) x86::cli();

    uintptr_t addr;
    asm volatile ("mov %%cr2, %0" : "=g"(addr));

    Terminal::newline();
    Terminal::print("Page Fault", Terminal::fg_dred);
    Terminal::print(" @0x");
    Terminal::print_hex(addr);
    Terminal::newline();

    Terminal::print("Caused by a ");
    if (errcode & pf_present) Terminal::print("page-protection violation");
    else Terminal::print("non-present page");
    Terminal::print(" during a ");
    if (errcode & pf_write) Terminal::print("write");
    else Terminal::print("read");
    Terminal::print(" in ");
    if (errcode & pf_user) Terminal::print("user");
    else Terminal::print("kernel");
    Terminal::print(" mode.\n");

    if (errcode & pf_res_write) Terminal::print("One or more page directory entries contain reserved bits which are set to 1.\n");
    if (errcode & pf_instr_fetch) Terminal::print("Caused by an instruction fetch.\n");
    if (errcode & pf_protection_key) Terminal::print("Caused by a protection-key violation.\n");
    if (errcode & pf_shadow_stack) Terminal::print("Caused by a shadow stack access.\n");
    if (errcode & pf_sgx_violation) Terminal::print("Caused by an SGX violation.\n");
    if (errcode & ~ (pf_sgx_violation | pf_shadow_stack | pf_protection_key | pf_instr_fetch | pf_res_write | pf_present | pf_write | pf_user) ) {
      Interact::warning("Unknown flags");
      Terminal::print_hex(errcode);
      Terminal::newline();
    }
    fatal_error(ctxp);
  }

  extern "C" void handler_mf_wrapper();
  extern "C" void handler_mf(Context *ctxp) {
    fatal_error_name(ctxp, "x87 Floating-Point Exception");
  }

  extern "C" void handler_ac_wrapper();
  extern "C" void handler_ac(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "Alignment Check", errcode);
  }

  extern "C" void handler_mc_wrapper();
  extern "C" void handler_mc(Context *ctxp) {
    fatal_error_name(ctxp, "Machine Check");
  }

  extern "C" void handler_xm_wrapper();
  extern "C" void handler_xm(Context *ctxp) {
    fatal_error_name(ctxp, "SIMD Floating-Point Exception");
  }

  extern "C" void handler_ve_wrapper();
  extern "C" void handler_ve(Context *ctxp) {
    fatal_error_name(ctxp, "Virtualization Exception");
  }

  extern "C" void handler_cp_wrapper();
  extern "C" void handler_cp(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "Control Protection Exception", errcode);
  }

  extern "C" void handler_hv_wrapper();
  extern "C" void handler_hv(Context *ctxp) {
    fatal_error_name(ctxp, "Hypervisor Injection Exception");
  }

  extern "C" void handler_vc_wrapper();
  extern "C" void handler_vc(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "VMM Communication Exception", errcode);
  }

  extern "C" void handler_sx_wrapper();
  extern "C" void handler_sx(Context *ctxp, uint64_t errcode) {
    fatal_error_name_errcode(ctxp, "Security Exception", errcode);
  }

  bool init_done = false;
  void init() {
    if (init_done) return;
    // Require
    Interact::init();
    Interrupt::init();
    // Init
    Interact::print_init("Exceptions");

    setup_handler(DE_INT, handler_de_wrapper);
    setup_handler(DB_INT, handler_db_wrapper);

    setup_handler(BP_INT, handler_bp_wrapper);
    setup_handler(OF_INT, handler_of_wrapper);
    setup_handler(BR_INT, handler_br_wrapper);
    setup_handler(UD_INT, handler_ud_wrapper);
    setup_handler(NM_INT, handler_nm_wrapper);
    setup_handler(DF_INT, handler_pf_wrapper, 1); // 1 : IST number

    setup_handler(TS_INT, handler_ts_wrapper);
    setup_handler(NP_INT, handler_np_wrapper);
    setup_handler(SS_INT, handler_ss_wrapper);
    setup_handler(GP_INT, handler_gp_wrapper);
    setup_handler(PF_INT, handler_pf_wrapper, 1); // 1 : IST number

    setup_handler(MF_INT, handler_mf_wrapper);
    setup_handler(AC_INT, handler_ac_wrapper);
    setup_handler(MC_INT, handler_mc_wrapper);
    setup_handler(XM_INT, handler_xm_wrapper);
    setup_handler(VE_INT, handler_ve_wrapper);
    setup_handler(CP_INT, handler_cp_wrapper);

    setup_handler(HV_INT, handler_hv_wrapper);
    setup_handler(VC_INT, handler_vc_wrapper);
    setup_handler(SX_INT, handler_sx_wrapper);

    // Done
    init_done = true;
    Interact::print_done();
  }
}
