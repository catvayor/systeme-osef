#include "signal.hpp"

#include <interact>
#include <kvector>
#include <syscall>
#include "kinship.hpp"
#include "process.hpp"
#include "process_pool.hpp"

namespace ProcessPool {
  namespace Signal {
    SigInfo::SigInfo() {
      for (unsigned i = 0; i < Syscall::NBSIG; ++i)
        actions[i].sa_handler = SIG_DFL;
    }

    SigContext::SigContext() {}
    SigContext::SigContext(uint64_t s, uint64_t m, Context c) : sig(s), mask(m), ctx(c) {}

    int64_t kill(pid_t pid, uint64_t sig) {
      if (sig > Syscall::NBSIG)
        return Syscall::err_badsig;
      if (processes.used(pid) && !processes[pid].is_dead()) {
        processes[pid].sig_info.pending |= 1 << sig;
        check(pid);
        return 0;
      } else return Syscall::err_badpid;
    }

    void call_handler(pid_t pid, uint64_t sig, Syscall::sigact action) {
      Process& proc = processes[pid];
      Context& ctx = proc.context;

      auto opt_sigctxp = LazyMemory::UserPtr<SigContext>(pid, (SigContext*)(ctx.rsp - sizeof(SigContext))).get_rw();
      if (opt_sigctxp) **opt_sigctxp = SigContext(sig, proc.sig_info.mask, ctx);
      else {
        ProcessPool::kill(pid);
        return;
      }

      ctx.rsp -= sizeof(SigContext) + sizeof(uintptr_t);
      auto opt_sigrstp = LazyMemory::UserPtr<uintptr_t>(pid, (uintptr_t*)ctx.rsp).get_rw();
      if (opt_sigrstp) **opt_sigrstp = (uintptr_t)action.sa_restorer;
      else {
        ProcessPool::kill(pid);
        return;
      }
      ctx.rip = (uint64_t)action.sa_handler;

      ctx.rdi = sig;

      proc.sig_info.mask = action.sa_mask;
    }

    void check(pid_t pid) {
      uint64_t sigs;
      if ((sigs = processes[pid].sig_info.pending & ~processes[pid].sig_info.mask) != 0) {
        unsigned i;
        for (i = 0; i < Syscall::NBSIG && !(sigs & (1 << i)); ++i);
        processes[pid].sig_info.pending &= ~(1 << i);
        if (i >= Syscall::NBSIG) return;
        auto action = processes[pid].sig_info.actions[i];
        if (action.sa_handler == SIG_DFL) {
          switch (i) {
            case Syscall::SIGKILL:
              if (processes[pid].is_waiting())
                processes[pid].wake();
              Kinship::exit(Syscall::SIGKILL_EXIT, pid);
              break;
            case Syscall::SIGINT:
              if (processes[pid].is_waiting())
                processes[pid].wake();
              Kinship::exit(Syscall::SIGINT_EXIT, pid);
              break;
            case Syscall::SIGABRT:
              if (processes[pid].is_waiting())
                processes[pid].wake();
              ProcessPool::kill(pid);
              break;
            default: // Ignore
              break;
          }
        } else if (action.sa_handler != SIG_IGN) {
          if (processes[pid].is_waiting())
            processes[pid].wake();
          LazyMemory::UserConstPtr<uintptr_t> user_handler(pid, (const uintptr_t*)action.sa_handler);
          LazyMemory::UserConstPtr<uintptr_t> user_restorer(pid, (const uintptr_t*)action.sa_restorer);
          if (!user_handler.get_ro(user_handler) || !user_handler.get_ro(user_restorer)) {
            Interact::warning("Bad signal handler, killing user.");
            ProcessPool::kill(pid);
          }
          call_handler(pid, i, action);
        }
      }
    }

    int64_t sigaction(uint64_t sig, LazyMemory::UserConstPtr<Syscall::sigact> new_action, LazyMemory::UserPtr<Syscall::sigact> old_action) {
      if (sig >= Syscall::NBSIG) return Syscall::err_badsig;
      if (sig == Syscall::SIGKILL) return 0;
      auto opt_new = new_action.get_ro();
      if (!opt_new) return Syscall::err_fault;
      Process &proc = cur_process();
      if (old_action) {
        auto opt_old = old_action.get_rw();
        if (!opt_old) return Syscall::err_fault;
        **opt_old = proc.sig_info.actions[sig];
      }
      proc.sig_info.actions[sig] = **opt_new;
      return 0;
    }

    void sigreturn() {
      pid_t pid = cur_pid();
      Process &proc = cur_process();
      auto opt_sigctxp = LazyMemory::UserPtr<SigContext>(pid, (SigContext*)proc.context.rsp).get_ro();
      if (!opt_sigctxp) {
        ProcessPool::kill(pid);
        return;
      }
      const SigContext &sc = **opt_sigctxp;

      if (sc.sig == Syscall::SIGABRT)
        ProcessPool::kill(pid);
      else {
        proc.context = sc.ctx;
        proc.sig_info.mask = sc.mask;
        check(pid);
      }
    }

    int64_t sigprocmask(Syscall::sig_mask_action how, LazyMemory::UserConstPtr<uint64_t> set, LazyMemory::UserPtr<uint64_t> oldset) {
      if (oldset) {
        auto opt_oldset = oldset.get_rw();
        if (!opt_oldset)
          return Syscall::err_fault;
        **opt_oldset = cur_process().sig_info.mask;
      }
      if (set) {
        auto opt_set = set.get_ro();
        if (!opt_set)
          return Syscall::err_fault;
        switch (how) {
          case Syscall::SIG_BLOCK:
            cur_process().sig_info.mask |= **opt_set & ~Syscall::SIG_NO_BLOCK;
            break;
          case Syscall::SIG_UNBLOCK:
            cur_process().sig_info.mask &= ~**opt_set;
            break;
          case Syscall::SIG_SETMASK:
            cur_process().sig_info.mask = **opt_set & ~Syscall::SIG_NO_BLOCK;
            break;
          default:
            return Syscall::err_error;
        }
      }
      check(cur_pid());
      return 0;
    }
  }
}
