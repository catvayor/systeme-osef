#pragma once

#include <array>
#include <syscall>

namespace ProcessPool {
  namespace Signal {
    struct SigInfo {
      uint64_t pending = 0;
      uint64_t mask = 0;
      std::array<Syscall::sigact, Syscall::NBSIG> actions;

      SigInfo();
    };
  }
}
