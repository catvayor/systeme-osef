#include "scheduler.hpp"

#include <array>
#include <cstring>
#include <interact>
#include <io_access>
#include <kchains>
#include <kvector>
#include <priority_queue>
#include "exceptions.hpp"
#include "gdt.hpp"
#include "ksyscall.hpp"
#include "lazy_memory.hpp"
#include "pic.hpp"
#include "process_pool.hpp"
#include "signal.hpp"
#include "window_manager.hpp"

namespace ProcessPool {
  namespace Scheduler {
    typedef uint64_t sleep_time_t;

    sleep_time_t cur_time = 0;

    struct Sleeper {
      pid_t pid;
      sleep_time_t time;

      Sleeper() {}
      Sleeper(pid_t p, sleep_time_t t) : pid(p), time(t) {}

      bool operator<(const Sleeper& other) const {
        return time < other.time;
      }

      bool operator>(const Sleeper& other) const {
        return time < other.time;
      }

      bool operator<=(sleep_time_t t) const {
        return time <= t;
      }
    };

    std::priority_queue<Sleeper, kvector<Sleeper>, std::greater<Sleeper>> sleepqueue;

    void put_to_sleep(pid_t pid, uint64_t ms) {
      unschedule(pid);
      sleep_time_t time = cur_time + ms * timer_freq / 1000;
      sleepqueue.push(Sleeper(pid, time));
    }

    void wake_sleepers() {
      while (!sleepqueue.empty() && sleepqueue.top() <= cur_time) {
        pid_t pid = sleepqueue.top().pid;
        if (processes[pid].is_runnable())
          schedule(pid);
        sleepqueue.pop();
      }
    }

    kchains<uint64_t> runqueues(1);

    bool timer_seen = false;

    extern "C" void handler_timer_wrapper();
    extern "C" void handler_timer(Context *ctxp) {
      timer_seen = true;
      cur_time++;
      if (ctxp->is_user()) {
        save_context(ctxp);
        Pic::PIC_sendEOI(Pic::IRQ_TIMER);
        iret_to_scheduler();
      }
      Pic::PIC_sendEOI(Pic::IRQ_TIMER);
    }

    void init_timer() {
      // Require
      Interact::init();
      Interrupt::init();
      Pic::init();
      // Init
      Interact::print_init("Timer");

      Interrupt::setup_handler(Pic::IRQ_INT + Pic::IRQ_TIMER, handler_timer_wrapper);

      constexpr uint8_t channel0 = 0b00 << 6;
      constexpr uint8_t lobyte_hibyte_access = 0b11 << 4;
      constexpr uint8_t rate_generator = 0b0100;
      outb(0x43, channel0 | lobyte_hibyte_access | rate_generator);

      outb(0x40, timer_div & 0xFF);     // Low byte
      outb(0x40, (timer_div & 0xFF00) >> 8); // High byte

      Pic::IRQ_clear_mask(Pic::IRQ_TIMER);

      // Done
      Interact::print_done();
    }

    bool init_done = false;
    void init() {
      if (init_done) return;
      // Require
      Gdt::init();
      Interact::init();
      Interrupt::init();
      KSyscall::init();
      Process::init();
      LazyMemory::init();
      init_timer();
      // Init
      Interact::print_init("Scheduler");

      // Done
      init_done = true;
      Interact::print_done();
    }

    // true on success
    bool choose_process() {
      if (!runqueues.empty(0)) {
        if (timer_seen) {
          runqueues.push_back(0, *runqueues.pop_front(0));
        }
      }
      while (!runqueues.empty(0)) {
        pid_t pid = cur_pid();
        Signal::check(pid);
        if (pid == cur_pid()) break;
      }
      timer_seen = false;
      return !runqueues.empty(0);
    }

    void scheduling_loop() {
      while (true) {
        WindowManager::handle_keyboard();

        wake_sleepers();

        static sleep_time_t last_refresh = 0;
        if (last_refresh + (timer_freq / 30) < cur_time) {
          WindowManager::draw_workspace();
          last_refresh = cur_time;
        }
        if (choose_process()) {
          cur_process().run();
        } else x86::hlt();
      }
    }

    void unschedule(pid_t pid) {
      if (pid == no_pid) return;
      runqueues.pop_elem(pid);
    }

    void schedule(pid_t pid) {
      runqueues.fit_elem(pid);
      runqueues.push_back(0, pid);
    }

    pid_t cur_pid() {
      return runqueues.empty(0) ? no_pid : runqueues.front(0);
    }

    [[noreturn]] void iret_to_scheduler() {
      asm volatile (
        "pushq $0     // stack segment\n"
        "pushq %0     // rsp\n"
        "pushq $0x200 // rflags (only interrupt bit set)\n"
        "pushq %1     // code segment\n"
        "pushq %2     // ret to virtual addr\n"
        "iretq\n"
        :: "g"(Gdt::tss.rsp0), "g"(Gdt::kcs_sel), "rm"(&scheduling_loop));
      Interact::error("iret failed");
    }
  }
}
