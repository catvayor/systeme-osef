#pragma once

#include "lazy_memory.hpp"
#include "process.hpp"

namespace ProcessPool {
  namespace LazyMemory {
    template<typename T> UserPtr<T>::UserPtr() : pid(no_pid), ptr(nullptr) {}
    template<typename T> UserPtr<T>::UserPtr(pid_t pid, T* ptr) : pid(pid), ptr(ptr) {}
    template<typename T> bool UserPtr<T>::is_nullptr() const {
      return ptr == nullptr;
    }
    template<typename T> UserPtr<T>::operator bool() const {
      return !is_nullptr();
    }

    template<typename T> std::optional<T const*> UserPtr<T>::get_ro(std::size_t size) {
      Process &p = processes[pid];
      if (p.owns((uintptr_t)ptr, size * sizeof(T))) {
        p.activate_main_page();
        make_readable(ptr, size * sizeof(T));
        return std::optional<T const*>(ptr);
      } else {
        return std::optional<T const*>();
      }
    }

    template<typename T> std::optional<T*> UserPtr<T>::get_rw(std::size_t size) {
      Process &p = processes[pid];
      if (p.owns((uintptr_t)ptr, size * sizeof(T))) {
        p.activate_main_page();
        make_writable(ptr, size * sizeof(T));
        return std::optional<T*>(ptr);
      } else {
        return std::optional<T*>();
      }
    }

    template<typename T> UserConstPtr<T>::UserConstPtr() : pid(no_pid), ptr(nullptr) {}
    template<typename T> UserConstPtr<T>::UserConstPtr(pid_t pid, T const* ptr) : pid(pid), ptr(ptr) {}
    template<typename T> bool UserConstPtr<T>::is_nullptr() const {
      return ptr == nullptr;
    }
    template<typename T> UserConstPtr<T>::operator bool() const {
      return !is_nullptr();
    }

    template<typename T> std::optional<T const*> UserConstPtr<T>::get_ro(std::size_t size) {
      Process &p = processes[pid];
      if (p.owns((uintptr_t)ptr, size * sizeof(T))) {
        p.activate_main_page();
        make_readable(ptr, size * sizeof(T));
        return std::optional<T const*>(ptr);
      } else {
        return std::optional<T const*>();
      }
    }

    template<typename T> std::optional<T const*> UserConstPtr<T>::get_ro_until_null(std::size_t maxsize) {
      Process &p = processes[pid];
      p.activate_main_page();
      uintptr_t last_page = 0;
      for (std::size_t k = 0; k < maxsize; k++) {
        if (p.owns((uintptr_t)(ptr + k), sizeof(T))) {
          uintptr_t page = ((uintptr_t)(ptr + k) + sizeof(T) - 1) & ~0xfffull;
          if (page != last_page) {
            make_readable(ptr + k, sizeof(T));
            last_page = page;
          }
          if (!ptr[k]) return std::optional<T const*>(ptr);
        }
      }
      return std::optional<T const*>();
    }
  }
}
