#pragma once

#include <optional>
#include <kqueue>
#include "process_pool.hpp"
#include "virtual_file.hpp"

namespace ProcessPool {
  namespace FileDescriptor {
    struct FileDescriptor {
      static constexpr unsigned FD_NONBLOCK = 1;

      Files::VirtualFile *file = nullptr;
      unsigned nb_read_refs = 0;
      unsigned nb_write_refs = 0;
      unsigned flags = FD_NONBLOCK;
      int64_t cursor = 0;

      void reset();
    };

    enum fd_error {
      fd_success = 0,
      fd_eof     = -1,
      fd_closed  = -2,
    };

    bool seekable(fd_t);
    bool readable(fd_t);
    bool writable(fd_t);
    kvector<uint8_t> read(fd_t, uint64_t max);
    void write(fd_t, kqueue<uint8_t>&);
    int64_t lseek(fd_t fd, int64_t ofs, Files::VirtualFile::SeekMode whence);

    fd_t open_fd(Files::VirtualFile *file, bool read, bool write);
    void connect(fd_t fd, bool read, bool write);
    void disconnect(fd_t fd, bool read, bool write);
    void add_waiting_process_write(fd_t fd, pid_t pid);
    void add_waiting_process_read(fd_t fd, pid_t pid);
    void remove_waiting_process(pid_t pid);
  }
}
