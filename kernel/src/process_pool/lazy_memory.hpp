#pragma once

#include <cstring>
#include <optional>
#include <stdint.h>
#include "process_pool.hpp"

namespace ProcessPool {
  namespace LazyMemory {
    constexpr uint64_t bytes_per_page = 0x1000;
    constexpr unsigned entries_per_page = bytes_per_page / sizeof(uintptr_t);

    typedef void* phys_ptr;
    typedef void* virt_ptr;
    typedef void const* const_virt_ptr;

    void init();

    void make_writable(virt_ptr ptr, std::size_t size = sizeof(uint64_t));
    void make_readable(const_virt_ptr ptr, std::size_t size = sizeof(uint64_t));

    phys_ptr new_main_page();
    phys_ptr clone_main_page(phys_ptr);

    void free_main_page(phys_ptr);

    void activate_main_page(phys_ptr main_page, bool force = false);

    template<typename T> struct UserPtr {
      pid_t pid;
      T *ptr;

      UserPtr();
      UserPtr(pid_t, T*);
      bool is_nullptr() const;
      operator bool() const;
      std::optional<T const*> get_ro(std::size_t size = 1);
      std::optional<T*> get_rw(std::size_t size = 1);
    };

    template<typename T> struct UserConstPtr {
      pid_t pid;
      T const* ptr;

      UserConstPtr();
      UserConstPtr(pid_t, T const*);
      bool is_nullptr() const;
      operator bool() const;
      std::optional<T const*> get_ro(std::size_t size = 1);
      std::optional<T const*> get_ro_until_null(std::size_t maxsize);
    };
  }
}
