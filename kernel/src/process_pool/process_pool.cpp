#include "scheduler.hpp"

#include <cstring>
#include <interact>
#include <io_access>
#include "exceptions.hpp"
#include "path.hpp"
#include "gdt.hpp"
#include "kinship.hpp"
#include "ksyscall.hpp"
#include "lazy_memory.hpp"
#include "pic.hpp"
#include "process.hpp"
#include "scheduler.hpp"
#include "window_manager.hpp"
#include "virtual_cursor_file.hpp"
#include "virtual_file.hpp"

namespace ProcessPool {
  kmemory_arena<Process> processes;

  Files::VirtualDir* pipe_dir = Files::root->new_dir("process_pipes");
  Files::VirtualDir* proc_dir = Files::root->new_dir("proc");

  void init() {
    static bool init_done = false;
    if (init_done) return;
    // Children
    Kinship::init();
    KSyscall::init();
    LazyMemory::init();
    Process::init();
    Scheduler::init();
    WindowManager::init();
    // Init
    Interact::print_init("ProcessPool");

    // Done
    init_done = true;
    Interact::print_done();
  }

  pid_t new_process(Process process, pid_t ppid) {
    if (processes.size() < max_pid) {
      pid_t pid = processes.push(process);
      Scheduler::schedule(pid);
      processes[pid].parent = ppid;
      Kinship::setup_kinship(pid);
      Files::VirtualDir* proc_pid_dir = proc_dir->new_dir(to_kstring(pid));
      proc_pid_dir->create_file<Files::VirtualCursorFile>("stat");
      return pid;
    } else {
      Interact::error("Cannot create any more processes");
    }
  }

  pid_t launch(kvector<kstring> command) {
    if (command.empty()) return no_pid;
    return execv(command[0], command);
  }

  pid_t exec(kstring path) {
    kvector<kstring> argv;
    argv.push_back(path);
    return execv(path, argv);
  }

  pid_t execv(kstring path, kvector<kstring> argv) {
    kvector<kstring> envp;
    envp.push_back("MEN=/mnt/usr");
    return execve(path, argv, envp);
  }

  pid_t execve(kstring path, kvector<kstring> argv, kvector<kstring> envp) {
    auto opt = Files::root->find_file(Files::Path(path));
    if (opt) {
      opt->open();
      pid_t pid = new_process(Process(opt->data(), path));
      opt->close();
      processes[pid].set_args(argv);
      processes[pid].set_env(envp);
      processes[pid].save_stat();
      return pid;
    }
    return no_pid;
  }

  void free_process(pid_t pid) {
    processes[pid].set_free();
    processes.pop(pid);
  }

  pid_t cur_pid() {
    return Scheduler::cur_pid();
  }

  Process& cur_process() {
    return processes[cur_pid()];
  }

  void put_to_sleep(pid_t pid, uint64_t ms) {
    Scheduler::put_to_sleep(pid, ms);
  }

  void unschedule(pid_t pid) {
    Scheduler::unschedule(pid);
  }

  void set_runnable(pid_t pid) {
    if (!processes[pid].is_runnable()) {
      processes[pid].set_runnable();
      Scheduler::schedule(pid);
    }
  }

  void kill(pid_t pid) {
    if (pid == no_pid) pid = cur_pid();
    Interact::warning("Killing user.");
    Kinship::exit(Process::terminated, pid);
  }

  void save_context(Context* ctxp) {
    processes[cur_pid()].save_context(ctxp);
  }

  void set_rax(pid_t pid, uint64_t rax) {
    processes[pid].context.rax = rax;
  }
}
