#include "keyboard_shortcuts.hpp"

#include <kunordered_map>
#include <utility>
#include "keyboard.hpp"
#include "window_manager.hpp"

namespace ProcessPool {
  namespace WindowManager {
    namespace Shortcut {
      void exec_action(Action action) {
        switch (action.type) {
          case TOGGLE_DEBUG_SIZE:
            toggle_debug_size();
            break;
          case TOGGLE_LAYOUT:
            Keyboard::switch_layout();
            break;
          case MOVE_PREV:
            move_prev();
            break;
          case MOVE_NEXT:
            move_next();
            break;
          case MOVE_WS:
            move_ws(action.arg);
            break;
          case LAUNCH_HYALMA:
            ProcessPool::launch({"/mnt/usr/sarati.elf", "/mnt/usr/hyalma.elf"});
            break;
          case TOGGLE_TABS:
            toggle_tabs();
            break;
          case TOGGLE_FULLSCREEN:
            toggle_fullscreen();
            break;
          case FOCUS_PREV:
            focus_prev();
            break;
          case FOCUS_NEXT:
            focus_next();
            break;
          case FOCUS_WS:
            focus_ws(action.arg);
            break;
          case NONE:
            break;
        }
      }

      kunordered_map<int16_t, Action> common_shortcuts;
      std::array<kunordered_map<int16_t, Action>, 2> layout_shortcuts;

      void init() {
        static bool done = false;
        if (done) return;

        layout_shortcuts[Keyboard::LAY_US] = kunordered_map<int16_t, Action>();
        layout_shortcuts[Keyboard::LAY_FR] = kunordered_map<int16_t, Action>();

        common_shortcuts['\n' | Keyboard::Ctrl] = LAUNCH_HYALMA;
        common_shortcuts['\n' | Keyboard::Mod4] = LAUNCH_HYALMA;
        common_shortcuts['w' | Keyboard::Ctrl] = TOGGLE_TABS;
        common_shortcuts['w' | Keyboard::Mod4] = TOGGLE_TABS;
        common_shortcuts['f' | Keyboard::Ctrl] = TOGGLE_FULLSCREEN;
        common_shortcuts['f' | Keyboard::Mod4] = TOGGLE_FULLSCREEN;
        common_shortcuts['g' | Keyboard::Ctrl] = TOGGLE_DEBUG_SIZE;
        common_shortcuts['g' | Keyboard::Mod4] = TOGGLE_DEBUG_SIZE;
        common_shortcuts['\033' | Keyboard::Alt] = TOGGLE_LAYOUT;
        common_shortcuts[Keyboard::K_LEFT | Keyboard::Ctrl] = FOCUS_PREV;
        common_shortcuts[Keyboard::K_LEFT | Keyboard::Mod4] = FOCUS_PREV;
        common_shortcuts[Keyboard::K_RIGHT | Keyboard::Ctrl] = FOCUS_NEXT;
        common_shortcuts[Keyboard::K_RIGHT | Keyboard::Mod4] = FOCUS_NEXT;
        common_shortcuts[Keyboard::K_LEFT | Keyboard::Ctrl | Keyboard::Shift] = MOVE_PREV;
        common_shortcuts[Keyboard::K_LEFT | Keyboard::Mod4 | Keyboard::Shift] = MOVE_PREV;
        common_shortcuts[Keyboard::K_RIGHT | Keyboard::Ctrl | Keyboard::Shift] = MOVE_NEXT;
        common_shortcuts[Keyboard::K_RIGHT | Keyboard::Mod4 | Keyboard::Shift] = MOVE_NEXT;

        const char* us_shift_digits = "!@#$%^&*(";
        const char* fr_unshift_digits = "&é\"'(-è_ç";
        for (unsigned char i = 0; i < 9; ++i) {
          layout_shortcuts[Keyboard::LAY_US][(i + '1') | Keyboard::Ctrl] = Action(FOCUS_WS, i);
          layout_shortcuts[Keyboard::LAY_US][(i + '1') | Keyboard::Mod4] = Action(FOCUS_WS, i);
          layout_shortcuts[Keyboard::LAY_US][us_shift_digits[i] | Keyboard::Ctrl | Keyboard::Shift] = Action(MOVE_WS, i);
          layout_shortcuts[Keyboard::LAY_US][us_shift_digits[i] | Keyboard::Mod4 | Keyboard::Shift] = Action(MOVE_WS, i);

          layout_shortcuts[Keyboard::LAY_FR][(i + '1') | Keyboard::Ctrl | Keyboard::Shift] = Action(MOVE_WS, i);
          layout_shortcuts[Keyboard::LAY_FR][(i + '1') | Keyboard::Mod4 | Keyboard::Shift] = Action(MOVE_WS, i);
          layout_shortcuts[Keyboard::LAY_FR][fr_unshift_digits[i] | Keyboard::Ctrl] = Action(FOCUS_WS, i);
          layout_shortcuts[Keyboard::LAY_FR][fr_unshift_digits[i] | Keyboard::Mod4] = Action(FOCUS_WS, i);
        }

        done = true;
      }

      bool check_shortcut(int16_t code) {
        if (common_shortcuts.contains(code)) {
          exec_action(common_shortcuts[code]);
          return true;
        }
        unsigned lay_id = Keyboard::get_layout();
        if (layout_shortcuts[lay_id].contains(code)) {
          exec_action(layout_shortcuts[lay_id][code]);
          return true;
        }
        return false;
      }
    }
  }
}
