#include "lazy_memory.hpp"

#include <array>
#include <cstring>
#include <interact>
#include <virtual_memory_base>
#include "exceptions.hpp"
#include "interrupt.hpp"
#include <kunordered_map>
#include "process.hpp"
#include "scheduler.hpp"

/*
  Each user data physical page has a reference counter.
  If a copy is requested:
  - the page is set to readonly in both page trees.
  - the refcounter is incremented.
  If a write is requested on a readonly page:
  - if its ref counter is 1, allow writing.
  - otherwise, make a true copy and decrement refcounter.

  Note: it is actually more comlpex due to the tree structure.
*/
namespace ProcessPool {
  namespace LazyMemory {
    struct RefCounter {
      struct Counter {
        uint64_t count;
        Counter() : count(1) {}
      };

      kunordered_map<uintptr_t, Counter> data;

      uint64_t& operator[](phys_ptr phys_addr) {
        return data[(uintptr_t)phys_addr].count;
      }

      void erase(phys_ptr phys_addr) {
        data.erase((uintptr_t)phys_addr);
      }
    } ref_counter;

    phys_ptr kernel_main_page;
    uint64_t buffer[entries_per_page] alignas(bytes_per_page); // used to manipulate physical pages

    extern "C" void handler_pf_lazy_wrapper();
    extern "C" void handler_pf_lazy(Context *ctxp, uint64_t errcode) {
      using enum Exceptions::PF_bits;
      if (!ctxp->is_user()) x86::cli();

      uintptr_t addr;
      asm volatile ("mov %%cr2, %0" : "=g"(addr));

      if ( ctxp->is_user()
           && cur_process().owns(addr, 1)
           && !(errcode & ~(pf_present | pf_write | pf_user)) ) {
        save_context(ctxp);
        make_writable((virt_ptr)addr, 1);
        Scheduler::iret_to_scheduler();
      }

      Exceptions::handler_pf(ctxp, errcode);
    }

    bool init_done = false;
    void init() {
      if (init_done) return;
      // Require
      Exceptions::init();
      Interact::init();
      Interrupt::init();
      // Init
      Interact::print_init("LazyMemory");

      kernel_main_page = virtual_memory::getPhysicalAddress(virtual_memory::ptr_to_PML4());

      Interrupt::setup_handler(Exceptions::PF_INT, handler_pf_lazy_wrapper, 1);

      // Done
      init_done = true;
      Interact::print_done();
    }

    phys_ptr entry_to_phys_ptr(uint64_t entry) {
      return (phys_ptr)(entry & virtual_memory::Address);
    }

    phys_ptr phys_copy(virt_ptr virt_addr) {
      phys_ptr new_phys_addr = virtual_memory::frame_alloc(1);
      virtual_memory::PML0_entry(buffer) = (uint64_t)new_phys_addr | virtual_memory::Present | virtual_memory::Writable;
      virtual_memory::invlpg(buffer);
      std::memcpy(buffer, virt_addr, bytes_per_page);
      return new_phys_addr;
    }

    void clear_buffer() {
      virtual_memory::PML0_entry(buffer) = 0;
      virtual_memory::invlpg(buffer);
    }

    void create_leaf_page(virt_ptr virt_addr) {
      constexpr bool user = true;
      constexpr bool writable = true;
      constexpr bool local = false;
      virtual_memory::make_virtual_address(virt_addr, bytes_per_page, user, writable, local);
    }

    void make_entry_writable(uint64_t& entry, virt_ptr virt_addr, bool leaf) {
      phys_ptr phys_addr = virtual_memory::getPhysicalAddress(virt_addr);
      unsigned start_ref_count = ref_counter[phys_addr];

      if (start_ref_count == 0) Interact::error("Ref not erased");
      else if (start_ref_count == 1) {
        if (!(entry & virtual_memory::Writable)) {
          entry |= virtual_memory::Writable;
          virtual_memory::invlpg(virt_addr);
        }
      } else {
        ref_counter[phys_addr]--;
        if (!leaf) {
          // rw to edit
          entry |= virtual_memory::Writable;
          virtual_memory::invlpg(virt_addr);
          // edit
          uint64_t* virt_addr_u64 = (uint64_t*)virt_addr;
          for (unsigned k = 0; k < entries_per_page; k++) {
            if (entry_to_phys_ptr(virt_addr_u64[k]) != nullptr) {
              virt_addr_u64[k] &= ~virtual_memory::Writable;
              ref_counter[entry_to_phys_ptr(virt_addr_u64[k])]++;
            }
          }
        }
        phys_ptr new_phys_addr = phys_copy(virt_addr);
        clear_buffer();
        entry = (uint64_t)(new_phys_addr)
                | virtual_memory::Present
                | virtual_memory::Writable
                | virtual_memory::User;
        virtual_memory::invlpg(virt_addr); // no need to recurse: no changes bellow
      }
    }

    void make_writable_base(virt_ptr virt_addr) {
      virt_addr = (virt_ptr)(((uintptr_t)virt_addr) & ~0xfffull);
      uint64_t& pml3_entry = virtual_memory::PML3_entry(virt_addr);
      if (pml3_entry & virtual_memory::Present) {
        make_entry_writable(pml3_entry, virtual_memory::ptr_to_PML3(virt_addr), false);

        uint64_t& pml2_entry = virtual_memory::PML2_entry(virt_addr);
        if (pml2_entry & virtual_memory::Present) {
          make_entry_writable(pml2_entry, virtual_memory::ptr_to_PML2(virt_addr), false);

          uint64_t& pml1_entry = virtual_memory::PML1_entry(virt_addr);
          if (pml1_entry & virtual_memory::Present) {
            make_entry_writable(pml1_entry, virtual_memory::ptr_to_PML1(virt_addr), false);

            uint64_t& pml0_entry = virtual_memory::PML0_entry(virt_addr);
            if (pml0_entry & virtual_memory::Present) {
              make_entry_writable(pml0_entry, virt_addr, true);
              return;
            }
          }
        }
      }
      create_leaf_page(virt_addr);
    }

    void make_writable(virt_ptr virt_addr, std::size_t size) {
      uintptr_t end = ((uintptr_t)virt_addr) + size;
      for (uintptr_t ptr = ((uintptr_t)virt_addr) & ~0xfffull; ptr < end; ptr += bytes_per_page) {
        make_writable_base((virt_ptr)ptr);
      }
    }

    void make_readable_base(const_virt_ptr virt_addr) {
      virt_ptr virt_addr_ali = (virt_ptr)(((uintptr_t)virt_addr) & ~0xfffull);
      uint64_t& pml3_entry = virtual_memory::PML3_entry(virt_addr_ali);
      if (pml3_entry & virtual_memory::Present) {
        uint64_t& pml2_entry = virtual_memory::PML2_entry(virt_addr_ali);
        if (pml2_entry & virtual_memory::Present) {
          uint64_t& pml1_entry = virtual_memory::PML1_entry(virt_addr_ali);
          if (pml1_entry & virtual_memory::Present) {
            uint64_t& pml0_entry = virtual_memory::PML0_entry(virt_addr_ali);
            if (pml0_entry & virtual_memory::Present) {
              return;
            }
          }
        }
      }
      create_leaf_page(virt_addr_ali);
    }

    void make_readable(const_virt_ptr virt_addr, std::size_t size) {
      uintptr_t end = ((uintptr_t)virt_addr) + size;
      for (uintptr_t ptr = ((uintptr_t)virt_addr) & ~0xfffull; ptr < end; ptr += bytes_per_page) {
        make_readable_base((virt_ptr)ptr);
      }
    }

    void activate_main_page(phys_ptr main_page, bool force) {
      static phys_ptr cur_main_page = nullptr;
      if (force || main_page != cur_main_page) virtual_memory::loadPaging(main_page);
      cur_main_page = main_page;
    }

    phys_ptr new_main_page() {
      phys_ptr phys_addr = phys_copy(virtual_memory::ptr_to_PML4());
      ((uint64_t*)buffer)[511] = (uintptr_t)(phys_addr) | virtual_memory::Present | virtual_memory::Writable; // loopback
      ((uint64_t*)buffer)[0] = 0; // clear eventual user data
      clear_buffer();
      activate_main_page(phys_addr, true);
      return phys_addr;
    }

    phys_ptr clone_main_page(phys_ptr main_page) {
      activate_main_page(main_page);
      uint64_t& entry = virtual_memory::PML3_entry(nullptr);
      ref_counter[entry_to_phys_ptr(entry)]++;
      entry &= ~virtual_memory::Writable; // no need to invlpg, we are switching soon
      phys_ptr phys_addr = phys_copy(virtual_memory::ptr_to_PML4());
      ((uint64_t*)buffer)[511] = (uintptr_t)(phys_addr) | virtual_memory::Present | virtual_memory::Writable; // loopback
      clear_buffer();
      activate_main_page(phys_addr, true);
      return phys_addr;
    }

    void free_user_page(virt_ptr virt_addr, unsigned level) {
      phys_ptr phys_addr = virtual_memory::getPhysicalAddress(virt_addr);
      if (--ref_counter[phys_addr] == 0) {
        ref_counter.erase(phys_addr);
        if (level != 0) {
          uint64_t* virt_addr_u64 = (uint64_t*)virt_addr;
          for (unsigned k = 0; k < entries_per_page; k++) {
            if (entry_to_phys_ptr(virt_addr_u64[k]) != nullptr) {
              free_user_page((virt_ptr)(((uintptr_t)virt_addr << 9) + (k << 12)), level - 1);
            }
          }
        }
        virtual_memory::frame_free(phys_addr, 1);
      }
    }

    void free_main_page(phys_ptr main_page) {
      activate_main_page(main_page);
      free_user_page(virtual_memory::ptr_to_PML3(nullptr), 3);

      phys_ptr user_main_page = virtual_memory::getPhysicalAddress(virtual_memory::ptr_to_PML4());
      if (user_main_page == kernel_main_page) {
        Interact::error("Trying to free kernel main page.");
      }
      activate_main_page(kernel_main_page);
      virtual_memory::frame_free(user_main_page, 1);
    }

    void memcpy_to_user(virt_ptr dest, virt_ptr src, std::size_t count) {
      make_writable(dest, count);
      std::memcpy(dest, src, count);
    }

    void memcpy_from_user(virt_ptr dest, virt_ptr src, std::size_t count) {
      make_writable(src, count);
      std::memcpy(dest, src, count);
    }
  }
}
