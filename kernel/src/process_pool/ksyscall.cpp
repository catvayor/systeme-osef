#include "ksyscall.hpp"

#include <interact>
#include <syscall>
#include <terminal>
#include "interrupt.hpp"
#include "kinship.hpp"
#include "lazy_memory.hpp"
#include "scheduler.hpp"
#include "signal.hpp"
#include "userref.hpp"

namespace ProcessPool {
  namespace KSyscall {
    void syscall() {
      Context &ctx = cur_process().context;
      uint64_t arg1 = ctx.rdi;
      uint64_t arg2 = ctx.rsi;
      uint64_t arg3 = ctx.rdx;

      switch (ctx.rax) {
        case Syscall::sys_exit:
          Kinship::exit(arg1, cur_pid());
          break;
        case Syscall::sys_fork:
          Kinship::fork();
          break;
        case Syscall::sys_waitpid:
          Kinship::waitpid((pid_t)arg1, LazyMemory::UserPtr<int64_t>(cur_pid(), (int64_t*)arg2), (int64_t)arg3);
          break;
        case Syscall::sys_microsleep:
          cur_process().microsleep(arg1);
          break;
        case Syscall::sys_execve:
          cur_process().execve(LazyMemory::UserConstPtr<char>(cur_pid(), (char const*)arg1),
                               LazyMemory::UserConstPtr<char const*>(cur_pid(), (char const * const*)arg2),
                               LazyMemory::UserConstPtr<char const*>(cur_pid(), (char const * const*)arg3));
          break;
        case Syscall::sys_open:
          cur_process().open(LazyMemory::UserConstPtr<char>(cur_pid(), (char const*)arg1), (int64_t)arg2);
          break;
        case Syscall::sys_close:
          cur_process().close(arg1);
          break;
        case Syscall::sys_dup2:
          cur_process().dup2(arg1, arg2);
          break;
        case Syscall::sys_pipe:
          cur_process().pipe(LazyMemory::UserPtr<uint64_t>(cur_pid(), (uint64_t*)arg1));
          break;
        case Syscall::sys_flush_pipe:
          cur_process().pipe(LazyMemory::UserPtr<uint64_t>(cur_pid(), (uint64_t*)arg1), true);
          break;
        case Syscall::sys_sbrk:
          cur_process().sbrk(arg1);
          break;
        case Syscall::sys_write:
          cur_process().write(arg1, LazyMemory::UserConstPtr<uint8_t>(cur_pid(), (uint8_t const*)arg2), arg3);
          break;
        case Syscall::sys_read:
          cur_process().read(arg1, LazyMemory::UserPtr<uint8_t>(cur_pid(), (uint8_t*)arg2), arg3);
          break;
        case Syscall::sys_pollin:
          cur_process().pollin(LazyMemory::UserPtr<uint64_t>(cur_pid(), (uint64_t*)arg1), arg2);
          break;
        case Syscall::sys_chdir:
          cur_process().chdir(LazyMemory::UserConstPtr<char>(cur_pid(), (const char*)arg1));
          break;
        case Syscall::sys_opendir:
          cur_process().opendir(LazyMemory::UserConstPtr<char>(cur_pid(), (const char*)arg1));
          break;
        case Syscall::sys_readdir:
          cur_process().readdir(LazyMemory::UserPtr<char>(cur_pid(), (char*)arg1));
          break;
        case Syscall::sys_raise:
          ctx.rax = Signal::kill(cur_pid(), arg1);
          break;
        case Syscall::sys_kill:
          ctx.rax = Signal::kill(arg1, arg2);
          break;
        case Syscall::sys_sigaction:
          ctx.rax = Signal::sigaction(arg1,
                                      LazyMemory::UserConstPtr<Syscall::sigact>(cur_pid(), (const Syscall::sigact*)arg2),
                                      LazyMemory::UserPtr<Syscall::sigact>(cur_pid(), (Syscall::sigact*)arg3));
          break;
        case Syscall::sys_sigreturn:
          Signal::sigreturn();
          break;
        case Syscall::sys_sigprocmask:
          ctx.rax = Signal::sigprocmask((Syscall::sig_mask_action)arg1,
                                        LazyMemory::UserConstPtr<uint64_t>(cur_pid(), (uint64_t*)arg2),
                                        LazyMemory::UserPtr<uint64_t>(cur_pid(), (uint64_t*)arg3));
          break;
        case Syscall::sys_window_get:
          cur_process().get_window();
          break;
        case Syscall::sys_window_refresh:
          cur_process().refresh_window(LazyMemory::UserConstPtr<Syscall::WindowData>(cur_pid(), (Syscall::WindowData*)arg1));
          break;
        case Syscall::sys_get_key:
          cur_process().get_key();
          break;
        default:
          Terminal::newline();
          Terminal::print("Unknown syscall", Terminal::fg_dred);
          Terminal::print(" 0x");
          Terminal::print_hex(ctx.rax);
          Terminal::newline();
          kill(cur_pid());
          break;
      }
    }

    extern "C" void handler_syscall_wrapper();
    extern "C" void handler_syscall(Context *ctxp) {
      if (ctxp->is_user()) {
        save_context(ctxp);
        syscall();
        Scheduler::iret_to_scheduler();
      } else {
        Interrupt::fatal_error_name(ctxp, "Syscall in kernel mode");
      }
    }

    bool init_done = false;
    void init() {
      if (init_done) return;
      // Require
      Interact::init();
      Interrupt::init();
      // Init
      Interact::print_init("Syscall");

      Interrupt::setup_handler(Syscall::syscall_int, handler_syscall_wrapper, 0, true);

      // Done
      init_done = true;
      Interact::print_done();
    }
  }
}
