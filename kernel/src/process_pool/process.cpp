#include "process.hpp"

#include <cstring>
#include <interact>
#include <syscall>
#include <quenya_base>
#include <virtual_memory_base>
#include "pipe_files.hpp"
#include "file_descriptor.hpp"
#include "gdt.hpp"
#include "lazy_memory.hpp"
#include "userref.hpp"
#include "signal.hpp"
#include "window_manager.hpp"

#include "utils.hpp"

namespace ProcessPool {
  Files::VirtualFile* dev_null = nullptr;

  void Process::init() {
    static bool init_done = false;
    if (init_done) return;
    // Require
    Gdt::init();
    Interact::init();
    Interrupt::init();
    LazyMemory::init();
    // Init
    Interact::print_init("Process");

    dev_null = Files::root->find_file("/dev/null");
    if (!dev_null) {
      Interact::print_failed();
      Interact::error("/dev/null not found.");
    }

    // Done
    init_done = true;
    Interact::print_done();
  }

  Process::Process() {
    state = ps_free;
    running = false;
  }

  Process::Process(void const* file, kstring path) {
    cmd = path;

    constexpr bool user = true;
    constexpr bool local = false;

    state = ps_runnable;
    running = false;

    main_page_phys = LazyMemory::new_main_page(); // activates main page

    stack_full = default_stack_full;
    heap_end = heap_begin;

    window = no_win;

    open_file_at(STDIN_FILENO, dev_null, true, false);
    open_file_at(STDOUT_FILENO, dev_null, false, true);
    open_file_at(STDERR_FILENO, dev_null, false, true);

    // Iret values
    context.rip = (uintptr_t)(quenya::loadElfHeader_risky(file, user, local));
    context.cs = Gdt::ucs_sel;
    context.rflags = 0x200; // only interrupt bit set
    context.rsp = stack_end;
    context.ss = Gdt::uds_sel;
  }

  Process Process::clone() {
    Process ret;

    ret.state = state;
    ret.running = false;

    ret.main_page_phys = LazyMemory::clone_main_page(main_page_phys);

    ret.stack_full = stack_full;
    ret.heap_end = heap_end;

    ret.window = no_win;

    ret.fds = fds;
    for (FdRW& fd : fds)
      clone_fd(fd);

    ret.cur_dir = cur_dir;
    ret.dir_ls_pos = dir_ls_pos;
    ret.dir_ls = dir_ls;

    ret.context = context;

    ret.sig_info = sig_info;

    ret.cmd = cmd;

    return ret;
  }

  bool Process::owns(uintptr_t ptr, std::size_t size) {
    if (is_dead()) return false;
    bool in_heap = (heap_begin <= ptr) && (ptr + size <= heap_end);
    bool in_stack = (stack_full <= ptr) && (ptr + size <= stack_end);
    bool in_elf = (elf_begin <= ptr) && (ptr + size <= elf_end);
    return in_heap || in_stack || in_elf;
  }

  void Process::activate_main_page() {
    LazyMemory::activate_main_page(main_page_phys);
  }

  void Process::run() {
    if (!is_runnable()) {
      Terminal::print_hex(self_pid);
      Terminal::newline();
      Terminal::print_hex(state);
      Interact::error("Trying to run a non-runnable process");
    }
    if (running) Interact::error("Trying to run an unsaved process");
    running = true;

    activate_main_page();

    context.restore();
  }

  void Process::pause() {
    context.rax = Syscall::err_intr;
    state = ps_waiting_pause;
    save_stat();
    unschedule(self_pid);
  }

  void Process::save_context(Context *ctx) {
    if (!running) Interact::error("Trying to save context of non-running process");
    running = false;
    context = *ctx;
  }

  void Process::set_free() {
    if (!is_zombie()) Interact::error("Trying to free a non-zombie process");
    state = ps_free;
    delete Files::root->find_dir("proc/" + to_kstring(self_pid));
  }

  void Process::set_runnable() {
    if (is_free() || is_zombie()) Interact::error("Trying to make runnable a free or zombie process");
    state = ps_runnable;
    save_stat();
  }

  void Process::waitpid(pid_t pid, LazyMemory::UserPtr<int64_t> status, int64_t options) {
    context.rax = Syscall::err_intr;
    state = ps_waiting_child;
    waitpid_pid = pid;
    waitpid_status = status;
    waitpid_options = options;
    unschedule(self_pid);
    save_stat();
  }

  void Process::wake() {
    switch (state) {
      case ps_free:
        Interact::error("Wake on free process");
      case ps_zombie:
        Interact::error("One should not try to wake the dead");
      case ps_runnable:
        return;
      case ps_waiting_child:
      case ps_waiting_pause:
        break;
      case ps_waiting_read:
      case ps_waiting_write:
        FileDescriptor::remove_waiting_process(self_pid);
        break;
      default:
        Interact::error("Wake not supported in current state");
    }

    ProcessPool::set_runnable(self_pid);
  }

  void Process::microsleep(uint64_t ms) {
    ProcessPool::put_to_sleep(self_pid, ms);
    context.rax = Syscall::err_success;
  }

  void Process::exit(int64_t code) {
    if (!is_runnable()) Interact::error("Trying to exit a non-runnable process");
    state = ps_zombie;
    context.rax = code;

    LazyMemory::free_main_page(main_page_phys);

    while (!fds.empty())
      close_fd(fds.front_idx());

    WindowManager::close_window(window);
    window = no_win;

    save_stat();
  }

  void Process::sbrk(int64_t delta) {
    if (delta < 0) {
      if (heap_end < (uint64_t)(-delta)) {
        context.rax = Syscall::err_fault;
        return;
      }
    }
    if (delta > 0) {
      if (heap_end + (uint64_t)(delta) > max_heap_end) {
        context.rax = Syscall::err_nomem;
        return;
      }
    }
    context.rax = heap_end;
    heap_end += delta;
  }

  bool Process::exec_base(kstring path) {
    Files::VirtualFile* file_ptr = Files::root->find_file(abspath(path));
    if (!file_ptr) {
      context.rax = Syscall::err_notfound;
      return false;
    }
    file_ptr->open();
    void const* file = file_ptr->data();;

    constexpr bool user = true;
    constexpr bool local = false;

    LazyMemory::free_main_page(main_page_phys);

    main_page_phys = LazyMemory::new_main_page(); // activates main page

    // Iret values
    context.rip = (uintptr_t)(quenya::loadElfHeader_risky(file, user, local));
    context.cs = Gdt::ucs_sel;
    context.rflags = 0x200; // only interrupt bit set
    context.rsp = stack_end;
    context.ss = Gdt::uds_sel;

    sig_info = Signal::SigInfo();

    file_ptr->close();

    cmd = path;
    save_stat();

    return true;
  }

  void Process::execve(LazyMemory::UserConstPtr<char> path_ptr, LazyMemory::UserConstPtr<char const*> argv_usr, LazyMemory::UserConstPtr<char const*> envp_usr) {
    auto opt_path = path_ptr.get_ro_until_null(Syscall::max_io_size);
    if (!opt_path) {
      context.rax = Syscall::err_fault;
      return;
    }

    kstring path = *opt_path;

    kvector<kstring> argv;
    auto opt_argv = argv_usr.get_ro_until_null(Syscall::max_io_size);
    if (!opt_argv) {
      context.rax = Syscall::err_fault;
      return;
    }

    char const* const* argv_ok = *opt_argv;
    while (argv_ok[0]) {
      auto opt_arg = LazyMemory::UserConstPtr<char>(argv_usr.pid, argv_ok[0]).get_ro_until_null(Syscall::max_io_size);
      if (!opt_arg) {
        context.rax = Syscall::err_fault;
        return;
      }

      argv.push_back(*opt_arg);
      argv_ok++;
    }

    kvector<kstring> envp;
    auto opt_envp = envp_usr.get_ro_until_null(Syscall::max_io_size);
    if (!opt_envp) {
      context.rax = Syscall::err_fault;
      return;
    }

    char const* const* envp_ok = *opt_envp;
    while (envp_ok[0]) {
      auto opt_env = LazyMemory::UserConstPtr<char>(envp_usr.pid, envp_ok[0]).get_ro_until_null(Syscall::max_io_size);
      if (!opt_env) {
        context.rax = Syscall::err_fault;
        return;
      }

      envp.push_back(*opt_env);
      envp_ok++;
    }

    if (exec_base(path)) {
      set_args(argv);
      set_env(envp);
    }
  }

  void Process::set_args(kvector<kstring> argv) {
    activate_main_page();
    uint64_t argc = argv.size();
    context.rsp -= sizeof(uint64_t) * (argc + 1);
    uint64_t* layer1 = (uint64_t*)context.rsp;
    LazyMemory::make_writable(layer1, sizeof(uint64_t) * (argc + 1));
    for (unsigned k = 0; k < argc; k++) {
      unsigned size = argv[k].size() + 1;
      context.rsp -= size;
      char* layer2 = (char*)context.rsp;
      LazyMemory::make_writable(layer2, size);
      std::strcpy(layer2, argv[k].c_str());
      layer1[k] = context.rsp;
    }
    layer1[argc] = 0;

    context.rdi = argc;
    context.rsi = (uintptr_t)layer1;
  }

  void Process::set_env(kvector<kstring> envp) {
    activate_main_page();
    uint64_t envc = envp.size();
    context.rsp -= sizeof(uint64_t) * (envc + 1);
    uint64_t* layer1 = (uint64_t*)context.rsp;
    LazyMemory::make_writable(layer1, sizeof(uint64_t) * (envc + 1));
    for (unsigned k = 0; k < envc; k++) {
      unsigned size = envp[k].size() + 1;
      context.rsp -= size;
      char* layer2 = (char*)context.rsp;
      LazyMemory::make_writable(layer2, size);
      std::strcpy(layer2, envp[k].c_str());
      layer1[k] = context.rsp;
    }
    layer1[envc] = 0;

    context.rdx = (uintptr_t)layer1;
  }

  Process::FdRW::FdRW() {}
  Process::FdRW::FdRW(fd_t f, bool r, bool w) : fd(f), read(r), write(w) {}

  void Process::open_file_at(uint64_t fd, Files::VirtualFile* file, bool read, bool write) {
    fds.push_back_idx(fd, FdRW(FileDescriptor::open_fd(file, read, write), read, write));
  }

  uint64_t Process::open_file(Files::VirtualFile* file, bool read, bool write) {
    return fds.push_back(FdRW(FileDescriptor::open_fd(file, read, write), read, write));
  }

  uint64_t Process::dup_fd_rw(uint64_t fd, bool read, bool write) {
    FileDescriptor::connect(fds[fd].fd, read, write);
    return fds.push_back(FdRW(fds[fd].fd, read, write));
  }

  void Process::dup_fd_same_at(uint64_t dst, uint64_t src) {
    close_fd(dst);
    clone_fd(fds[src]);
    fds.push_back_idx(dst, fds[src]);
  }

  void Process::close_fd(uint64_t fd) {
    if (fds.used(fd)) {
      FileDescriptor::disconnect(fds[fd].fd, fds[fd].read, fds[fd].write);
      fds.pop_idx(fd);
    }
  }

  void Process::clone_fd(FdRW fdrw) {
    FileDescriptor::connect(fdrw.fd, fdrw.read, fdrw.write);
  }

  void Process::write(uint64_t fdid, LazyMemory::UserConstPtr<uint8_t> buf, uint64_t count) {
    if (count > Syscall::max_io_size) {
      context.rax = Syscall::err_size;
      return;
    }
    if (!fds.used(fdid) || !fds[fdid].write) {
      context.rax = Syscall::err_badfd;
      return;
    }
    auto opt = buf.get_ro(count);
    if (!opt) {
      context.rax = Syscall::err_fault;
      return;
    }
    const uint8_t* ptr = *opt;

    context.rax = Syscall::err_intr;
    write_fdid = fdid;
    write_count = 0;
    write_buf.clear();
    for (uint64_t k = 0; k < count; k++) {
      write_count++;
      write_buf.push(*ptr++);
    }
    resume_write();
  }

  void Process::read(uint64_t fdid, LazyMemory::UserPtr<uint8_t> buf, uint64_t count) {
    if (count > Syscall::max_io_size) {
      context.rax = Syscall::err_size;
      return;
    }
    if (!fds.used(fdid) || !fds[fdid].read) {
      context.rax = Syscall::err_badfd;
      return;
    }
    auto opt = buf.get_rw(count);
    if (!opt) {
      context.rax = Syscall::err_fault;
      return;
    }

    context.rax = Syscall::err_intr;
    read_fdid = fdid;
    read_buf = buf;
    read_count = count;
    resume_read();
  }

  void Process::resume_write() {
    FileDescriptor::write(fds[write_fdid].fd, write_buf);
    if (!write_buf.empty()) {
      if (write_buf.size() < write_count) context.rax = write_count - write_buf.size(); // in case of interrupt
      if (is_runnable()) {
        state = ps_waiting_write;
        save_stat();
        unschedule(self_pid);
        FileDescriptor::add_waiting_process_write(fds[write_fdid].fd, self_pid);
      }
      return;
    } else context.rax = write_count;
    if (!is_runnable()) ProcessPool::set_runnable(self_pid);
  }

  void Process::resume_read() {
    if (FileDescriptor::readable(fds[read_fdid].fd)) {
      kvector<uint8_t> buf = FileDescriptor::read(fds[read_fdid].fd, read_count);

      uint8_t *ptr = read_buf.get_rw(read_count).value();
      for (uint8_t x : buf) {
        *ptr++ = x;
      }

      context.rax = buf.size();
    } else {
      if (is_runnable()) {
        state = ps_waiting_read;
        save_stat();
        unschedule(self_pid);
        FileDescriptor::add_waiting_process_read(fds[read_fdid].fd, self_pid);
      }
      return;
    }

    if (!is_runnable()) ProcessPool::set_runnable(read_buf.pid);
  }

  void Process::pollin(LazyMemory::UserPtr<uint64_t> fdids, uint64_t count) {
    if (count > Syscall::max_io_size) {
      context.rax = Syscall::err_size;
      return;
    }

    auto fdids_opt = fdids.get_rw(count);
    if (!fdids_opt) {
      context.rax = Syscall::err_fault;
      return;
    }

    uint64_t ready_count = 0;
    for (unsigned k = 0; k < count; k++) {
      if (!fds.used((*fdids_opt)[k])) {
        context.rax = Syscall::err_badfd;
        return;
      }

      if (FileDescriptor::readable(fds[(*fdids_opt)[k]].fd)) {
        (*fdids_opt)[ready_count++] = (*fdids_opt)[k];
      }
    }
    context.rax = ready_count;
  }

  void Process::get_window() {
    if (window == no_win) window = WindowManager::new_window();
    context.rax = WindowManager::get_window(window);
  }

  void Process::refresh_window(LazyMemory::UserConstPtr<Syscall::WindowData> ptr) {
    if (WindowManager::refresh_window(window, ptr)) context.rax = Syscall::err_success;
    else context.rax = Syscall::err_nowin;
  }

  void Process::put_key(int16_t key) {
    kbd_buf.push(key);
  }

  void Process::get_key() {
    if (kbd_buf.empty()) context.rax = -1;
    else context.rax = kbd_buf.pop().value();
  }

  void Process::open(LazyMemory::UserConstPtr<char> path, int64_t flags) {
    auto tmp_path = path.get_ro_until_null(Syscall::max_io_size);
    if (!tmp_path) {
      context.rax = Syscall::err_fault;
      return;
    }
    kstring str_path = *tmp_path;

    auto file = Files::root->find_file(abspath(str_path), flags & Syscall::O_CREAT);
    if (!file) {
      context.rax = Syscall::err_notfound;
      return;
    }

    if (flags & Syscall::O_APPEND) {
      if (!file->seekable()) {
        context.rax = Syscall::err_error;
        return;
      }
    }
    if (flags & Syscall::O_TRUNC) {
      if (!file->truncable()) {
        context.rax = Syscall::err_error;
        return;
      }
    }

    context.rax = open_file(file, flags & Syscall::O_RDONLY, flags & Syscall::O_WRONLY);

    if (flags & Syscall::O_TRUNC)
      file->trunc();

    if (flags & Syscall::O_APPEND)
      FileDescriptor::lseek(fds[context.rax].fd, 0, Files::VirtualFile::SEEK_END);

    return;
  }

  void Process::close(uint64_t fd) {
    if (!fds.used(fd)) context.rax = Syscall::err_badfd;
    else {
      close_fd(fd);
      context.rax = Syscall::err_success;
    }
  }

  void Process::dup2(uint64_t oldfd, uint64_t newfd) {
    if (!fds.used(oldfd) || newfd >= Syscall::max_fd)
      context.rax = Syscall::err_badfd;
    else {
      dup_fd_same_at(newfd, oldfd);
      context.rax = newfd;
    }
  }

  void Process::pipe(LazyMemory::UserPtr<uint64_t> ptr, bool flushpipe) {
    if (!ptr.get_rw(2)) {
      context.rax = Syscall::err_fault;
      return;
    }

    uint64_t infd;
    if (flushpipe) infd = open_file(pipe_dir->create_file<Files::FlushPipe>("flushpipe"), false, true);
    else infd = open_file(pipe_dir->create_file<Files::Pipe>("pipe"), false, true);
    uint64_t outfd = dup_fd_rw(infd, true, false);

    uint64_t* ptr_usr = ptr.get_rw(2).value();
    ptr_usr[Syscall::pipe_out] = outfd;
    ptr_usr[Syscall::pipe_in] = infd;
    context.rax = 0;
  }

  Files::Path Process::abspath(kstring s) {
    if (!s.empty() && s[0] == '/') return Files::Path(s);
    else return cur_dir + s;
  }

  void Process::chdir(LazyMemory::UserConstPtr<char> path) {
    auto tmp_path = path.get_ro_until_null(Syscall::max_io_size);
    if (!tmp_path) {
      context.rax = Syscall::err_fault;
      return;
    }
    kstring str_path = *tmp_path;
    if (str_path.back() != '/') str_path.push_back('/');
    Files::Path abs_path = abspath(str_path);
    auto tmp_dir = Files::root->find_dir(abs_path);
    if (!tmp_dir) {
      context.rax = Syscall::err_notfound;
      return;
    }
    context.rax = Syscall::err_success;
    cur_dir = abs_path;
  }

  void Process::opendir(LazyMemory::UserConstPtr<char> path) {
    auto tmp_path = path.get_ro_until_null(Syscall::max_io_size);
    if (!tmp_path) {
      context.rax = Syscall::err_fault;
      return;
    }
    kstring str_path = *tmp_path;
    if (str_path.back() != '/') str_path.push_back('/');
    auto tmp_dir = Files::root->find_dir(abspath(str_path));
    if (!tmp_dir) {
      context.rax = Syscall::err_notfound;
      dir_ls_pos = 0;
      dir_ls.clear();
      return;
    }
    context.rax = Syscall::err_success;
    dir_ls_pos = 0;
    dir_ls = tmp_dir->dir_ls();
  }

  void Process::readdir(LazyMemory::UserPtr<char> name) {
    auto tmp_name = name.get_rw(Syscall::max_io_size);
    if (!tmp_name) {
      context.rax = Syscall::err_fault;
      return;
    }
    if (dir_ls_pos < dir_ls.size()) {
      std::strcpy(*tmp_name, dir_ls[dir_ls_pos].name.c_str());
      context.rax = dir_ls[dir_ls_pos].type;
      dir_ls_pos++;
    } else {
      std::strcpy(*tmp_name, "");
      context.rax = Files::et_null;
    }
  }

  void Process::save_stat() {
    kstring stat = to_kstring(self_pid);
    auto file = Files::root->find_file("/proc/" + stat + "/stat");
    stat += " ";
    switch (state) {
      case ps_free:
        stat += "X";
        break;
      case ps_zombie:
        stat += "Z";
        break;
      case ps_runnable:
        stat += "R";
        break;
      default:
        stat += "W";
        break;
    }
    stat += " ";
    stat += to_kstring(parent);
    stat += " ";
    stat += cmd;
    stat += "\n";
    file->open();
    file->write_at((const uint8_t*)stat.c_str(), stat.size(), 0);
    file->resize(stat.size());
    file->close();
  }
}
