#pragma once

#include <array>
#include <stdint.h>
#include "interrupt.hpp"
#include <kmemory_arena>
#include <kstring>
#include <kvector>
#include "virtual_dir.hpp"

namespace ProcessPool {
  using Interrupt::Context;

  extern Files::VirtualDir* pipe_dir;
  extern Files::VirtualDir* proc_dir;

  typedef int64_t pid_t;
  constexpr pid_t max_pid = 0x100;
  constexpr pid_t no_pid = -1;

  typedef int64_t winid_t;
  constexpr winid_t no_win = -1;

  typedef int64_t fd_t;
  constexpr fd_t no_fd = -1;

  struct Process;

  extern kmemory_arena<Process> processes;

  void init();

  pid_t cur_pid();
  Process& cur_process();
  pid_t new_process(Process, pid_t ppid = no_pid);
  pid_t launch(kvector<kstring> command);
  pid_t exec(kstring path);
  pid_t execv(kstring path, kvector<kstring> argv);
  pid_t execve(kstring path, kvector<kstring> argv, kvector<kstring> envp);
  void free_process(pid_t);
  void put_to_sleep(pid_t pid, uint64_t ms);
  void unschedule(pid_t);
  void set_runnable(pid_t);
  void kill(pid_t);
  void save_context(Context*);
  void set_rax(pid_t, uint64_t);
}
