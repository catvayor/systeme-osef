#include "window_manager.hpp"

#include <interact>
#include <terminal>
#include <kchains>
#include <kmemory_arena>
#include "keyboard.hpp"
#include "keyboard_shortcuts.hpp"
#include "process.hpp"
#include "time.hpp"
#include "userref.hpp"

namespace ProcessPool {
  namespace WindowManager {
    bool debug_size = false;
    uint32_t screen_x = 0;
    uint32_t screen_y = 0;
    uint32_t screen_w = 80;
    uint32_t screen_h = 25;
    uint32_t bar_h = 1;
    constexpr unsigned nb_workspaces = 9;
    constexpr uint8_t color_default = Terminal::bg_black;
    constexpr uint8_t color_focus = Terminal::bg_green;
    constexpr uint8_t color_bright = Terminal::fg_white;
    constexpr uint8_t color_dim = Terminal::fg_dgray;
    constexpr uint8_t color_hour = Terminal::fg_lgray;
    constexpr uint8_t color_hour_focus = Terminal::fg_black;

    struct Window {
      pid_t owner;
      bool used, refresh;
      uint32_t x, y, w, h;
      LazyMemory::UserConstPtr<Syscall::WindowData> buffer;

      void reset() {
        used = false;
        refresh = false;
        x = 0;
        y = 0;
        w = 0;
        h = 0;
        buffer = LazyMemory::UserConstPtr<Syscall::WindowData>();
      }

      Window() {
        reset();
      }
    };

    kmemory_arena<Window> windows;
    uint64_t cur_workspace = 0;
    kchains<uint64_t> workspaces(nb_workspaces);
    std::array<kchains<uint64_t>::iterator, nb_workspaces> cur_window;
    std::array<bool, nb_workspaces> is_fullscreen;
    std::array<bool, nb_workspaces> is_tabs;
    bool refresh_full = true;
    bool refresh_bar = false;

    bool init_done = false;
    void init() {
      if (init_done) return;
      // Require
      Interact::init();
      Keyboard::init();
      LazyMemory::init();
      Time::init();
      // Init
      Interact::print_init("WindowManager");

      Shortcut::init();
      cur_window.fill(workspaces.iter(0).end());
      is_fullscreen.fill(false);
      is_tabs.fill(false);

      // Done
      init_done = true;
      Interact::print_done();
    }

    void unfocus(winid_t);

    bool resize_win(Window& win, uint32_t x, uint32_t y, uint32_t w, uint32_t h) {
      bool changed = win.x != x || win.y != y || win.w != w || win.h != h;

      win.x = x;
      win.y = y;
      win.w = w;
      win.h = h;
      win.refresh = win.refresh || changed;

      return changed;
    }

    void resize_all(uint64_t workspace) {
      if (is_fullscreen[cur_workspace]) {
        if (cur_window[cur_workspace].is_elem()) {
          auto& w = windows[*cur_window[cur_workspace]];
          resize_win(w, screen_x, screen_y, screen_w, screen_h);
        }
      } else {
        if (is_tabs[cur_workspace]) {
          for (auto wid : workspaces.iter(workspace)) {
            auto& w = windows[wid];
            if (resize_win(w, screen_x, screen_y, screen_w, screen_h - bar_h)) refresh_bar = true;
          }
        } else {
          uint32_t cur_x = screen_x;
          unsigned rem_windows = workspaces.get_size(workspace);

          for (auto wid : workspaces.iter(workspace)) {
            auto& w = windows[wid];

            uint32_t rem_width = screen_x + screen_w - cur_x;
            uint32_t cur_w = rem_width / rem_windows;

            if (resize_win(w, cur_x, screen_y, cur_w, screen_h - bar_h)) refresh_bar = true;

            rem_windows--;
            cur_x += cur_w;
          }
        }
      }
    }

    void update_screen_size() {
      if (debug_size) {
        screen_x = 10;
        screen_y = 0;
        screen_w = 60;
        screen_h = 19;
      } else {
        screen_x = 0;
        screen_y = 0;
        screen_w = 80;
        screen_h = 25;
      }

      for (unsigned k = 0; k < nb_workspaces; k++)
        resize_all(k);

      refresh_full = true;
    }

    winid_t new_window() {
      winid_t ret = windows.push(Window());
      workspaces.fit_elem(ret);
      workspaces.push_back(cur_workspace, ret);
      windows[ret].used = true;
      windows[ret].owner = cur_pid();
      cur_window[cur_workspace] = workspaces.iter_elem(ret);
      resize_all(cur_workspace);
      return ret;
    }

    void close_window(winid_t wid) {
      if (wid == no_win) return;
      unfocus(wid);
      windows[wid].reset();
      windows.pop(wid);
      workspaces.pop_elem(wid);
      resize_all(cur_workspace);
    }

    bool draw_window(winid_t wid, bool force = false) {
      auto& w = windows[wid];
      if (w.used && (force || w.refresh)) {
        struct {
          uint32_t w = 0;
          uint32_t h = 0;
          uint16_t const* data = nullptr;
        } buf;

        if (w.buffer) {
          std::optional<Syscall::WindowData const*> opt = w.buffer.get_ro();
          if (opt) {
            uint32_t buf_w = (*opt)->w;
            uint32_t buf_h = (*opt)->h;
            auto opt2 = LazyMemory::UserConstPtr<uint16_t>(w.buffer.pid, (*opt)->data).get_ro(buf_w * buf_h);
            if (opt2) {
              buf.w = buf_w;
              buf.h = buf_h;
              buf.data = *opt2;
            } else {
              Interact::warning("Bad user window data 2");
              kill(w.buffer.pid);
              return false;
            }
          } else {
            Interact::warning("Bad user window data 1");
            kill(w.buffer.pid);
            return false;
          }
        }

        for (unsigned x = 0; x < w.w; x++) {
          for (unsigned y = 0; y < w.h; y++) {
            uint8_t grid_color = ((x ^ y) & 1) ? Terminal::bg_magenta : Terminal::bg_black;
            uint16_t code = (x < buf.w && y < buf.h) ? buf.data[x + y * buf.w] : (((uint16_t)grid_color) << 8);
            Terminal::char_at(w.x + x, w.y + y) = code;
          }
        }
      }

      return true;
    }

    void draw_background() {
      if (is_fullscreen[cur_workspace])
        Interact::display_banner(screen_x, screen_y, screen_w, screen_h);
      else
        Interact::display_banner(screen_x, screen_y, screen_w, screen_h - bar_h);
    }

    void draw_bar() {
      uint32_t cur_x = screen_x;
      uint32_t cur_y = screen_y + screen_h - bar_h;

      unsigned focus_begin = 0;
      unsigned focus_end = 0;
      {
        uint32_t cur_x = screen_x;
        unsigned rem_windows = workspaces.get_size(cur_workspace);

        for (auto wid : workspaces.iter(cur_workspace)) {
          uint32_t rem_width = screen_x + screen_w - cur_x;
          uint32_t cur_w = rem_width / rem_windows;

          if (wid == *cur_window[cur_workspace]) {
            focus_begin = cur_x;
            focus_end = cur_x + cur_w;
          }

          rem_windows--;
          cur_x += cur_w;
        }
      }

      for (unsigned k = 0; k < nb_workspaces; k++) {
        if (k != cur_workspace && workspaces.empty(k)) continue;
        uint8_t bg = (cur_x >= focus_begin && cur_x < focus_end) ? color_focus : color_default;
        uint8_t fg = (k == cur_workspace) ? color_bright : color_dim;
        uint8_t color = fg | bg;
        uint16_t code = (((uint16_t)color) << 8) + ('1' + k);
        Terminal::char_at(cur_x++, cur_y) = code;
      }

      kstring date_time = Time::get_time();

      while (cur_x < screen_x + screen_w - date_time.size()) {
        uint8_t color = (cur_x >= focus_begin && cur_x < focus_end) ? color_focus : color_default;
        uint16_t code = ((uint16_t)color) << 8;
        Terminal::char_at(cur_x++, cur_y) = code;
      }

      for (char c : date_time) {
        uint8_t bg = (cur_x >= focus_begin && cur_x < focus_end) ? color_focus : color_default;
        uint8_t fg = (cur_x >= focus_begin && cur_x < focus_end) ? color_hour_focus : color_hour;;
        uint8_t color = fg | bg;
        uint16_t code = (((uint16_t)color) << 8) + c;
        Terminal::char_at(cur_x++, cur_y) = code;
      }
    }

    void draw_workspace() {
      if (Time::time_updated()) refresh_bar = true;
      if (refresh_full) refresh_bar = true;

      bool ok = false;
      while (!ok) {
        ok = true;
        if (is_fullscreen[cur_workspace] || is_tabs[cur_workspace]) {
          if (cur_window[cur_workspace].is_elem())
            ok = draw_window(*cur_window[cur_workspace], refresh_full);
        } else {
          for (auto wid : workspaces.iter(cur_workspace)) {
            if (!draw_window(wid, refresh_full)) {
              ok = false;
              break;
            }
          }
        }
      }

      if (workspaces.empty(cur_workspace)) draw_background();

      if (!is_fullscreen[cur_workspace] && refresh_bar) draw_bar();

      refresh_full = false;
      refresh_bar = false;
    }

    void unfocus(winid_t win) {
      for (uint64_t ws = 0; ws < nb_workspaces; ws++) {
        if (*cur_window[ws] != (uint64_t)win) continue;
        auto tmp = cur_window[ws];
        if ((--cur_window[ws]).is_entry()) cur_window[ws] = ++tmp;
        refresh_bar = true;
      }
    }

    void focus_prev() {
      if (cur_window[cur_workspace].is_entry()) return;
      if ((--cur_window[cur_workspace]).is_entry()) --cur_window[cur_workspace];
      resize_all(cur_workspace);
      refresh_bar = true;
    }

    void focus_next() {
      if (cur_window[cur_workspace].is_entry()) return;
      if ((++cur_window[cur_workspace]).is_entry()) ++cur_window[cur_workspace];
      resize_all(cur_workspace);
      refresh_bar = true;
    }

    void move_prev() {
      if (cur_window[cur_workspace].is_entry()) return;
      auto prev_window = cur_window[cur_workspace];
      if ((--prev_window).is_elem()) workspaces.push_before(*prev_window, *cur_window[cur_workspace]);
      else workspaces.push_back(cur_workspace, *cur_window[cur_workspace]);
      resize_all(cur_workspace);
    }

    void move_next() {
      if (cur_window[cur_workspace].is_entry()) return;
      auto next_window = cur_window[cur_workspace];
      if ((++next_window).is_elem()) workspaces.push_after(*next_window, *cur_window[cur_workspace]);
      else workspaces.push_front(cur_workspace, *cur_window[cur_workspace]);
      resize_all(cur_workspace);
    }

    void focus_ws(unsigned k) {
      if (k == cur_workspace) return;
      cur_workspace = k;
      resize_all(cur_workspace);
      refresh_full = true;
    }

    void move_ws(unsigned k) {
      if (cur_window[cur_workspace].is_entry()) return;
      if (k == cur_workspace) return;
      winid_t tmp = *cur_window[cur_workspace];
      unfocus(tmp);
      workspaces.push_back(k, tmp);
      cur_window[k] = workspaces.iter_elem(tmp);
      resize_all(cur_workspace);
    }

    void toggle_tabs() {
      is_tabs[cur_workspace] = !is_tabs[cur_workspace];
      resize_all(cur_workspace);
    }

    void toggle_fullscreen() {
      is_fullscreen[cur_workspace] = !is_fullscreen[cur_workspace];
      resize_all(cur_workspace);
    }

    void toggle_debug_size() {
      debug_size = !debug_size;
      update_screen_size();
    }

    void handle_keyboard() {
      while (Keyboard::haschar()) {
        int16_t c = Keyboard::getchar();
        bool forward = !Shortcut::check_shortcut(c);

        if (forward) {
          // Forward input
          if (cur_window[cur_workspace].is_entry()) return;
          processes[windows[(winid_t)*cur_window[cur_workspace]].owner].put_key(c);
        }
      }
    }

    uint64_t get_window(winid_t wid) {
      if (wid == no_win || !windows[wid].used) return 0;
      return (((uint64_t)windows[wid].h) << 32) + windows[wid].w;
    }

    bool refresh_window(winid_t wid, LazyMemory::UserConstPtr<Syscall::WindowData> ptr) {
      if (wid == no_win || !windows[wid].used) return false;
      windows[wid].refresh = true;
      windows[wid].buffer = ptr;
      return true;
    }
  }
}
