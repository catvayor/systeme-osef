#pragma once

#include <stdint.h>
#include "process_pool.hpp"
#include "userref.hpp"

namespace ProcessPool {
  namespace Kinship {
    static inline void init() {}
    void setup_kinship(pid_t pid);
    void exit(int64_t, pid_t);
    void fork();
    void waitpid(pid_t, LazyMemory::UserPtr<int64_t>, int64_t);
  }
}
