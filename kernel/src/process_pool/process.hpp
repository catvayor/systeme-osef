#pragma once

#include <stdint.h>
#include <syscall>
#include "virtual_dir.hpp"
#include "virtual_file.hpp"
#include "path.hpp"
#include "interrupt.hpp"
#include <kchained_memory_arena>
#include <kstack>
#include <kstring>
#include <kqueue>
#include <kvector>
#include "lazy_memory.hpp"
#include "process_pool.hpp"
#include "siginfo.hpp"

namespace ProcessPool {
  struct Process {
    static constexpr uintptr_t elf_begin = 1ull << 4;
    static constexpr uintptr_t elf_end = 1ull << 30;
    static constexpr uintptr_t data_begin = 1ull << 37;
    static constexpr uintptr_t data_end = 1ull << 38;
    static constexpr uintptr_t heap_begin = data_begin;
    static constexpr uintptr_t stack_end = data_end;
    static constexpr uintptr_t max_heap_end = heap_begin + (1ull << 35);
    static constexpr uintptr_t min_stack_full = stack_end - (1ull << 35);
    static constexpr uintptr_t default_stack_full = min_stack_full;

    static constexpr unsigned STDIN_FILENO = 0;
    static constexpr unsigned STDOUT_FILENO = 1;
    static constexpr unsigned STDERR_FILENO = 2;

    enum ProcessState : uint8_t {
      ps_free,
      ps_zombie,
      ps_runnable,
      ps_waiting_child,
      ps_waiting_read,
      ps_waiting_write,
      ps_waiting_pause,
    } state;

    enum ReturnCode : int64_t {
      terminated = -1,
      success = 0,
    };

    void* main_page_phys;
    Context context;

    // Debug
    bool running;

    // Heap/stack
    uintptr_t heap_end;
    uintptr_t stack_full;

    // IO
    struct FdRW {
      fd_t fd;
      bool read;
      bool write;
      int64_t cursor;

      FdRW();
      FdRW(fd_t f, bool r, bool w);
    };
    kchained_memory_arena<FdRW> fds;
    // read
    uint64_t read_fdid;
    uint64_t read_count;
    LazyMemory::UserPtr<uint8_t> read_buf;
    // write
    uint64_t write_fdid;
    uint64_t write_count;
    kqueue<uint8_t> write_buf;
    // dir
    Files::Path cur_dir;
    unsigned dir_ls_pos;
    kvector<Files::DirEntry> dir_ls;
    // keyboard
    kqueue<int16_t> kbd_buf;
    // command
    kstring cmd;

    // Window
    winid_t window;

    // Kinship
    pid_t self_pid;
    pid_t parent;
    LazyMemory::UserPtr<int64_t> waiting_status;
    pid_t waitpid_pid;
    LazyMemory::UserPtr<int64_t> waitpid_status;
    int64_t waitpid_options;

    // Signals
    Signal::SigInfo sig_info;

    static void init();

    Process();
    Process(void const* file, kstring path);
    Process clone();

    void set_args(kvector<kstring>);
    void set_env(kvector<kstring>);

    bool owns(uintptr_t ptr, std::size_t size = sizeof(uint64_t));
    void activate_main_page();

    void run();
    void pause();
    void waitpid(pid_t, LazyMemory::UserPtr<int64_t>, int64_t);
    void save_context(Context* ctx);

    void wake();
    void microsleep(uint64_t ms);

    void exit(int64_t code);
    void sbrk(int64_t delta);

    [[nodiscard]] bool exec_base(kstring);
    void execve(LazyMemory::UserConstPtr<char>, LazyMemory::UserConstPtr<char const*>, LazyMemory::UserConstPtr<char const*>);

    void open_file_at(uint64_t fd, Files::VirtualFile* file, bool read, bool write);
    uint64_t open_file(Files::VirtualFile* file, bool read, bool write);
    uint64_t dup_fd_rw(uint64_t src, bool read, bool write);
    void dup_fd_same_at(uint64_t dst, uint64_t src);
    void close_fd(uint64_t fd);
    void clone_fd(FdRW fdrw);

    void write(uint64_t fdid, LazyMemory::UserConstPtr<uint8_t> buf, uint64_t count);
    void resume_write();
    void read(uint64_t fdid, LazyMemory::UserPtr<uint8_t> buf, uint64_t count);
    void pollin(LazyMemory::UserPtr<uint64_t> fdids, uint64_t count);
    void resume_read();
    void open(LazyMemory::UserConstPtr<char> path, int64_t flags);
    void close(uint64_t fd);
    void dup2(uint64_t oldfd, uint64_t newfd);
    void pipe(LazyMemory::UserPtr<uint64_t>, bool flushpipe = false);
    Files::Path abspath(kstring);
    void chdir(LazyMemory::UserConstPtr<char> path);
    void opendir(LazyMemory::UserConstPtr<char> path);
    void readdir(LazyMemory::UserPtr<char> name);

    void get_window();
    void refresh_window(LazyMemory::UserConstPtr<Syscall::WindowData>);
    void put_key(int16_t);
    void get_key();

    void save_stat();

    void set_free();
    void set_zombie();
    void set_runnable();
    inline bool is_free() {
      return state == ps_free;
    }
    inline bool is_runnable() {
      return state == ps_runnable;
    }
    inline bool is_zombie() {
      return state == ps_zombie;
    }
    inline bool is_waiting_child(pid_t wpid = no_pid) {
      if (state != ps_waiting_child) return false;
      if (wpid == -1 || waitpid_pid == -1) return true;
      return waitpid_pid == wpid;
    }
    inline bool is_waiting_read() {
      return state == ps_waiting_read;
    }
    inline bool is_waiting() {
      switch (state) {
        case ps_waiting_child:
        case ps_waiting_read:
        case ps_waiting_write:
        case ps_waiting_pause:
          return true;
        default:
          return false;
      }
    }
    inline bool is_dead() {
      return is_free() || is_zombie();
    }
  };
}
