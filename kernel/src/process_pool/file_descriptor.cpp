#include "file_descriptor.hpp"

#include <interact>
#include <kmemory_arena>
#include <kchains>
#include "process.hpp"

namespace ProcessPool {
  namespace FileDescriptor {
    kmemory_arena<FileDescriptor> fds;
    kchains<uint64_t> waiting_processes;

    uint64_t write_entry(fd_t fd) {
      return fd * 2;
    }
    uint64_t read_entry(fd_t fd) {
      return fd * 2 + 1;
    }

    void add_waiting_process_write(fd_t fd, pid_t pid) {
      waiting_processes.fit_entry(write_entry(fd));
      waiting_processes.fit_elem(pid);
      waiting_processes.push_back(write_entry(fd), pid);
    }
    void add_waiting_process_read(fd_t fd, pid_t pid) {
      waiting_processes.fit_entry(read_entry(fd));
      waiting_processes.fit_elem(pid);
      waiting_processes.push_back(read_entry(fd), pid);
    }
    void remove_waiting_process(pid_t pid) {
      waiting_processes.pop_elem(pid);
    }

    void wake_write(fd_t fd) {
      waiting_processes.fit_entry(write_entry(fd));
      while (writable(fd) && !waiting_processes.empty(write_entry(fd))) {
        pid_t pid = *waiting_processes.pop_front(write_entry(fd));
        processes[pid].resume_write();
      }
    }

    void wake_read(fd_t fd) {
      waiting_processes.fit_entry(read_entry(fd));
      while (readable(fd) && !waiting_processes.empty(read_entry(fd))) {
        pid_t pid = *waiting_processes.pop_front(read_entry(fd));
        processes[pid].resume_read();
      }
    }

    void FileDescriptor::reset() {
      file = nullptr;
      nb_read_refs = 0;
      nb_write_refs = 0;
      cursor = 0;
    }

    bool seekable(fd_t fd) {
      if (!fds.used(fd)) Interact::error("Seekable on closed fd");
      if (fds[fd].file == nullptr) Interact::error("nullptr");
      return fds[fd].file->seekable();
    }

    bool writable(fd_t fd) {
      if (!fds.used(fd)) Interact::error("Writable on closed fd");
      if (fds[fd].file == nullptr) Interact::error("nullptr");
      return fds[fd].file->writable();
    }

    bool readable(fd_t fd) {
      if (!fds.used(fd)) Interact::error("Readable on closed fd");
      if (fds[fd].file == nullptr) Interact::error("nullptr");
      return fds[fd].file->readable();
    }

    void write(fd_t fd, kqueue<uint8_t>& buf) {
      if (!fds.used(fd)) Interact::error("Writing to closed fd");
      if (fds[fd].file == nullptr) Interact::error("nullptr");

      if (fds[fd].file->seekable()) {
        //use write_at
        kvector<uint8_t> buffer;
        std::optional<uint8_t> tmp;
        while ((tmp = buf.pop()))
          buffer.push_back(*tmp);
        fds[fd].file->write_at(buffer.data(), buffer.size(), fds[fd].cursor);
        fds[fd].cursor += buffer.size();
      } else
        fds[fd].file->write(buf);

      wake_read(fd);
    }

    kvector<uint8_t> read(fd_t fd, uint64_t max) {
      if (!fds.used(fd)) Interact::error("Reading from closed fd");
      if (fds[fd].file == nullptr) Interact::error("nullptr");
      kvector<uint8_t> ret;

      if (fds[fd].file->seekable()) {
        //use read_at
        ret.resize(std::min<int64_t>(max, fds[fd].file->size() - fds[fd].cursor));
        fds[fd].file->read_at(ret.data(), ret.size(), fds[fd].cursor);
        fds[fd].cursor += ret.size();
      } else
        ret = fds[fd].file->read(max);

      wake_write(fd);
      return ret;
    }

    int64_t lseek(fd_t fd, int64_t ofs, Files::VirtualFile::SeekMode whence) {
      if (!fds.used(fd)) Interact::error("Seeking from closed fd");
      if (fds[fd].file == nullptr) Interact::error("nullptr");
      int64_t& cursor_pos = fds[fd].cursor;
      switch (whence) {
        case Files::VirtualFile::SEEK_SET:
          if (ofs >= 0 && ofs <= fds[fd].file->size())
            return cursor_pos = ofs;
          break;
        case Files::VirtualFile::SEEK_CUR:
          if ((cursor_pos + ofs) >= 0 && (cursor_pos + ofs) <= fds[fd].file->size())
            return cursor_pos += ofs;
          break;
        case Files::VirtualFile::SEEK_END:
          if ((fds[fd].file->size() + ofs) >= 0 && ofs <= 0)
            return cursor_pos = fds[fd].file->size() + ofs;
          break;
      }
      return -1;
    }

    fd_t new_fd() {
      return fds.push(FileDescriptor());
    }

    void set_fd(fd_t fd, Files::VirtualFile *file) {
      fds[fd].file = file;
    }

    fd_t open_fd(Files::VirtualFile *file, bool read, bool write) {
      fd_t fd = fds.push(FileDescriptor());
      fds[fd].file = file;
      connect(fd, read, write);
      return fd;
    }

    void connect(fd_t fd, bool read, bool write) {
      if (read || write) {
        if (fds[fd].nb_read_refs + fds[fd].nb_write_refs == 0) {
          fds[fd].file->open();
        }
      }
      if (read) {
        if (fds[fd].nb_read_refs++ == 0) {
          fds[fd].file->open_read();
          wake_write(fd);
        }
      }
      if (write) {
        if (fds[fd].nb_write_refs++ == 0) {
          fds[fd].file->open_write();
          wake_read(fd);
        }
      }
    }

    void disconnect(fd_t fd, bool read, bool write) {
      if (!fds.used(fd)) Interact::error("Closing a closed fd");
      if (read) {
        if (fds[fd].nb_read_refs == 0)
          Interact::error("Closing read on read-closed fd");
        if (--fds[fd].nb_read_refs == 0) {
          fds[fd].file->close_read();
          wake_write(fd);
        }
      }
      if (write) {
        if (fds[fd].nb_write_refs == 0)
          Interact::error("Closing write on write-closed fd");
        if (--fds[fd].nb_write_refs == 0) {
          fds[fd].file->close_write();
          wake_read(fd);
        }
      }

      if (fds[fd].nb_read_refs + fds[fd].nb_write_refs == 0) {
        fds[fd].file->close();
        fds[fd].reset();
        fds.pop(fd);
      }
    }
  }
}
