#pragma once

#include <array>
#include <syscall>
#include "process_pool.hpp"
#include "userref.hpp"

namespace ProcessPool {
  namespace Signal {
    struct SigContext {
      uint64_t sig;
      uint64_t mask;
      Context ctx;

      SigContext();
      SigContext(uint64_t, uint64_t, Context);
    };

    int64_t kill(pid_t pid, uint64_t sig);
    void check(pid_t pid);
    int64_t sigaction(uint64_t sig, LazyMemory::UserConstPtr<Syscall::sigact> new_action, LazyMemory::UserPtr<Syscall::sigact> old_action);
    void sigreturn();
    int64_t sigprocmask(Syscall::sig_mask_action how, LazyMemory::UserConstPtr<uint64_t> set, LazyMemory::UserPtr<uint64_t> oldset);
  }
}
