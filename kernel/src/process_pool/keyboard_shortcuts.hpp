#pragma once

#include <cstdint>

namespace ProcessPool {
  namespace WindowManager {
    namespace Shortcut {
      enum ActionType {
        NONE,
        TOGGLE_DEBUG_SIZE,
        TOGGLE_LAYOUT,
        MOVE_PREV,
        MOVE_NEXT,
        MOVE_WS,
        LAUNCH_HYALMA,
        TOGGLE_TABS,
        TOGGLE_FULLSCREEN,
        FOCUS_PREV,
        FOCUS_NEXT,
        FOCUS_WS,
      };

      struct Action {
        ActionType type;
        uint64_t arg;

        Action(ActionType t = NONE, uint64_t a = 0): type(t), arg(a) {}
      };

      void exec_action(Action action);
      bool check_shortcut(int16_t code); // Returns true iff a shortcut is captured
      void init();
    }
  }
}
