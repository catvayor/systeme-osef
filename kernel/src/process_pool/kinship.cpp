#include "kinship.hpp"

#include <kchains>
#include "lazy_memory.hpp"
#include "process.hpp"

namespace ProcessPool {
  namespace Kinship {
    kchains<uint64_t> kinships;
    uint64_t alive_entry(pid_t pid) {
      return pid * 2;
    }
    uint64_t zombie_entry(pid_t pid) {
      return pid * 2 + 1;
    }

    void setup_orphans(pid_t pid) {
      for (auto cpid : kinships.iter(zombie_entry(pid)))
        free_process(cpid);

      for (auto cpid : kinships.iter(alive_entry(pid)))
        processes[cpid].parent = no_pid;

      kinships.clear_entry(alive_entry(pid));
      kinships.clear_entry(zombie_entry(pid));
    }

    void setup_kinship(pid_t pid) {
      processes[pid].self_pid = pid;
      pid_t ppid = processes[pid].parent;
      kinships.fit_elem(pid);
      kinships.fit_entry(zombie_entry(pid));
      if (ppid != no_pid) {
        kinships.push_back(alive_entry(ppid), pid);
      }
    }

    void pop_alive(pid_t pid) {
      kinships.pop_elem(pid);
    }

    void push_zombie(pid_t pid) {
      pid_t ppid = processes[pid].parent;
      kinships.push_back(zombie_entry(ppid), pid);
    }

    void pop_zombie(pid_t pid) {
      kinships.pop_elem(pid);
      free_process(pid);
    }

    void exit(int64_t code, pid_t pid) {
      Process &p = processes[pid];
      pid_t ppid = p.parent;
      setup_orphans(pid);
      p.exit(code);
      unschedule(pid);
      if (ppid != no_pid) {
        Process &pp = processes[ppid];
        if (pp.is_waiting_child(pid)) {
          pop_alive(pid);

          pp.context.rax = pid;
          if (!pp.waitpid_status.is_nullptr()) {
            auto opt = pp.waitpid_status.get_rw();
            if (opt) **opt = p.context.rax;
            else {
              pp.context.rax = Syscall::err_fault;
              push_zombie(pid);
            }
          }
          set_runnable(ppid);
          free_process(pid);
        } else {
          push_zombie(pid);
        }
      } else {
        free_process(pid);
      }
    }

    void fork() {
      pid_t new_pid = new_process(cur_process().clone(), cur_pid());
      processes[new_pid].context.rax = no_pid;
      processes[cur_pid()].context.rax = new_pid;
    }

    void waitpid(pid_t wpid, LazyMemory::UserPtr<int64_t> status, int64_t options) {
      pid_t ppid = cur_pid();
      Process &p = cur_process();
      if (wpid == no_pid) {
        if (!kinships.empty(zombie_entry(ppid))) {
          pid_t ret = kinships.front(zombie_entry(ppid));
          auto opt = status.get_rw();
          if (!status.is_nullptr()) {
            if (opt) **opt = processes[ret].context.rax;
            else {
              p.context.rax = Syscall::err_fault;
              return;
            }
          }
          p.context.rax = ret;
          pop_zombie(ret);
        } else if (kinships.empty(alive_entry(ppid))) {
          p.context.rax = Syscall::err_child;
        } else if (options & Syscall::WNOHANG) {
          p.context.rax = Syscall::err_nohang;
        } else {
          p.waitpid(wpid, status, options);
        }
      } else {
        if (wpid < 0 || processes[wpid].is_free() || processes[wpid].parent != ppid) {
          p.context.rax = Syscall::err_child;
        } else if (processes[wpid].is_zombie()) {
          if (!status.is_nullptr()) {
            auto opt = status.get_rw();
            if (opt) **opt = processes[wpid].context.rax;
            else {
              p.context.rax = Syscall::err_fault;
              return;
            }
          }
          p.context.rax = wpid;
          pop_zombie(wpid);
        } else if (options & Syscall::WNOHANG) {
          p.context.rax = Syscall::err_nohang;
        } else {
          p.waitpid(wpid, status, options);
        }
      }
    }
  }
}
