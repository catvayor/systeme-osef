#pragma once

#include <syscall>
#include "userref.hpp"
#include "process_pool.hpp"

namespace ProcessPool {
  namespace WindowManager {
    void init();

    winid_t new_window();
    void close_window(winid_t);
    void handle_keyboard();
    void draw_workspace();
    uint64_t get_window(winid_t);
    bool refresh_window(winid_t, LazyMemory::UserConstPtr<Syscall::WindowData>); // true on success

    void move_prev();
    void move_next();
    void focus_prev();
    void focus_next();
    void move_ws(unsigned k);
    void focus_ws(unsigned k);
    void toggle_tabs();
    void toggle_fullscreen();
    void toggle_debug_size();
  }
}
