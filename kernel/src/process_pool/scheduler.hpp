#pragma once

#include <array>
#include <stdint.h>
#include "interrupt.hpp"
#include "process.hpp"

namespace ProcessPool {
  namespace Scheduler {
    constexpr unsigned timer_base_freq = 0x1234de; // 1193182 Hz
    constexpr unsigned timer_div = 0x100; // range : [0x10000;0x2]
    constexpr unsigned timer_freq = timer_base_freq / timer_div; // Hz

    void init();

    [[noreturn]] void iret_to_scheduler();
    void scheduling_loop();
    void put_to_sleep(pid_t pid, uint64_t ms);
    void unschedule(pid_t);
    void schedule(pid_t);
    pid_t cur_pid();
  }
}
