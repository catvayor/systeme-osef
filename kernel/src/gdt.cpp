#include "gdt.hpp"

#include <interact>
#include <virtual_memory_base>
#include <x86>

namespace Gdt {
  TSS tss;

  uint64_t TSS::lo_desc() {
    return
      (sizeof(TSS) - 1) // limit, -1 because inclusive
      | ((0x00ffffff & ((uintptr_t)(this))) << 16)
      | (0b1001ull << 40) // type (0b1001 = available 64-bit tss)
      | ((0xff000000 & ((uintptr_t)(this))) << 32)
      | virtual_memory::gdt_present;
  }

  uint64_t TSS::hi_desc() {
    return ((uintptr_t)(this)) >> 32;
  }

  std::array<uint64_t, 6> gdt;

  bool init_done = false;
  void init() {
    if (init_done) return;
    // Require
    Interact::init();
    // Init
    Interact::print_init("Gdt");
    Interact::print_failed();
    x86::ud2_loop();
  }

  void init(void* rsp0) {
    if (init_done) return;
    // Require
    Interact::init();
    // Init
    Interact::print_init("Gdt");

    tss.rsp0 = rsp0;

    gdt[nul_idx] = 0;
    //kernel code
    gdt[kcs_idx] = virtual_memory::gdt_L | virtual_memory::gdt_present | virtual_memory::gdt_S |
                   virtual_memory::gdt_executable | virtual_memory::gdt_RW;
    //user code
    gdt[ucs_idx] = virtual_memory::gdt_L | virtual_memory::gdt_present | virtual_memory::gdt_S |
                   virtual_memory::gdt_executable | virtual_memory::gdt_RW | (3 * virtual_memory::gdt_privilege);
    //user data
    gdt[uds_idx] = virtual_memory::gdt_L | virtual_memory::gdt_present | virtual_memory::gdt_S |
                   virtual_memory::gdt_RW | (3 * virtual_memory::gdt_privilege);

    // low tss
    gdt[tss_idx] = tss.lo_desc();
    // high tss
    gdt[tss_idx + 1] = tss.hi_desc();

    {
      struct {
        uint16_t len = gdt.size() * 8 - 1;
        uintptr_t base = (uintptr_t)gdt.data();
      } __attribute__((packed)) gdtr;

      asm volatile goto("lgdtq (%0)\n\t"
                        "pushq $0x8\n\t"
                        "movabsq $%l1, %%rax\n\t"
                        "pushq %%rax\n\t"
                        "retfq"
                        :
                        : "g"(&gdtr)
                        : "memory", "rax"
                        : gdtloaded);
gdtloaded:
      asm volatile ("xorw %%ax, %%ax\n\t"
                    "movw %%ax, %%ds\n\t"
                    "movw %%ax, %%es\n\t"
                    "movw %%ax, %%fs\n\t"
                    "movw %%ax, %%gs\n\t"
                    "movw %%ax, %%ss" ::: "ax", "memory");

    }

    // Load tss
    asm volatile("ltr %0" :: "rm"(tss_sel));

    // Done
    init_done = true;
    Interact::print_done();
  }
}
