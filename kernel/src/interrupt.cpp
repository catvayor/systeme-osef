#include "interrupt.hpp"

#include <interact>
#include <io_access>
#include <stdint.h>
#include <terminal>
#include <virtual_memory_base>
#include <x86>
#include "gdt.hpp"
#include "pic.hpp"
#include "scheduler.hpp"

namespace Interrupt {
  constexpr uintptr_t ist1_start = 0xFFFF808000000000;
  constexpr uint64_t ist1_size = 0x1000;
  constexpr uintptr_t ist1_top = ist1_start + ist1_size;

  bool Context::is_user() {
    return (cs & 3) == 3;
  }

  void Context::restore() {
    // set stack pointer to context pointer
    // pop out registers
    // iretq pops the 5 last registers
    asm volatile (
      "mov %0, %%rsp;\n"
      "pop %%rbp; pop %%rax; pop %%rbx; pop %%rcx; pop %%rdx; pop %%rsi; pop %%rdi; pop %%r8; pop %%r9;\n"
      "pop %%r10; pop %%r11; pop %%r12; pop %%r13; pop %%r14; pop %%r15; iretq;\n"
      :: "g"(this));
  }

  [[noreturn]] void fatal_error_name(Context *ctxp, const char* name) {
    if (!ctxp->is_user()) x86::cli();
    Terminal::newline();
    Terminal::print(name, Terminal::fg_dred);
    Terminal::newline();
    fatal_error(ctxp);
  }

  [[noreturn]] void fatal_error_name_errcode(Context *ctxp, const char* name, uint64_t errcode) {
    if (!ctxp->is_user()) x86::cli();
    Terminal::newline();
    Terminal::print(name, Terminal::fg_dred);
    Terminal::newline();
    Terminal::print("Errcode: ");
    Terminal::print_hex(errcode);
    Terminal::newline();
    fatal_error(ctxp);
  }

  [[noreturn]] void fatal_error(Context *ctxp) {
    if (ctxp->is_user()) {
      Terminal::print("Fatal exception: Terminating task.\n");
    } else {
      x86::cli();
      Terminal::print("Fatal exception: Freezing kernel.\n");
    }

    Terminal::print("ip=");
    Terminal::print_hex(ctxp->rip);
    Terminal::print(" cs=");
    Terminal::print_hex((uint16_t)(ctxp->cs));
    Terminal::print(" flags=");
    Terminal::print_hex(ctxp->rflags);
    Terminal::print(" sp=");
    Terminal::print_hex(ctxp->rsp);
    Terminal::print(" ss=");
    Terminal::print_hex((uint16_t)(ctxp->ss));

    if (ctxp->is_user()) {
      ProcessPool::save_context(ctxp);
      ProcessPool::kill(ProcessPool::cur_pid());
      ProcessPool::Scheduler::iret_to_scheduler();
    } else {
      x86::freeze();
    }
  }

  extern "C" void handler_default(Context *ctxp) {
    fatal_error_name(ctxp, "Unhandled interrupt!");
  }

  bool init_done = false;
  void init() {
    if (init_done) return;
    // Require
    Gdt::init();
    Interact::init();
    Pic::init();
    // Init
    Interact::print_init("Interrupt");

    virtual_memory::make_virtual_address((void*)ist1_start, ist1_size, false, true, true);
    Gdt::tss.ist1 = (void*)ist1_top;

    init_base();

    // Done
    init_done = true;
    Interact::print_done();
  }
}
