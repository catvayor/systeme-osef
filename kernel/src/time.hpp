#pragma once

#include <cstdint>
#include <kstring>

namespace Time {
  struct RealTime {
    uint8_t sec;
    uint8_t min;
    uint8_t hour;
    uint8_t day;
    uint8_t month;
    uint8_t year_end;

    operator kstring();
  };

  void init();

  RealTime get_time();
  bool time_updated();
}
