# Scheduler

## Round Robin

Le scheduler implémente du scheduling Round Robin, a l'aide d'une `chains` (cf `stdlib.md`)

La `chains` permet notament de retirer un processus de sa file d'exécution où qu'il soit.

La préemption des process est effectuée par l'interrupt du timer PIT.

## Sleep

Le scheduler dispose aussi d'une priority queue permettant de gerer le syscall `microsleep`.

Comme la priority queue ne permet pas, contrairement à chains, la suppression arbitraire, un bug connu est que si un processus meurt avant d'etre sorti de son someil, cela pourra impacter un nouveau processus qui aurait le meme pid.
