# Dictionnaire quenya-francais partiel

elfique | français
--------|-----------------------
ahya    | changer
alda    | arbre
amba    | plus, d'avantage
anon    | fils
appa    | toucher
atta    | deux
cesta   | chercher
elda    | Elfe (Haut Elfe)
er      | un
hyalma  | coquillage
men     | chemin, endroit
nallama | echo
nelde   | trois
netha   | tuer, tue (imperatif)
nire    | pousser
ol      | croître
onta    | génerer, créer
ovea    | similaire, pareil
sarat   | lettre, signe

# Commandes

Linux    | Hauts-Elfes
---------|-------------
echo     | nallama
ls       | ca
cat      | ovea
cd       | am
find     | cesta
kill     | netha
more     | amba
ps       | ce
shell    | hyalma
sokoban  | nire
sort     | ol
terminal | sarati
touch    | appa
tree     | alda


# Version complète

Un dictionnaire quenya-français français-quenya est disponible ici :

[https://www.ambar-eldaron.com/telechargements/quenya-fr-A4.pdf](https://www.ambar-eldaron.com/telechargements/quenya-fr-A4.pdf)
