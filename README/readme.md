# Hauts-elfes

Projet d'OS de Lubin Bailly, Sylvain Gay et Victor Miquel.

N'hésitez pas à poser toute question par mail sur nos adresses ENS.

## Librairie standard

Voir `stdlib.md`.

## Bootlib

Voir `bootlib.md`.

## Bootloader

Voir `bootloader.md` pour le fonctionnement du booloader.

## Kernel

### Processus

Voir `scheduler.md` pour le scheduler, voir `kinship.md` pour les liens entre process.

### Mémoire

Voir `lazy_memory.md` pour la gestion de mémoire paresseuse, voir `memory_map.md` pour la structure générale de la mémoire.

### Interrupt

Voir `interrupt.md` pour des informations sur notre gestion des interrupts (dont les exceptions), du PIC (interrupts hardware), du clavier.

### WindowManager

Voir `window_manager.md` pour le window manager, voir `ui.md` pour les shortcuts disponible.

### Signaux

Voir `signal.md`.

### Fichiers

Voir `files.md`.

## Apps

### La langue

Voir `quenya.md` pour la correspondance entre commande linux et commande Hauts-elfes.

### Syscalls

Voir `syscalls.md` pour les syscalls.

### Terminal

Voir `sarati.md` pour le terminal.

### Shell

Voir `hyalma.md`.
