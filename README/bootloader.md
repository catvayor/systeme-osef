# Bootloader

Le bootloader est séparé en 3 étages.

## 1er étage

Le premier étage est dans le premier secteur, avec les informations de la structure exFat. Il a donc une taille maximale de 390 octects (il fait 237 octets).

Cet étage va charger le 2nd étage dans les 8 secteurs suivants prévus par exFat à l'adresse 0x1000, activer la ligne A20 si elle ne l'est pas déjà et enfin passer dans le second étage.

## 2nd étage

Le second étage est dans les 8 secteur d'extented boot sector prévu par exFat, soit une taille maximale de 4ko (taille réélle difficile à estimer mais inférieure à 2ko), avec des signatures régulières. pour assurer que les signatures ne s'écrivent pas dans le code, le code sépare explicitement les secteurs par des `.align`.

Le rôle de cet étage est de charger le 3eme depuis le système de fichier.

Les secteurs ont les rôles suivants :

* secteur 1 : main
* secteurs 2 à 6 : inutilisés
* secteur 6 : fonctions de recherche de `/sys/boot` (une pour `/sys` dans la racine et une pour `boot` dans `/sys/`) et fonction de chargement dans la mémoire de `/sys/boot` une fois trouvé.
* secteur 7 : lecture du système de fichier, notament pour itérer la fonction citée précédemment sur les différents clusters.
* secteur 8 : fonctions pour émettre des message d'erreur. 

## 3eme étage

Le 3eme étage est dans le fichier `/sys/boot` (26.5 ko).

Dans l'ordre, il va :

* Détecter la mémoire disponible
* Passer en mode protégé
* Vérifier qu'on dispose du long mode
* Initialiser le paging (identity map)
* Passer en long mode
* Initialiser frame alloc, l'addresse vituelle du buffer vga
* Initialiser kmalloc (la structure sera donné au kernel)
* Initialiser le driver ata
* Charger le kernel, lui préparer une pile
* Stocker des informations diverse en `nullptr` pour être récupérer par le kernel (notament où est la heap et frame\_alloc)
* Lancer le kernel

