# Hyalma

Hyalma est le shell (coquillage) des Hauts Elfes. Son code se situe dans `apps/hyalma/`

## Flex et Bison

Hyalma utilise `flex` pour générer son lexer à partir de `lexer.lpp` et `bison` pour générer son parser à partir de `parser.ypp`.

La documentation sur l'utilisation de `flex` et `bison` avec `C++` (et non `C`) étant quasi-inexistante, nous sommes partis de la configuration proposée dans [https://github.com/ezaquarii/bison-flex-cpp-example](https://github.com/ezaquarii/bison-flex-cpp-example).

## Command

Une classe `Command` (dans `command.hpp`, `command.cpp`) représente les commandes. Sont notables :

- Les arguments, `vector<string> argv`
- Éventuellement, les redirections de flux (`<, >, 2>, <<, >>, 2>>`)
- Éventuellement, un pointeur vers une commande à effectuer auparavant ou simultanément (séquences : `;, &&, ||, |`)

## Lexer

Pas de subtilité particulière dans `lexer.lpp`. Actuellement, les guillemets n'expandent pas les variables.

La classe `Scanner` (`scanner.hpp`) est la déclaration du lexer généré par `flex`.

## Parser

Le bloc de base est `subst_word`, une concaténation de mots (`WORD`) et de variables substituées (`VAR`).

Une `simple_command` ne contient pas de séquence.

Les `BLANK` sont gérés de manière à éviter les conflits :

- le token `BLANK` contient une quantité arbitraire de blancs
- certains `BLANK` optionnels sont développés directement dans la règle sans utiliser `opt_blank` (`bison` ne semble pas permettre d'inliner des règles)
- on prend la convention que `simple_command` ne commence ni ne finit par un `BLANK`, alors que `command` permet les `BLANK` en début et en fin

Le parser appelle la fonction `exec` du shell, avec éventuellement un paramètre `background` pour gérer `&`.

## Shell

`shell.cpp` gère la lecture et l'exécution des commandes, le prompt et le `Ctrl+C`.

### Exécution

Plusieurs niveaux :

- Exécution d'une commande simple :
	- Prend en paramètre les `fd` à utiliser pour `stdin,stdout,stderr`
	- Parse les éventuelles affectations en début de commande
	- Gère les commandes internes (`am=cd, export, exit`)
	- Si exécution au premier plan, attend la terminaison du fils
- Exécution d'une commande complexe :
	- fork
	- le fils exécute la commande vers laquelle on stocke un pointeur
	- attendre la terminaison du fils, stocker la valeur de retour
	- exécuter la commande simple
	- pour `|`, les deux en même temps
- `exec()`
	- appelle `exec_complex()` en passant les fds par défaut (`stdin,stdout,stderr`)
	- stocke la valeur de retour pour `$?`

### Ctrl+C

Le handler se contente de passer un booléen à `true`. Selon là où en est le shell (prompt, exécution d'une commande), le signal est transmis à un fils ou le buffer est vidé.

Nous ne supportons pas les `read` non bloquants sur un pipe, ce qui empêche de vider `stdin` lors d'un `Ctrl+C` : on bufferise donc l'entrée (voir bigbuf). Ce n'est pas suffisant : actuellement, si on tape du texte pendant l'exécution d'une commande, il ne sera pas vidé.
