# Syscalls

List of syscalls :

POSIX-compliant (except use of 64 integer instead of 32bits):

* execve
* execv
* exec
* sbrk
* close
* dup2
* pipe
* write
* read
* raise
* kill
* sigprocmask
* sigreturn
* pause

POSIX-non-compliant:

* exit
* fork
* waitpid
* wait
* microsleep
* open
* flushpipe
* pollin
* chdir
* opendir
* readdir
* sigaction
* window\_get
* window\_refresh
* get\_key

If a syscall is sucessful, it returns a null or positive value.
On error a negative error value is returned.

## exit

`void exit(int64_t code)`

Exit process with code `code`.
Zero should be used on success, positive values on errors.

Does not return.

## fork

`int64_t fork()`

Creates an independent copy of the process and attaches it as a child of the original.

Return value:

* Parent: pid(>=0) of the newly created process (child)
* Child: `-1`.

## waitpid

`int64_t waitpid(pid_t pid, int64_t *status, int64_t options)`

Wait for a child process to exit.

If pid is `-1` (aka `no_pid`), waits on any child.
Otherwise, waits on a child with given pid.

If the `WNOHANG` bit is set in options, returns immediatly even if there is an alive child matching pid.

Return value:

* On success: pid of the involved child.
* `err_fault`: `status` was not nullptr and caused a page fault.
* `err_child`: No children matched (neither alive nor zombie).
* `err_nohang`: `WNOHANG` option was set, and no zombie children matched.
* `err_intr`: An interrupt happened before a matched children death.

## wait

`int64_t wait(int64_t *status)`

Wrapper for `waitpid(no_pid, status, 0)`

## microsleep

`int64_t microsleep(uint64_t ms)`

Sleeps for at most `ms` microseconds.

Return value:

* `err_success`

## execve

`int64_t execve(char const* path, char const* const* argv, char const* const* envp)`

Replaces the current process image with another process image, found in `path`. Passes as arguments `argv` (`NULL`-terminated array of `NULL`-terminated strings). Environment for the process is set to `envp` (`NULL`-terminated array of `NULL`-terminated strings). As a convention, strings in `envp` follow the `variable=value` syntax.

Return value:

* `err_fault`: The arguments caused a page fault.
* `err_notfound`: There was no file at `path`.

## execv

`int64_t execve(char const* path, char const* const* argv)`

Wrapper for `execve(path, argv, environ)`, where `environ` is the current environment.

## exec

`int64_t exec(char const* path)`

Wrapper for `execv(path, {path, nullptr})`.

## sbrk

`uintptr_t sbrk(int64_t delta)`

Changes heap size by delta.

Return value:

* On success: old heap end
* `err_fault`: the new end induce negative heap size.
* `err_nomem`: the kernel refused to change heap size.

## open

`int64_t open(const char* path, int64_t flags)`

Open the file at `path` and return the newly created file descriptor.

Flag bits:

* `O_RDONLY`: Open in read only mode
* `O_WRONLY`: Open in write only mode
* `O_CREAT`: Create the file if it does not exist
* `O_APPEND`: Position the cursor at the end of the file.
* `O_TRUNC`: Clear the file content.

Return value:

* On success: newly created file descriptor.
* `err_fault`: The arguments caused a page fault.
* `err_notfound`: file (without `O_CREAT`) or folder not found.
* `err_error`: some of the given flags were not supported on the file.

## close

`int64_t close(int64_t fd)`

Closes file descriptor `fd`.

Return value:

* `err_success`: success.
* `err_badfd`: `fd` was not an open file descriptor.

## dup2

`int64_t dup2(uint64_t oldfd, uint64_t newfd)`

Duplicates file descriptor `oldfd` into file descriptor `newfd`.

Closes `newfd` beforehand if it was open.

Return value:

* On success: `newfd`
* `err_badfd`: `oldfd` was not an open file descriptor or `newfd` was greater or equal to `max_fd`

## pipe

`int64_t pipe(uint64_t fds[2])`

Creates a pipe, read end is stored in fds[pipe_out], write end in fds[pipe_in].

Return value:

* `err_success`: success.
* `err_fault`: Writing to `fds` caused a page fault.

## flushpipe

`int64_t flush_pipe(fd_t fds[2])`

Same as pipe, but the created pipe is a FlushPipe.

## write

`int64_t write(fd_t fd, const char* buf, uint64_t count)`
`int64_t write(fd_t fd, const uint8_t* buf, uint64_t count)`

Writes at most count bytes from buf to file descriptor fd.

Return value:

* On success: The number of bytes written. Note that this might differ from `count` in which case one should perform `write` again.
* `err_size`: `count` was greater than `max_io_size`.
* `err_badfd`: `fd` is not open or does not support writing.
* `err_fault`: Reading `count` bytes from `buffer` caused a page fault.
* `err_intr`: An interrupt happened before any data was written.

## read

`int64_t read(fd_t fd, uint8_t* buf, uint64_t count)`
`int64_t read(fd_t fd, char* buf, uint64_t count)`

Reads at most count bytes to buf from file descriptor fd.

Return value:

* `0`: eof
* On success: The number of bytes read. Note that this might differ from `count`.
* `err_size`: `count` was greater than `max_io_size`.
* `err_badfd`: `fd` is not open or does not support reading.
* `err_fault`: Reading `count` bytes from `buffer` caused a page fault.
* `err_intr`: An interrupt happened before any data was read.

## pollin

`int64_t pollin(fd_t* fds, uint64_t count)`

Edits `fds` in place to put the file descriptors ready to be read at the begining.
Note that the end of fds becomes garbage.

Return value:

* On success: the number of file descriptors that are ready.
* `err_size`: `count` was greater than `max_io_size`.

## chdir

`int64_t chdir(const char* path)`

Changes current directory to `path`

Return value:

* `err_success`: success
* `err_fault`: Reading path caused a page fault.
* `err_notfound`: `path` did not lead to a existing folder.

## opendir

`int64_t opendir(char const* path = ".")`

Prepare directory for readdir.

Return value:

* `err_success`: success
* `err_fault`: Reading path caused a page fault.
* `err_notfound`: `path` did not lead to a existing folder.

## readdir

`int64_t readdir(char* buf)`

Reads an entry from the last directory opened with `opendir` and stores its name in `buf`.

Return value:

* `et_null`: There are no more entries.
* `et_dir`: The entry was a directory.
* `et_file`: The entry was a file.
* `err_fault`: `buf` caused a page fault.

## raise

`int64_t raise(uint64_t sig)`

Equivalent to `kill(self_pid, sig)`. where `self_pid` is the pid of the current process.

## kill

`int64_t kill(pid_t pid, uint64_t sig)`

Send signal `sig` to process `pid`.

If `sig` is not a valid signal, returns `err_badsig`. Valid signals are `0` to `NBSIG` (excluded).

Implemented signals are the following :

- `SIGNONE = 0` : always ignored, checks the validity of the pid.
- `SIGABRT = 1` : by default terminates the user. If caught and the handler erturns, terminates the user.
- `SIGINT = 2` : keyboard interrupt. By default, terminates the user.
- `SIGKILL = 3` : immediatly terminates the user. Cannot be caught or blocked.

Return value:

* `err_success`: success.
* `err_badsig`: `sig` was not a valid signal. Valid signals are `0` to `NBSIG` (excluded).
* `err_badpid`: `pid` is not a valid process id.

## sigaction

`int64_t sigaction(uint64_t sig, const sigact* new_action, sigact* oldaction = nullptr)`

``` cpp
struct sigact {
  void (*sa_handler)(int) = nullptr;
  uint64_t sa_mask = 0;
  void (*sa_restorer)(void) = &sigreturn;
};
```

Sets a handler for signal `sig`.

`sa_handler` can be a function, `SIG_DFL` (to set ot default) or `SIG_IGN` (to ignore signal).
During the execution of the handler, the signal mask will temporarily be set to `sa_mask`.
`sa_restorer` is the address to which the handler will return, most of the time it should not be modified.

Return value:

* `err_success`: success
* `err_badsig`: `sig` was not a valid signal. Valid signals are `0` to `NBSIG` (excluded).
* `err_fault`: The arguments caused a page fault.

## sigprocmask

`int64_t sigprocmask(sig_mask_action how, uint64_t *set, uint64_t *oldset = nullptr)`

Sets the signal mask and/or write back the old mask. The signal `i` will be pending until `1<<i` is cleared from the mask.

If `set` is not null, sets the signal mask :

`how` can be one of the following :

* `SIG_BLOCK` (block signals in `*set`)
* `SIG_UNBLOCK` (unblocks signals in `*set`)
* `SIG_SETMASK` (sets mask to `*set`)

If `oldset` is not null, sets `*oldset` to the old signal mask.

Return value:

* `err_success`: success
* `err_fault`: The arguments caused a page fault.
* `err_error`: `how` was invalid.

## sigreturn

`void sigreturn()`

Used internally. Should not be called directly.

## pause

`int64_t pause()`

Waits for a signal that is not ignored. Returns iff interrupted by a signal caught by a handler.

Return value:

* `err_intr`



## window\_get

`WindowSize window_get()`

``` cpp
struct WindowSize {
  uint32_t w;
  uint32_t h;
};
```

Opens a window if the process doesn't have one.

Returns the size of the window.

## window\_refresh

`void window_refresh(*WindowData)`

``` cpp
struct WindowData {
  uint32_t w;
  uint32_t h;
  uint16_t *data;
};
```

Changes window buffer.

## get\_key

`int16_t get_key()`

Receive a keycode from the Window Manager.

Return value:

* On success: (positive) keycode
* `-1`: no key was available
