# Interrupt

## Table des interrupt

La table des interrupts est gérée dans `libs/bootlib/interrupt_base.hpp` et `libs/bootlib/interrupt_base.cpp`. On fournit une fonction `Interrupt::setup_handler()` qui remplit la structure `InterruptDescriptor` spécifique à l'architecture `x86-64`.

## Handler wrappers

Un minimum de code assembleur est nécessaire pour sauvegarder le contexte utilisateur. Vu le grand nombre d'interrupts (et donc de handler wrappers), ce code est généré automatiquement par `kernel/auto_src/handlers.py`. Il se charge de pousser les registres sur la pile et d'appeler le handler correspondant, puis de restaurer les registres (en pratique, la plupart du temps, cela passe par le `scheduler.md`).

Un handler wrapper par défaut (et un handler par défaut) est mis en place pour les interrupts non gérés.

## PIC

Les fichiers `kernel/src/pic.hpp` et `kernel/src/pic.cpp` interagissent avec la PIC, la puce qui s'occupe des interrupts hardware. La communication est effectuée par les ports I/O du processeur.

## Clavier

Les interrupts claviers, produits par le PIC, signalent qu'il faut aller lire la touche dans un port I/O spécifique. Un état (`KbdState`) maintient en mémoire l'ensemble des touches enfoncées, ce qui permet en particulier de gérer les modificateurs `Alt`, `Shift`... Ceux-ci sont représentés par des bits au-delà du 8ème, `Keyboard` renvoie ainsi des entiers `16` bits qui sont ensuite gérés par le `window_manager.md`.

Un modificateur particulier `Ext` est utilisé pour les touches pour lesquelles le clavier envoie 2 keycodes, comme `Mod4` ou les flèches.

Pour le mappage des touches vers les symboles selon le layout qwerty/azerty, on utilise une structure qui stocke une chaîne de caractères (une pour chaque keycode, caractère null lorsque le caractère n'est pas géré). Une deuxième chaîne est utilisée pour `Shift`, une troisième pour `AltGr` en azerty.

## Exceptions

Nous avons implémentés des messages d'erreur pour chaque exception.

Deux d'entre elles nécessitent un traitement particulier : Page Fault (PF) et Double Fault (DF) utilise une stack alternative, spécifiée à `setup_handler()`, pour pouvoir gérer un éventuel stack overflow.