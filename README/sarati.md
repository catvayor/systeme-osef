# Sarati

Cette application sert de terminal et agit comme une interface entre le Window Manager et les programmes textes qui se servent de stdin et stdout.

Sarati récupère les touches du clavier, et les forward vers le stdin du process affiché, récupère le stdout, le met en forme et l'envoi au WM.

Pour éviter des blocages, Sarati poll le stdout du process affiché ainsi que le clavier, et dort quand rien ne change pour économiser de la puissance de calcul.

## Mode "cooked" sous linux, FlushPipe sous Hauts Elfes

Sous linux, dans un terminal l'entrée standard est géré par le terminal driver, par defaut configuré en mode "cooked".

Le mode cooked gère plusieurs actions spéciales telles que backspace et ctrl-d

Observations sur l'effet de ctrl-d (faute d'avoir trouvé de la documentation):

- Il a pour effet d'inserer une "interruption de lecture": en effet si le buffer est vide, cela est interprété comme EOF (lecture de taille nulle), si le buffer comporte des caractères, ceux-ci sont envoyés au process.
- Si un programme dort un certain temps avant de lire, et qu'il ignore les EOF (= les considère simplement comme des lectures de taille nulle), alors le programme arretera ses lecture à chaque emplacement où le terminal a recu ctrl-d.

Pour mimiquer ce phénomène, le concept de `FlushPipe` a été introduit dans l'OS.

Ce type de pipe particulier a pour effet d'arrêter la lecture là où l'écriture s'était arretée.

En pratique, la structure utilisée est `queue<queue<uint8_t>>`: chaque write ajoute un element à la file exterieure, et un read s'arrete s'il epuise une file intérieure.

On peut ainsi faire des écritures de taille nulle pour simuler un ctrl-d avec buffer vide.


Le reste des fonctionalités primordiales du mode cooked telles que backspace sont gérées côté utilisateur par Sarati qui remplit un buffer de caractères, en efface au besoin, et fait un write lorsqu'il voit `entrée` ou `ctrl-d`.

## Support du resize

Sarati conserve un historique (borné à la fois en nombre de lignes et en nombre de caractères) pour pouvoir afficher le nécessaire lorsque la fenêtre change de taille.

Comme la taille de l'écran est connue, les bornes ont été choisies en fonction.

## Double Buffering

Sarati utilise du double buffering pour éviter que l'affichage comporte des glitchs.
