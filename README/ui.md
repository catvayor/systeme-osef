# Interface utilisateur

## Keybindings dans le Window Manager

* `Alt` + `Échap`: changer la disposition du clavier (qwerty/azerty)

Tous les keybindings suivants necessitent d'appuyer sur `Ctrl` ou `Mod4` (touche windows ou autre).

* `Entrée`: Lancer un terminal (Hyalma)
* `W`: passer du mode split au mode tabs et vice-versa
* `F`: activer/desactiver le mode plein écran
* `G`: activer/desactiver le mode debug (window manager de taille reduite)
* `Flèche droite/gauche`: changer le focus
* `Maj` + `flèche droite/gauche`: déplacer la fenetre.
* `1-9`: changer de workspace.
* `Maj` + `1-9`: envoyer la fenetre vers un workspace.

## Keybindings dans Sarati

* `Ctrl` + `C`: Envoyer le signal SIGINT
* `Ctrl` + `D`: Flush le buffer keyboard (provoque eof si le buffer est vide)
