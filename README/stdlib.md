# Librairie standard

## Headers standard

### Compatibilité linux

Nous avons codé notre shell sous Linux avant de le traduire en elfique. Nos syscalls n'étant pas tout à fait compatibles avec Linux (par exemple, nos `pid` commencent à `0`), nous avons codé un header de compatibilité `<linuxcompat>`. Nous avons finit par traduire shell en hyalma, ce header n'est donc plus à jour mais a le mérite d'exister.

## Structures maison

### chains

Stoque des listes doublement chainées d'entiers positifs, ayant des supports disjoints.

`entry` designe un identifiant de chaine
`elem` designe un element.

Opérations en temps constant:

- `bool empty(entry)`
- `bool is_free(elem)`
- `elem front(entry)`
- `elem back(entry)`
- `pop_elem(elem)`
- `pop_front(entry)`
- `pop_back(entry)`
- `push_after(elem pos, elem x)`
- `push_before(elem pos, elem x)`
- `push_front(entry, elem)`
- `push_back(entry, elem)`
- `append(entry src, entry dst)`
- `prepend(entry src, entry dst)`

Des iterateurs sont aussi fournis, ainsi que quelques opérations en temps linéaire.

### memory_arena

Gère les indices d'un `vector` avec une `stack` d'indices vides, pour allouer un indice lorsque l'on veut insérer un objet.

Opérations (en temps constant) :
``` cpp
index = arena.push(objet);
arena[index];
arena.pop(index);
arena.size();
```

Espace mémoire en `O(max(nb_elems))`, le max étant effectué sur les instants `t'<t`.

### chained_memory_arena

Similaire, mais utilise `chains` en interne : une chaîne d'indices disponibles et une chaîne d'indices utilisés. Cela permet, en particulier, d'avoir :

- des itérateurs avec incrémentation en `O(1)`
- l'insertion à un index fixé
- l'insertion à un emplacement fixé dans la chaîne (nous n'utilisons en fait pas cette possibilité)

Voir `libs/stdlib/chained_memory_arena.hpp` pour la liste exhaustive des opérations possibles (toutes en `O(1)`).
