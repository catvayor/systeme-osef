# Bootlib

Bootlib contient du code commun au kernel et bootloader, mais qui n'a pas à être exposée aux users.

Elle fournit :

* Le driver ata
* Les fonctions `load_boot_info` et `store_boot_info` qui permettent le passage du bootloader au kernel la taille du bootloader, la heap et le `frame_alloc`
* Les éléments de structure du système de fichier
* Une interface vers le buffer vga pour avoir des fonctions comme `print` ou `print_hex`
* Les fonctions de base des interupt
* Les fonctions de base pour la mémoire virtuelle (voir `virtual_memory.md`)
* Un elf loader assez basique

