# Lazy Memory

Système de gestion de la mémoire des process reposant sur bootlib pour les opérations très bas niveau (cf `virtual_memory.md`)

## Page Fault handler

Seules les pages contenant l'exécutable sont chargées au lancement d'un process.

Lorsqu'un process fait une Page Fault, la légitimité de l'accès est verifié (addresse dans l'executable, dans le heap ou le stack ?), et si elle est valide le page fault est corrigé et l'execution reprend.

Sinon, le process est tué.

## Allocate-on-access

Si le Page Fault se produit à une addresse virtuelle ne correspondant à aucune page physique, une nouvelle page physique est allouée.

Ainsi l'execution d'un programme accédant à cette mémoire peut poursuivre son execution.

Cela se produit dès que le programme va chercher dans une nouvelle page virtuelle de stack ou de heap qui lui est attribuée (cf syscall `sbrk`).

## Copy-on-write

Si le Page Fault se produit sur une page en read-only, la page en question est dupliquée sur une nouvelle page physique et tout le chemin dans l'arbre d'addresses virtuelle est rendu writable (en dupliquant au passage les pages intermediaires qui ne sont sont pas).

## Efficacité en mémoire

Le nombre de référence sur chaque page sont stockés dans une map.

Pour éviter d'utiliser de la mémoire superflue, seules les pages avec 2 références ou plus y sont référencées : si une page physique est atteignable depuis un arbre d'addressage, et qu'il n'est pas présent dans la map, alors il est référencé une seule fois.

Ainsi le module ne coute rien pour une page non dupliquée, et est très bénéfique pour les pages dupliquées.
