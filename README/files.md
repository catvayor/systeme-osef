# Système de fichiers

## Système Physique

Pour le système de fichier physique, on utilise exFat avec quelques libertées pour faciliter son implémentation (principalement, le nom des fichiers est case-sensitive).

On trouve les éléments constituants dans `libs/bootlib/fs_struct.hpp`, et la manipulation du système dans `kernel/src/filesystem/fs.hpp` (et `cpp`).

Il est initialisé par la bibliotèque host `FSutils` (voir `FSutils.md`).

## Fichiers ouverts

On stocke un tableau de fichiers ouverts, géré par une `memory_arena` (voir `stdlib.md`). On stocke pour chacun le mode d'ouverture (lecture et/ou écriture) et un pointeur vers le fichier à proprement parler.

On utilise également une structure de `chains` (voir `stdlib.md`) pour stocker, pour chaque fichier ouvert, la liste des processus en attente de lecture ou d'écriture pour ce fichier (en particulier pour les pipes).

Ceci est géré dans `kernel/src/process_pool/process.hpp` (et `cpp`).

## Descripteurs de fichiers

Pour chaque processus, une table de `fd` assigne à chaque `fd` utilisé un index de la `memory_arena` des fichiers ouverts.

Les processus peuvent faire appel à `dup2()`, il faut donc permettre d'insérer un indice quelconque. On utilise donc une `chained_memory_arena` plutôt qu'une `memory_arena` (voir `stdlib.md`).

Ceci est géré dans `kernel/src/process_pool/file_descriptor.hpp` (et `cpp`).
