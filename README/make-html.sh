#!/bin/bash

HTML_DIR=html/
file=$1
html_file=$2
echo Making ${file} into ${html_file}...
cat README/html/header.html > ${html_file}
python3 -m markdown -x fenced_code -x codehilite -x tables $file |
sed -e "s#<code>\([^<]*\).md</code>#<a href='\1.md.html'><code>\1</code></a>#g" >> ${html_file}
#	-e "s#<code>cpp#<div class='codehilite'><pre><code>#g" \
#	-e "s#cpp</code>#</code></pre></div>#g" >> ${html_file}
cat README/html/footer.html >> ${html_file}
