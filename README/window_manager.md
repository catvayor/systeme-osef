# WindowManager

## Fonctionalités

- Workspaces (1-9)
- 1 mode multi-fenêtres : Split
- 2 modes mono-fenêtre : Tabs, Fullscreen

## Scheduling

Le WM est appelé a fréquence constante par le scheduler, et est plus ou moins lent, selon la quantité de caractères a rafraîchir.

Malgré les efforts dans cette direction, si les process causent des rafraîchissements massifs en permanence la performance globale est impactée, partant du principe que si l'utilisateur souhaite afficher une fenêtre, la mettre a jour dès que possible est primordial.

Évidemment, si l'utilisateur décide d'aller dans un workspace vide - ou avec des fenêtres plus calmes - plus aucun impact de performance n'est observable.

## Interaction avec les process

Lorsqu'un process appelle le syscall `get_window` une fenêtre est créé s'il n'en a pas, et il reçoit dans tous les cas la taille actuelle de sa fenêtre.

Lorsqu'un process meurt, sa fenêtre - s'il en a une - est automatiquement fermée.

Les process envoient l'addresse du buffer qu'ils souhaitent afficher avec le syscall `refresh_window`.

## Affichage d'une fenetre

Si une fenêtre est visible, le WM tente de l'afficher.
Si le buffer est trop grand, il l'affiche tronqué, s'il est trop petit il affiche un damier magenta dans la zone qui déborde.

## Focus et déplacements.

Les id des fenêtres ouvertes sont stockés dans une structure `chains` (cf `stdlib.md`), dont les entrées sont les workspaces.

La fenetre active de chaque workspace est stocké sous la forme d'un itérateur de `chains`.

Les changements de focus se font via incréments/décréments de l'itérateur actif.

Les déplacements de fenêtre se font par ré-insertion de l'id en question à un autre endroit dans la `chains`.

## Layouts

Les changements de layout se font simplement en mettant le bit de rafraichissement complet à 1, et en retaillant les fenêtres de manière adéquate.

À l'affichage, une ou plusieurs fenêtres sont affichées, selon le mode.

## Shortcuts

Nous avons implémenté une structure facilement extensible pour gérer les shortcuts en qwerty et en azerty.

On sépare d'une part les actions (`Action`) et les raccourcis. Trois `unordered_map`s, `common_shortcuts`, `layout_shortcuts[Keyboard::LAY_US]`, `layout_shortcuts[Keyboard::LAY_FR]`, assignent des actions aux raccourcis.

Les raccourcis utilisent des `uint16_t`, pour gérer les modificateurs `Shift`, `Alt`, `Ctrl`, `Mod4`, qui sont représentés par des bits au-delà du 8e.

Voir `ui.md` pour la description des shosrtcuts disponibles.

## Optimisation

Chaque fenêtre possède un flag de mise à jour, et il y a 2 flags globaux, un pour l'entièreté de l'écran, et un pour la barre.

Si un flag est activé, tous les éléments en question sont redessinés, sinon le window manager ne fait rien, ce qui permet une nette augmentation dans son efficacité.
