# Kinship

Ce module gère les liens de filliation entre process.

## Structure

Chaque process stocke son parent (ou le fait de ne pas avoir de parent).

Note : Contrairement à linux, il n'y a pas de pid servant des roles particuliers tels que le ramassage de zombie ou un processus inactif : notre système est conçu de sorte qu'aucun zombie ne soit orphelin, et le scheduler se met en pause si aucun processus ne cherche a s'executer.

Une structure `chains` (cf `stdlib.md`) stocke les fils vivant d'un process dans la chaine `2*pid` et ses process zombies dans la chaine `2*pid+1`.

## Exit

Si un process meurt, tous ses fils sont détachés, et s'il est lui même attaché a un parent, il change de chaîne et passe dans la chaîne zombie de ce dernier.

## Wait

Si un parent wait son enfant, ce dernier est pop de la chaîne zombie du parent.

Si il n'y a pas d'enfant zombie satisfaisant, mais des enfants vivants satisfaisants, le processus parent est mis en veille.

Il sera reveillé a la mort d'un enfant satisfaisant.
