#!/bin/bash

if ! [[ "$(dirname $0)" = "." ]]; then
    echo "please exec in the directory"
    exit 1
fi

TARGET_DIR="target-$1"

rm -fr "$TARGET_DIR"
mkdir "$TARGET_DIR"
mkdir "$TARGET_DIR/src"
tar -xf binutils.tar.xz -C "$TARGET_DIR/src"
tar -xf gcc.tar.xz -C "$TARGET_DIR/src"
cd "$TARGET_DIR"

echo "cp of src in $(pwd) done"

PREFIX="$(pwd)"
TARGET=$1
PATH="$PREFIX/bin:$PATH"

#making binutils

    cd $PREFIX/src
    mkdir build-binutils
    cd build-binutils
    ../binutils-*/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
    make -j$(nproc)
    make -j$(nproc) install

#making gcc

    cd $PREFIX/src

    # The $PREFIX/bin dir _must_ be in the PATH. We did that above.
    which -- $TARGET-as || echo $TARGET-as is not in the PATH

    mkdir build-gcc
    cd build-gcc
    ../gcc-*/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ -without-headers 
    make -j$(nproc) all-gcc
    make -j$(nproc) all-target-libgcc CFLAGS_FOR_TARGET='-g -O2 -mcmodel=large -mno-red-zone'
    make -j$(nproc) install-gcc
    make -j$(nproc) install-target-libgcc
