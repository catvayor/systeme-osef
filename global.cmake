enable_language(CXX ASM)

target_compile_features(${Target} PUBLIC cxx_std_20)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra --pedantic")

set_target_properties(${Target} PROPERTIES DEST_FILE "${Dest}/${Target}")

macro(set_os_path path)
    set_target_properties(${Target} PROPERTIES OS_PATH ${path})
    set(ENV{Target_list} "$ENV{Target_list};${Target}")
endmacro()

macro(add_link_dependency DEP)
    get_target_property(L ${Target} LINK_DEPENDS)
    if("${L}" STREQUAL "L-NOTFOUND")
        set(L "")
    endif()
    set_target_properties(${Target} PROPERTIES LINK_DEPENDS "${L};${DEP}")
endmacro()
