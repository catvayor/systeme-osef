#include <iostream>
#include <fstream>
#include <cstring>
#include <ostream>
#include <vector>
#include <chrono>
#include <bitset>
#include <filesystem>

#include <fs>

int main(int argc, char** argv) {

  if (argc < 4) {
    std::cerr << "usage : " << argv[0] << " fsFile path fileToAdd" << std::endl;
    return 1;
  }

  std::fstream file(argv[1]);
  fs::filesys_t filesys(file);

  std::filesystem::path path(argv[2]);

  std::u16string filename;
  if (path.has_filename()) {
    filename = path.filename().u16string();
    path.remove_filename();
  } else
    filename = std::filesystem::path(argv[3]).filename().u16string();

  std::ifstream inFile(argv[3]);

  fs::dir_t* dir = nullptr;//nullptr for root

  auto nxtdir = [&dir, &filesys](std::u16string const & name, std::vector<fs::dir_t*>& dirs, bool & changed) {
    for (auto d : dirs)
      if (d->filename == name) {
        dir = d;
        dir->read_data();
        return;
      }
    changed = true;
    dir = new fs::dir_t(&filesys, name);
    dirs.push_back(dir);
  };

  auto i = path.begin();
  if (i == path.end())
    goto placefile;
  if (*i == "/")
    ++i;
  if (i == path.end())
    goto placefile;

  nxtdir(i->u16string(), filesys.root->subdirs, filesys.root->changed);

  ++i;
  for (; i != path.end(); ++i)
    if (*i != "")
      nxtdir(i->u16string(), dir->subdirs, dir->changed);

placefile:
  std::vector<fs::file_t*>* file_vect;

  if (dir == nullptr) {
    filesys.root->changed = true;
    file_vect = &filesys.root->files;
  } else {
    dir->changed = true;
    file_vect = &dir->files;
  }

  fs::file_t* file_obj = nullptr;
  for (auto f : *file_vect) {
    if (f->filename == filename) {
      file_obj = f;
      break;
    }
  }

  //create file object if needed
  if (!file_obj) {
    file_obj = new fs::file_t(&filesys, filename);
    file_vect->push_back(file_obj);
  }

  //give data
  inFile.seekg(0, std::ios_base::end);
  uint64_t inSize = inFile.tellg();

  file_obj->changed = true;
  file_obj->fdata.resize(inSize);
  inFile.seekg(0, std::ios_base::beg);
  inFile.read(file_obj->fdata.data(), inSize);

  filesys.flush();

  file.close();

  return 0;
}
