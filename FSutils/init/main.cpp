#include <iostream>
#include <fstream>
#include <cstring>
#include <ostream>
#include <vector>
#include <chrono>
#include <bitset>
#include <fs>

#include "cap_table.inl"

int main(int argc, char** argv) {

  if (argc < 5) {
    std::cout << "Usage :\n\t"
              << argv[0] << " outfile volume_size sector_shift cluster_shift [boot_code [extended_boot]]" << std::endl;
    return 1;
  }

  std::ofstream outfile(argv[1]);
  uint64_t vol_size_byte = std::stoull(argv[2]);
  uint8_t sector_shift = std::stoul(argv[3]);
  uint8_t cluster_shift = std::stoul(argv[4]);

  uint64_t vol_size = vol_size_byte >> sector_shift;

  uint32_t cluster_count = (vol_size - 24) >> cluster_shift;
  uint32_t fat_length = vol_size - 24 - (cluster_count << cluster_shift);

  while ( (fat_length << (sector_shift - 2)) < (cluster_count + 2) ) {
    //fat not big enough -> transform a cluster into fat
    fat_length += 1 << cluster_shift;
    --cluster_count;
  }

  uint32_t bitmap_nb_clus = 1 + (cluster_count >> (3 + cluster_shift + sector_shift));//1 bit per cluster
  uint32_t cap_table_nb_clus = 1 + (cap_table_len >> (cluster_shift + sector_shift));

  fs::bootsector_t bootsector;
  {
    //boot regions
    bootsector.volume_size = vol_size;
    bootsector.fat_off = 24;
    bootsector.fatLength = fat_length;
    bootsector.clusterHead_off = 24 + fat_length;
    bootsector.cluster_count = cluster_count;
    bootsector.root = 2 + bitmap_nb_clus + cap_table_nb_clus;
    auto t = std::chrono::system_clock::now().time_since_epoch().count();
    bootsector.serial_nb = uint32_t(t >> 2);
    bootsector.sector_shift = sector_shift;
    bootsector.cluster_shift = cluster_shift;
    bootsector.fat_count = 1;
    bootsector.drive_select = 0x80;
    bootsector.percent_in_use = (bitmap_nb_clus + cap_table_nb_clus + 1) / cluster_count;

    if (argc > 5) {
      std::ifstream bootfile(argv[5]);
      bootfile.read((char*)bootsector.bootcode, 390);
      if (!bootfile.eof())
        std::cerr << argv[5] << ":\033[1m\033[95mwarning:\033[0m bootfile as been truncated" << std::endl;
    }

    std::vector<uint8_t> fs_beg(12 << sector_shift, 0);
    std::memcpy(&fs_beg.front(), &bootsector, sizeof(fs::bootsector_t));

    if (argc > 6) {
      std::ifstream ext_boot(argv[6]);
      ext_boot.read((char*)&fs_beg[1 << sector_shift], 8 << sector_shift);
      if (!ext_boot.eof())
        std::cerr << argv[6] << ":\033[1m\033[95mwarning:\033[0m extended bootfile as been truncated" << std::endl;
    }

    for (uint32_t i = 1; i < 9; ++i)
      *(uint32_t*)&fs_beg[((i + 1) << sector_shift) - 4] = 0xAA550000;

    uint32_t check_sum = fs::boot_region_checksum(&fs_beg.front(), sector_shift);
    for (uint32_t i = 0; i < (1u << (sector_shift - 2)); ++i)
      *(uint32_t*)&fs_beg[(11 << sector_shift) + (i << 2)] = check_sum;

    outfile.write((char*)&fs_beg.front(), fs_beg.size());//main boot region
    outfile.write((char*)&fs_beg.front(), fs_beg.size());//back up boot region
  }

  {
    //initial fat
    constexpr uint32_t end_of_chain = 0xffffffff;
    std::vector<uint32_t> fat(fat_length << (sector_shift - 2), 0);
    fat[0] = 0xfffffff8;
    fat[1] = end_of_chain;
    for (uint32_t i = 2; i < (bitmap_nb_clus - 1); ++i)
      fat[i] = i + 1;
    fat[bitmap_nb_clus + 1] = end_of_chain;
    for (uint32_t i = bitmap_nb_clus + 2; i < (bootsector.root - 1); ++i)
      fat[i] = i + 1;
    fat[bootsector.root - 1] = end_of_chain;
    fat[bootsector.root] = end_of_chain;

    outfile.write((char*)&fat.front(), fat.size() * 4);
  }

  {
    std::vector<std::bitset<8> > alloc_bitmap(bitmap_nb_clus << (sector_shift + cluster_shift), 0);
    uint32_t nb_used = bitmap_nb_clus + cap_table_nb_clus + 1;
    std::size_t i = 0;
    for (; nb_used > 8; nb_used -= 8)
      alloc_bitmap[i++] = 0xff;
    alloc_bitmap[i] = (1 << nb_used) - 1;

    outfile.write((char*)&alloc_bitmap.front(), alloc_bitmap.size());
  }

  {
    std::vector<uint8_t> cap_clusters(cap_table_nb_clus << (sector_shift + cluster_shift), 0);
    std::memcpy(&cap_clusters.front(), cap_table, cap_table_len);
    outfile.write((char*)&cap_clusters.front(), cap_clusters.size());
  }

  {
    std::vector<fs::dir_entry_t> root_cluster(1 << (cluster_shift + sector_shift - 5), fs::dir_entry_t());

    fs::vol_lab_entry_t label;
    label.char_count = 11;
    std::memcpy(label.lab_str, u"Hauts-Elfes", 22);
    root_cluster[0] = label;

    fs::alloc_entry_t bitmap;
    bitmap.first_clus = 2;
    bitmap.len = (cluster_count + 7) / 8;
    root_cluster[1] = bitmap;

    fs::cap_table_entry_t cap;
    cap.table_checksum = fs::checksum32(cap_table, cap_table_len);
    cap.first_clus = bitmap_nb_clus + 2;
    cap.len = cap_table_len;
    root_cluster[2] = cap;

    outfile.write((char*)&root_cluster.front(), root_cluster.size() * sizeof(fs::dir_entry_t));
  }

  for (uint64_t i = (bootsector.clusterHead_off + (bootsector.root << cluster_shift)) << sector_shift;
       i < vol_size_byte; ++i)
    outfile.put(0);
  outfile.close();

  return 0;
}
