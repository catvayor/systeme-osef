#include "fs.hpp"
#include <iostream>

namespace fs {

  alloc_entry_t::alloc_entry_t(dir_entry_t ent) {
    std::memcpy(this, &ent, sizeof(dir_entry_t));
  }
  cap_table_entry_t::cap_table_entry_t(dir_entry_t ent) {
    std::memcpy(this, &ent, sizeof(dir_entry_t));
  }
  vol_lab_entry_t::vol_lab_entry_t(dir_entry_t ent) {
    std::memcpy(this, &ent, sizeof(dir_entry_t));
  }
  file_entry_t::file_entry_t(dir_entry_t ent) {
    std::memcpy(this, &ent, sizeof(dir_entry_t));
  }
  stream_extension_entry_t::stream_extension_entry_t(dir_entry_t ent) {
    std::memcpy(this, &ent, sizeof(dir_entry_t));
  }
  filename_entry_t::filename_entry_t(dir_entry_t ent) {
    std::memcpy(this, &ent, sizeof(dir_entry_t));
  }

  filename_entry_t::filename_entry_t(char16_t* name, uint8_t nb) {
    std::memset(filename, 0, 30);
    std::memcpy(filename, name, nb * 2);
  }

  dir_entry_t::dir_entry_t() {
    std::memset(this, 0, sizeof(dir_entry_t));
  }
  dir_entry_t::dir_entry_t(const alloc_entry_t& entry) {
    std::memcpy(this, &entry, sizeof(dir_entry_t));
  }
  dir_entry_t::dir_entry_t(const cap_table_entry_t& entry) {
    std::memcpy(this, &entry, sizeof(dir_entry_t));
  }
  dir_entry_t::dir_entry_t(const vol_lab_entry_t& entry) {
    std::memcpy(this, &entry, sizeof(dir_entry_t));
  }
  dir_entry_t::dir_entry_t(const file_entry_t& entry) {
    std::memcpy(this, &entry, sizeof(dir_entry_t));
  }
  dir_entry_t::dir_entry_t(const stream_extension_entry_t& entry) {
    std::memcpy(this, &entry, sizeof(dir_entry_t));
  }
  dir_entry_t::dir_entry_t(const filename_entry_t& entry) {
    std::memcpy(this, &entry, sizeof(dir_entry_t));
  }

  bool dir_entry_t::is_active() const {
    return type & 0x80;
  }

  bool dir_entry_t::is_null() const {
    return type == 0;
  }

  fs_elem::fs_elem(filesys_t* filesys): fs(filesys) { }

  gen_file::gen_file(filesys_t* filesys, dir_entry_t* entries)
    : fs_elem(filesys), entry(*(file_entry_t*)entries) {
    uint8_t following = entry.secondary_count;
    std::vector<filename_entry_t> names_ent;
    bool found_ext = false;
    uint8_t filename_size;
    for (auto i = entries + 1; i != entries + following + 1; ++i) {
      if (i->type == File_name)
        names_ent.push_back((filename_entry_t&)*i);
      else if (i->type == Stream_extension) {
        found_ext = true;
        stream_extension_entry_t& str_ext = (stream_extension_entry_t&) * i;
        nofat = str_ext.seconFlags & 2;
        filename_size = str_ext.nameLen;
        filename_hash = str_ext.nameHash;
        first_clus = str_ext.first_clus;
        len = str_ext.len;
        valid_len = str_ext.valid_data_len;
      } else
        std::cerr << "unsupported sub_entry type " << i->type << std::endl;
    }
    if (!found_ext)
      std::cerr << "stream extension field not found (mandatory)" << std::endl;
    if (filename_size > names_ent.size() * 15)
      std::cerr << "not enough filename entry found (" << names_ent.size() << " found)" << std::endl;
    for (filename_entry_t const& ent : names_ent)
      for (char16_t c : ent.filename)
        filename.push_back(c);
    filename.resize(filename_size);
  }

  gen_file::gen_file(filesys_t* filesys, std::u16string const& filename, uint16_t attrib)
    : fs_elem(filesys), filename(filename) {
    uint32_t nb_name_ent = (filename.size() + 14) / 15;
    entry.secondary_count = nb_name_ent + 1;
    entry.attrib = attrib;
    changed = true;
    first_clus = 0xFFFFFFFF;
    valid_len = len = 0;
    nofat = true;
    data_read = true;
  }

  bool gen_file::flush() {
    if (!changed)
      return false;
    if (first_clus == 0xFFFFFFFF) {
      first_clus = fs->first_unused_clus();
      fs->mark_clus(first_clus, true);
    }
    if (nofat) {
      uint32_t curr_nb_clus = (std::max<std::size_t>(len, 1) + fs->bytes_per_clus() - 1)
                              / fs->bytes_per_clus();
      uint32_t req_nb_clus = (std::max<std::size_t>(required_len(), 1) + fs->bytes_per_clus() - 1)
                             / fs->bytes_per_clus();
      if (req_nb_clus > curr_nb_clus) {
        uint32_t i = first_clus + curr_nb_clus;
        for (; i < (first_clus + req_nb_clus); ++i)
          if (fs->used_clus(i)) {
            nofat = false;
            for (uint32_t j = first_clus; j < i - 1; ++j)
              fs->fat[j] = j + 1;
            fs->fat[i - 1] = 0xFFFFFFFF;
            break;
          } else
            fs->mark_clus(i, true);
      } else {
        uint32_t i = first_clus + req_nb_clus;
        for (; i < (first_clus + curr_nb_clus); ++i)
          fs->mark_clus(i, false);
      }
    }

    fs->wt_chained_clus(first_clus, data(), required_len(), !nofat);
    len = required_len();
    valid_len = len;
    changed = false;

    return true;
  }

  void gen_file::add_entries(std::vector<dir_entry_t>& dir) {
    std::vector<dir_entry_t> file_ent;
    file_ent.push_back(entry);
    std::u16string capt = fs->capitalized(filename);
    stream_extension_entry_t ent;
    ent.seconFlags = uint8_t(nofat ? 3 : 1);
    ent.nameLen = uint8_t(filename.size());
    ent.nameHash = checksum16(capt.data(), capt.size() * 2);
    ent.valid_data_len = valid_len;
    ent.first_clus = first_clus;
    ent.len = len;
    file_ent.push_back(ent);
    for (uint32_t i = 0; i < filename.size(); i += 15)
      file_ent.push_back(filename_entry_t(filename.data() + i, std::min<uint8_t>(filename.size() - i, 15)));
    entry.set_checksum = fileset_checksum(file_ent.data(), file_ent.size());
    dir.push_back(entry);
    for (auto i = file_ent.begin() + 1; i != file_ent.end(); ++i)
      dir.push_back(*i);
  }

  file_t::file_t(filesys_t* filesys, dir_entry_t* entries)
    : gen_file(filesys, entries) { }

  file_t::file_t(filesys_t* filesys, std::u16string const& filename)
    : gen_file(filesys, filename, 0x20) { }

  void file_t::read_data() {
    fdata.resize(valid_len);
    fs->rd_chained_clus(fdata.data(), first_clus, valid_len, !nofat);
  }

  uint64_t file_t::required_len() {
    return fdata.size();
  }

  char* file_t::data() {
    return fdata.data();
  }

  dir_t::dir_t(filesys_t* filesys, dir_entry_t* entries)
    : gen_file(filesys, entries) { }

  dir_t::dir_t(filesys_t* filesys, std::u16string const& dirname)
    : gen_file(filesys, dirname, 0x10) { }

  dir_t::~dir_t() {
    for (auto f : files)
      delete f;
    for (auto d : subdirs)
      delete d;
  }

  bool dir_t::flush() {
    if (!data_read)
      return false;
    entries.clear();
    for (dir_t* subd : subdirs) {
      changed |= subd->flush();
      subd->add_entries(entries);
    }
    for (file_t* f : files) {
      changed |= f->flush();
      f->add_entries(entries);
    }

    return gen_file::flush();
  }

  void dir_t::read_data() {
    files.clear();
    std::vector<dir_entry_t> entries;
    {
      std::vector<dir_entry_t> rd_buf(len / sizeof(dir_entry_t) + 1, dir_entry_t());
      fs->rd_chained_clus((char*)&rd_buf.front(), first_clus, len, !nofat);
      for (auto const& entry : rd_buf)
        if (entry.is_active())
          entries.push_back(entry);
        else if (entry.is_null())
          break;
    }

    for (std::size_t i = 0; i < entries.size(); ++i) {
      dir_entry_t& entry = entries[i];
      switch (entry.type) {
        case Alloc:
          std::cerr << "unexpected allocation table" << std::endl;
          break;
        case Cap_table:
          std::cerr << "unexpected cap's table" << std::endl;
          break;
        case Volume_label:
          std::cerr << "unexpected volume label" << std::endl;
          break;
        case File:
          if (((file_entry_t*)&entry)->attrib == 0x20)
            files.push_back(new file_t(fs, &entry));
          else if (((file_entry_t*)&entry)->attrib == 0x10)
            subdirs.push_back(new dir_t(fs, &entry));
          else
            std::cerr << "file entry not supported : " << ((file_entry_t*)&entry)->attrib;
          break;
        case Stream_extension:
        case File_name:
          break;//skip (read by file objs)
        default:
          //wtf
          std::cerr << "unknown entry type : " << entry.type << std::endl;
      }
    }
    data_read = true;
  }

  uint64_t dir_t::required_len() {
    return entries.size() * sizeof(dir_entry_t);
  }

  char* dir_t::data() {
    return (char*)entries.data();
  }

  root_dir::root_dir(filesys_t* filesys)
    : fs_elem(filesys) { }

  root_dir::~root_dir() {
    for (auto f : files)
      delete f;
    for (auto d : subdirs)
      delete d;
  }

  bool root_dir::flush() {
    if (!data_read)
      return false;
    std::vector<dir_entry_t> entries;

    entries.push_back(vol_labl_entry);
    entries.push_back(alloc_tbl_entry);
    entries.push_back(cap_table_entry);

    for (dir_t* subd : subdirs) {
      changed |= subd->flush();
      subd->add_entries(entries);
    }
    for (file_t* f : files) {
      changed |= f->flush();
      f->add_entries(entries);
    }

    if (changed) {
      fs->wt_chained_clus(first_clus, (char*)entries.data(), entries.size()*sizeof(dir_entry_t), true);
      changed = false;
      return true;
    }
    return false;
  }

  void root_dir::read_data() {
    first_clus = fs->bootsector.root;
    files.clear();
    subdirs.clear();
    std::vector<dir_entry_t> entries;
    {
      std::vector<dir_entry_t> rd_buf(fs->bytes_per_clus() / sizeof(dir_entry_t), dir_entry_t());
      for (uint32_t clus = first_clus; clus < 0xFFFFFFF8; clus = fs->fat_nxt(clus)) {
        fs->rd_clus((char*)&rd_buf.front(), clus, fs->bytes_per_clus());
        for (auto const& entry : rd_buf) {
          if (entry.is_active())
            entries.push_back(entry);
          else if (entry.is_null())
            break;
        }
      }
    }
    for (std::size_t i = 0; i < entries.size(); ++i) {
      dir_entry_t& entry = *&entries[i];
      switch (entry.type) {
        case Alloc:
          alloc_tbl_entry = entry;
          fs->alloc_bitmap.resize(alloc_tbl_entry.len);
          fs->rd_chained_clus((char*)fs->alloc_bitmap.data(), alloc_tbl_entry.first_clus, alloc_tbl_entry.len, true);
          break;
        case Cap_table:
          cap_table_entry = entry;
          fs->cap_table.resize(cap_table_entry.len / 2);
          fs->rd_chained_clus(fs->cap_table.data(), cap_table_entry.first_clus, cap_table_entry.len);
          break;
        case Volume_label:
          vol_labl_entry = entry;
          break;//ignore it?
        case File:
          if (((file_entry_t*)&entry)->attrib == 0x20)
            files.push_back(new file_t(fs, &entry));
          else if (((file_entry_t*)&entry)->attrib == 0x10)
            subdirs.push_back(new dir_t(fs, &entry));
          else
            std::cerr << "file entry not supported : " << ((file_entry_t*)&entry)->attrib;
          break;
        case Stream_extension:
        case File_name:
          break;//skip (read by file objs)
        default:
          //wtf
          std::cerr << "unknown entry type : " << int(entry.type) << std::endl;
      }
    }
    data_read = true;
  }

  filesys_t::filesys_t(std::fstream& f): fs_file(f) {
    f.seekg(0, std::ios_base::beg);
    f.read((char*)&bootsector, 512);

    f.seekg(bootsector.fat_off << bootsector.sector_shift, std::ios_base::beg);
    fat.resize((bootsector.fatLength << bootsector.sector_shift) / 4);
    f.read((char*)&fat.front(), fat.size() * 4);

//root dir
    root = new root_dir(this);
    root->read_data();
  }

  filesys_t::~filesys_t() {
    delete root;
  }

  uint32_t filesys_t::clus_byte_shift() {
    return bootsector.sector_shift + bootsector.cluster_shift;
  }

  uint64_t filesys_t::bytes_per_clus() {
    return 1ull << clus_byte_shift();
  }

  uint64_t filesys_t::clus_beg(uint32_t clus_nb) {
    uint64_t clus_st = bootsector.clusterHead_off << bootsector.sector_shift;
    return clus_st + (uint64_t(clus_nb - 2) << clus_byte_shift());
  }

  uint32_t& filesys_t::fat_nxt(uint32_t clus) {
    return fat[clus];
  }

  uint32_t filesys_t::req_fat_nxt(uint32_t clus) {
    uint32_t nxt = fat_nxt(clus);
//     std::cout << "req nxt : " << clus << ", got : " << nxt;
    if (nxt < 0xFFFFFFF8u && nxt > 1) {
//         std::cout << " good" << std::endl;
      return nxt;
    }

    nxt = first_unused_clus();
    fat_nxt(clus) = nxt;
    fat_nxt(nxt) = 0xFFFFFFFF;
    mark_clus(nxt, true);
//     std::cout << " alloc " << nxt << std::endl;
    return nxt;
  }

  void filesys_t::rd_clus(void* dest, uint32_t clus_nb, uint64_t count) {
    fs_file.seekg(clus_beg(clus_nb), std::ios_base::beg);
    fs_file.read((char*)dest, count);
  }

  void filesys_t::rd_chained_clus(void* dest, uint32_t first_clus, uint64_t len, bool fat) {
    if (fat) {
      uint64_t nb_read = 0;
      for (uint32_t clus = first_clus; clus < 0xFFFFFFF8; clus = fat_nxt(clus)) {
        uint64_t reading = std::min(len - nb_read, bytes_per_clus());
        rd_clus((char*)dest + nb_read, clus, reading);
        nb_read += reading;
      }
    } else
      rd_clus(dest, first_clus, len);
  }

  void filesys_t::wt_clus(uint32_t clus_nb, void* data, uint64_t count) {
    fs_file.seekg(clus_beg(clus_nb), std::ios_base::beg);
    fs_file.write((char*)data, count);
  }
  void filesys_t::wt_chained_clus(uint32_t first_clus, void* data, uint64_t len, bool fat) { //if !fat, allocate fat as needed
    if (fat) {
      uint64_t nb_wrote = 0;
      uint32_t clus = first_clus;
      for (; nb_wrote < len;) {
        uint64_t writting = std::min(len - nb_wrote, bytes_per_clus());
        wt_clus(clus, (char*)data + nb_wrote, writting);
        nb_wrote += bytes_per_clus();
        if (nb_wrote >= len)
          break;
        clus = req_fat_nxt(clus);
      }
      uint32_t tmp_clus = fat_nxt(clus);
      fat_nxt(clus) = 0xFFFFFFFF;
      while (tmp_clus < 0xFFFFFFF8) {
        mark_clus(tmp_clus, false);
        tmp_clus = fat_nxt(tmp_clus);
      }
    } else
      wt_clus(first_clus, data, len);
  }

  bool filesys_t::used_clus(uint32_t clus_nb) {
    if (clus_nb < 2)
      return true;
    return 1 & (alloc_bitmap[(clus_nb - 2) / 8] >> ((clus_nb - 2) & 7));
  }

  uint32_t filesys_t::first_unused_clus() {
    uint32_t i = 2;
    while (used_clus(i)) ++i;
    return i;
  }

  void filesys_t::flush() {
    if (root->flush()) {
      wt_chained_clus(root->alloc_tbl_entry.first_clus, alloc_bitmap.data(), alloc_bitmap.size(), true);
      fs_file.seekg(bootsector.fat_off << bootsector.sector_shift, std::ios_base::beg);
      fs_file.write((char*)fat.data(), fat.size() * 4);
    }
  }

  void filesys_t::mark_clus(uint32_t clus, bool used) {
    //std::cout << "marked clus " << clus << " " << used << std::endl;
    if (used)
      alloc_bitmap[(clus - 2) / 8] |= 1 << ((clus - 2) & 7);
    else
      alloc_bitmap[(clus - 2) / 8] &= ~(1 << ((clus - 2) & 7));
  }

  std::u16string filesys_t::capitalized(std::u16string const& str) {
    std::u16string ret;
    for (char16_t c : str)
      ret.push_back(cap_table[uint16_t(c)]);
    return ret;
  }


  uint32_t checksum32(const void* data, uint64_t len) {
    uint8_t const* d = reinterpret_cast<uint8_t const*>(data);
    uint32_t checksum = 0;

    for (uint64_t i = 0; i < len; ++i)
      checksum = ((checksum << 31) | (checksum >> 1)) + d[i];
    return checksum;
  }

  uint16_t checksum16(const void* data, uint64_t len) {
    uint8_t const* d = reinterpret_cast<uint8_t const*>(data);
    uint16_t checksum = 0;

    for (uint64_t i = 0; i < len; ++i)
      checksum = ((checksum << 15) | (checksum >> 1)) + d[i];
    return checksum;
  }

  uint32_t boot_region_checksum(const uint8_t* sectors, uint32_t sector_shift) {
    uint32_t byte_count = (1 << sector_shift) * 11;
    uint32_t checksum = 0;

    for (uint64_t i = 0; i < byte_count; ++i)
      if ( !((i == 106) || (i == 107) || (i == 112)) )
        checksum = ((checksum << 31) | (checksum >> 1)) + sectors[i];
    return checksum;
  }

  uint16_t fileset_checksum(const void* data, uint32_t entry_count) {
    uint32_t byte_count = entry_count * sizeof(dir_entry_t);
    uint8_t const* d = reinterpret_cast<uint8_t const*>(data);
    uint16_t checksum = 0;

    for (uint64_t i = 0; i < byte_count; ++i)
      if ( !((i == 2) || (i == 3)) )
        checksum = ((checksum << 15) | (checksum >> 1)) + d[i];
    return checksum;
  }


}
