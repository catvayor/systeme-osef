#pragma once
#include <vector>
#include <cstdint>
#include <cstring>
#include <fstream>

namespace fs {

  struct bootsector_t {
    uint8_t jmp[3] = {0xEB, 0x76, 0x90};
    uint8_t fsName[8] = {'E', 'X', 'F', 'A', 'T', ' ', ' ', ' '};
    uint8_t Zero[53] = {};
    uint64_t partition_offset = 0;
    uint64_t volume_size;
    uint32_t fat_off = 24;
    uint32_t fatLength;
    uint32_t clusterHead_off;
    uint32_t cluster_count;
    uint32_t root;
    uint32_t serial_nb;
    uint16_t revision = 0x0100;
    uint16_t volFlags = 0;
    uint8_t sector_shift;
    uint8_t cluster_shift;
    uint8_t fat_count;
    uint8_t drive_select;
    uint8_t percent_in_use;
    uint8_t reserved[7] = {};
    uint8_t bootcode[390] = {};
    uint16_t boot_signature = 0xAA55;
  };

  enum entry_type_t {
    Alloc = 0x81,
    Cap_table = 0x82,
    Volume_label = 0x83,
    File = 0x85,
    Stream_extension = 0xC0,
    File_name = 0xC1
  };

  struct dir_entry_t;

  struct alloc_entry_t {
    uint8_t type = Alloc;
    uint8_t flags = 0;
    char reserved[18] = {};
    uint32_t first_clus;
    uint64_t len;

    alloc_entry_t() = default;
    alloc_entry_t(dir_entry_t ent);
  };

  struct cap_table_entry_t {
    uint8_t type = Cap_table;
    char reserved[3] = {};
    uint32_t table_checksum;
    char reserved2[12] = {};
    uint32_t first_clus;
    uint64_t len;

    cap_table_entry_t() = default;
    cap_table_entry_t(dir_entry_t ent);
  };

  struct vol_lab_entry_t {
    uint8_t type = Volume_label;
    uint8_t char_count;
    char16_t lab_str[11];
    char reserved[8] = {};

    vol_lab_entry_t() = default;
    vol_lab_entry_t(dir_entry_t ent);
  };

  struct file_entry_t {
    uint8_t type = File;
    uint8_t secondary_count;
    uint16_t set_checksum;
    uint16_t attrib;
    char reserved[2] = {};
    uint32_t create_ts = 042;
    uint32_t modif_ts  = 42;
    uint32_t access_ts = 0x42;
    uint8_t create_10msInc = 0;
    uint8_t modif_10msInc  = 0;
    uint8_t create_TZoff   = 0;
    uint8_t modif_TZoff    = 0;
    uint8_t access_TZoff   = 0;
    char reserved2[7] = {};

    file_entry_t() = default;
    file_entry_t(dir_entry_t ent);
  };

  struct stream_extension_entry_t {
    uint8_t type = Stream_extension;
    uint8_t seconFlags;
    char reserved = 0;
    uint8_t nameLen;
    uint16_t nameHash;
    char reserved2[2] = {};
    uint64_t valid_data_len;
    char reserved3[4] = {};
    uint32_t first_clus;
    uint64_t len;

    stream_extension_entry_t() = default;
    stream_extension_entry_t(dir_entry_t ent);
  };

  struct filename_entry_t {
    uint8_t type = File_name;
    uint8_t seconFlag = 0;
    char16_t filename[15];

    filename_entry_t() = default;
    filename_entry_t(char16_t* name, uint8_t nb);
    filename_entry_t(dir_entry_t ent);
  };

  struct dir_entry_t {
    uint8_t type;
    char data[31];

    dir_entry_t();
    dir_entry_t(const alloc_entry_t& entry);
    dir_entry_t(const cap_table_entry_t& entry);
    dir_entry_t(const vol_lab_entry_t& entry);
    dir_entry_t(const file_entry_t& entry);
    dir_entry_t(const stream_extension_entry_t& entry);
    dir_entry_t(const filename_entry_t& entry);

    bool is_active() const;
    bool is_null() const;
  };

  class filesys_t;

  struct fs_elem {
    filesys_t* fs;
    bool changed = false;
    bool data_read = false;

    fs_elem(filesys_t* filesys);
    virtual ~fs_elem() = default;

    virtual bool flush() = 0;
    virtual void read_data() = 0;
  };

  struct gen_file : public fs_elem {
    bool nofat;
    uint32_t first_clus;
    uint64_t len;
    uint64_t valid_len;

    std::u16string filename;
    uint16_t filename_hash;
    file_entry_t entry;

    gen_file(filesys_t* filesys, dir_entry_t* entries);
    gen_file(filesys_t* filesys, std::u16string const& filename, uint16_t attrib);

    bool flush() override;
    void add_entries(std::vector<dir_entry_t>& dir);

    virtual uint64_t required_len() = 0;
    virtual char* data() = 0;
  };

  struct file_t : public gen_file {
    std::vector<char> fdata;

    file_t(filesys_t* filesys, dir_entry_t* entries);
    file_t(filesys_t* filesys, std::u16string const& filename);

    char* data() override;
    void read_data() override;
    uint64_t required_len() override;
  };

  struct dir_t : public gen_file {
    std::vector<file_t*> files;
    std::vector<dir_t*> subdirs;
    std::vector<dir_entry_t> entries;

    dir_t(filesys_t* filesys, dir_entry_t* entries);
    dir_t(filesys_t* filesys, std::u16string const& dirname);

    ~dir_t();

    bool flush() override;
    char* data() override;
    void read_data() override;
    uint64_t required_len() override;
  };

  struct root_dir : public fs_elem {
    std::vector<file_t*> files;
    std::vector<dir_t*> subdirs;
    uint32_t first_clus;

    alloc_entry_t alloc_tbl_entry;
    cap_table_entry_t cap_table_entry;
    vol_lab_entry_t vol_labl_entry;

    root_dir(filesys_t* filesys);

    ~root_dir();

    bool flush() override;
    void read_data() override;
  };

  struct filesys_t {
    std::fstream& fs_file;
    bootsector_t bootsector;
    std::vector<uint32_t> fat;
    std::vector<uint8_t> alloc_bitmap;
    std::vector<char16_t> cap_table;
    root_dir* root;

    filesys_t(std::fstream& f);
    ~filesys_t();

    uint32_t clus_byte_shift();
    uint64_t bytes_per_clus();
    uint64_t clus_beg(uint32_t clus_nb);
    void rd_clus(void* dest, uint32_t clus_nb, uint64_t count);
    void rd_chained_clus(void* dest, uint32_t first_clus, uint64_t len, bool fat = true);
    void wt_clus(uint32_t clus_nb, void* data, uint64_t count);
    void wt_chained_clus(uint32_t first_clus, void* data, uint64_t len, bool fat = true);
    uint32_t& fat_nxt(uint32_t clus);
    uint32_t req_fat_nxt(uint32_t clus);
    bool used_clus(uint32_t clus_nb);
    uint32_t first_unused_clus();
    void mark_clus(uint32_t clus, bool used);
    std::u16string capitalized(std::u16string const& str);

    void flush();
  };

  uint32_t checksum32(const void* data, uint64_t len);
  uint16_t checksum16(const void* data, uint64_t len);

  uint32_t boot_region_checksum(const uint8_t* sectors, uint32_t sector_shift);
  uint16_t fileset_checksum(const void* data, uint32_t entry_count);

}
