cmake_minimum_required(VERSION 3.16)

project(FSutils)

set(Dest "${CMAKE_BINARY_DIR}/fs")

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${Dest}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${Dest}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${Dest}")
set(CMAKE_COMPILE_PDB_OUTPUT_DIRECTORY "${Dest}")

add_subdirectory(fsbase)
add_subdirectory(init)
add_subdirectory(addFile)
