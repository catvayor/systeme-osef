cmake_policy(SET CMP0076 NEW)

include(${CMAKE_CURRENT_LIST_DIR}/global.cmake)

#compilers (called for ld)
set(CMAKE_CXX_COMPILER ${GXX_CROSS})
set(CMAKE_ASM_COMPILER ${GXX_CROSS})

#includes work
foreach(DIR IN LISTS SRC_INCLUDE)
    target_include_directories(${Target} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/${DIR})
endforeach()

#auto src work
set(AUTO_SRC "")

foreach(GEN IN LISTS AUTO_AS_FILES)
    set(OUT ${CMAKE_CURRENT_BINARY_DIR}/${GEN}.s)

    get_filename_component(DIR ${OUT} DIRECTORY)
    file(MAKE_DIRECTORY ${DIR})

    add_custom_command(OUTPUT ${OUT}
        COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/${GEN} > ${OUT}
        MAIN_DEPENDENCY ${GEN})
    list(APPEND AUTO_SRC ${OUT})
endforeach()

foreach(GEN IN LISTS AUTO_CXX_FILES)
    set(OUT ${CMAKE_CURRENT_BINARY_DIR}/${GEN}.cpp)

    get_filename_component(DIR ${OUT} DIRECTORY)
    file(MAKE_DIRECTORY ${DIR})

    add_custom_command(OUTPUT ${OUT}
        COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/${GEN} > ${OUT}
        MAIN_DEPENDENCY ${GEN})
    list(APPEND AUTO_SRC ${OUT})
endforeach()

#c++ flags
set(FEATURE_FLAGS "-O2 -fno-exceptions -ffreestanding -fno-rtti -nostdlib -mgeneral-regs-only -mno-red-zone")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${FEATURE_FLAGS}")

target_sources(${Target} PRIVATE ${AS_FILES} ${CXX_FILES} ${AUTO_SRC})
