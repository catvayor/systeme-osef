#include <iostream>
#include <string>

using namespace std;

int main(uint64_t argc, char* argv[]) {
  uint64_t i = 1;
  bool newline = true;
  bool escape = false;
  bool option = true;
  for (; i < argc; ++i) {
    if (option) {
      string str = argv[i];
      if (str == "-n")
        newline = false;
      else if (str == "-e")
        escape = true;
      else if (str == "-E")
        escape = false;
      else option = false;
    }
    if (!option) {
      if (escape) {
        for (char *p = argv[i]; *p; ++p) {
          if (*p == '\\') {
            switch (*++p) {
              case 'n':
                cout << '\n';
                break;
              case '\\':
                cout << '\\';
                break;
              case 'b':
                cout << '\b';
                break;
              case 'f':
                cout << '\f';
                break;
              case 't':
                cout << '\t';
                break;
              case 'v':
                cout << '\v';
                break;
              case 'r':
                cout << '\r';
                break;
              case 'c':
                return 0;
              case '\0':
                p--;
                break;
              //case 'e':
              default:
                break;
            }
          } else
            cout << *p;
        }
      } else cout << argv[i];
      if (i != argc - 1)
        cout << ' ';
    }
  }

  if (newline)
    cout << endl;

  return 0;
}