#include <syscall>
#include <iostream>

using namespace std;
using namespace Syscall;

ifstream usrin(STDUSRIN_FILENO);

bool amba_stream(istream& is) {
  string tmp;
  string line;
  getline(is, line);
  while (is.good()) {
    cout << line;
    getline(is, line);
    if (is.good() && usrin.good()) {
      getline(usrin, tmp);
    }
  }
  if (is.eof()) {
    cout << line;
  }
  cout << endl;

  return !is.bad();
}

bool amba(string name) {
  bool ok = true;

  if (name == "-") {
    if (!amba_stream(cin)) {
      cerr << "amba: error while reading from stdin" << endl;
      ok = false;
    }
  } else {
    int64_t ret = open(name.c_str(), O_RDONLY);
    if (ret < 0) {
      cerr << "amba: failed to open " << name << endl;
      ok = false;
    } else {
      ifstream file = ifstream(ret);
      if (!amba_stream(file)) {
        cerr << "amba: error while reading from " << name << endl;
        ok = false;
      }
    }
    close(ret);
  }

  return ok;
}

int64_t main(uint64_t argc, char* argv[]) {
  bool ok = true;

  const unsigned files_start = 1;

  if (argc < files_start) {
    cerr << "Usage: amba [FILES]" << endl;
    return 1;
  }

  if (argc == files_start) ok = ok && amba("-");

  for (unsigned k = files_start; k < argc; k++) {
    ok = ok && amba(string(argv[k]));
  }

  return ok ? 0 : 1;
}
