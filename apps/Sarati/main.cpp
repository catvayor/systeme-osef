#include <iostream>
#include <string>
#include <syscall>
#include <vector>
#include <windows>

struct TextWindow {
  std::vector<std::string> history;
  Windows::DBWindow window;
  unsigned cur_x, cur_y;
  Windows::Color color;
  unsigned cur_line = -1;
  static constexpr unsigned max_h = 25;
  static constexpr unsigned max_w = 80;

  TextWindow(Windows::Color c = Windows::fg_white) {
    color = c;
    history.push_back("");
    full_draw();
  }

  void full_draw() {
    unsigned win_w = window.back().w;
    unsigned win_h = window.back().h;
    if (win_w == 0 || win_h == 0) return;

    for (unsigned y = 0; y < win_h; y++)
      for (unsigned x = 0; x < win_w; x++)
        window.back().set(x, y, ' ', color);

    cur_x = 0;
    cur_y = 0;
    unsigned cur_line = 0;
    if (history.size() >= win_h) cur_line = history.size() - win_h;

    for (; cur_line < history.size(); cur_line++) {
      for (auto const& c : history[cur_line]) {
        putc(c, false);
      }
      if (cur_line + 1 != history.size()) newline();
    }
  }

  void newline() {
    unsigned win_w = window.back().w;
    unsigned win_h = window.back().h;
    if (cur_y != win_h - 1) {
      cur_y++;
      cur_x = 0;
      return;
    }
    for (unsigned y = 0; y < win_h - 1; y++)
      for (unsigned x = 0; x < win_w; x++)
        window.back().set(x, y, window.back().get(x, y + 1));
    for (unsigned x = 0; x < win_w; x++)
      window.back().set(x, cur_y, ' ', color);
    cur_x = 0;
  }

  void putc(uint8_t c, bool log = true) {
    unsigned win_w = window.back().w;
    if (c == '\n') {
      newline();
      if (log) {
        history.push_back("");
        if (history.size() == max_h * 2) {
          history.erase(history.begin(), history.begin() + max_h);
        }
      }
      return;
    }
    if (log) {
      history.back().push_back(c);
      if (history.back().size() > max_w * max_h * 2) history.back().erase(history.back().begin(), history.back().begin() + max_w * max_h);
    }
    if (cur_x == win_w)
      newline();
    window.back().set(cur_x++, cur_y, c, color);
  }

  bool update_size() {
    bool ret = window.back().update_size();
    if (ret) full_draw();
    return ret;
  }

  void flip() {
    if (!window.flip_copy()) full_draw();
  }

  void erasec() {
    if (history.back().empty()) return;
    history.back().pop_back();
    full_draw();
  }
};

inline bool is_print_custom(uint8_t c) {
  return c >= 0x20 || c == '\n' || c == '\t';
}

int64_t main(uint64_t argc, char* argv[]) {
  Syscall::fd_t display_pipe[2];
  Syscall::fd_t keyboard_pipe[2];
  Syscall::pipe(display_pipe);
  Syscall::flush_pipe(keyboard_pipe);
  Syscall::pid_t cpid = Syscall::fork();
  if (cpid != -1) {
    TextWindow twin;
    twin.flip();
    std::array<uint8_t, 256> buf;
    std::array<Syscall::fd_t, 1> poll_fds;
    std::string kbin = "";

    while (true) {
      int64_t status;
      int16_t key;
      bool changed = false;
      while ((key = Syscall::get_key()) != -1) {
        changed = true;
        bool flush = false;

        if (key & Keyboard::Ctrl) {
          switch (key & ~Keyboard::Ctrl) {
            case 'd':
              flush = true;
              break;
            case 'c':
              if (!kbin.empty()) {
                Syscall::write(keyboard_pipe[Syscall::pipe_in], kbin.data(), kbin.size());
                kbin = "";
              }
              twin.putc('^');
              twin.putc('C');
              twin.putc('\n');
              Syscall::kill(cpid, Syscall::SIGINT);
              break;
          }
        } else if ((key & ~Keyboard::Shift & ~Keyboard::AltGr) < 0x100) {
          key &= ~Keyboard::Shift;
          if (key == '\b') {
            if (!kbin.empty()) {
              twin.erasec();
              kbin.pop_back();
            }
          } else if (is_print_custom(key)) {
            twin.putc(key);
            kbin.push_back(key);
            if (key == '\n') {
              flush = true;
            }
          }
        }

        if (flush) {
          Syscall::write(keyboard_pipe[Syscall::pipe_in], kbin.data(), kbin.size());
          kbin = "";
        }
      }
      poll_fds[0] = display_pipe[Syscall::pipe_out];
      int64_t poll_ret = Syscall::pollin(poll_fds.data(), 1);
      if (poll_ret < 0) Syscall::exit(1);
      if (!twin.update_size() && poll_ret == 0) {
        if (waitpid(-1, &status, Syscall::WNOHANG) >= 0) break;
        Syscall::microsleep(10);
      } else {
        changed = true;
      }
      for (unsigned k = 0; k < poll_ret; k++) {
        if (poll_fds[k] == display_pipe[Syscall::pipe_out]) {
          int64_t read_ret = Syscall::read(display_pipe[Syscall::pipe_out], buf.data(), 256);
          for (unsigned k = 0; k < read_ret; k++) {
            twin.putc(buf[k]);
          }
        }
      }
      if (changed) twin.flip();
    }
  } else {
    Syscall::dup2(display_pipe[Syscall::pipe_in], Syscall::STDOUT_FILENO);
    Syscall::dup2(display_pipe[Syscall::pipe_in], Syscall::STDERR_FILENO);
    Syscall::dup2(keyboard_pipe[Syscall::pipe_out], Syscall::STDIN_FILENO);
    if (argc > 1) {
      Syscall::execv(argv[1], argv + 1);
      std::cout << "Failed to launch " << argv[1] << std::endl;
    } else {
      for (char i = 0; i < 16; ++i) {
        for (char j = 0; j < 16; ++j)
          std::cout << (char)(16 * i + j);
        std::cout << std::endl;
      }
      std::cout << std::endl;
    }
    std::cout << "Ctrl+D to exit" << std::endl;
    char x;
    while (std::cin.get(x).good());
  }
  return 0;
}
