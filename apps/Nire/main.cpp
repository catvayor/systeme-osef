#include <cstdlib>
#include <string>
#include <vector>
#include <windows>
#include <iostream>

using namespace std;
using namespace Syscall;

struct CenterWindow {
  Windows::DBWindow window;
  Windows::Color bg_color;
  unsigned content_w, content_h;
  std::vector<std::vector<uint16_t>> content;

  CenterWindow(unsigned w = 0, unsigned h = 0, Windows::Color c = Windows::fg_white) : bg_color(c) {
    resize_content(w, h);
    full_draw();
  }

  void resize_content(unsigned w, unsigned h) {
    content_w = w;
    content_h = h;
    content.resize(w);
    for (auto& k : content)
      k.resize(h);
  }

  void full_draw() {
    unsigned win_w = window.back().w;
    unsigned win_h = window.back().h;
    window.back().fill(bg_color);

    if (content_w > win_w || content_h > win_h) return;

    unsigned offset_x = (win_w - content_w) / 2;
    unsigned offset_y = (win_h - content_h) / 2;

    for (unsigned x = 0; x < content_w; x++) {
      for (unsigned y = 0; y < content_h; y++) {
        window.back().set(offset_x + x, offset_y + y, content[x][y]);
      }
    }
  }

  bool update_size() {
    bool ret = window.back().update_size();
    if (ret) full_draw();
    return ret;
  }

  void flip() {
    if (!window.flip_copy()) full_draw();
  }
};

Windows::Color colors[5][2] = {{Windows::bg_black, Windows::bg_pink}, {Windows::bg_dgray, Windows::bg_pink}, {Windows::bg_lgray, Windows::bg_magenta}, {Windows::bg_brown, Windows::bg_dgreen}, {Windows::bg_lblue, Windows::bg_dcyan}};

using Level = vector<string>;

struct Game {
  enum {
    none,
    wall,
    floor,
    box,
    player,
  };

  unsigned w, h;
  unsigned pos_x, pos_y;
  unsigned nb_mark;
  vector<vector<pair<int, bool>>> data;
  Level level;

  Game() {}

  void load() {
    h = level.size();
    w = level[0].size();
    nb_mark = 0;

    data.resize(w);
    for (unsigned x = 0; x < w; x++) {
      data[x].resize(h);
      for (unsigned y = 0; y < h; y++) {
        pair<int, bool> val = {none, false};
        switch (level[y][x]) {
          case 'w':
            val = {wall, false};
            break;
          case 'f':
            val = {floor, false};
            break;
          case 'F':
            val = {floor, true};
            nb_mark++;
            break;
          case 'b':
            val = {box, false};
            break;
          case 'B':
            val = {box, true};
            break;
          case 'p':
            val = {player, false};
            pos_x = x;
            pos_y = y;
            break;
        }
        data[x][y] = val;
      }
    }
  }

  Game(Level l) : level(l) {
    load();
  }

  vector<pair<int, bool>>& operator[](unsigned x) {
    return data[x];
  }

  bool input(int16_t key) {
    int dir_x = 0;
    int dir_y = 0;
    bool move = true;

    switch (key) {
      case 'r':
        load();
        return false;
      case 'q':
        return true;
      case Keyboard::K_UP:
        dir_y = -1;
        break;
      case Keyboard::K_DOWN:
        dir_y =  1;
        break;
      case Keyboard::K_LEFT:
        dir_x = -1;
        break;
      case Keyboard::K_RIGHT:
        dir_x =  1;
        break;
      default:
        move = false;
        break;
    }

    if (move) {
      bool ok = false;
      if (data[pos_x + dir_x][pos_y + dir_y].first == floor) {
        ok = true;
      } else if (data[pos_x + dir_x][pos_y + dir_y].first == box) {
        if (data[pos_x + 2 * dir_x][pos_y + 2 * dir_y].first == floor) {
          ok = true;
          data[pos_x + 2 * dir_x][pos_y + 2 * dir_y].first = box;
          if (data[pos_x + dir_x][pos_y + dir_y].second) nb_mark++;
          if (data[pos_x + 2 * dir_x][pos_y + 2 * dir_y].second) nb_mark--;
          if (nb_mark == 0) return true;
        }
      }
      if (ok) {
        data[pos_x + dir_x][pos_y + dir_y].first = player;
        data[pos_x][pos_y].first = floor;
        pos_x += dir_x;
        pos_y += dir_y;
      }
    }

    return false;
  }
};

int64_t main(uint64_t argc, char* argv[]) {
  Level level1 = {
    "  www   ",
    "  wFw   ",
    "  wfwwww",
    "wwwbfbFw",
    "wFfbpwww",
    "wwwwbw  ",
    "   wFw  ",
    "   www  ",
  };

  Level level2 = {
    "wwwww    ",
    "wpffw www",
    "wfbbw wFw",
    "wfbfwwwFw",
    "wwwffffFw",
    " wfffwffw",
    " wfffwwww",
    " wwwww   ",
  };

  Level level3 = {
    " wwwwwww  ",
    " wfffffwww",
    "wwbwwwfffw",
    "wfpfbffbfw",
    "wfFFwfbfww",
    "wwFFwfffw ",
    " wwwwwwww ",
  };

  Level level4 = {
    " wwww ",
    "wwffw ",
    "wpbfw ",
    "wwbfww",
    "wwfbfw",
    "wFbffw",
    "wFFBFw",
    "wwwwww",
  };

  Level level5 = {
    " wwwww  ",
    " wpfwww ",
    " wfbffw ",
    "wwwfwfww",
    "wFwfwffw",
    "wFbffwfw",
    "wFfffbfw",
    "wwwwwwww",
  };

  constexpr unsigned nb_levels = 5;
  Level levels[nb_levels] = {level1, level2, level3, level4, level5};

  if (argc != 2) {
    cerr << "Usage: nire [level]" << endl;
    exit(1);
  }
  if (argv[1][1] != 0 || argv[1][0] < '1' || argv[1][0] > char('0' + nb_levels)) {
    cerr << "Available levels: 1-" << nb_levels << endl;
    exit(1);
  }

  Game game(levels[argv[1][0] - '1']);

  unsigned block_w = 5;
  unsigned block_h = 3;

  CenterWindow cwin(block_w * game.w, block_h * game.h, Windows::bg_black);

  while (true) {
    int16_t key;
    while ((key = get_key()) >= 0) if (game.input(key)) exit(0);

    for (unsigned x = 0; x < game.w; x++) {
      for (unsigned y = 0; y < game.h; y++) {
        for (unsigned x2 = 0; x2 < block_w; x2++) {
          for (unsigned y2 = 0; y2 < block_h; y2++) {
            cwin.content[x * block_w + x2][y * block_h + y2] = ((uint16_t)(colors[game[x][y].first][game[x][y].second])) << 8;
          }
        }
      }
    }
    cwin.full_draw();
    cwin.flip();
  }

  return 0;
}
