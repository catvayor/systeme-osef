#include <cstdlib>
#include <iostream>
#include <syscall>
#include <string>

using namespace std;

void handler(int sig) {
  cout << "Handler! sig=" << sig << endl;
}

int64_t main(uint64_t argc, char* argv[]) {
  if (argc < 2) {
    cout << "Usage: kill [-n] pid [pid2 ...]" << endl;
    Syscall::exit(1);
  }

  int64_t arg1 = strtol(argv[1]);
  int64_t sig = Syscall::SIGKILL;
  if (arg1 < 0)
    sig = -arg1;
  else
    Syscall::kill(arg1, sig);
  for (unsigned i = 2; i < argc; ++i) {
    Syscall::pid_t pid = strtol(argv[i]);
    Syscall::kill(pid, sig);
  }

  return 0;
}
