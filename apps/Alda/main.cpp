#include <array>
#include <string>
#include <syscall>
#include <iostream>
#include <vector>

using namespace std;

array<char, 256> buf;

vector<bool> indents;

unsigned nb_dirs = 0;
unsigned nb_files = 0;

const uint8_t dash = 196;
const unsigned indent_size = 3;
const uint8_t end_connect[indent_size + 1] = {195, dash, ' ', 0};
const uint8_t end_last[indent_size + 1] = {192, dash, ' ', 0};
const uint8_t mid_blank[indent_size + 1] = {' ', ' ', ' ', 0};
const uint8_t mid_vert[indent_size + 1] = {179, ' ', ' ', 0};

void reset_indent() {
  indents.clear();
}

void new_indent() {
  indents.push_back(true);
}

void end_indent() {
  indents.pop_back();
}

void indent(bool last) {
  for (unsigned k = 0; k < indents.size() - 1; k++) {
    if (indents[k]) cout << (const char*)mid_vert;
    else cout << (const char*)mid_blank;
  }
  if (last) {
    cout << (const char*)end_last;
    indents.back() = false;
  } else cout << (const char*)end_connect;
}

void explore(string path, unsigned depth = 0) {
  if (depth == 0) {
    cout << path << endl;
    reset_indent();
    new_indent();
    return explore(path, 1);
  }
  if (Syscall::opendir(path.c_str()) < 0) {
    cout << "Error opening: " << path << endl;
    return;
  }
  int64_t tmp_ret;
  vector<string> subdirs;
  vector<string> files;
  while ((tmp_ret = Syscall::readdir(buf.data()))) {
    if (tmp_ret == Syscall::et_file) files.push_back(string(buf.data()));
    if (tmp_ret == Syscall::et_dir) subdirs.push_back(string(buf.data()));
  }

  unsigned count = subdirs.size() + files.size();

  for (auto const& name : files) {
    indent(--count == 0);
    cout << name << endl;
    nb_files++;
  }
  for (auto const& name : subdirs) {
    indent(--count == 0);
    cout << name << endl;
    new_indent();
    explore(path + name, depth + 1);
    end_indent();
    nb_dirs++;
  }
}

int64_t main() {
  explore(string("/"));
  cout << endl;
  cout << nb_dirs << " directories, " << nb_files << " files" << endl;
  return 0;
}
