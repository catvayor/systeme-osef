#include <array>
#include <cctype>
#include <iostream>
#include <string>
#include <syscall>

using namespace std;
using namespace Syscall;

template<typename T>
T pad_left(T t, size_t size, typename T::value_type with) {
  if (size > t.size())
    return T(size - t.size(), with) + t;
  else
    return t;
}

template<typename T>
T pad_right(T t, size_t size, typename T::value_type with) {
  if (size > t.size())
    return t + T(size - t.size(), with);
  else
    return t;
}

int64_t main() {
  array<char, max_io_size> buf;
  const char* path = "/proc/";
  Syscall::opendir(path);

  constexpr size_t pad_left_pid = 3;
  constexpr size_t pad_left_stat = 6;
  constexpr size_t pad_left_ppid = 4;

  cout << pad_right(string("PID"), pad_left_pid, ' ') << " "
       << pad_right(string("Status"), pad_left_stat, ' ') << " "
       << pad_right(string("PPID"), pad_left_ppid, ' ')
       << " Command" << endl;
  while (readdir(buf.data())) {
    bool only_digits = true;
    for (auto p = buf.begin(); *p != '/'; ++p) {
      if (!isdigit(*p)) {
        only_digits = false;
        break;
      }
    }
    if (only_digits) {
      int64_t fd = open(("/proc/" + string(buf.data()) + "stat").c_str(), O_RDONLY);
      if (fd >= 0) {
        ifstream file(fd);
        string pid, status, ppid, command;
        file >> pid >> status >> ppid >> command;
        close(fd);

        if (ppid == "-1") ppid = "none";

        cout << pad_left(pid, pad_left_pid, ' ') << " "
             << pad_right(status, pad_left_stat, ' ') << " "
             << pad_left(ppid, pad_left_ppid, ' ') << " "
             << command;
      }
    }
  }

  return 0;
}
