#include <syscall>
#include <iostream>

using namespace std;
using namespace Syscall;

bool match(string const& line, string const& pattern) {
  for (unsigned start = 0; start + pattern.size() <= line.size(); start++) {
    bool ok = true;
    for (unsigned k = 0; k < pattern.size(); k++) {
      if (pattern[k] != line[start + k]) {
        ok = false;
        break;
      }
    }
    if (ok) return true;
  }
  return false;
}

bool cesta_stream(istream& is, string const& pattern) {
  string line = "";
  while (getline(is, line).good()) {
    if (match(line, pattern))
      cout << line << endl;
  }
  if (is.bad()) return false;

  if (match(line, pattern) && line != "")
    cout << line << endl;
  return true;
}

bool cesta(string name, string const& pattern) {
  bool ok = true;

  if (name == "-") {
    if (!cesta_stream(cin, pattern)) {
      cerr << "cesta: error while reading from stdin" << endl;
      ok = false;
    }
  } else {
    int64_t ret = open(name.c_str(), O_RDONLY);
    if (ret < 0) {
      cerr << "cesta: failed to open " << name << endl;
      ok = false;
    } else {
      ifstream file = ifstream(ret);
      if (!cesta_stream(file, pattern)) {
        cerr << "cesta: error while reading from " << name << endl;
        ok = false;
      }
    }
    close(ret);
  }

  return ok;
}

int64_t main(uint64_t argc, char* argv[]) {
  bool ok = true;

  const unsigned files_start = 2;

  if (argc < files_start) {
    cerr << "Usage: cesta PATTERN [FILES]" << endl;
    return 1;
  }

  string pattern = argv[1];

  if (argc == files_start) ok = ok && cesta("-", pattern);

  for (unsigned k = files_start; k < argc; k++) {
    ok = ok && cesta(string(argv[k]), pattern);
  }

  return ok ? 0 : 1;
}
