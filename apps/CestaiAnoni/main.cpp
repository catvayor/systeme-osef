#include <array>
#include <syscall>
#include <iostream>

using namespace std;

int64_t main(uint64_t argc, char* argv[]) {
  array<char, Syscall::max_io_size> buf;
  const char* path;

  if (argc == 1) path = ".";
  else if (argc == 2) path = argv[1];
  else {
    cout << "Usage: ls [dir]" << endl;
    Syscall::exit(1);
  }

  if (Syscall::opendir(path) < 0) {
    cout << "No such directory: " << path << endl;
    Syscall::exit(1);
  }

  while (Syscall::readdir(buf.data()))
    cout << buf.data() << endl;

  return 0;
}
