#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include "shell.hpp"

namespace Shell {

  class Command {
   public:
    static constexpr uint8_t RED_IN  = 1;
    static constexpr uint8_t RED_OUT = 2;
    static constexpr uint8_t RED_ERR = 4;
    static constexpr uint8_t RED_APP = 8;
    struct Redirect {
      Redirect(std::string _file, uint8_t _flags = 0);
      std::string file;
      uint8_t flags;
    };

    static constexpr uint8_t SEQ_COL = 1;
    static constexpr uint8_t SEQ_AND = 2;
    static constexpr uint8_t SEQ_OR  = 4;
    static constexpr uint8_t SEQ_PIP = 8;

    Command();
    Command(const Command &c);
    Command& operator=(const Command &c) = default;
    Command(const std::vector<std::string>& _argv);
    Command(const std::vector<std::string>& _argv, const Shell::env_t& _aff);
    ~Command();
    std::string command() const;
    void redirect(Redirect r);
    void sequence(const Command cmd, uint8_t type);
    const std::vector<std::string>& get_argv() const;
    const std::vector<Redirect>& get_redirects() const;
    const Command get_seq_cmd() const;
    uint8_t get_seq_type() const;
    bool empty() const;
    const Shell::env_t& get_affects() const;
    void affect(std::pair<std::string, std::string> const&);
   private:
    std::vector<std::string> argv;
    std::vector<Redirect> redirects;
    Shell::env_t affects;
    uint8_t seq_type;
    Command* seq_cmd;
  };

}
