#pragma once

#if ! defined(yyFlexLexerOnce)
#undef yyFlexLexer
#define yyFlexLexer Shell_FlexLexer // the trick with prefix; no namespace here :(
#include <FlexLexer.h>
#endif

// Scanner method signature is defined by this macro. Original yylex() returns int.
// Sinice Bison 3 uses symbol_type, we must change returned type. We also rename it
// to something sane, since you cannot overload return type.
#undef YY_DECL
#define YY_DECL Shell::Parser::symbol_type Shell::Scanner::get_next_token()

#include "parser.hpp" // this is needed for symbol_type
#include <iostream>

namespace Shell {
  class Scanner : public yyFlexLexer {
   public:
    Scanner() {}
    Scanner(std::istream *stream): yyFlexLexer(stream) {}
    virtual ~Scanner() {}
    virtual Shell::Parser::symbol_type get_next_token();
  };
}
