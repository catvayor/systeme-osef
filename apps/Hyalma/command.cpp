#include "command.hpp"

namespace Shell {
  Command::Redirect::Redirect(std::string _file, uint8_t _flags):
    file(_file), flags(_flags) {}
  Command::Command():
    argv(1, "EMPTY COMMAND"), redirects(), seq_type(0), seq_cmd(NULL) {}
  Command::Command(const Command &c):
    argv(c.argv), redirects(c.redirects), seq_type(c.seq_type), seq_cmd(c.seq_cmd ? new Command(*c.seq_cmd) : 0) {}
  Command::Command(const std::vector<std::string>& _argv):
    argv(_argv), redirects(), affects(), seq_type(0), seq_cmd(NULL) {}
  Command::Command(const std::vector<std::string>& _argv, const Shell::env_t& _aff):
    argv(_argv), redirects(), affects(_aff), seq_type(0), seq_cmd(NULL) {}
  Command::~Command() {
    if (seq_cmd != NULL)
      delete seq_cmd;
  }

  std::string Command::command() const {
    return argv[0];
  }

  void Command::redirect(Redirect r) {
    redirects.push_back(r);
  }

  void Command::sequence(const Command cmd, uint8_t type) {
    seq_cmd = new Command(cmd);
    seq_type = type;
  }

  const std::vector<std::string>& Command::get_argv() const {
    return argv;
  };

  const std::vector<Command::Redirect>& Command::get_redirects() const {
    return redirects;
  };

  const Command Command::get_seq_cmd() const {
    return *seq_cmd;
  }
  uint8_t Command::get_seq_type() const {
    return seq_type;
  }

  bool Command::empty() const {
    return argv.size() == 0;
  }

  const Shell::env_t& Command::get_affects() const {
    return affects;
  }

  void Command::affect(const std::pair<std::string, std::string>& p) {
    affects[p.first] = p.second;
  }
}
