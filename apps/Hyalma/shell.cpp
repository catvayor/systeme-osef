#include <iostream>
#include <sstream>
#include <syscall>
#include <cstring>

#include "scanner.hpp"
#include "parser.hpp"

#include "shell.hpp"

volatile bool got_ctrl_c = false;

using namespace Syscall;

void kill_if_ctrl_c(pid_t pid) {
  if (got_ctrl_c) {
    got_ctrl_c = false;
    kill(pid, SIGINT);
  }
}

namespace Shell {
  env_t genv;
  env_t lenv;

  std::pair<std::string, std::string> parse_affect(const std::string& str) {
    auto eg = str.find('=');
    if (eg == str.end())
      return std::make_pair(str, std::string());
    else
      return std::make_pair(std::string(str.begin(), eg), std::string(eg + 1, str.end()));
  }

  std::string get_var(const std::string& str) {
    if (genv.contains(str))
      return genv[str];
    else if (lenv.contains(str))
      return lenv[str];
    else return "";
  }

  void affect_env(env_t &into, const env_t &from) {
    for (const auto& [var, val] : from)
      into[var] = val;
  }

  void affect_env(env_t &glob, env_t &loc, const env_t &from) {
    for (const auto& [var, val] : from) {
      if (glob.contains(var))
        glob[var] = val;
      else
        loc[var] = val;
    }
  }

  int64_t status_waitpid(pid_t pid) {
    int64_t status = 0;
    while (true) {
      volatile int64_t ret;
      ret = waitpid(pid, &status, 0);
      if (ret == pid) break;
      if (ret == err_intr) {
        kill_if_ctrl_c(pid);
      } else {
        std::cerr << "Hyalma: wait: ";
        switch (ret) {
          case err_fault:
            std::cerr << "page fault" << std::endl;
            break;
          case err_child:
            std::cerr << "lost child " << pid << std::endl;
            break;
          default:
            std::cerr << "error " << ret << std::endl;
            break;
        }
        return 1;
      }
    }
    return status;
  }

  void prompt() {
    int pid;
    while ((pid = waitpid(-1, NULL, WNOHANG)) >= 0) {
      std::cout << "[" << pid << "] Done" << std::endl;
    }
    std::cout << "> ";
  }

  int am(const Command& c) {
    std::vector<std::string> argv_str = c.get_argv();

    if (argv_str.size() == 1) argv_str.push_back("/");
    if (argv_str.size() > 2) {
      std::cerr << "Hyalma: usage: am [dir]" << std::endl;
      return 1;
    }
    int ret;
    if ((ret = chdir(argv_str[1].c_str()))) {
      std::cerr << "Hyalma: am: ";
      switch (ret) {
        case err_fault:
          std::cerr << "page fault" << std::endl;
          break;
        case err_notfound:
          std::cerr << "directory \"" << argv_str[1] << "\" not found" << std::endl;
          break;
        default:
          std::cerr << "error " << ret << std::endl;
          break;
      }
      return 1;
    } else {
      return 0;
    }
  }

  [[noreturn]] void exec_path(std::string execpath, const std::vector<std::string>& argv_str, const env_t &env_map) {
    std::vector<const char*> argv;
    for (size_t i = 0; i < argv_str.size(); ++i)
      argv.push_back(argv_str[i].c_str());
    argv.push_back(NULL);
    std::vector<std::string> env_str;
    std::vector<const char*> envp;
    for (auto const & [var, val] : env_map) {
      env_str.push_back(var + "=" + val);
      envp.push_back(env_str.back().c_str());
    }
    envp.push_back(NULL);
    int64_t ret;
    if (execpath.find('/') == execpath.end()) {
      const auto beg = genv["MEN"].begin();
      auto right = beg;
      auto left = beg;
      const auto end = genv["MEN"].end();
      do {
        left = right;
        while (right != end && *right != ':') right++;
        std::string cmd = std::string(left, right) + "/" + execpath + ".elf";
        ret = execve(cmd.c_str(), (char*const*)argv.data(), (char*const*)envp.data());
        left = right + 1;
      } while (right++ != end);
    } else
      ret = execve(execpath.c_str(), (char*const*)argv.data(), (char*const*)envp.data());

    std::cerr << "Hyalma: exec: ";
    switch (ret) {
      case err_fault:
        std::cerr << "page fault" << std::endl;
        break;
      case err_notfound:
        std::cerr << "\"" << execpath << "\" not found" << std::endl;
        break;
      default:
        std::cerr << "error " << ret << std::endl;
        break;
    }
    exit(1);
  }

  int exec_simple(const Command& c, bool bg, int fd_in, int fd_out, int fd_err) {
    if (c.empty()) {
      affect_env(genv, lenv, c.get_affects());
      return 0;
    } else if (c.command() == "export") {
      auto argv = c.get_argv();
      for (auto it = argv.begin() + 1; it != argv.end(); ++it)
        genv.insert_or_assign(parse_affect(*it));
      return 0;
    } else if (c.command() == "am") return am(c);
    else if (c.command() == "exit") exit(0);

    std::string cmd = c.command();
    std::string::iterator it = cmd.find('=');
    if (it != cmd.end() && it != cmd.begin()) {
      auto argv = c.get_argv();
      std::vector<std::string> v(argv.begin() + 1, argv.end());
      Command nc(v, c.get_affects());
      nc.affect(parse_affect(cmd));
      return exec_simple(nc, bg, fd_in, fd_out, fd_err);
    }

    pid_t pid = fork();
    if (pid != -1) {
      if (bg) {
        std::cout << "[" << pid << "]" << std::endl;
        return 0;
      } else {
        return status_waitpid(pid);
      }
    } else {
      if (fd_in == STDIN_FILENO && bg) close(STDIN_FILENO);
      if (fd_in != STDIN_FILENO) dup2(fd_in, STDIN_FILENO);
      if (fd_out != STDOUT_FILENO) dup2(fd_out, STDOUT_FILENO);
      if (fd_err != STDERR_FILENO) dup2(fd_err, STDERR_FILENO);

      for (auto r : c.get_redirects()) {
        int flags = 0, dupfd = STDIN_FILENO;
        if (r.flags & Command::RED_IN)
          flags |= O_RDONLY;
        else {
          flags |= O_WRONLY | O_CREAT;
          dupfd = (r.flags & Command::RED_OUT) ? STDOUT_FILENO : STDERR_FILENO;
          flags |= (r.flags & Command::RED_APP) ? O_APPEND : O_TRUNC;
        }
        int newfd = open(r.file.c_str(), flags);
        if (newfd < 0) {
          std::cerr << "Hyalma: open: ";
          switch (newfd) {
            case err_fault:
              std::cerr << "page fault" << std::endl;
              break;
            case err_notfound:
              std::cerr << "file \"" << r.file << "\" not found" << std::endl;
              break;
            default:
              std::cerr << "error " << newfd << " opening " << r.file << std::endl;
              break;
          }
          exit(1);
        }
        dup2(newfd, dupfd);
      }

      auto env = genv;
      affect_env(env, c.get_affects());
      exec_path(c.command(), c.get_argv(), env);
    }
  }

  int exec_complex(const Command& c, bool bg, int fd_in, int fd_out, int fd_err) {
    int ret;
    switch (c.get_seq_type()) {
      case Command::SEQ_AND:
        ret = exec_complex(c.get_seq_cmd(), false, fd_in, fd_out, fd_err);
        if (ret) return ret;
        return exec_simple(c, bg, fd_in, fd_out, fd_err);
      case Command::SEQ_OR:
        ret = exec_complex(c.get_seq_cmd(), false, fd_in, fd_out, fd_err);
        if (!ret) return ret;
        return exec_simple(c, bg, fd_in, fd_out, fd_err);
      case Command::SEQ_COL:
        exec_complex(c.get_seq_cmd(), false, fd_in, fd_out, fd_err);
        return exec_simple(c, bg, fd_in, fd_out, fd_err);
      case Command::SEQ_PIP:
        int64_t pipe_fds[2];
        pipe(pipe_fds); // [0] read, [1] write
        int pid = fork();
        if (pid != -1) {
          close(pipe_fds[1]);
          ret = exec_simple(c, bg, pipe_fds[0], fd_out, fd_err);
          if (bg) return 0;
          close(pipe_fds[0]);

          return status_waitpid(pid);
        } else {
          close(pipe_fds[0]);
          ret = exec_complex(c.get_seq_cmd(), bg, fd_in, pipe_fds[1], fd_out);
          close(pipe_fds[1]);
          exit(ret);
        }
    }
    return exec_simple(c, bg, fd_in, fd_out, fd_err);
  }

  int exec(const Command& c, bool bg) {
    int ret = exec_complex(c, bg, STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO);
    lenv["?"] = std::to_string(ret);
    return ret;
  }
}

std::string buf;

void ctrl_c(int) {
  got_ctrl_c = true;
}

int bigbuf_cur = 0;
int bigbuf_size = 0;
char bigbuf[4096];
bool get_bigbuf(char &c) {
  if (bigbuf_cur >= bigbuf_size) {
    int64_t nbread = read(STDIN_FILENO, bigbuf, 4096);
    if (nbread <= 0)
      return false;
    bigbuf_size = nbread;
    bigbuf_cur = 0;
  }
  c = bigbuf[bigbuf_cur++];
  return true;
}

int main([[maybe_unused]] uint64_t argc, [[maybe_unused]] char* argv[], char* envp[]) {
  while (*envp)
    Shell::genv.insert_or_assign(Shell::parse_affect(*envp++));
  if (!Shell::genv.contains("MEN"))
    Shell::genv["MEN"] = "/mnt/usr";
  Shell::lenv["?"] = "0";

  sigact sa;
  sa.sa_handler = &ctrl_c;
  sigaction(SIGINT, &sa, nullptr);

  dup2(STDIN_FILENO, STDUSRIN_FILENO);

  int res = 0;
  bool quit = false;
  while (!quit) {
    Shell::prompt();
    char c;
    while (!quit) {
      bool readok = get_bigbuf(c);
      if (got_ctrl_c) {
        got_ctrl_c = false;
        buf = "";
        bigbuf_size = 0;
        break;
      } else if (readok) {
        if (c == '\n') {
          if (!buf.empty() && buf.back() == '\\')
            buf.back() = ' ';
          else {
            buf.push_back('\n');
            break;
          }
        } else
          buf.push_back(c);
      } else
        quit = true;
    }
    if (quit)
      break;
    std::isstream stream(buf);
    Shell::Scanner scanner(&stream);
    Shell::Parser parser(&scanner);
    res = parser.parse();
    buf = "";
  }
  std::cout << "exit" << std::endl;
  return res;
}
