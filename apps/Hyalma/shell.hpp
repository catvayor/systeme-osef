#pragma once

#include <cstdlib>
#include <utility>
#include <vector>
#include <string>
#include <unordered_map>

#include "command.hpp"

namespace Shell {
  typedef std::unordered_map<std::string, std::string> env_t;
  class Command;

  int exec(const Command& c, bool bg = false);
  std::string get_var(const std::string&);
}
