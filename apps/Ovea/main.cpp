#include <iostream>
#include <syscall>

using namespace std;
using namespace Syscall;

constexpr unsigned buf_size = std::min((uint64_t)0x1000, Syscall::max_io_size);
char buf[buf_size];

bool ovea_stream(istream& is) {
  while (is.readsome(buf, buf_size).good()) {
    cout.write(buf, is.gcount());
  }
  return !is.bad();
}

bool ovea(string name) {
  bool ok = true;

  if (name == "-") {
    if (!ovea_stream(cin)) {
      cerr << "ovea: error while reading from stdin" << endl;
      ok = false;
    }
  } else {
    int64_t ret = open(name.c_str(), O_RDONLY);
    if (ret < 0) {
      cerr << "ovea: failed to open " << name << endl;
      ok = false;
    } else {
      ifstream file = ifstream(ret);
      if (!ovea_stream(file)) {
        cerr << "ovea: error while reading from " << name << endl;
        ok = false;
      }
    }
    close(ret);
  }

  return ok;
}

int64_t main(uint64_t argc, char* argv[]) {
  bool ok = true;

  const unsigned files_start = 1;

  if (argc < files_start) {
    cerr << "Usage: ovea [FILES]" << endl;
    return 1;
  }

  if (argc == files_start) ok = ok && ovea("-");

  for (unsigned k = files_start; k < argc; k++) {
    ok = ok && ovea(string(argv[k]));
  }

  return ok ? 0 : 1;
}
