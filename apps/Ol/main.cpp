#include <syscall>
#include <iostream>
#include <vector>

using namespace std;
using namespace Syscall;

bool read_stream(istream& is, vector<string>& lines) {
  string line = "";

  while (getline(is, line).good()) lines.push_back(line);

  if (is.bad()) return false;

  if (line != "") lines.push_back(line);

  return true;
}

bool read(string name, vector<string>& lines) {
  bool ok = true;

  if (name == "-") {
    if (!read_stream(cin, lines)) {
      cerr << "ol: error while reading from stdin" << endl;
      ok = false;
    }
  } else {
    int64_t ret = open(name.c_str(), O_RDONLY);
    if (ret < 0) {
      cerr << "ol: failed to open " << name << endl;
      ok = false;
    } else {
      ifstream file = ifstream(ret);
      if (!read_stream(file, lines)) {
        cerr << "ol: error while reading from " << name << endl;
        ok = false;
      }
    }
    close(ret);
  }

  return ok;
}

int64_t main(uint64_t argc, char* argv[]) {
  bool ok = true;
  vector<string> lines;

  const unsigned files_start = 1;

  if (argc < files_start) {
    cerr << "Usage: ol [FILES]" << endl;
    return 1;
  }

  if (argc == files_start) ok = ok && read("-", lines);

  for (unsigned k = files_start; k < argc; k++) {
    ok = ok && read(string(argv[k]), lines);
  }

  sort(lines.begin(), lines.end());

  for (auto const& line : lines) {
    cout << line << endl;
  }

  return ok ? 0 : 1;
}
