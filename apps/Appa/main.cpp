#define SPEAK_QUENYA

#include <iostream>
#include <syscall>

using namespace Syscall;

int main(uint64_t argc, char* argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " file" << std::endl;
    return 1;
  }

  int64_t flags = O_WRONLY | O_CREAT | O_APPEND;
  int64_t fd = open(argv[1], flags);
  if (fd >= 0)
    close(fd);
  else {
    std::cerr << "Alda: open: error opening " << argv[1] << std::endl;
    return 1;
  }

  return 0;
}