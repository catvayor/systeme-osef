default_target: os

.PHONY: astyle slides

$(VERBOSE).SILENT:

$(VERBOSE)MAKESILENT = -s

RECVERBOSE = "VERBOSE=1"
$(VERBOSE)RECVERBOSE = 

ASTYLE=astyle --style=google --indent=spaces=2 --pad-oper --pad-header --indent-switches --indent-namespaces --suffix=none --formatted --recursive --exclude=build --ignore-exclude-errors

clean: clean-os clean-slides clean-site #full clean

all: os slides site


os: build/Makefile
	make $(MAKESILENT) -j$(nproc) -C build $(RECVERBOSE)

test: build/Makefile
	make $(MAKESILENT) -j$(nproc) -C build qemu-run $(RECVERBOSE)

testusb: build/Makefile
	make $(MAKESILENT) -j$(nproc) -C build qemu-run-usb $(RECVERBOSE)

build/Makefile:
	cmake -B build

astyle:
	$(ASTYLE) "*.cpp" || true
	$(ASTYLE) "*.hpp" || true
	$(ASTYLE) "*.c" || true
	$(ASTYLE) "*.h" || true

deps:
	mkdir -p build
	echo 'set(GRAPHVIZ_CUSTOM_TARGETS true)' > build/CMakeGraphVizOptions.cmake
	cmake --graphviz=build/deps.dot -B build 

clean-os:
	rm -fr build

clean-obj:
	make $(MAKESILENT) -j$(nproc) -C build clean $(RECVERBOSE)

clean-fs:
	rm -f build/*.dd

slides: slides/makefile
	make -C slides slides.pdf

show-slides: slides
	evince slides/slides.pdf &

clean-slides:
	rm -f slides/slides.{aux,bcf,log,nav,out,pdf,run.xml,snm,toc,vrb}

md_htmls := $(shell find README | grep md | grep -v html | sed -e "s,README/,README/html/,g" -e "s,\.md,.md.html,g")
non_md_htmls := README/html/header.html README/html/footer.html

deploy-site-sylvain: site
	rsync -auv README/html/ bra:public_html/hauts-elfes/

deploy-site-lubin:
	ssh $$SAS_PERSO 'cd Documents/systeme-osef && git pull && make site'

site: ${md_htmls}

README/html/%.md.html: README/%.md ${non_md_htmls} README/make-html.sh
	README/make-html.sh $< $@

clean-site:
	rm -f README/html/*.md.html
