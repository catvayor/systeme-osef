# Hauts-elfes

Projet d'OS de Lubin Bailly, Sylvain Gay et Victor Miquel.

Le git du projet est accessible à l'addresse https://gitlab.com/catvayor/systeme-osef

N'hésitez pas à poser toute question par mail sur nos adresses ENS.

On a jugé peu pratique de faire un readme unique, il y a donc de multiples readmes dans le dossier `README`.
On a également la possibilité d'en faire une version html : `make site` puis `README/html/index.html`.

Pour compiler le projet, les dépendances sont :

* CMake
* Flex
* Bison
* Une version cross-compilateur de `g++`, target `x86_64-elf`

`cmake`, `flex` et `bison` sont supposé dans le path.
Pour le cross-compilateur, on le suppose accessible au chemin `${CROSS_PATH}/target-x86_64-elf/bin/x86_64-elf-g++` avec les crt stuff compilé avec `-mcmodel=large`.

Le script `gcc_maker.sh` fera la compilation de g++ avec tout les prérequis s'il se trouve au chemin `${CROSS_PATH}`. `gcc_maker.sh` suppose que toutes les dépendences précisées sur la page https://wiki.osdev.org/GCC_Cross-Compiler#Installing_Dependencies sont installé.

Pour lancer le script, il faut le placer dans le dossier souhaité, avec les codes sources de gcc et binutils dans les fichier `gcc.tar.xz` et `binutils.tar.xz` et executer `./gcc_maker.sh x86_64-elf`

Attention, si vous avez fait le dossier de build pour CMake avant de setup la variable d'environement `CROSS_PATH`, il doit être refait (la variable ne sert que pendant l'initialisation du dossier de build).

Le makefile du projet propose les targets suivant :

* os (défaut) : compile le projet
* test : lance le projet dans qemu (dépend de `qemu-system-x86_64`)
* clean-os
* site : génère le site pour les readmes (dépend de `python-markdown`)
* clean-site
* slides : compile nos slides de la soutenance (dépend de `pdflatex` et de quelques packages latex)
* clean-slides
* all qui fait os, site et slides
* clean qui fait les 3 cleans précédant
