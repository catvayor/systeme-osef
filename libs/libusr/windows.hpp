#pragma once

#include <array>
#include <vector>

namespace Windows {
  enum Color : uint8_t {
    fg_black   = 0x00,
    fg_dblue   = 0x01,
    fg_dgreen  = 0x02,
    fg_dcyan   = 0x03,
    fg_dred    = 0x04,
    fg_magenta = 0x05,
    fg_brown   = 0x06,
    fg_lgray   = 0x07,
    fg_dgray   = 0x08,
    fg_lblue   = 0x09,
    fg_lgreen  = 0x0a,
    fg_lcyan   = 0x0b,
    fg_lred    = 0x0c,
    fg_pink    = 0x0d,
    fg_yellow  = 0x0e,
    fg_white   = 0x0f,

    bg_black   = 0x00,
    bg_dblue   = 0x10,
    bg_dgreen  = 0x20,
    bg_dcyan   = 0x30,
    bg_dred    = 0x40,
    bg_magenta = 0x50,
    bg_brown   = 0x60,
    bg_lgray   = 0x70,
    bg_dgray   = 0x80,
    bg_lblue   = 0x90,
    bg_lgreen  = 0xa0,
    bg_lcyan   = 0xb0,
    bg_lred    = 0xc0,
    bg_pink    = 0xd0,
    bg_yellow  = 0xe0,
    bg_white   = 0xf0,
  };

  struct Buffer {
    unsigned w = 0, h = 0;
    uint16_t* data_ptr;
    std::vector<uint16_t> data;

    inline uint16_t const& get(unsigned x, unsigned y) const {
      return data[x + w * y];
    };
    inline void set(unsigned x, unsigned y, uint8_t c, Color color = fg_white) {
      data[x + w * y] = c + (((uint16_t)color) << 8);
    }
    inline void set(unsigned x, unsigned y, uint16_t v) {
      data[x + w * y] = v;
    }
    bool update_size(); // true on size change
    void show();
    void fill(Color color = fg_white);
  };

  struct DBWindow {
    unsigned k;
    std::array<Buffer, 2> bufs;

    DBWindow();
    inline Buffer& back() {
      return bufs[k];
    }
    inline Buffer const& front() const {
      return bufs[!k];
    }
    void flip();
    bool flip_copy(); // true on success
  };
}
