#include "windows.hpp"

#include <syscall>

namespace Windows {
  bool Buffer::update_size() {
    Syscall::WindowSize size = Syscall::window_get();
    bool size_change = w != size.w || h != size.h;
    if (size_change) {
      w = size.w;
      h = size.h;
      data_ptr = nullptr;
      data.resize(w * h);
    }
    return size_change;
  }

  void Buffer::show() {
    data_ptr = data.data();
    Syscall::window_refresh((Syscall::WindowData*)this);
  }

  void Buffer::fill(Color color) {
    uint16_t code = ((uint16_t)color) << 8;
    for (auto& k : data)
      k = code;
  }

  DBWindow::DBWindow() {
    k = 0;
    back().update_size();
  }

  void DBWindow::flip() {
    back().show();
    k = !k;
    back().update_size();
  }

  bool DBWindow::flip_copy() {
    flip();
    if (front().w == back().w && front().h == back().h) {
      back().data = front().data;
      return true;
    }
    return false;
  }
}
