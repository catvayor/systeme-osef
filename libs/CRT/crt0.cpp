#include <__malloc>
#include <syscall>

struct MallocData {
  static uintptr_t heap_start;
  static uintptr_t heap_end;
  void operator()(void*, uint64_t delta) {
    Syscall::sbrk(delta);
  }
};

uintptr_t MallocData::heap_start;
uintptr_t MallocData::heap_end;

void __init_malloc(void* start, uintptr_t size) {
  __malloc::init_malloc<MallocData>(start, size);
}

[[nodiscard]] void* malloc(uint64_t size) {
  return __malloc::malloc<MallocData>(size);
}

[[nodiscard]] void* aligned_alloc(uint64_t alignment, uint64_t size) {
  return __malloc::aligned_alloc<MallocData>(alignment, size);
}

void free(void* addr) {
  __malloc::free<MallocData>(addr);
}

[[nodiscard]] void* realloc(void* ptr, std::size_t size) {
  return __malloc::realloc<MallocData>(ptr, size);
}

void *operator new (std::size_t size) {
  return malloc(size);
}

void *operator new[](std::size_t size) {
  return malloc(size);
}

void operator delete (void *p) {
  free(p);
}

void operator delete[](void *p) {
  free(p);
}

void operator delete (void *p, std::size_t) {
  free(p);
}

void operator delete[](void *p, std::size_t) {
  free(p);
}

extern "C" void _init();
extern "C" void _fini();
extern "C" void __cxa_finalize(void(*f)(void*));

typedef void (*func_ptr)(void);
extern "C" func_ptr __init_array_start;
extern "C" func_ptr __init_array_end;
extern "C" func_ptr __fini_array_start;
extern "C" func_ptr __fini_array_end;

void __arr_init(void) {
  for ( func_ptr* func = &__init_array_start; func != &__init_array_end; func++ )
    (*func)();
}

void __arr_fini(void) {
  for ( func_ptr* func = &__fini_array_start; func != &__fini_array_end; func++ )
    (*func)();
}
extern int64_t main(uint64_t argc, char* argv[], char* envp[]);

char **environ;

extern "C" void _start(uint64_t argc, char* argv[], char* envp[]) {
  environ = envp;
  void* heap = (void*)Syscall::sbrk(72);
  __init_malloc(heap, 72);
  __arr_init();
  _init();
  int64_t ret = main(argc, argv, envp);
  _fini();
  __arr_fini();
  __cxa_finalize(nullptr);
  Syscall::exit(ret);
}
