#define ATEXIT_MAX_FUNCS 128

#include <cstdint>

struct __cxa_atexit_data_struct {
  void(*fun)(void*);
  void* obj;
  void* dso;
  void call() {
    if (fun) {
      fun(obj);
      fun = nullptr;
    }
  }
};

unsigned __cxa_atexit_count = 0;

__cxa_atexit_data_struct __cxa_atexit_data[ATEXIT_MAX_FUNCS];

extern "C" int __cxa_atexit(void (*f)(void*), void* arg, void* dso) {
  if (__cxa_atexit_count < ATEXIT_MAX_FUNCS) {
    __cxa_atexit_data[__cxa_atexit_count++] = {
      .fun = f,
      .obj = arg,
      .dso = dso,
    };
    return 0;
  }
  return -1;
}
extern "C" void __cxa_finalize(void(*f)(void*)) {
  unsigned i = __cxa_atexit_count;
  if (!f) {
    while (i--)
      __cxa_atexit_data[i].call();
    __cxa_atexit_count = 0;
    return;
  }
  while (i--)
    if (__cxa_atexit_data[i].fun == f) {
      __cxa_atexit_data[i].call();
      while (++i < __cxa_atexit_count)
        __cxa_atexit_data[i - 1] = __cxa_atexit_data[i];
      --__cxa_atexit_count;
      return;
    }
}

extern "C" void __cxa_pure_virtual() {
  uintptr_t virt_caller = 0;
  asm("movq 8(%%rbp), %0" : "=r"(virt_caller));
  asm("ud2");
}
