execute_process(COMMAND ${CMAKE_CXX_COMPILER} ${CMAKE_CXX_FLAGS} -print-file-name=crtbegin.o OUTPUT_VARIABLE CRT_BEG)
execute_process(COMMAND ${CMAKE_CXX_COMPILER} ${CMAKE_CXX_FLAGS} -print-file-name=crtend.o OUTPUT_VARIABLE CRT_END)
string(STRIP "${CRT_BEG}" CRT_BEG)
string(STRIP "${CRT_END}" CRT_END)
set(CRTI crti.s.o)
set(CRTN crtn.s.o)
set(CRT0 crt0.cpp.o)
set(CXA cxabi.cpp.o)

set_source_files_properties(${CRT_BEG} PROPERTIES GENERATED TRUE EXTERNAL_OBJECT TRUE)
set_source_files_properties(${CRT_END} PROPERTIES GENERATED TRUE EXTERNAL_OBJECT TRUE)
set_source_files_properties(${CRTI} PROPERTIES GENERATED TRUE)
set_source_files_properties(${CRTN} PROPERTIES GENERATED TRUE)
set_source_files_properties(${CRT0} PROPERTIES GENERATED TRUE)
set_source_files_properties(${CXA} PROPERTIES GENERATED TRUE)

add_dependencies(${Target} CRT)
add_custom_command(TARGET ${Target} PRE_LINK COMMAND bash -c
    "for i in $(echo '$<TARGET_OBJECTS:CRT>' | tr '[;]' ' ');\
        do ${CMAKE_COMMAND} -E copy_if_different $i .;\
    done"
    DEPENDS $<TARGET_OBJECTS:CRT> VERBATIM)
add_link_dependency("$<TARGET_OBJECTS:CRT>")
