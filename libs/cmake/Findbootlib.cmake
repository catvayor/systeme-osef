
target_include_directories(${Target} PRIVATE ${CMAKE_CURRENT_LIST_DIR}/../bootlib/include)

target_link_libraries(${Target} PRIVATE bootlib)
