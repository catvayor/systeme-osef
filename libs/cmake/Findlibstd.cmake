
target_include_directories(${Target} PRIVATE ${CMAKE_CURRENT_LIST_DIR}/../libstd/include)

target_link_libraries(${Target} PRIVATE libstd)
