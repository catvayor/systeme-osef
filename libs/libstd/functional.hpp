#pragma once
#include <cstddef>
#include <utility>

namespace std {

// ////////////////////////////// arithmetic
  template<typename T = void>
  struct plus {
    constexpr T operator()(const T& lhs, const T& rhs) const {
      return lhs + rhs;
    }
  };
  template<>
  struct plus<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs + rhs) {
      return lhs + rhs;
    }
  };

  template<typename T = void>
  struct minus {
    constexpr T operator()(const T& lhs, const T& rhs) const {
      return lhs - rhs;
    }
  };
  template<>
  struct minus<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs - rhs) {
      return lhs - rhs;
    }
  };

  template<typename T = void>
  struct multiplies {
    constexpr T operator()(const T& lhs, const T& rhs) const {
      return lhs * rhs;
    }
  };
  template<>
  struct multiplies<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs * rhs) {
      return lhs * rhs;
    }
  };

  template<typename T = void>
  struct divides {
    constexpr T operator()(const T& lhs, const T& rhs) const {
      return lhs / rhs;
    }
  };
  template<>
  struct divides<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs / rhs) {
      return lhs / rhs;
    }
  };

  template<typename T = void>
  struct modulus {
    constexpr T operator()(const T& lhs, const T& rhs) const {
      return lhs % rhs;
    }
  };
  template<>
  struct modulus<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs % rhs) {
      return lhs % rhs;
    }
  };

  template<typename T = void>
  struct negate {
    constexpr T operator()(const T& arg) const {
      return -arg;
    }
  };
  template<>
  struct negate<void> {
    template<typename T>
    constexpr auto operator()(const T& arg) const
    -> decltype(-arg) {
      return -arg;
    }
  };

// ////////////////////////////// comparison

  template<typename T = void>
  struct equal_to {
    constexpr bool operator()(const T& lhs, const T& rhs) const {
      return lhs == rhs;
    }
  };
  template<>
  struct equal_to<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs == rhs) {
      return lhs == rhs;
    }
  };

  template<typename T = void>
  struct not_equal_to {
    constexpr bool operator()(const T& lhs, const T& rhs) const {
      return lhs != rhs;
    }
  };
  template<>
  struct not_equal_to<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs != rhs) {
      return lhs != rhs;
    }
  };

  template<typename T = void>
  struct greater {
    constexpr bool operator()(const T& lhs, const T& rhs) const {
      return lhs > rhs;
    }
  };
  template<>
  struct greater<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs > rhs) {
      return lhs > rhs;
    }
  };

  template<typename T = void>
  struct less {
    constexpr bool operator()(const T& lhs, const T& rhs) const {
      return lhs < rhs;
    }
  };
  template<>
  struct less<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs < rhs) {
      return lhs < rhs;
    }
  };

  template<typename T = void>
  struct greater_equal {
    constexpr bool operator()(const T& lhs, const T& rhs) const {
      return lhs >= rhs;
    }
  };
  template<>
  struct greater_equal<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs >= rhs) {
      return lhs >= rhs;
    }
  };

  template<typename T = void>
  struct less_equal {
    constexpr bool operator()(const T& lhs, const T& rhs) const {
      return lhs <= rhs;
    }
  };
  template<>
  struct less_equal<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs <= rhs) {
      return lhs <= rhs;
    }
  };

// ////////////////////////////// logical

  template<typename T = void>
  struct logical_and {
    constexpr bool operator()(const T& lhs, const T& rhs) const {
      return lhs && rhs;
    }
  };
  template<>
  struct logical_and<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs && rhs) {
      return lhs && rhs;
    }
  };

  template<typename T = void>
  struct logical_or {
    constexpr bool operator()(const T& lhs, const T& rhs) const {
      return lhs || rhs;
    }
  };
  template<>
  struct logical_or<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs || rhs) {
      return lhs || rhs;
    }
  };

  template<typename T = void>
  struct logical_not {
    constexpr bool operator()(const T& arg) const {
      return !arg;
    }
  };
  template<>
  struct logical_not<void> {
    template<typename T>
    constexpr auto operator()(const T& arg) const
    -> decltype(!arg) {
      return !arg;
    }
  };

// ////////////////////////////// bitwise

  template<typename T = void>
  struct bit_and {
    constexpr T operator()(const T& lhs, const T& rhs) const {
      return lhs & rhs;
    }
  };
  template<>
  struct bit_and<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs & rhs) {
      return lhs & rhs;
    }
  };

  template<typename T = void>
  struct bit_or {
    constexpr T operator()(const T& lhs, const T& rhs) const {
      return lhs | rhs;
    }
  };
  template<>
  struct bit_or<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs | rhs) {
      return lhs | rhs;
    }
  };

  template<typename T = void>
  struct bit_xor {
    constexpr T operator()(const T& lhs, const T& rhs) const {
      return lhs ^ rhs;
    }
  };
  template<>
  struct bit_xor<void> {
    template<typename T, typename U>
    constexpr auto operator()(const T& lhs, const U& rhs) const
    -> decltype(lhs ^ rhs) {
      return lhs ^ rhs;
    }
  };

  template<typename T = void>
  struct bit_not {
    constexpr T operator()(const T& arg) const {
      return ~arg;
    }
  };
  template<>
  struct bit_not<void> {
    template<typename T>
    constexpr auto operator()(const T& arg) const
    -> decltype(~arg) {
      return ~arg;
    }
  };

// ///////////////////////////// other
  template<typename K>
  struct hash {
    hash() = delete;
    hash(const hash&) = delete;
    hash(hash&&) = delete;
  };

  template<typename T>
  struct reference_wrapper {
    T* m_ptr;

    reference_wrapper(T& ref): m_ptr(&ref) {}

    T& get() {
      return *m_ptr;
    }

    operator T&() {
      return get();
    }

    template<typename... Args>
    auto operator()(Args&&... args) ->
    decltype(get()(std::forward<Args>(args)...)) {
      return get()(std::forward<Args>(args)...);
    }
  };

}

#include "hash.hpp"
