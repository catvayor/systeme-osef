#pragma once

#include <cstdlib>

inline void assert(int x) {
  if (!x)
    abort();
}
