#pragma once

#include <cstdint>
#include <iostream>

namespace std {
  typedef int64_t FILE;
  extern FILE* stdin;
  extern FILE* stdout;
  extern FILE* stderr;

  static inline int fprintf(FILE *stream, const char *format) {
    ofstream out(*stream);
    out << format;
    return 0;
  }

  template<typename T, typename... Targs>
  int fprintf(FILE *stream, const char *format, T value, Targs... args) {
    ofstream out(*stream);
    for (; *format; format++) {
      if (*format == '%') {
        switch (format[1]) {
          case 's':
          case 'i':
          case 'd':
            out << value;
            return fprintf(stream, format + 2, args...);
          case '%':
            break;
          default:
            return -1;
        }
      }
      out << *format;
    }
    return -2;
  }

  template<typename... Targs>
  int printf(const char* format, Targs... args) {
    return fprintf(stdout, format, args...);
  }
}

using std::stdin;
using std::stdout;
using std::stderr;
using std::fprintf;
