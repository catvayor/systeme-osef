#pragma once
#include <cstddef>
#include <type_traits>
#include <utility>
#include <new>

extern void* malloc(size_t size);
extern void free(void* addr);
extern void* realloc(void* addr, size_t new_size);

namespace std {

  template<typename T, typename Type, typename = void>
  struct __diff_type {
    typedef T type;
  };
  template<typename T, typename Type>
  struct __diff_type<T, Type, void_t<typename Type::difference_type> > {
    typedef Type::difference_type type;
  };

  template<typename T, typename Ptr, typename = void>
  struct __ptr_rebind {
    typedef __replace_first_arg_t<Ptr, T> type;
  };
  template<typename T, typename Ptr>
  struct __ptr_rebind<T, Ptr, void_t<typename Ptr::template rebind<T>::other> > {
    typedef typename Ptr::template rebind<T>::other type;
  };

  template<typename Ptr>
  struct pointer_traits {
    typedef Ptr pointer;
    typedef typename __diff_type<ptrdiff_t, Ptr>::type difference_type;

    template<typename T>
    using rebind = typename __ptr_rebind<T, Ptr>::type;
  };

  template<typename T>
  struct pointer_traits<T*> {
    typedef T* pointer;
    typedef ptrdiff_t difference_type;

    template<typename U>
    using rebind = U*;
  };


  template<typename T, typename Alloc, typename = void>
  struct __pointer {
    typedef T type;
  };
  template<typename T, typename Alloc>
  struct __pointer<T, Alloc, void_t<typename Alloc::pointer> > {
    typedef Alloc::pointer type;
  };

  template<typename T, typename Alloc, typename = void>
  struct __cpointer {
    typedef T type;
  };
  template<typename T, typename Alloc>
  struct __cpointer<T, Alloc, void_t<typename Alloc::const_pointer> > {
    typedef Alloc::const_pointer type;
  };

  template<typename T, typename Alloc, typename = void>
  struct __vpointer {
    typedef T type;
  };
  template<typename T, typename Alloc>
  struct __vpointer<T, Alloc, void_t<typename Alloc::void_pointer> > {
    typedef Alloc::void_pointer type;
  };

  template<typename T, typename Alloc, typename = void>
  struct __cvpointer {
    typedef T type;
  };
  template<typename T, typename Alloc>
  struct __cvpointer<T, Alloc, void_t<typename Alloc::const_void_pointer> > {
    typedef Alloc::const_void_pointer type;
  };

  template<typename T, typename Alloc, typename = void>
  struct __size {
    typedef T type;
  };
  template<typename T, typename Alloc>
  struct __size<T, Alloc, void_t<typename Alloc::size_type> > {
    typedef Alloc::size_type type;
  };

  template<typename T, typename Alloc, typename = void>
  struct __alloc_rebind {
    typedef __replace_first_arg_t<Alloc, T> type;
  };
  template<typename T, typename Alloc>
  struct __alloc_rebind<T, Alloc, void_t<typename Alloc::template rebind<T>::other> > {
    typedef typename Alloc::template rebind<T>::other type;
  };

  template<typename Alloc>
  struct allocator_traits {

    typedef Alloc allocator_type;
    typedef Alloc::value_type value_type;
    typedef typename __pointer<value_type*, Alloc>::type pointer;
    typedef typename __cpointer<typename pointer_traits<pointer>::rebind<const value_type>, Alloc>::type const_pointer;
    typedef typename __vpointer<typename pointer_traits<pointer>::rebind<void>, Alloc>::type void_pointer;
    typedef typename __cvpointer<typename pointer_traits<pointer>::rebind<const void>, Alloc>::type const_void_pointer;
    typedef typename __diff_type<typename pointer_traits<pointer>::difference_type, Alloc>::type difference_type;
    typedef typename __diff_type<make_unsigned_t<difference_type>, Alloc>::type size_type;

//         typedef Alloc::propagate_on_container_move_assignment propagate_on_container_move_assignment;
//         typedef Alloc::propagate_on_container_copy_assignment propagate_on_container_copy_assignment;
//         typedef Alloc::propagate_on_container_swap propagate_on_container_swap;
//         typedef Alloc::is_always_equal is_always_equal;

    template<typename T>
    using rebind_alloc = typename __alloc_rebind<T, Alloc>::type;
    template<typename T>
    using rebind_traits = allocator_traits<rebind_alloc<T> >;

    static pointer allocate(Alloc& al, size_type n) {
      return al.allocate(n);
    }

    static void deallocate(Alloc& al, pointer ptr, size_type n) {
      al.deallocate(ptr, n);
    }

    template<typename... Args>
    static void construct(Alloc&, pointer p, Args&&... args) {
      new (static_cast<void*>(p)) value_type(std::forward<Args>(args)...);
    }

    static void destruct(Alloc&, pointer p) {
      p->~value_type();
    }
  };

  template<typename T>
  struct allocator {
    typedef T value_type;

    allocator() = default;
    template<typename U>
    allocator([[maybe_unused]] const allocator<U>& other) { }
    template<typename U>
    allocator([[maybe_unused]] allocator<U>&& other) { }

    [[nodiscard]] T* allocate(std::size_t nb) {
      return (T*)malloc(nb * sizeof(T));
    }

    void deallocate(T* ptr, std::size_t) {
      free(ptr);
    }
  };
}
