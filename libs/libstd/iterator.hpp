#pragma once
#include <cstddef>
#include <type_traits>

namespace std {

  template<typename Iter>
  struct iterator_traits {
    using difference_type = typename Iter::difference_type;
    using value_type = typename Iter::value_type;
    using pointer = typename Iter::pointer;
    using reference = typename Iter::reference;
    //using iterator_category = typename Iter::iterator_category;
  };

  template<typename T>
  struct iterator_traits<T*> {
    using difference_type = ptrdiff_t;
    using value_type = remove_cv_t<T>;
    using pointer = T*;
    using reference = T&;
    //using iterator_category = typename Iter::iterator_category;
  };

  template<class Iter> class reverse_iterator {
   public:
    typedef Iter iterator_type;
    typedef iterator_traits<Iter>::iterator_category iterator_category;
    typedef iterator_traits<Iter>::value_type value_type;
    typedef iterator_traits<Iter>::difference_type difference_type;
    typedef iterator_traits<Iter>::pointer pointer;
    typedef iterator_traits<Iter>::reference reference;

   private:
    Iter m_it;

   public:
    reverse_iterator() {}
    reverse_iterator(Iter it): m_it(it) {}
    reverse_iterator(const reverse_iterator<Iter>& rev_it): m_it(rev_it.m_it) {}
    Iter base() const {
      return m_it;
    }
    reference operator*() const {
      return *m_it;
    }
    reverse_iterator operator+(difference_type n) const {
      return reverse_iterator(m_it - n);
    }
    reverse_iterator& operator++() {
      --m_it;
      return *this;
    }
    reverse_iterator operator++(int) {
      reverse_iterator tmp = *this;
      m_it--;
      return tmp;
    }
    reverse_iterator& operator+=(difference_type n) {
      m_it -= n;
      return *this;
    }
    reverse_iterator operator-(difference_type n) const {
      return reverse_iterator(m_it + n);
    }
    reverse_iterator& operator--() {
      ++m_it;
      return *this;
    }
    reverse_iterator operator--(int) {
      reverse_iterator tmp = *this;
      m_it++;
      return tmp;
    }
    reverse_iterator& operator-=(difference_type n) {
      m_it += n;
      return *this;
    }
    pointer operator->() const {
      return &(operator*());
    }
    reference operator[](difference_type n) const {
      return base()[-n - 1];
    }
  };
}
