#pragma once
#include <cstddef>

[[nodiscard]] void* operator new (std::size_t, void* ptr);

[[nodiscard]] void* operator new[](std::size_t, void* ptr);
