#pragma once

#include <syscall>
#include <cerrno>

#include "sys/wait.h"
#include "sys/types.h"
#include "unistd.h"
#include "fcntl.h"
