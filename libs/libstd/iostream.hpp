#pragma once

#include <cstddef>
#include <string>

namespace std {
  constexpr const char* endl = "\n";

  typedef int64_t streamsize;
  typedef uint8_t iostate;

  class streambuf {
   public:
    virtual streamsize sgetn(char*, streamsize) = 0;
    virtual streamsize sputn(const char*, streamsize) = 0;
  };

  class fstreambuf: public streambuf {
   private:
    uint64_t m_fd;
   public:
    fstreambuf(uint64_t fd);
    streamsize sgetn(char*, streamsize);
    streamsize sputn(const char*, streamsize);
  };

  class ios {
   protected:
    streambuf *m_sb;

    uint8_t state;

   public:
    static constexpr iostate goodbit = 0;
    static constexpr iostate eofbit  = 1 << 0;
    static constexpr iostate failbit = 1 << 1;
    static constexpr iostate badbit  = 1 << 2;

    explicit ios(uint64_t fd);
    explicit ios(streambuf*);

    void clear(iostate _state = goodbit);
    bool good() const;
    bool eof() const;
    bool fail() const;
    bool bad() const;
    bool operator!() const;
    explicit operator bool() const;

    streambuf* rdbuf();
    streambuf* rdbuf(streambuf*);
  };

  class istream : public ios {
    streamsize read_count;

   public:
    //explicit istream(uint64_t fd);
    explicit istream(streambuf*);

    streamsize gcount() const;
    istream& get(char&);
    istream& read(char*, streamsize);
    istream& readsome(char*, streamsize);
    istream& operator>>(std::string &str);
  };

  class ostream : public ios {
   public:
    //explicit ostream(uint64_t fd);
    explicit ostream(streambuf*);

    ostream& put(char c);
    ostream& write(const char*, streamsize);

    ostream& operator<<(char);
    ostream& operator<<(char const*);
    ostream& operator<<(string);
    ostream& operator<<(int32_t);
    ostream& operator<<(int64_t);
    ostream& operator<<(uint32_t);
    ostream& operator<<(uint64_t);
  };

  istream& getline(istream& is, string& str, char delim = '\n');

  class ifstream : public istream {
    fstreambuf m_fsb;
   public:
    ifstream(uint64_t fd): istream(&m_fsb), m_fsb(fd) {}
  };
  class ofstream : public ostream {
    fstreambuf m_fsb;
   public:
    ofstream(uint64_t fd): ostream(&m_fsb), m_fsb(fd) {}
  };

  extern ifstream cin;
  extern ofstream cout;
  extern ofstream cerr;
}
