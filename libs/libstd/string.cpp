#include "string.hpp"
#include <stack>

namespace std {
  string generic_to_string(uint64_t x, bool neg) {
    string ret = "";
    stack<char> st;

    if (neg) ret.push_back('-');

    do {
      st.push("0123456789"[x % 10]);
      x /= 10;
    } while (x);

    while (!st.empty()) ret.push_back(*st.pop());
    return ret;
  }

  string to_string(int32_t x) {
    return x < 0 ? generic_to_string(-x, true) : generic_to_string(x, false);
  }

  string to_string(int64_t x) {
    return x < 0 ? generic_to_string(-x, true) : generic_to_string(x, false);
  }

  string to_string(uint32_t x) {
    return generic_to_string(x, false);
  }

  string to_string(uint64_t x) {
    return generic_to_string(x, false);
  }
}
