#pragma once

#include <optional>
#include <vector>

namespace std {

  template<typename index_t, template<typename U> class TemplateAlloc>
  class chains {
   public:
    struct link {
      index_t prev;
      index_t next;
      link() {};
      link(index_t i) : prev(i), next(i) {}
    };

    class iterator {
      index_t cur;
      chains* ch;

     public:
      iterator() : ch(nullptr) {}
      iterator(index_t c, chains* p) : cur(c), ch(p) {}

      bool operator==(iterator const& other) {
        return ch == other.ch && cur == other.cur;
      }

      index_t operator*() {
        return cur;
      }

      iterator& operator++() {
        cur = ch->get(cur).next;
        return *this;
      }

      iterator& operator--() {
        cur = ch->get(cur).prev;
        return *this;
      }

      bool is_elem() {
        return cur <= max_index;
      }

      bool is_entry() {
        return cur > max_index;
      }
    };

    class chain_entry {
      static constexpr index_t cpl(index_t k) {
        return ~k;
      }
      index_t e_id;
      chains* ch;

     public:
      chain_entry(index_t i, chains* p) : e_id(i), ch(p) {}

      iterator begin() {
        return iterator(ch->get(e_id).next, ch);
      }

      iterator end() {
        return iterator(e_id, ch);
      }
    };

   private:
    static constexpr index_t cpl(index_t k) {
      return ~k;
    }
    static constexpr index_t max_index = (cpl(0)) >> 1;
    static_assert(index_t(0) < max_index, "index_t must be unsigned");

    vector<link, TemplateAlloc<link> > entry;
    vector<link, TemplateAlloc<link> > elem;

    link& get(index_t k) {
      if (k > max_index) return entry[cpl(k)];
      else return elem[k];
    }

    void pop(index_t prev, index_t k, index_t next) {
      get(prev).next = get(k).next;
      get(next).prev = get(k).prev;
      get(k).next = k;
      get(k).prev = k;
    }

    void push(index_t prev, index_t k, index_t next) {
      get(k).next = get(prev).next;
      get(k).prev = get(next).prev;
      get(prev).next = k;
      get(next).prev = k;
    }

    void add_entry() {
      entry.push_back(link(cpl(entry.size())));
    }

    void del_entry() {
      clear_entry(entry.size() - 1);
      entry.pop_back();
    }

    void add_elem() {
      elem.push_back(link(elem.size()));
    }

    void add_elem_attach(index_t e) {
      elem.push_back(link(elem.size()));
      push_back(e, elem.size() - 1);
    }

    void del_elem() {
      pop_elem(elem.size() - 1);
      elem.pop_back();
    }

   public:
    chains(size_t nb_entries = 0, size_t nb_elems = 0) {
      resize_entries(nb_entries);
      resize_elems(nb_elems);
    }

    void fit_entry(index_t last) {
      if (last > max_index) asm("ud2");
      while (entry.size() <= last) add_entry();
    }

    void resize_entries(size_t newsize) {
      if (entry.size() < newsize) fit_entry(newsize - 1);
      while (entry.size() > newsize) del_entry();
    }

    void fit_elem(index_t last) {
      if (last > max_index) asm("ud2");
      while (elem.size() <= last) add_elem();
    }

    void resize_elems(size_t newsize) {
      if (elem.size() < newsize) fit_elem(newsize - 1);
      while (elem.size() > newsize) del_elem();
    }

    void fit_elem_attach(index_t last, index_t e) {
      if (last > max_index) asm("ud2");
      while (elem.size() <= last) add_elem_attach(e);
    }

    void resize_elems_attach(size_t newsize, index_t e) {
      if (elem.size() < newsize) fit_elem_attach(newsize - 1, e);
      while (elem.size() > newsize) del_elem();
    }

    bool empty(index_t e) {
      return entry[e].next == cpl(e);
    }

    index_t front(index_t e) {
      return entry[e].next;
    }

    index_t back(index_t e) {
      return entry[e].prev;
    }

    bool is_free(index_t x) {
      return elem[x].next == x;
    }

    void pop_elem(index_t x) {
      pop(elem[x].prev, x, elem[x].next);
    }

    void push_after(index_t pos, index_t x) {
      if (!is_free(x)) pop_elem(x);
      push(pos, x, elem[pos].next);
    }

    void push_before(index_t pos, index_t x) {
      if (!is_free(x)) pop_elem(x);
      push(elem[pos].prev, x, pos);
    }

    optional<index_t> pop_front(index_t e) {
      index_t x = front(e);
      if (x > max_index) return optional<index_t>();
      pop_elem(x);
      return optional<index_t>(x);
    }

    optional<index_t> pop_back(index_t e) {
      index_t x = back(e);
      if (x > max_index) return optional<index_t>();
      pop_elem(x);
      return optional<index_t>(x);
    }

    void push_front(index_t e, index_t x) {
      if (!is_free(x)) pop_elem(x);
      push(cpl(e), x, entry[e].next);
    }

    void push_back(index_t e, index_t x) {
      if (!is_free(x)) pop_elem(x);
      push(entry[e].prev, x, cpl(e));
    }

    void clear_entry(index_t e) {
      while (pop_back(e));
    }

    size_t get_size(index_t e) {
      size_t ret = 0;
      index_t cur = cpl(e);
      while ((cur = get(cur).next) <= max_index) ret++;
      return ret;
    }

    void append(index_t e_src, index_t e_dst) {
      if (empty(e_src)) return;

      index_t a = entry[e_src].next;
      index_t b = entry[e_src].prev;
      index_t c = entry[e_dst].prev;

      elem[c].next = a;
      elem[a].prev = c;

      elem[b].next = cpl(e_dst);
      entry[e_dst].prev = b;

      entry[e_src].prev = cpl(e_src);
      entry[e_src].next = cpl(e_src);
    }

    void prepend(index_t e_src, index_t e_dst) {
      if (empty(e_src)) return;

      index_t a = entry[e_src].next;
      index_t b = entry[e_src].prev;
      index_t c = entry[e_dst].next;

      entry[e_dst].next = a;
      elem[a].prev = cpl(e_dst);

      elem[b].next = c;
      elem[c].prev = b;

      entry[e_src].prev = cpl(e_src);
      entry[e_src].next = cpl(e_src);
    }

    chain_entry iter(index_t e) {
      return chain_entry(cpl(e), this);
    }

    iterator iter_elem(index_t x) {
      return iterator(x, this);
    }
  };

}
