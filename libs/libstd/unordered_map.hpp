#pragma once
#include <functional>
#include <memory>
#include <optional>
#include <utility>
#include <vector>

namespace std {
  template<typename Key, typename T,
           typename Hash = hash<Key>,
           typename KeyEqual = equal_to<Key>,
           typename Alloc = allocator<char> >//Whatever type for alloc, it will be rebinded
  class unordered_map {
   public:
    typedef std::pair<const Key, T> value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    class iterator {
      size_t M_bucket_idx;
      size_t M_hash;
      unordered_map<Key, T, Hash, KeyEqual, Alloc> *M_container;
     public:
      iterator(size_t bucket_idx, size_t hash, unordered_map<Key, T, Hash, KeyEqual, Alloc>* container):
        M_bucket_idx(bucket_idx), M_hash(hash), M_container(container) {}
      reference operator*() const {
        return *M_container->M_buckets[M_bucket_idx].data();
      }
      bool operator==(iterator it) const {
        return M_container == it.M_container && M_bucket_idx == it.M_bucket_idx;
      }
      iterator operator++() {
        size_t nxt;
        if (M_bucket_idx != ~0ul)
          nxt = M_container->M_buckets[M_bucket_idx].nxt;
        else {
          M_hash = 0;
          nxt = M_container->M_hash_bucket[0];
        }
        while (M_container->M_buckets[nxt].free() && M_hash < M_container->M_hash_bucket.size() - 1) {
          M_hash++;
          nxt = M_container->M_hash_bucket[M_hash];
        }
        if (M_container->M_buckets[nxt].free()) { // end
          M_bucket_idx = ~0;
          M_hash = ~0;
        } else
          M_bucket_idx = nxt;

        return *this;
      }
    };
    class const_iterator {
      size_t M_bucket_idx;
      size_t M_hash;
      const unordered_map<Key, T, Hash, KeyEqual, Alloc> *M_container;
     public:
      const_iterator(size_t bucket_idx, size_t hash, const unordered_map<Key, T, Hash, KeyEqual, Alloc>* container):
        M_bucket_idx(bucket_idx), M_hash(hash), M_container(container) {}
      const_reference operator*() const {
        return *M_container->M_buckets[M_bucket_idx].data();
      }
      bool operator==(const_iterator it) const {
        return M_container == it.M_container && M_bucket_idx == it.M_bucket_idx;
      }
      const_iterator operator++() {
        size_t nxt;
        if (M_bucket_idx != ~0ul)
          nxt = M_container->M_buckets[M_bucket_idx].nxt;
        else {
          M_hash = 0;
          nxt = M_container->M_hash_bucket[0];
        }
        while (M_container->M_buckets[nxt].free() && M_hash < M_container->M_hash_bucket.size() - 1) {
          M_hash++;
          nxt = M_container->M_hash_bucket[M_hash];
        }
        if (M_container->M_buckets[nxt].free()) { // end
          M_bucket_idx = ~0;
          M_hash = ~0;
        } else
          M_bucket_idx = nxt;

        return *this;
      }
    };

   private:
    template<typename U>
    using allocator_t = typename allocator_traits<Alloc>::template rebind_alloc<U>;

    static constexpr size_t __no_nxt = ~0ull;
    static constexpr size_t __free = ~1ull;
    struct node {
      char M_data[max(sizeof(value_type), sizeof(size_t))] alignas(value_type);
      size_t nxt;
      node(): nxt(__free) {
        nxt_free() = __no_nxt;
      }
      node(const node& other): nxt(other.nxt) {
        if (!other.free())
          new (data()) value_type(*other.data());
        else
          nxt_free() = other.nxt_free();
      }
      node(node&& other): nxt(other.nxt) {
        if (!other.free())
          new (data()) value_type(std::move(*other.data()));
        else
          nxt_free() = other.nxt_free();
      }
      template<typename V>
      node(V&& val, size_t n): nxt(n) {
        new (data()) value_type(std::forward<V>(val));
      }
      ~node() {
        if (!free())
          data()->~value_type();
      }

      template<typename V>
      void emplace(V&& val, size_t n) {
        nxt = n;
        new (data()) value_type(std::forward<V>(val));
      }

      bool free() const {
        return nxt == __free;
      }

      value_type* data() {
        return (value_type*)M_data;
      }
      value_type const* data() const {
        return (value_type const*)M_data;
      }
      size_t& nxt_free() {
        return *(size_t*)M_data;
      }
      size_t const& nxt_free() const {
        return *(size_t const*)M_data;
      }
    };

    std::vector<size_t, allocator_t<size_t> > M_hash_bucket;
    std::vector<node, allocator_t<node> > M_buckets;
    size_t free_stack_top;
    Hash M_hash;
    KeyEqual M_eq;
    size_t M_size;

    size_t getNewBlk() {
      if (free_stack_top == __no_nxt) {
        M_buckets.emplace_back();
        if (M_buckets.size() > M_hash_bucket.size() * 2) {
          M_hash_bucket.resize(M_hash_bucket.size() * 2);
          rehash();
        }
        return M_buckets.size() - 1;
      }
      size_t ret = free_stack_top;
      free_stack_top = M_buckets[ret].nxt_free();
      return ret;
    }

    size_t hash_bucket(const size_t& h) const {
      return h % M_hash_bucket.size();
    }

   public:
    unordered_map(Hash const& hash = Hash(), KeyEqual const& eq = KeyEqual(), Alloc const& alloc = Alloc())
      : M_hash_bucket(1, __no_nxt, allocator_t<size_t>(alloc)), M_buckets(allocator_t<node>(alloc)),
        free_stack_top(__no_nxt), M_hash(hash), M_eq(eq), M_size(0) { }

    void rehash() {
      for (size_t& b : M_hash_bucket)
        b = __no_nxt;
      for (size_t i = 0; i < M_buckets.size(); ++i) {
        node& n = M_buckets[i];
        if (n.free())
          continue;
        size_t hash = hash_bucket(M_hash(n.data()->first));
        n.nxt = M_hash_bucket[hash];
        M_hash_bucket[hash] = i;
      }
    }

    pair<bool, iterator> insert(const value_type& val) {
      size_t hash = M_hash(val.first);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      while (bucket != __no_nxt) {
        if (M_eq(val.first, M_buckets[bucket].data()->first))
          return make_pair(false, iterator(bucket, hash, this));
        bucket = M_buckets[bucket].nxt;
      }
      size_t dest_bucket = getNewBlk();
      size_t& bucket_top = M_hash_bucket[hash_bucket(hash)];
      M_buckets[dest_bucket].emplace(val, bucket_top);
      bucket_top = dest_bucket;
      M_size++;
      return make_pair(true, iterator(dest_bucket, hash, this));
    }

    template<typename M>
    pair<bool, iterator> insert_or_assign(const Key& k, M&& obj) {
      size_t hash = M_hash(k);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      while (bucket != __no_nxt) {
        if (M_eq(k, M_buckets[bucket].data()->first)) {
          M_buckets[bucket].data()->second = std::forward<M>(obj);
          return make_pair(false, iterator(bucket, hash, this));
        }
        bucket = M_buckets[bucket].nxt;
      }
      size_t dest_bucket = getNewBlk();
      size_t& bucket_top = M_hash_bucket[hash_bucket(hash)];
      M_buckets[dest_bucket].emplace(std::make_pair(k, std::forward<M>(obj)), bucket_top);
      bucket_top = dest_bucket;
      M_size++;
      return make_pair(true, iterator(dest_bucket, hash, this));
    }

    pair<bool, iterator> insert_or_assign(const value_type p) {
      return insert_or_assign(p.first, p.second);
    }

    T& at(const Key& key) {
      size_t hash = M_hash(key);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      while (bucket != __no_nxt) {
        if (M_eq(key, M_buckets[bucket].data()->first))
          return M_buckets[bucket].data()->second;
        bucket = M_buckets[bucket].nxt;
      }
      for (;;) asm("ud2"); //should throw
    }

    T const& at(const Key& key) const {
      return const_cast<unordered_map*>(this)->at(key);
    }

    T& operator[](const Key& key) {
      size_t hash = M_hash(key);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      while (bucket != __no_nxt) {
        if (M_eq(key, M_buckets[bucket].data()->first))
          return M_buckets[bucket].data()->second;
        bucket = M_buckets[bucket].nxt;
      }
      size_t dest_bucket = getNewBlk();
      size_t& bucket_top = M_hash_bucket[hash_bucket(hash)];
      M_buckets[dest_bucket].emplace(std::make_pair(key, T()), bucket_top);
      bucket_top = dest_bucket;
      M_size++;
      return M_buckets[dest_bucket].data()->second;
    }

    bool erase(const Key& key) {
      size_t hash = M_hash(key);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      size_t prec_blk = bucket;
      while (bucket != __no_nxt) {
        if (M_eq(key, M_buckets[bucket].data()->first)) {
          M_buckets[bucket].data()->~value_type();
          if (prec_blk == bucket) //top of bucket
            M_hash_bucket[hash_bucket(hash)] = M_buckets[bucket].nxt;
          else
            M_buckets[prec_blk].nxt = M_buckets[bucket].nxt;
          M_buckets[bucket].nxt = __free;
          M_buckets[bucket].nxt_free() = free_stack_top;
          free_stack_top = bucket;
          M_size--;
          return true;
        }
        prec_blk = bucket;
        bucket = M_buckets[bucket].nxt;
      }
      return false;
    }

    bool contains(const Key& key) const {
      size_t hash = M_hash(key);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      //size_t prec_blk = bucket;
      while (bucket != __no_nxt) {
        if (M_eq(key, M_buckets[bucket].data()->first))
          return true;
        //prec_blk = bucket;
        bucket = M_buckets[bucket].nxt;
      }
      return false;
    }

    optional<T*> find(const Key& key) {
      size_t hash = M_hash(key);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      while (bucket != __no_nxt) {
        if (M_eq(key, M_buckets[bucket].data()->first))
          return &M_buckets[bucket].data()->second;
        bucket = M_buckets[bucket].nxt;
      }
      return {};
    }

    size_t size() const {
      return M_size;
    }
    bool empty() const {
      return size() == 0;
    }

    iterator end() {
      return iterator(~0, ~0, this);
    }
    const_iterator end() const {
      return const_iterator(~0, ~0, this);
    }

    iterator begin() {
      if (empty()) return end();
      else return ++end();
    }
    const_iterator begin() const {
      if (empty()) return end();
      else return ++end();
    }
  };
}
