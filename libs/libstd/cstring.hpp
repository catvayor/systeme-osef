#pragma once

#include <cstddef>
#include <stdint.h>

namespace std {

  typedef decltype(sizeof(int)) size_t;

  extern "C" {

    void* memset(void* dest, int ch, size_t count);

    void* memcpy(void* dest, void const* src, size_t count);

    int memcmp(void const* lhs, void const* rhs, size_t count);

    int strcmp(const char* s1, const char* s2);

    void strcpy(char *str1, const char* str2);

    void* memchr(void* __s, int __c, size_t __n);

    char* strchr(char* __s, int __n);

    char* strpbrk(char* __s1, const char* __s2);

    char* strrchr(char* __s, int __n);

    char* strstr(char* __s1, const char* __s2);

    size_t strlen(const char* str);

  }

}

using std::memset;
using std::memcpy;
using std::memcmp;
using std::strcmp;
using std::strcpy;
