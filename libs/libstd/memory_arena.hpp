#pragma once

#include <optional>
#include <stack>
#include <vector>

namespace std {

  template<typename T, template<typename U> class TemplateAlloc>
  class memory_arena {
    typedef uint64_t index_t;
    stack<index_t, TemplateAlloc<index_t> > free_idx;
    vector<optional<T>, TemplateAlloc<optional<T> > > data;

   public:
    size_t size() const {
      return data.size() - free_idx.size();
    }

    bool used(index_t idx) const {
      return idx < data.size() && data[idx];
    }

    T const& operator[](index_t idx) const {
      return *data[idx];
    }

    T& operator[](index_t idx) {
      return *data[idx];
    }

    index_t push(const T& x) {
      index_t idx = data.size();
      if (free_idx.empty()) data.push_back(optional<T>(x));
      else data[idx = *free_idx.pop()] = optional<T>(x);
      return idx;
    }

    void pop(index_t idx) {
      if (!used(idx)) return;
      free_idx.push(idx);
      data[idx] = optional<T>();
    }
  };

}
