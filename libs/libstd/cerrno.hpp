#pragma once

#include <syscall>

extern int errno;

constexpr int ENOTDIR = Syscall::err_notfound;
constexpr int ENOENT  = Syscall::err_notfound;
constexpr int EINTR   = Syscall::err_intr;
