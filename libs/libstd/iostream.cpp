#include "iostream.hpp"
#include <cstdlib>

namespace std {
  ifstream cin(Syscall::STDIN_FILENO);
  ofstream cout(Syscall::STDOUT_FILENO);
  ofstream cerr(Syscall::STDERR_FILENO);



  fstreambuf::fstreambuf(uint64_t fd) : m_fd(fd) {}
  int64_t fstreambuf::sgetn(char* p, streamsize n) {
    return Syscall::read(m_fd, p, n);
  }
  int64_t fstreambuf::sputn(const char* p, streamsize n) {
    return Syscall::write(m_fd, p, n);
  }



  ios::ios(streambuf* sb) : m_sb(sb), state(0) {}

  streambuf* ios::rdbuf() {
    return m_sb;
  }

  streambuf* ios::rdbuf(streambuf* sb) {
    return m_sb = sb;
  }

  void ios::clear(iostate _state) {
    state = _state;
  }

  bool ios::good() const {
    return state == 0;
  }
  bool ios::eof() const {
    return state & eofbit;
  }
  bool ios::fail() const {
    return state & (failbit | badbit);
  }
  bool ios::bad() const {
    return state & badbit;
  }
  bool ios::operator!() const {
    return fail();
  }
  ios::operator bool() const {
    return !fail();
  }



  istream::istream(streambuf* sb) : ios(sb) {}

  streamsize istream::gcount() const {
    return read_count;
  }

  istream& istream::get(char& c) {
    return read(&c, 1);
  }

  istream& istream::read(char* p, streamsize s) {
    read_count = 0;
    while (s > read_count) {
      int64_t ret = rdbuf()->sgetn(p, s - read_count);
      if (ret == Syscall::err_intr) {
        continue;
      } else if (ret < 0) {
        clear(badbit);
        break;
      } else if (ret == 0) {
        clear(failbit | eofbit);
        break;
      } else {
        read_count += ret;
        p += ret;
      }
    }
    return *this;
  }

  istream& istream::readsome(char* p, streamsize s) {
    read_count = 0;
    int64_t ret = rdbuf()->sgetn(p, s);
    if (ret == Syscall::err_intr) {
    } else if (ret < 0) {
      clear(badbit);
    } else if (ret == 0) {
      clear(eofbit);
    } else {
      read_count = ret;
    }
    return *this;
  }

  istream& istream::operator>>(string &str) {
    str = "";
    char c;
    while (true) {
      get(c);
      if (bad() || eof()) break;
      if (fail()) {
        clear();
        continue;
      }
      if (isblank(c)) break;
      str.push_back(c);
    }
    return *this;
  }

  istream& getline(istream& is, string& str, char delim) {
    str = "";
    char c;
    while (true) {
      is.get(c);
      if (is.bad() || is.eof()) break;
      if (is.fail()) {
        is.clear();
        continue;
      }
      if (c == delim) break;
      str.push_back(c);
    }
    return is;
  }



  ostream::ostream(streambuf* sb) : ios(sb) {}

  ostream& ostream::put(char c) {
    return write(&c, 1);
  }

  ostream& ostream::write(const char* p, streamsize s) {
    rdbuf()->sputn(p, s);
    return *this;
  }

  ostream& ostream::operator<<(char c) {
    return put(c);
  }

  ostream& ostream::operator<<(char const* p) {
    return write(p, strlen(p));
  }

  ostream& ostream::operator<<(string s) {
    return write(s.c_str(), s.size());
  }

  ostream& ostream::operator<<(int32_t x) {
    string s = to_string(x);
    return operator<<(s);
  }

  ostream& ostream::operator<<(int64_t x) {
    string s = to_string(x);
    return operator<<(s);
  }

  ostream& ostream::operator<<(uint32_t x) {
    string s = to_string(x);
    return operator<<(s);
  }

  ostream& ostream::operator<<(uint64_t x) {
    string s = to_string(x);
    return operator<<(s);
  }
}
