#pragma once
#include <type_traits>

namespace std {

  template<typename _Tp>
  constexpr _Tp&& forward(typename remove_reference<_Tp>::type& __t) noexcept {
    return static_cast < _Tp && > (__t);
  }

  template<typename _Tp>
  constexpr _Tp&& forward(typename remove_reference<_Tp>::type&& __t) noexcept {
    static_assert(!is_lvalue_reference<_Tp>::value, "template argument"
                  " substituting _Tp must not be an lvalue reference type");
    return static_cast < _Tp && > (__t);
  }

  template<typename _Tp>
  constexpr typename remove_reference<_Tp>::type&& move(_Tp&& __t) noexcept {
    return static_cast < typename remove_reference<_Tp>::type && > (__t);
  }

  template<typename T>
  T declval() noexcept;

  template<typename T1, typename T2>
  struct pair {
    T1 first;
    T2 second;

    pair() =  default;

    template<typename T, typename U>
    constexpr pair(T const& v1, U const& v2): first(v1), second(v2) { }
    template<typename T, typename U>
    constexpr pair(T&& v1, U&& v2): first(v1), second(v2) { }

    template<typename T, typename U>
    constexpr pair(pair<T, U> const& other): pair(other.first, other.second) { }
    template<typename T, typename U>
    constexpr pair(pair<T, U>&& other): pair(std::move(other.first), std::move(other.second)) { }

    template<typename T, typename U>
    pair& operator=(pair<T, U> const& other) {
      first = other.first;
      second = other.second;
    }
  };

  template<typename T1, typename T2>
  constexpr pair<T1, T2> make_pair(T1&& v1, T2&& v2) {
    return pair<T1, T2>(v1, v2);
  }

  template<typename T>
  void swap(T& a, T& b) {
    T tmp = a;
    a = b;//move
    b = tmp; //move
  }

}
