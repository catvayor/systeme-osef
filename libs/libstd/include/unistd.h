#pragma once

#include <cerrno>
#include <syscall>
#include "sys/types.h"

extern char **environ;

enum filenos : fd_t {
  STDIN_FILENO = Syscall::STDIN_FILENO,
  STDOUT_FILENO = Syscall::STDOUT_FILENO,
  STDERR_FILENO = Syscall::STDERR_FILENO,
};

static inline pid_t fork() {
  int64_t ret = Syscall::fork();
  if (ret < -1) {
    errno = ret;
    return -1;
  } else return ret + 1;
}

static inline int execv(const char* path, char *const argv[]) {
  return Syscall::execv(path, argv);
}
