#pragma once

#include <syscall>

constexpr int SIGABRT = Syscall::SIGABRT;
constexpr int SIGKILL = Syscall::SIGKILL;
constexpr int SIGINT  = Syscall::SIGINT;

static inline int kill(int pid, int sig) {
  int64_t ret = Syscall::kill(pid - 1, sig);
  if (ret < 0) {
    errno = ret;
    return -1;
  } else return 0;
}
