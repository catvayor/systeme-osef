#pragma once

#include <cerrno>
#include <syscall>
#include "types.h"

enum wait_flags : int {
  WNOHANG = Syscall::WNOHANG,
};

static inline pid_t waitpid(pid_t pid, int *wstatus, int options) {
  if (pid == 0 || pid < -1) asm("ud2");
  if (pid > 0) pid--;
  int64_t ret;
  if (wstatus) {
    int64_t qstatus = *wstatus; // workaround for nullptr warning...
    ret = Syscall::waitpid(pid, &qstatus, options);
    *wstatus = qstatus;
  } else {
    ret = Syscall::waitpid(pid, NULL, options);
  }
  if (ret == Syscall::err_nohang) {
    return 0;
  }
  if (ret < -1) {
    errno = ret;
    return -1;
  } else return ret + 1;
}

static inline pid_t wait(int *wstatus) {
  return waitpid(-1, wstatus, 0);
}
