#pragma once

#ifdef LINUXCOMPAT
typedef int pid_t;
typedef int mode_t;
typedef int fd_t;
#else
#include <syscall>
using Syscall::pid_t;
using Syscall::mode_t;
using Syscall::fd_t;
#endif
