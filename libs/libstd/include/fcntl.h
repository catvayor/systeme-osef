#pragma once

#include <syscall>

enum open_flags : int {
  O_RDONLY  = Syscall::O_RDONLY,
  O_WRONLY  = Syscall::O_WRONLY,
  O_CREAT   = Syscall::O_CREAT,
  O_APPEND  = Syscall::O_APPEND,
  O_TRUNC   = Syscall::O_TRUNC,
};

enum mode_flags : mode_t {
  S_IRWXU = Syscall::S_IRWXU,
  S_IRUSR = Syscall::S_IRUSR,
  S_IWUSR = Syscall::S_IWUSR,
  S_IXUSR = Syscall::S_IXUSR,
  S_IRWXG = Syscall::S_IRWXG,
  S_IRGRP = Syscall::S_IRGRP,
  S_IWGRP = Syscall::S_IWGRP,
  S_IXGRP = Syscall::S_IXGRP,
  S_IRWXO = Syscall::S_IRWXO,
  S_IROTH = Syscall::S_IROTH,
  S_IWOTH = Syscall::S_IWOTH,
  S_IXOTH = Syscall::S_IXOTH,
};

static inline int open(const char* path, int flags, mode_t mode) {
  int64_t ret = Syscall::open(path, flags, mode);
  if (ret < 0) {
    errno = ret;
    return -1;
  } else return ret;
}

static inline int close(fd_t fd) {
  int64_t ret = Syscall::close(fd);
  if (ret < 0) {
    errno = ret;
    return -1;
  } else return ret;
}

static inline int dup2(fd_t oldfd, fd_t newfd) {
  int64_t ret = Syscall::dup2(oldfd, newfd);
  if (ret < 0) {
    errno = ret;
    return -1;
  } else return ret;
}

static inline int pipe(fd_t pipefd[2]) {
  int64_t pipefd_64[2];
  int64_t ret = Syscall::pipe(pipefd_64);
  pipefd[0] = pipefd_64[0];
  pipefd[1] = pipefd_64[1];
  if (ret < 0) {
    errno = ret;
    return -1;
  } else return ret;
}

static inline int chdir(const char* path) {
  int64_t ret = Syscall::chdir(path);
  if (ret < 0) {
    errno = ret;
    return -1;
  } else return ret;
}
