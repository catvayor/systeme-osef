#pragma once

#define __min_int(nb_bits) (1<<((nb_bits) - 1))
#define __max_int(nb_bits) ((1<<((nb_bits) - 1)) - 1)
#define __max_uint(nb_bits) ((1<<(nb_bits)) - 1)

constexpr long LONG_MIN = __min_int(8 * sizeof(long));
constexpr long LONG_MAX = __max_int(8 * sizeof(long));
constexpr unsigned long ULONG_MAX = __max_uint(8 * sizeof(unsigned long));