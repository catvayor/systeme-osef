#include "new.hpp"

[[nodiscard]] void* operator new (std::size_t, void* ptr) {
  return ptr;
}

[[nodiscard]] void* operator new[](std::size_t, void* ptr) {
  return ptr;
}
