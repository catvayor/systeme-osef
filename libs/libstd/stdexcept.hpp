#pragma once

namespace std {
  class runtime_error {
   public:
    runtime_error( [[maybe_unused]] const std::string& what_arg ) {
      asm("ud2");
    }
    runtime_error( [[maybe_unused]] const char* what_arg ) {
      asm("ud2");
    }
    virtual const char* what() const noexcept {
      for (;;) asm("ud2");
    }
  };
}

