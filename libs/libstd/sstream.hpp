#pragma once

#include <iostream>

namespace std {
  class stringbuf : public streambuf {
   private:
    string m_str;
    size_t idx;
   public:
    stringbuf(string str): m_str(str), idx(0) {}
    stringbuf(): stringbuf("") {}
    streamsize sgetn(char* p, streamsize n) {
      streamsize nb_read = 0;
      while (nb_read < n && idx < m_str.size()) {
        *p++ = m_str[idx++];
        nb_read++;
      }
      return nb_read;
    }
    streamsize sputn(const char* p, streamsize n) {
      m_str += string(p, p + n);
      return n;
    }
  };

  class isstream : public istream {
   private:
    stringbuf m_buf;
   public:
    isstream(string str): istream(&m_buf), m_buf(str) {}
    isstream(): istream(&m_buf) {}
  };
}