#pragma once

namespace std {
  template<class T, class Container = std::vector<T>, class Compare = std::less<typename Container::value_type>>
  class priority_queue {
   public:
    typedef Container container_type;
    typedef Compare value_compare;
    typedef Container::value_type value_type;
    typedef Container::size_type size_type;
    typedef Container::reference reference;
    typedef Container::const_reference const_reference;

   protected:
    Container M_data;
    unsigned up(unsigned pos) {
      return (pos - 1) / 2;
    }
    unsigned left(unsigned pos) {
      return 2 * pos + 1;
    }
    unsigned right(unsigned pos) {
      return 2 * pos + 2;
    }

   public:
    void push(const value_type& value) {
      unsigned pos = M_data.size();
      M_data.emplace_back();

      while (pos) {
        unsigned next = up(pos);
        if (Compare()(M_data[next], value)) {
          M_data[pos] = move(M_data[next]);
          pos = next;
        } else break;
      }

      M_data[pos] = value;
    }

    void push(value_type&& value) {
      unsigned pos = M_data.size();
      M_data.emplace_back();

      while (pos) {
        unsigned next = up(pos);
        if (Compare()(M_data[next], value)) {
          M_data[pos] = move(M_data[next]);
          pos = next;
        } else break;
      }

      M_data[pos] = move(value);
    }

    void pop() {
      if (M_data.empty()) return;

      unsigned pos = 0;
      unsigned last = M_data.size() - 1;

      while (true) {
        if (right(pos) < last && Compare()(M_data.back(), M_data[right(pos)]) && Compare()(M_data[left(pos)], M_data[right(pos)])) {
          M_data[pos] = M_data[right(pos)];
          pos = right(pos);
        } else if (left(pos) < last && Compare()(M_data.back(), M_data[left(pos)])) {
          M_data[pos] = M_data[left(pos)];
          pos = left(pos);
        } else {
          break;
        }
      }

      M_data[pos] = move(M_data.back());
      M_data.pop_back();
    }

    const_reference top() {
      return M_data.front();
    }

    size_t size() {
      return M_data.size();
    }

    bool empty() {
      return M_data.empty();
    }
  };
}
