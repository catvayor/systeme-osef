#pragma once
#include <cstring>
#include <functional>
#include <iterator>
#include <utility>

namespace std {

  template<typename ForwardIt, typename Predicate>
  constexpr bool all_of(ForwardIt beg, ForwardIt end, Predicate p) {
    for (; beg != end; ++beg)
      if (!p(*beg))
        return false;
    return true;
  }

  template<typename ForwardIt, typename Predicate>
  constexpr bool any_of(ForwardIt beg, ForwardIt end, Predicate p) {
    for (; beg != end; ++beg)
      if (p(*beg))
        return true;
    return false;
  }

  template<typename ForwardIt, typename Predicate>
  constexpr bool none_of(ForwardIt beg, ForwardIt end, Predicate p) {
    return !any_of(beg, end, p);
  }

  template<typename ForwardIt, typename UnaryFunction>
  constexpr UnaryFunction for_each(ForwardIt beg, ForwardIt end, UnaryFunction f) {
    for (; beg != end; ++beg)
      f(*beg);
    return f;
  }

  template<typename ForwardIt, typename Size, typename UnaryFunction>
  constexpr ForwardIt for_each_n(ForwardIt beg, Size n, UnaryFunction f) {
    for (Size i = 0; i < n; ++i) {
      f(*beg);
      ++beg;
    }
    return beg;
  }

  template<typename ForwardIt, typename T>
  constexpr typename iterator_traits<ForwardIt>::difference_type
  count(ForwardIt beg, ForwardIt end, const T& val) {
    typename iterator_traits<ForwardIt>::difference_type ret = 0;
    for (; beg != end; ++beg)
      if (*beg == val)
        ++ret;
    return ret;
  }

  template<typename ForwardIt, typename Predicate>
  constexpr typename iterator_traits<ForwardIt>::difference_type
  count_if(ForwardIt beg, ForwardIt end, Predicate p) {
    typename iterator_traits<ForwardIt>::difference_type ret = 0;
    for (; beg != end; ++beg)
      if (p(*beg))
        ++ret;
    return ret;
  }

  template<typename ForwardIt1, typename ForwardIt2, typename BinPredicate>
  constexpr std::pair<ForwardIt1, ForwardIt2> mismatch(ForwardIt1 beg1, ForwardIt1 end1,
      ForwardIt2 beg2, BinPredicate p) {
    while (beg1 != end1 && p(*beg1, *beg2)) {
      ++beg1;
      ++beg2;
    }
    return make_pair(beg1, beg2);
  }

  template<typename ForwardIt1, typename ForwardIt2, typename BinPredicate>
  constexpr std::pair<ForwardIt1, ForwardIt2> mismatch(ForwardIt1 beg1, ForwardIt1 end1,
      ForwardIt2 beg2, ForwardIt2 end2, BinPredicate p) {
    while (beg1 != end1 && beg2 != end2 && p(*beg1, *beg2)) {
      ++beg1;
      ++beg2;
    }
    return make_pair(beg1, beg2);
  }

  template<typename ForwardIt1, typename ForwardIt2>
  constexpr std::pair<ForwardIt1, ForwardIt2> mismatch(ForwardIt1 beg1, ForwardIt1 end1,
      ForwardIt2 beg2) {
    while (beg1 != end1 && (*beg1 == *beg2)) {
      ++beg1;
      ++beg2;
    }
    return make_pair(beg1, beg2);
  }

  template<typename ForwardIt1, typename ForwardIt2>
  constexpr std::pair<ForwardIt1, ForwardIt2> mismatch(ForwardIt1 beg1, ForwardIt1 end1,
      ForwardIt2 beg2, ForwardIt2 end2) {
    while (beg1 != end1 && beg2 != end2 && (*beg1 == *beg2)) {
      ++beg1;
      ++beg2;
    }
    return make_pair(beg1, beg2);
  }

  template<typename ForwardIt, typename T>
  constexpr ForwardIt find(ForwardIt beg, ForwardIt end, const T& val) {
    for (; beg != end; ++beg)
      if (*beg == val)
        break;
    return beg;
  }

  template<typename ForwardIt, typename Predicate>
  constexpr ForwardIt find_if(ForwardIt beg, ForwardIt end, Predicate p) {
    for (; beg != end; ++beg)
      if (p(*beg))
        break;
    return beg;
  }

  template<typename ForwardIt, typename Predicate>
  constexpr ForwardIt find_if_not(ForwardIt beg, ForwardIt end, Predicate p) {
    for (; beg != end; ++beg)
      if (!p(*beg))
        break;
    return beg;
  }

  template<class ForwardIt1, class ForwardIt2>
  constexpr ForwardIt1 search(ForwardIt1 first, ForwardIt1 last,
                              ForwardIt2 s_first, ForwardIt2 s_last) {
    while (1) {
      ForwardIt1 it = first;
      for (ForwardIt2 s_it = s_first; ; ++it, ++s_it) {
        if (s_it == s_last) return first;
        if (it == last)  return last;
        if (!(*it == *s_it)) break;
      }
      ++first;
    }
  }

  template<class ForwardIt1, class ForwardIt2, class BinaryPredicate>
  constexpr ForwardIt1 search(ForwardIt1 first, ForwardIt1 last,
                              ForwardIt2 s_first, ForwardIt2 s_last,
                              BinaryPredicate p) {
    while (1) {
      ForwardIt1 it = first;
      for (ForwardIt2 s_it = s_first; ; ++it, ++s_it) {
        if (s_it == s_last) return first;
        if (it == last) return last;
        if (!p(*it, *s_it)) break;
      }
      ++first;
    }
  }

  template<class ForwardIt1, class ForwardIt2>
  ForwardIt1 find_end(ForwardIt1 first, ForwardIt1 last,
                      ForwardIt2 s_first, ForwardIt2 s_last) {
    if (s_first == s_last)
      return last;
    ForwardIt1 result = last;
    while (true) {
      ForwardIt1 new_result = std::search(first, last, s_first, s_last);
      if (new_result == last) {
        break;
      } else {
        result = new_result;
        first = result;
        ++first;
      }
    }
    return result;
  }

  template<class ForwardIt1, class ForwardIt2, class BinaryPredicate>
  ForwardIt1 find_end(ForwardIt1 first, ForwardIt1 last,
                      ForwardIt2 s_first, ForwardIt2 s_last,
                      BinaryPredicate p) {
    if (s_first == s_last)
      return last;
    ForwardIt1 result = last;
    while (true) {
      ForwardIt1 new_result = std::search(first, last, s_first, s_last, p);
      if (new_result == last) {
        break;
      } else {
        result = new_result;
        first = result;
        ++first;
      }
    }
    return result;
  }

  template<class ForwardIt>
  ForwardIt adjacent_find(ForwardIt first, ForwardIt last) {
    if (first == last) {
      return last;
    }
    ForwardIt next = first;
    ++next;
    for (; next != last; ++next, ++first) {
      if (*first == *next) {
        return first;
      }
    }
    return last;
  }

  template<class ForwardIt, class BinaryPredicate>
  ForwardIt adjacent_find(ForwardIt first, ForwardIt last,
                          BinaryPredicate p) {
    if (first == last) {
      return last;
    }
    ForwardIt next = first;
    ++next;
    for (; next != last; ++next, ++first) {
      if (p(*first, *next)) {
        return first;
      }
    }
    return last;
  }

  template<class ForwardIt, class Size, class T>
  ForwardIt search_n(ForwardIt first, ForwardIt last,
                     Size count, const T& value) {
    if (count <= 0) {
      return first;
    }
    for (; first != last; ++first) {
      if (!(*first == value)) {
        continue;
      }

      ForwardIt candidate = first;
      Size cur_count = 0;

      while (true) {
        ++cur_count;
        if (cur_count >= count) {
          // success
          return candidate;
        }
        ++first;
        if (first == last) {
          // exhausted the list
          return last;
        }
        if (!(*first == value)) {
          // too few in a row
          break;
        }
      }
    }
    return last;
  }

  template<class ForwardIt, class Size, class T, class BinaryPredicate>
  ForwardIt search_n(ForwardIt first, ForwardIt last,
                     Size count, const T& value, BinaryPredicate p) {
    if (count <= 0) {
      return first;
    }
    for (; first != last; ++first) {
      if (!p(*first, value)) {
        continue;
      }

      ForwardIt candidate = first;
      Size cur_count = 0;

      while (true) {
        ++cur_count;
        if (cur_count >= count) {
          // success
          return candidate;
        }
        ++first;
        if (first == last) {
          // exhausted the list
          return last;
        }
        if (!p(*first, value)) {
          // too few in a row
          break;
        }
      }
    }
    return last;
  }

  template<typename ForwardIt1, typename ForwardIt2>
  void iter_swap(ForwardIt1 a, ForwardIt2 b) {
    swap(*a, *b);
  }

  template<typename T, typename Compare = less<T> >
  constexpr pair<const T&, const T&> minmax(T const& v1, T const& v2, Compare comp = Compare{}) {
    if (comp(v1, v2))
      return make_pair(v1, v2);
    else
      return make_pair(v2, v1);
  }

  template<typename T, typename Compare = less<T> >
  constexpr const T & min(T const& v1, T const& v2, Compare comp = Compare{}) {
    return minmax(v1, v2, comp).first;
  }

  template<typename T, typename Compare = less<T> >
  constexpr const T & max(T const& v1, T const& v2, Compare comp = Compare{}) {
    return minmax(v1, v2, comp).second;
  }

  template<typename ForwardIt, typename Compare = less<typename iterator_traits<ForwardIt>::value_type> >
  void sort(ForwardIt first, ForwardIt last, Compare comp = Compare{}) {
    if (first == last) return;
    ForwardIt eq_beg = first;
    ForwardIt big_beg = first + 1;

    for (ForwardIt curr = first + 1; curr != last; ++curr) {
      if (comp(*curr, *eq_beg)) {
        iter_swap(curr, eq_beg);
        iter_swap(curr, big_beg);
        ++eq_beg;
        ++big_beg;
      } else if (!comp(*eq_beg, *curr)) {
        iter_swap(curr, big_beg);
        ++big_beg;
      }
    }
    sort(first, eq_beg, comp);
    sort(big_beg, last, comp);
  }

}
