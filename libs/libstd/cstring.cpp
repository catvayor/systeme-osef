#include "cstring.hpp"

namespace std {

  extern "C" {

    void* memset(void* dest, int ch, size_t count) {
      char* d = reinterpret_cast<char*>(dest);
      for (size_t i = 0; i < count; ++i)
        d[i] = char(ch);
      return d;
    }

    void* memcpy(void* dest, void const* src, size_t count) {
      char* d = reinterpret_cast<char*>(dest);
      char const* s = reinterpret_cast<char const*>(src);
      for (size_t i = 0; i < count; ++i)
        d[i] = s[i];
      return d;
    }

    int memcmp(void const* lhs, void const* rhs, size_t count) {
      uint8_t const* clhs = (uint8_t const*)lhs;
      uint8_t const* crhs = (uint8_t const*)rhs;
      for (size_t i = 0; i < count; ++i)
        if (clhs[i] != crhs[i])
          return int(clhs[i]) - int(crhs[i]);
      return 0;
    }

    int strcmp(const char* s1, const char* s2) {
      while (*s1 != 0) {
        if (*s1++ != *s2++)
          return false;
      }
      return *s2 == 0;
    }

    void strcpy(char *str1, const char* str2) {
      while ((*str1++ = *str2++));
    }

    void* memchr(void* __s, int __c, size_t __n) {
      return __builtin_memchr(__s, __c, __n);
    }

    char* strchr(char* __s, int __n) {
      return __builtin_strchr(__s, __n);
    }

    char* strpbrk(char* __s1, const char* __s2) {
      return __builtin_strpbrk(__s1, __s2);
    }

    char* strrchr(char* __s, int __n) {
      return __builtin_strrchr(__s, __n);
    }

    char* strstr(char* __s1, const char* __s2) {
      return __builtin_strstr(__s1, __s2);
    }

    size_t strlen(const char* start) {
      const char* end = start;
      for ( ; *end != '\0'; ++end);
      return end - start;
    }

  }

}
