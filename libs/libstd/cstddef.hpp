#pragma once

#define NULL nullptr

namespace std {
  typedef decltype( sizeof(int) ) size_t;
  typedef decltype( (char*)(nullptr) - (char*)(nullptr) ) ptrdiff_t;
  typedef decltype( nullptr ) nullptr_t;
}

using std::size_t;
using std::ptrdiff_t;
using std::nullptr_t;
