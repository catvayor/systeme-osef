#pragma once

inline bool isdigit(int c) {
  return c >= '0' && c <= '9';
}

inline bool isupper(int c) {
  return c >= 'A' && c <= 'Z';
}

inline bool islower(int c) {
  return c >= 'a' && c <= 'z';
}

inline bool isalpha(int c) {
  return isupper(c) || islower(c);
}

inline bool isalnum(int c) {
  return isalpha(c) || isdigit(c);
}

inline bool isblank(int c) {
  return c == ' ' || c == '\t';
}

inline bool isspace(int c) {
  return c == ' ' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v';
}

inline bool isascii(int c) {
  return 0 <= c && c < 128;
}

inline bool iscntrl(int c) {
  return 0 <= c && c < 32;
}

inline bool isprint(int c) {
  return 32 <= c && c < 128;
}

inline bool ispunct(int c) {
  return isprint(c) && !isalnum(c);
}

inline bool isgraph(int c) {
  return ispunct(c) && c != ' ';
}