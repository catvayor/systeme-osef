#pragma once
#include <utility>
#include <memory>
#include <algorithm>
#include <new>

namespace std {
  // character traits
  template<class CharT> struct char_traits {
    static constexpr int compare(const CharT* s1, const CharT* s2, size_t count) {
      for (unsigned k = 0; k < count; k++) {
        if (s1[k] != s2[k]) return (s1[k] < s2[k]) ? -1 : 1;
      }
      return 0;
    }
  };

  /*template<> struct char_traits<char>;
  template<> struct char_traits<char8_t>;
  template<> struct char_traits<char16_t>;
  template<> struct char_traits<char32_t>;
  template<> struct char_traits<wchar_t>;*/

  // basic string
  template<class CharT, class Traits = char_traits<CharT>,
           class Allocator = allocator<CharT>>
  class basic_string {
   public:
    typedef CharT value_type;
    typedef Allocator allocator_type;
    typedef allocator_traits<allocator_type> __traits;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef CharT& reference;
    typedef CharT const& const_reference;
    typedef __traits::pointer pointer;
    typedef __traits::const_pointer const_pointer;
    typedef CharT* iterator;
    typedef const CharT* const_iterator;

   protected:
    Allocator M_allocator;
    pointer M_array;
    size_type M_size;       // strlen, doesn't count '\0'
    size_type M_capacity;   // idem

    void reallocate(size_type new_capacity) {
      pointer new_array = M_allocator.allocate(new_capacity + 1);
      if (M_array != nullptr) {
        for (size_type i = 0; i <= M_size; ++i)
          new (new_array + i) CharT(std::move(operator[](i)));
        M_allocator.deallocate(M_array, M_capacity + 1);
      }
      M_array = new_array;
      M_capacity = new_capacity;
    }

    void zero_terminate() {
      new (data() + M_size) CharT();
    }

    void grow(size_type delta) {
      reallocate(M_size + std::max(M_size, delta));
    }

   public:
    basic_string(size_type count, const_reference value, const Allocator& alloc = Allocator())
      : M_allocator(alloc), M_array(nullptr), M_size(0), M_capacity(0) {
      resize(count, value);
    }

    explicit basic_string(const Allocator& alloc = Allocator()): M_allocator(alloc), M_array(nullptr), M_size(0), M_capacity(0) {
      reallocate(0);
      zero_terminate();
    }

    explicit basic_string(size_type count, const Allocator& alloc = Allocator())
      : basic_string(count, CharT(), alloc) { }

    template<typename InputIt>
    basic_string(InputIt beg, InputIt end, const Allocator& alloc = Allocator()) requires requires(InputIt a) {
      is_convertible_v<decltype(*a), CharT>;
    }
   :
    basic_string(0, alloc) {
      for (auto i = beg; i != end; ++i)
        push_back(*i);
    }

    basic_string(basic_string const& other)
      : basic_string(other.begin(), other.end(), other.M_allocator) { }

    basic_string(basic_string&& other)
      : M_allocator(std::move(other.M_allocator)), M_array(other.M_array),
        M_size(other.M_size), M_capacity(other.M_capacity) {
      other.M_array = nullptr;
    }

    basic_string(const_pointer p): basic_string() {
      while (*p)
        push_back(*p++);
    }

    ~basic_string() {
      if (M_array)
        M_allocator.deallocate(M_array, M_capacity + 1);
    }

    basic_string& operator=(const basic_string& other) {
      clear();
      for (const_reference elem : other)
        push_back(elem);
      return *this;
    }

    pointer data() {
      return M_array;
    }
    const_pointer data() const {
      return M_array;
    }
    const_pointer c_str() const {
      return data();
    }

    reference front() {
      return *data();
    }
    const_reference front() const {
      return *data();
    }

    reference back() {
      return *(data() + M_size - 1);
    }
    const_reference back() const {
      return *(data() + M_size - 1);
    }

    size_type size() const {
      return M_size;
    }
    size_type capacity() const {
      return M_capacity;
    }
    bool empty() const {
      return size() == 0;
    }

    void shrink_to_fit() {
      if (M_size != M_capacity) reallocate(M_size);
    }

    void push_back(const value_type& value) {
      if (M_size == M_capacity)
        grow(1);
      new (data() + (M_size++)) CharT(value);
      zero_terminate();
    }

    template<typename... Args>
    void emplace_back(Args&&... args) {
      if (M_size == M_capacity)
        grow(1);
      new (data() + (M_size++)) CharT(std::forward<Args>(args)...);
      zero_terminate();
    }

    void pop_back() {
      --M_size;
      zero_terminate();
    }

    void clear() {
      M_size = 0;
      zero_terminate();
    }

    void reserve(size_type count) {
      if (M_size < count || M_size == 0)
        reallocate(std::max(M_size + M_size, count));
    }

    void resize(size_type count, const value_type& value) {
      reserve(count);
      for (size_type i = M_size; i < count; ++i)
        new (data() + i) CharT(value);
      M_size = count;
      zero_terminate();
    }

    void resize(size_type count) {
      reserve(count);
      for (size_type i = M_size; i < count; ++i)
        new (data() + i) CharT();
      M_size = count;
      zero_terminate();
    }

    reference operator[](size_type idx) {
      return *(M_array + idx);
    }

    const_reference operator[](size_type idx) const {
      return *(M_array + idx);
    }

    //iterators
    iterator begin() {
      return M_array;
    }
    iterator end() {
      return M_array + M_size;
    }

    const_iterator begin() const {
      return M_array;
    }
    const_iterator end() const {
      return M_array + M_size;
    }

    const_iterator cbegin() const {
      return M_array;
    }
    const_iterator cend() const {
      return M_array + M_size;
    }

    iterator erase(iterator pos) {
      memcpy(pos, pos + 1, sizeof(CharT) * (end() - (pos + 1)));
      M_size--;
      zero_terminate();
      return pos;
    }

    iterator erase(iterator first, iterator last) {
      memcpy(first, last, sizeof(CharT) * (end() - last));
      M_size -= last - first;
      zero_terminate();
      return first;
    }

    iterator find(CharT c) {
      iterator p = begin();
      while (p != end() && *p != c) p++;
      return p;
    }
    const_iterator find(CharT c) const {
      const_iterator p = cbegin();
      while (p != cend() && *p != c) p++;
      return p;
    }

    constexpr void swap(basic_string<CharT, Traits, Allocator>& s) noexcept {
      std::swap(this->M_allocator, s.M_allocator);
      std::swap(this->M_array, s.M_array);
      std::swap(this->M_size, s.M_size);
      std::swap(this->M_capacity, s.M_capacity);
    }

    basic_string<CharT, Traits, Allocator> substr(size_type pos, size_type len) {
      return basic_string<CharT, Traits, Allocator>(data() + pos, len);
    }
    bool starts_with(const CharT *s) {
      CharT *p = data();
      while (*s) {
        if (*s != *p)
          return false;
        ++s;
        ++p;
      }
      return true;
    }
    bool starts_with(basic_string<CharT, Traits, Allocator> s) {
      return starts_with(s.c_str());
    }
    bool ends_with(const CharT *s) {
      CharT *p = end();
      const CharT *ps = s;
      if (!*ps) return true;
      while (*ps) ps++;
      do {
        ps--;
        p--;
        if (*p != *ps) return false;
      } while (ps != s && p != begin());
      return ps == s;
    }
    bool ends_with(basic_string<CharT, Traits, Allocator> s) {
      CharT *p = end();
      CharT *ps = s.end();
      do {
        ps--;
        p--;
        if (*p != *ps) return false;
      } while (ps != s.begin() && p != begin());
      return ps == s.begin();
    }

    int compare(const basic_string& s) const {
      size_type lhs_sz = size();
      size_type rhs_sz = s.size();
      int result = Traits::compare(data(), s.data(), min(lhs_sz, rhs_sz));
      if (result != 0)
        return result;
      if (lhs_sz < rhs_sz)
        return -1;
      if (lhs_sz > rhs_sz)
        return 1;
      return 0;
    }
  };

  template<class CharT, class Traits, class Allocator>
  basic_string<CharT, Traits, Allocator>
  operator+(const basic_string<CharT, Traits, Allocator>& lhs,
            const basic_string<CharT, Traits, Allocator>& rhs) {
    basic_string<CharT, Traits, Allocator> s = lhs;
    for (auto c : rhs)
      s.push_back(c);
    return s;
  }

  template<class CharT, class Traits, class Allocator>
  basic_string<CharT, Traits, Allocator>
  operator+(const basic_string<CharT, Traits, Allocator>& lhs,
            const CharT* rhs) {
    basic_string<CharT, Traits, Allocator> s = lhs;
    while (*rhs) {
      s.push_back(*rhs++);
    }
    return s;
  }

  template<class CharT, class Traits, class Allocator>
  basic_string<CharT, Traits, Allocator>
  operator+(const CharT* lhs,
            const basic_string<CharT, Traits, Allocator>& rhs) {
    basic_string<CharT, Traits, Allocator> s = lhs;
    for (auto c : rhs)
      s.push_back(c);
    return s;
  }

  template<class CharT, class Traits, class Allocator>
  basic_string<CharT, Traits, Allocator>&
  operator+=(basic_string<CharT, Traits, Allocator>& lhs,
             const basic_string<CharT, Traits, Allocator>& rhs) {
    for (auto c : rhs)
      lhs.push_back(c);
    return lhs;
  }

  template<class CharT, class Traits, class Allocator>
  basic_string<CharT, Traits, Allocator>&
  operator+=(basic_string<CharT, Traits, Allocator>& lhs,
             const CharT* rhs) {
    while (*rhs) {
      lhs.push_back(*rhs++);
    }
    return lhs;
  }

  template<class CharT, class Traits, class Allocator>
  constexpr bool
  operator<(const basic_string<CharT, Traits, Allocator>& lhs,
            const basic_string<CharT, Traits, Allocator>& rhs) noexcept {
    return lhs.compare(rhs) < 0;
  }

  template<class CharT, class Traits, class Allocator>
  constexpr bool
  operator==(const basic_string<CharT, Traits, Allocator>& lhs,
             const basic_string<CharT, Traits, Allocator>& rhs) noexcept {
    if (lhs.size() != rhs.size())
      return false;
    for (size_t i = 0; i < lhs.size(); ++i)
      if (lhs[i] != rhs[i])
        return false;
    return true;
  }

  template<class CharT, class Traits, class Allocator>
  constexpr bool
  operator==(const CharT* lhs_cp,
             const basic_string<CharT, Traits, Allocator>& rhs) noexcept {
    basic_string<CharT, Traits, Allocator> lhs = lhs_cp;
    if (lhs.size() != rhs.size())
      return false;
    for (size_t i = 0; i < lhs.size(); ++i)
      if (lhs[i] != rhs[i])
        return false;
    return true;
  }

  template<class CharT, class Traits, class Allocator>
  constexpr bool
  operator==(const basic_string<CharT, Traits, Allocator>& lhs,
             const CharT* rhs_cp) noexcept {
    basic_string<CharT, Traits, Allocator> rhs = rhs_cp;
    if (lhs.size() != rhs.size())
      return false;
    for (size_t i = 0; i < lhs.size(); ++i)
      if (lhs[i] != rhs[i])
        return false;
    return true;
  }

  template<class CharT, class Traits, class Allocator>
  constexpr void
  swap(basic_string<CharT, Traits, Allocator>& lhs,
       basic_string<CharT, Traits, Allocator>& rhs)
  noexcept {
    lhs.swap(rhs);
  }

  using string    = basic_string<char>;
  using u8string  = basic_string<char8_t>;
  using u16string = basic_string<char16_t>;
  using u32string = basic_string<char32_t>;
  using wstring   = basic_string<wchar_t>;

  string to_string(int32_t);
  string to_string(int64_t);
  string to_string(uint32_t);
  string to_string(uint64_t);

  template<>
  struct hash<string> {
    size_t operator()(const string& str) const {
      auto it = str.begin();
      size_t h = 5381;
      while (it != str.end())
        h += (h << 5) + *it++;
      return h;
    }
  };
}
