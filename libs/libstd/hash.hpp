#pragma once
#include <type_traits>

namespace std {

  constexpr size_t __hash_combine(const size_t& seed, const size_t& hash) noexcept {
    return (hash + 0x9e3779b9ull + (seed << 6) + (seed >> 2)) ^ seed;  //boost formula
  }

#define BASIC_HASH(Type) \
    template<> \
    struct hash<Type>{ \
        size_t operator()(const Type& k) const { return *reinterpret_cast<typename __sized_int<sizeof(Type)>::unsigned_t const*>(&k); }\
    }

  BASIC_HASH(char);
  BASIC_HASH(signed char);
  BASIC_HASH(unsigned char);
  BASIC_HASH(char8_t);
  BASIC_HASH(char16_t);
  BASIC_HASH(char32_t);
  BASIC_HASH(wchar_t);
  BASIC_HASH(short);
  BASIC_HASH(unsigned short);
  BASIC_HASH(int);
  BASIC_HASH(unsigned int);
  BASIC_HASH(long);
  BASIC_HASH(unsigned long);
  BASIC_HASH(long long);
  BASIC_HASH(unsigned long long);
  BASIC_HASH(float);
  BASIC_HASH(double);
// BASIC_HASH(long double); //too big (16 bytes)

  template<>
  struct hash<long double> {
    size_t operator()(const long double& k) const {
      return __hash_combine(reinterpret_cast<size_t const*>(&k)[0], reinterpret_cast<size_t const*>(&k)[1]);
    }
  };

#undef BASIC_HASH

  template<>
  struct hash<bool> {
    size_t operator()(const bool& k) const {
      return k ? 1 : 0;
    }
  };

  template<>
  struct hash<nullptr_t> {
    size_t operator()(const nullptr_t&) const {
      return 0;
    }
  };

  template<typename T>
  struct hash<T*> {
    size_t operator()(const T*& k) const {
      return reinterpret_cast<uintptr_t>(k);
    }
  };

}
