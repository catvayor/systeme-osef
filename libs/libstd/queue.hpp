#pragma once
#include <new>
#include <algorithm>
#include <memory>
#include <optional>

namespace std {

  template<typename T, typename Allocator = allocator<T> >
  class rotative_buffer {
   public:
    typedef T value_type;
    typedef Allocator allocator_type;
    typedef allocator_traits<allocator_type> __traits;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T& reference;
    typedef T const& const_reference;
    typedef __traits::pointer pointer;
    typedef __traits::const_pointer const_pointer;
    typedef pointer iterator;
    typedef const_pointer const_iterator;

   protected:
    Allocator M_allocator;
    pointer M_array;
    size_type M_beg;
    size_type M_end;
    size_type M_capacity;

    void reallocate(size_type new_capacity) {
      ++new_capacity;
      pointer new_array = M_allocator.allocate(new_capacity);

      size_t end = 0;
      if (M_beg <= M_end)
        for (size_type i = M_beg; i < M_end; ++i) {
          new (new_array + end) T(std::move(M_array[i]));
          M_array[i].~T();
          ++end;
        } else {
        for (size_type i = M_beg; i < M_capacity; ++i) {
          new (new_array + end) T(std::move(M_array[i]));
          M_array[i].~T();
          ++end;
        }
        for (size_type i = 0; i < M_end; ++i) {
          new (new_array + end) T(std::move(M_array[i]));
          M_array[i].~T();
          ++end;
        }
      }

      if (M_array != nullptr)
        M_allocator.deallocate(M_array, M_capacity);
      M_array = new_array;
      M_beg = 0;
      M_end = end;
      M_capacity = new_capacity;
    }

    void grow(size_type delta) {
      reallocate(capacity() + std::max(capacity(), delta));
    }

   public:

    rotative_buffer(const Allocator& alloc = Allocator()): M_allocator(alloc), M_array(nullptr), M_beg(0), M_end(0), M_capacity(1) { }

    rotative_buffer(rotative_buffer const& other)
      : rotative_buffer(other.M_allocator) {
      operator=(other);
    }

    ~rotative_buffer() {
      if (M_array != nullptr) {
        if (M_beg <= M_end)
          for (size_type i = M_beg; i < M_end; ++i)
            M_array[i].~T();
        else {
          for (size_type i = M_beg; i < M_capacity; ++i)
            M_array[i].~T();
          for (size_type i = 0; i < M_end; ++i)
            M_array[i].~T();
        }
        M_allocator.deallocate(M_array, M_capacity);
      }
    }

    rotative_buffer& operator=(const rotative_buffer& other) {
      clear();
      reserve(other.size());
      size_t end = 0;
      if (other.M_beg <= other.M_end)
        for (size_type i = other.M_beg; i < other.M_end; ++i) {
          new (M_array + end) T(other.M_array[i]);
          ++end;
        } else {
        for (size_type i = other.M_beg; i < other.M_capacity; ++i) {
          new (M_array + end) T(other.M_array[i]);
          ++end;
        }
        for (size_type i = 0; i < other.M_end; ++i) {
          new (M_array + end) T(other.M_array[i]);
          ++end;
        }
      }
      M_beg = 0;
      M_end = end;
      return *this;
    }

    reference front() {
      return M_array[M_beg];
    }
    const_reference front() const {
      return M_array[M_beg];
    }

    reference back() {
      return M_array[(M_end + M_capacity - 1) % M_capacity];
    }
    const_reference back() const {
      return M_array[(M_end + M_capacity - 1) % M_capacity];
    }

    size_type size() const {
      return M_end >= M_beg ? M_end - M_beg : capacity() + 1 - M_beg + M_end;
    }
    size_type capacity() const {
      return M_capacity - 1;
    }
    bool empty() const {
      return size() == 0;
    }

    void shrink_to_fit() {
      if (size() != capacity()) reallocate(size());
    }

    void push_back(const value_type& value) {
      if (size() == capacity())
        grow(1);
      new (M_array + M_end) T(value);
      M_end = (M_end + 1) % M_capacity;
    }

    void pop_back() {
      M_end = (M_end + M_capacity - 1) % M_capacity;
      (M_array + M_end)->~T();
    }

    void pop_front() {
      (M_array + M_beg)->~T();
      M_beg = (M_beg + 1) % M_capacity;
    }

    void clear() {
      while (!empty())
        pop_back();
    }

    void reserve(size_type count) {
      if (size() < count)
        reallocate(std::max(capacity() + capacity(), count));
    }

    reference operator[](size_type idx) {
      return M_array[(idx + M_beg) % M_capacity];
    }

    const_reference operator[](size_type idx) const {
      return M_array[(idx + M_beg) % M_capacity];
    }
  };

  template<typename T, typename Alloc = allocator<T> >
  struct queue {
    queue() { }

    explicit queue(const Alloc& alloc): M_container(alloc) { }

    bool empty() const {
      return M_container.empty();
    }

    size_t size() const {
      return M_container.size();
    }

    void clear() {
      M_container.clear();
    }

    T& front() {
      return M_container.front();
    }
    T const& front() const {
      return M_container.front();
    }

    T& back() {
      return M_container.back();
    }
    T const& back() const {
      return M_container.back();
    }

    optional<T> pop() {
      if (!empty()) {
        T ret = front();
        M_container.pop_front();
        return ret;
      }
      return optional<T>();
    }

    void push(const T& val) {
      M_container.push_back(val);
    }
   private:
    typedef rotative_buffer<T, Alloc> Container;
    Container M_container;
  };
}
