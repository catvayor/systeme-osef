#include <cstdio>

namespace std {
  FILE fd_0 = 0;
  FILE fd_1 = 1;
  FILE fd_2 = 2;
  FILE* stdin = &fd_0;
  FILE* stdout = &fd_1;
  FILE* stderr = &fd_2;
}
