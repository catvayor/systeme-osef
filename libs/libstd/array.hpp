#pragma once
#include <cstddef>
#include <stdint.h>

namespace std {

  template<typename T, std::size_t N>
  struct array {
    typedef T                                       value_type;
    typedef value_type*                             pointer;
    typedef const value_type*                       const_pointer;
    typedef value_type&                             reference;
    typedef const value_type&                       const_reference;
    typedef value_type*                             iterator;
    typedef const value_type*                       const_iterator;
    typedef std::size_t                             size_type;
    typedef std::ptrdiff_t                          difference_type;
    //typedef std::reverse_iterator<iterator>	        reverse_iterator;
    //typedef std::reverse_iterator<const_iterator>   const_reverse_iterator;

    T m_data[N];

    iterator begin() noexcept {
      return iterator(data());
    }

    const_iterator begin() const noexcept {
      return const_iterator(data());
    }

    iterator end() noexcept {
      return iterator(data() + N);
    }

    const_iterator end() const noexcept {
      return const_iterator(data() + N);
    }

//      _GLIBCXX17_CONSTEXPR reverse_iterator
//      rbegin() noexcept
//      { return reverse_iterator(end()); }
//
//      _GLIBCXX17_CONSTEXPR const_reverse_iterator
//      rbegin() const noexcept
//      { return const_reverse_iterator(end()); }
//
//      _GLIBCXX17_CONSTEXPR reverse_iterator
//      rend() noexcept
//      { return reverse_iterator(begin()); }
//
//      _GLIBCXX17_CONSTEXPR const_reverse_iterator
//      rend() const noexcept
//      { return const_reverse_iterator(begin()); }

    const_iterator cbegin() const noexcept {
      return const_iterator(data());
    }

    const_iterator cend() const noexcept {
      return const_iterator(data() + N);
    }

//      _GLIBCXX17_CONSTEXPR const_reverse_iterator
//      crbegin() const noexcept
//      { return const_reverse_iterator(end()); }
//
//      _GLIBCXX17_CONSTEXPR const_reverse_iterator
//      crend() const noexcept
//      { return const_reverse_iterator(begin()); }


    constexpr size_type size() const noexcept {
      return N;
    }

    constexpr size_type max_size() const noexcept {
      return N;
    }

    [[nodiscard]] constexpr bool empty() const noexcept {
      return size() == 0;
    }

    reference operator[](size_type __n) noexcept {
      return at(__n);
    }

    constexpr const_reference operator[](size_type __n) const noexcept {
      return at(__n);
    }

    reference at(size_type __n) {
      return m_data[__n];
    }

    constexpr const_reference at(size_type __n) const {
      return m_data[__n];
    }

    reference front() noexcept {
      return at(0);
    }

    constexpr const_reference front() const noexcept {
      return at(0);
    }

    reference back() noexcept {
      return at(N - 1);
    }

    constexpr const_reference back() const noexcept {
      return at(N - 1);
    }

    pointer data() noexcept {
      return m_data;
    }

    const_pointer data() const noexcept {
      return m_data;
    }

//      template<typename _Tp, typename... _Up>
//    array(_Tp, _Up...)
//      -> array<enable_if_t<(is_same_v<_Tp, _Up> && ...), _Tp>,
//	       1 + sizeof...(_Up)>;

    constexpr void fill(const T& value) {
      for (T& x : *this)
        x = value;
    }
  };

};
