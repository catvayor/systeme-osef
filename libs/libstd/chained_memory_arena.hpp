#pragma once

#include <chains>
#include <optional>
#include <vector>

namespace std {

  template<typename T, template<typename U> class TemplateAlloc>
  class chained_memory_arena {
    typedef uint64_t index_t;
    chains<index_t, TemplateAlloc> ch = chains<index_t, TemplateAlloc>(2);
    vector<optional<T>, TemplateAlloc<optional<T> > > data;

    index_t new_index() {
      if (!ch.empty(0)) return *ch.pop_front(0);
      else {
        index_t idx = data.size();
        ch.fit_elem_attach(idx, 0);
        data.emplace_back();
        return idx;
      }
    }

    void fit(index_t idx) {
      if (data.size() <= idx) {
        ch.fit_elem_attach(idx, 0);
        data.resize(idx + 1);
      }
    }

   public:
    class iterator {
      chains<index_t, TemplateAlloc>::iterator ch_it;
      chained_memory_arena* arena;
     public:
      iterator(chains<index_t, TemplateAlloc>::iterator _ch_it, chained_memory_arena* _arena): ch_it(_ch_it), arena(_arena) {}
      T& operator*() {
        return arena->operator[](*ch_it);
      }

      iterator& operator++() {
        ++ch_it;
        return *this;
      }

      iterator& operator--() {
        --ch_it;
        return *this;
      }

      bool operator==(iterator it) const {
        return it.arena == arena && it.ch_it == ch_it;
      }
    };

    iterator begin() {
      return iterator(ch.iter(1).begin(), this);
    }

    iterator end() {
      return iterator(ch.iter(1).end(), this);
    }

    bool oob(index_t i) {
      return i >= data.size();
    }

    T& operator[](index_t i) {
      return data[i].value();
    }

    void insert_before_idx(index_t pos, index_t idx, const T& x) {
      fit(idx);
      ch.push_before(pos, idx);
      data[idx] = optional<T>(x);
    }

    void insert_after_idx(index_t pos, index_t idx, const T& x) {
      fit(idx);
      ch.push_after(pos, idx);
      data[idx] = optional<T>(x);
    }

    index_t insert_before(index_t pos, const T& x) {
      index_t idx = new_index();
      insert_before_idx(pos, idx, x);
      return idx;
    }

    index_t insert_after(index_t pos, const T& x) {
      index_t idx = new_index();
      insert_after_idx(pos, idx, x);
      return idx;
    }

    void push_back_idx(index_t idx, const T& x) {
      fit(idx);
      ch.push_back(1, idx);
      data[idx] = optional<T>(x);
    }

    void push_front_idx(index_t idx, const T& x) {
      fit(idx);
      ch.push_front(1, idx);
      data[idx] = optional<T>(x);
    }

    index_t push_back(const T& x) {
      index_t idx = new_index();
      push_back_idx(idx, x);
      return idx;
    }

    index_t push_front(const T& x) {
      index_t idx = new_index();
      push_front_idx(idx, x);
      return idx;
    }

    bool used(index_t idx) {
      return idx < data.size() && data[idx];
    }

    void pop_idx(index_t idx) {
      if (!used(idx)) return;
      ch.push_back(0, idx);
      data[idx] = optional<T>();
    }

    optional<T> pop_back() {
      if (ch.empty(1)) return optional<T>();
      return optional<T>(pop_idx(ch.back(1)));
    }

    optional<T> pop_front() {
      if (ch.empty(1)) return optional<T>();
      return optional<T>(pop_idx(ch.front(1)));
    }

    bool empty() {
      return ch.empty(1);
    }

    index_t front_idx() {
      return ch.front(1);
    }

    index_t back_idx() {
      return ch.back(1);
    }

    T& front() {
      return operator[](front_idx());
    }

    T& back() {
      return operator[](back_idx());
    }
  };

}
