#pragma once
#include <functional>
#include <memory>
#include <optional>
#include <utility>
#include <vector>

namespace std {
  template<typename Key,
           typename Hash = hash<Key>,
           typename KeyEqual = equal_to<Key>,
           typename Alloc = allocator<char> >//Whatever type for alloc, it will be rebinded
  class unordered_set {
   public:
    typedef Key value_type;
    typedef const value_type& const_reference;
    class iterator {
      size_t M_bucket_idx;
      size_t M_hash;
      const unordered_set<Key, Hash, KeyEqual, Alloc> *M_container;
     public:
      iterator(size_t bucket_idx, size_t hash, const unordered_set<Key, Hash, KeyEqual, Alloc> *container):
        M_bucket_idx(bucket_idx), M_hash(hash), M_container(container) {}
      const_reference operator*() const {
        return *M_container->M_buckets[M_bucket_idx].data();
      }
      bool operator==(iterator it) const {
        return M_container == it.M_container && M_bucket_idx == it.M_bucket_idx;
      }
      iterator operator++() {
        size_t nxt;
        if (M_bucket_idx != ~0ul)
          nxt = M_container->M_buckets[M_bucket_idx].nxt;
        else {
          M_hash = 0;
          nxt = M_container->M_hash_bucket[0];
        }
        while (M_container->M_buckets[nxt].free() && M_hash < M_container->M_hash_bucket.size() - 1) {
          M_hash++;
          nxt = M_container->M_hash_bucket[M_hash];
        }
        if (M_container->M_buckets[nxt].free()) { // end
          M_bucket_idx = ~0;
          M_hash = ~0;
        } else
          M_bucket_idx = nxt;
        return *this;
      }
    };
   private:
    template<typename U>
    using allocator_t = typename allocator_traits<Alloc>::template rebind_alloc<U>;

    static constexpr size_t __no_nxt = ~0ull;
    static constexpr size_t __free = ~1ull;
    struct node {
      char M_data[max(sizeof(value_type), sizeof(size_t))] alignas(value_type);
      size_t nxt;
      node(): nxt(__free) {
        nxt_free() = __no_nxt;
      }
      node(const node& other): nxt(other.nxt) {
        if (!other.free())
          new (data()) value_type(std::move(*other.data()));
        else
          nxt_free() = other.nxt_free();
      }
      node(node&& other): nxt(other.nxt) {
        if (!other.free())
          new (data()) value_type(std::move(*other.data()));
        else
          nxt_free() = other.nxt_free();
      }
      template<typename V>
      node(V&& val, size_t n): nxt(n) {
        new (data()) value_type(std::forward<V>(val));
      }
      ~node() {
        if (!free())
          data()->~value_type();
      }

      template<typename V>
      void emplace(V&& val, size_t n) {
        nxt = n;
        new (data()) value_type(std::forward<V>(val));
      }

      bool free() const {
        return nxt == __free;
      }

      value_type* data() {
        return (value_type*)M_data;
      }
      value_type const* data() const {
        return (value_type const*)M_data;
      }
      size_t& nxt_free() {
        return *(size_t*)M_data;
      }
      size_t const& nxt_free() const {
        return *(size_t const*)M_data;
      }
    };

    std::vector<size_t, allocator_t<size_t> > M_hash_bucket;
    std::vector<node, allocator_t<node> > M_buckets;
    size_t free_stack_top;
    Hash M_hash;
    KeyEqual M_eq;
    size_t M_size;

    size_t getNewBlk() {
      if (free_stack_top == __no_nxt) {
        M_buckets.emplace_back();
        if (M_buckets.size() > M_hash_bucket.size() * 2) {
          M_hash_bucket.resize(M_hash_bucket.size() * 2);
          rehash();
        }
        return M_buckets.size() - 1;
      }
      size_t ret = free_stack_top;
      free_stack_top = M_buckets[ret].nxt_free();
      return ret;
    }

    size_t hash_bucket(const size_t& h) const {
      return h % M_hash_bucket.size();
    }

   public:
    unordered_set(Hash const& hash = Hash(), KeyEqual const& eq = KeyEqual(), Alloc const& alloc = Alloc())
      : M_hash_bucket(1, __no_nxt, allocator_t<size_t>(alloc)), M_buckets(allocator_t<node>(alloc)),
        free_stack_top(__no_nxt), M_hash(hash), M_eq(eq), M_size(0) { }

    void rehash() {
      for (size_t& b : M_hash_bucket)
        b = __no_nxt;
      for (size_t i = 0; i < M_buckets.size(); ++i) {
        node& n = M_buckets[i];
        if (n.free())
          continue;
        size_t hash = hash_bucket(M_hash(*n.data()));
        n.nxt = M_hash_bucket[hash];
        M_hash_bucket[hash] = i;
      }
    }

    std::pair<iterator, bool> insert(const value_type& val) {
      size_t hash = M_hash(val);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      while (bucket != __no_nxt) {
        if (M_eq(val, *M_buckets[bucket].data()))
          return make_pair(iterator(bucket, hash_bucket(hash), this), false);
        bucket = M_buckets[bucket].nxt;
      }
      size_t dest_bucket = getNewBlk();
      size_t& bucket_top = M_hash_bucket[hash_bucket(hash)];
      M_buckets[dest_bucket].emplace(val, bucket_top);
      bucket_top = dest_bucket;
      M_size++;
      return make_pair(iterator(dest_bucket, hash_bucket(hash), this), true);
    }

    bool erase(const value_type& key) {
      size_t hash = M_hash(key);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      size_t prec_blk = bucket;
      while (bucket != __no_nxt) {
        if (M_eq(key, *M_buckets[bucket].data())) {
          M_buckets[bucket].data()->~value_type();
          if (prec_blk == bucket) //top of bucket
            M_hash_bucket[hash_bucket(hash)] = M_buckets[bucket].nxt;
          else
            M_buckets[prec_blk].nxt = M_buckets[bucket].nxt;
          M_buckets[bucket].nxt = __free;
          M_buckets[bucket].nxt_free() = free_stack_top;
          free_stack_top = bucket;
          M_size--;
          return true;
        }
        prec_blk = bucket;
        bucket = M_buckets[bucket].nxt;
      }
      return false;
    }

    bool contains(const value_type& key) const {
      size_t hash = M_hash(key);
      size_t bucket = M_hash_bucket[hash_bucket(hash)];
      size_t prec_blk = bucket;
      while (bucket != __no_nxt) {
        if (M_eq(key, *M_buckets[bucket].data()))
          return true;
        prec_blk = bucket;
        bucket = M_buckets[bucket].nxt;
      }
      return false;
    }

    size_t size() const {
      return M_size;
    }

    bool empty() const {
      return M_size == 0;
    }

    iterator end() const {
      return iterator(~0, ~0, this);
    }

    iterator begin() const {
      return ++end();
    }
  };
}
