#pragma once
#include <cstddef>
#include <cstdint>

namespace std {

  template<typename T, T v>
  struct integral_constant {
    typedef T value_type;
    static constexpr T value = v;
    constexpr operator T() const noexcept {
      return v;
    }
    constexpr T operator()() const noexcept {
      return v;
    }
  };

  typedef integral_constant<bool, true> true_type;
  typedef integral_constant<bool, false> false_type;

//conditional
  template<bool B, typename T, typename F>
  struct conditional {
    typedef T type;
  };

  template<typename T, typename F>
  struct conditional<false, T, F> {
    typedef F type;
  };

  template<bool B, typename T, typename F>
  using conditional_t = typename conditional<B, T, F>::type;

//enable_if
  template<bool B, typename T = void>
  struct enable_if {
    typedef T type;
  };

  template<typename T>
  struct enable_if<false, T> {
  };

  template<bool B, typename T = void>
  using enable_if_t = typename enable_if<B, T>::type;

//conjunction
  template<typename... T>
  struct conjunction : true_type {};

  template<typename H, typename... Q>
  struct conjunction<H, Q...> : conditional_t<H::value, conjunction<Q...>, false_type> { };

  template<typename... T>
  inline constexpr bool conjunction_v = conjunction<T...>::value;

//disjunction
  template<typename... T>
  struct disjunction : false_type { };

  template<typename H, typename... Q>
  struct disjunction<H, Q...> : conditional_t<H::value, true_type, disjunction<Q...> > { };

  template<typename... T>
  inline constexpr bool disjunction_v = disjunction<T...>::value;

//negation
  template<typename T>
  struct negation : conditional_t<T::value, false_type, true_type> { };

  template<typename T>
  inline constexpr bool negation_v = negation<T>::value;

//type_identity
  template<typename T>
  struct type_identity {
    typedef T type;
  };

  template<typename T>
  using type_identity_t = typename type_identity<T>::type;

//is_same
  template<typename T, typename U>
  struct is_same : false_type {};
  template<typename T>
  struct is_same<T, T> : true_type {};

  template<typename T, typename U>
  inline constexpr bool is_same_v = is_same<T, U>::value;

//remove_cv
  template<typename T>
  struct remove_cv {
    typedef T type;
  };

  template<typename T>
  struct remove_cv<const T> {
    typedef T type;
  };

  template<typename T>
  struct remove_cv<volatile T> {
    typedef T type;
  };

  template<typename T>
  struct remove_cv<const volatile T> {
    typedef T type;
  };

  template<typename T>
  using remove_cv_t = typename remove_cv<T>::type;

//remove_const
  template<typename T>
  struct remove_const {
    typedef T type;
  };

  template<typename T>
  struct remove_const<const T> {
    typedef T type;
  };

  template<typename T>
  using remove_const_t = typename remove_const<T>::type;

//remove_volatile
  template<typename T>
  struct remove_volatile {
    typedef T type;
  };

  template<typename T>
  struct remove_volatile<volatile T> {
    typedef T type;
  };

  template<typename T>
  using remove_volatile_t = typename remove_volatile<T>::type;

//remove_reference
  template<typename T>
  struct remove_reference {
    typedef T type;
  };
  template<typename T>
  struct remove_reference<T&> {
    typedef T type;
  };
  template<typename T>
  struct remove_reference < T&& > {
    typedef T type;
  };

  template<typename T>
  using remove_reference_t = typename remove_reference<T>::type;

//make unsigned

  template<size_t s>
  struct __sized_int;

  template<>
  struct __sized_int<1> {
    typedef int8_t  signed_t;
    typedef uint8_t  unsigned_t;
  };
  template<>
  struct __sized_int<2> {
    typedef int16_t signed_t;
    typedef uint16_t unsigned_t;
  };
  template<>
  struct __sized_int<4> {
    typedef int32_t signed_t;
    typedef uint32_t unsigned_t;
  };
  template<>
  struct __sized_int<8> {
    typedef int64_t signed_t;
    typedef uint64_t unsigned_t;
  };

  template<typename T>
  struct make_unsigned {
    typedef typename __sized_int<sizeof(T)>::unsigned_t type;
  };

  template<typename T>
  using make_unsigned_t = typename make_unsigned<T>::type;

//is_void
  template<typename T>
  struct is_void : is_same<remove_cv_t<T>, void> {};

  template<typename T>
  inline constexpr bool is_void_v = is_void<T>::value;

//void_t

  template<typename...>
  using void_t = void;

//is_lvalue_reference
  template<typename T>
  struct is_lvalue_reference : false_type { };
  template<typename T>
  struct is_lvalue_reference<T&> : true_type { };

  template<typename T>
  inline constexpr bool is_lvalue_reference_v = is_lvalue_reference<T>::value;

//is_convertible
  template<typename T>
  auto __test_returnable(int) -> decltype(void(static_cast<T(*)()>(nullptr)), true_type {});
  template<typename>
  false_type __test_returnable(...);

  template<typename To>
  true_type __test_convertible(To);
  template<typename>
  false_type __test_convertible(...);

  template<typename From>
  From __try_decl(int);
  template<typename>
  false_type __try_decl(...);

  template<typename From, typename To>
  struct is_convertible :
  conjunction<decltype(__test_returnable<To>(0)),
  decltype(__test_returnable<From>(0)),
           decltype(__test_convertible<To>(__try_decl<From>(0)))> {};

  template<typename From, typename To>
  inline constexpr bool is_convertible_v = is_convertible<From, To>::value;

//replace first arg
  template<typename T, typename U>
  struct __replace_first_arg;

  template<template<typename, typename...> typename Temp, typename H, typename... Q, typename U>
  struct __replace_first_arg<Temp<H, Q...>, U> {
    typedef Temp<U, Q...> type;
  };

  template<typename T, typename U>
  using __replace_first_arg_t = typename __replace_first_arg<T, U>::type;

}
