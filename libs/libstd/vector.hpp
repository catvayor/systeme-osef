#pragma once
#include <memory>
#include <algorithm>
#include <new>
#include <initializer_list>

namespace std {

  template<typename T, typename Allocator = allocator<T> >
  class vector {
   public:
    typedef T value_type;
    typedef Allocator allocator_type;
    typedef allocator_traits<allocator_type> __traits;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T& reference;
    typedef T const& const_reference;
    typedef __traits::pointer pointer;
    typedef __traits::const_pointer const_pointer;
    typedef pointer iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef const_pointer const_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

   protected:
    Allocator M_allocator;
    pointer M_array;
    size_type M_size;
    size_type M_capacity;

    void reallocate(size_type new_capacity) {
      pointer new_array = M_allocator.allocate(new_capacity);
      for (size_type i = 0; i < M_size; ++i) {
        new (new_array + i) T(std::move(operator[](i)));
        operator[](i).~T();
      }
      if (M_array != nullptr)
        M_allocator.deallocate(M_array, M_capacity);
      M_array = new_array;
      M_capacity = new_capacity;
    }

    void grow(size_type delta) {
      reallocate(M_size + std::max(M_size, delta));
    }

   public:
    explicit vector(const Allocator& alloc = Allocator()): M_allocator(alloc), M_array(nullptr), M_size(0), M_capacity(0) { }

    vector(size_type count, const_reference value, const Allocator& alloc = Allocator())
      : vector(alloc) {
      resize(count, value);
    }

    explicit vector(size_type count, const Allocator& alloc = Allocator())
      : vector(alloc) {
      resize(count);
    }

    template<typename InputIt>
    vector(InputIt beg, InputIt end, const Allocator& alloc = Allocator()) requires requires(InputIt a) {
      is_convertible_v<decltype(*a), T>;
    }
   :
    vector(alloc) {
      for (auto i = beg; i != end; ++i)
        push_back(*i);
    }

    vector(vector const& other)
      : vector(other.begin(), other.end(), other.M_allocator) { }

    vector(vector&& other)
      : M_allocator(std::move(other.M_allocator)), M_array(other.M_array),
        M_size(other.M_size), M_capacity(other.M_capacity) {
      other.M_array = nullptr;
    }

    constexpr vector(std::initializer_list<T> init, const Allocator& alloc = Allocator()) : vector(alloc) {
      for (auto const& x : init)
        push_back(x);
    }

    ~vector() {
      if (M_array != nullptr) {
        for (T& elem : *this)
          elem.~T();
        M_allocator.deallocate(M_array, M_capacity);
      }
    }

    vector& operator=(const vector& other) {
      clear();
      for (const_reference elem : other)
        push_back(elem);
      return *this;
    }

    pointer data() {
      return M_array;
    }
    const_pointer data() const {
      return M_array;
    }

    reference front() {
      return *data();
    }
    const_reference front() const {
      return *data();
    }

    reference back() {
      return *(data() + M_size - 1);
    }
    const_reference back() const {
      return *(data() + M_size - 1);
    }

    size_type size() const {
      return M_size;
    }
    size_type capacity() const {
      return M_capacity;
    }
    bool empty() const {
      return size() == 0;
    }

    void shrink_to_fit() {
      if (M_size != M_capacity) reallocate(M_size);
    }

    void push_back(const value_type& value) {
      if (M_size == M_capacity)
        grow(1);
      new (data() + (M_size++)) T(value);
    }

    void push_back(value_type&& value) {
      if (M_size == M_capacity)
        grow(1);
      new (data() + (M_size++)) T(std::move(value));
    }

    template<typename... Args>
    void emplace_back(Args&&... args) {
      if (M_size == M_capacity)
        grow(1);
      new (data() + (M_size++)) T(std::forward<Args>(args)...);
    }

    void pop_back() {
      --M_size;
      operator[](M_size).~T();
    }

    void clear() {
      for (size_type i = 0; i < M_size; ++i)
        operator[](i).~T();
      M_size = 0;
    }

    void reserve(size_type count) {
      if (M_size < count)
        reallocate(std::max(M_size + M_size, count));
    }

    void resize(size_type count, const value_type& value) {
      reserve(count);
      for (size_type i = count; i < M_size; ++i)
        operator[](i).~T();
      for (size_type i = M_size; i < count; ++i)
        new (data() + i) T(value);
      M_size = count;
    }

    void resize(size_type count) {
      reserve(count);
      for (size_type i = count; i < M_size; ++i)
        operator[](i).~T();
      for (size_type i = M_size; i < count; ++i)
        new (data() + i) T();
      M_size = count;
    }

    reference operator[](size_type idx) {
      return *(M_array + idx);
    }

    const_reference operator[](size_type idx) const {
      return *(M_array + idx);
    }

    //iterators
    iterator begin() {
      return M_array;
    }
    iterator end() {
      return M_array + M_size;
    }

    const_iterator begin() const {
      return M_array;
    }
    const_iterator end() const {
      return M_array + M_size;
    }

    const_iterator cbegin() const {
      return M_array;
    }
    const_iterator cend() const {
      return M_array + M_size;
    }

    iterator erase(iterator pos) {
      for (iterator i = pos; i + 1 != end(); ++i) {
        pos->~T();
        new (pos) T(std::move(*(i + 1)));
      }
      pop_back();
      return pos;
    }

    iterator erase(iterator first, iterator last) {
      if (first == last)
        return last;
      iterator k = first;
      for (iterator i = last; i != end(); ++i) {
        k->~T();
        new (k) T(std::move(*i));
        ++k;
      }
      for (; k != end(); ++k)
        k->~T();
      M_size -= last - first;
      return first;
    }
  };

}
