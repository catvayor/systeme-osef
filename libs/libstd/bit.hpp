#pragma once

namespace std {

  template<typename T>
  constexpr T bit_width(T val) noexcept {
    T ret = T(0);
    for (; val != T(0); ++ret) val >>= T(1);
    return ret;
  }

  template<typename T>
  constexpr T bit_floor(T val) noexcept {
    if (val != 0)
      return 1 << (bit_width(val) - 1);
    return 0;
  }
}
