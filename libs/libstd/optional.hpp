#pragma once
#include <new>
#include <utility>

namespace std {

  template<typename T>
  class optional {
    char M_data[sizeof(T)] alignas(T);
    bool M_present;
   public:
    optional(): M_present(false) { }
    optional(const T& value): M_present(true) {
      new (M_data) T(value);
    }
    optional(T&& value): M_present(true) {
      new (M_data) T(move(value));
    }
    optional(const optional& other): M_present(other.M_present) {
      if (M_present)
        new (M_data) T(*other);
    }
    optional(optional&& other): M_present(other.M_present) {
      if (M_present)
        new (M_data) T(move(*other));
    }

    ~optional() {
      if (*this)
        (**this).~T();
    }

    optional& operator=(const T& value) {
      if (*this)
        **this = value;
      else {
        new (M_data) T(value);
        M_present = true;
      }
      return *this;
    }

    optional& operator=(const optional& other) {
      if (*this)
        if (other)
          **this = *other;
        else {
          (**this).~T();
          M_present = false;
        } else if (other) {
        new (M_data) T(*other);
        M_present = true;
      }
      return *this;
    }

    optional& operator=(optional&& other) {
      if (*this)
        if (other)
          **this = move(*other);
        else {
          (**this).~T();
          M_present = false;
        } else if (other) {
        new (M_data) T(move(*other));
        M_present = true;
      }
      return *this;
    }


    T* operator->() {
      return reinterpret_cast<T*>(M_data);
    }
    T& operator*() {
      return *operator->();
    }

    T const* operator->() const {
      return reinterpret_cast<T const*>(M_data);
    }
    T const& operator*() const {
      return *operator->();
    }

    bool has_value() const {
      return M_present;
    }
    explicit operator bool() const {
      return has_value();
    }

    T& value() {
      return **this;
    }
    T const& value() const {
      return **this;
    }

    T const& value_or(const T& def_val) const {
      return *this ? **this : def_val;
    }

    void reset() {
      **this = optional();
    }

    template<typename... Args>
    T& emplace(Args&&... args) {
      reset();
      new (M_data) T(forward<Args>(args)...);
      return **this;
    }
  };

  template<typename T, typename... Args>
  optional<T> make_optional(Args&&... args) {
    return optional<T>(forward<Args>(args)...);
  }
}
