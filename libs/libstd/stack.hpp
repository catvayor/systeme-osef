#pragma once

#include <optional>
#include <vector>

namespace std {

  template<typename T, typename Alloc = allocator<T> >
  class stack {
    vector<T, Alloc> M_data;
   public:
    explicit stack(Alloc const& alloc = Alloc()): M_data(alloc) { }

    bool empty() const {
      return M_data.empty();
    }
    size_t size() const {
      return M_data.size();
    }

    T& peek() {
      return M_data.back();
    }
    T const& peek() const {
      return M_data.back();
    }

    optional<T> pop() {
      if (!empty()) {
        T ret = M_data.back();
        M_data.pop_back();
        return ret;
      }
      return {};
    }

    void push(const T& val) {
      M_data.push_back(val);
    }
  };
}
