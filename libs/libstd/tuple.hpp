#pragma once
#include <cstddef>
#include <utility>
#include <type_traits>

namespace std {

  template<size_t I, typename T>
  struct __tuple_head {
    T v;

    __tuple_head() { }
    template<typename A>
    __tuple_head(A&& a): v(/*forward*/a) { }

    constexpr void swap(__tuple_head& other) {
      std::swap(v, other.v);
    }

    __tuple_head& operator= (__tuple_head const& other) {
      v = other.v;
      return *this;
    }
  };

  template<size_t, typename...>
  struct __tuple_tail {
    constexpr void swap(__tuple_tail&) { }

    __tuple_tail& operator= (__tuple_tail const&) {
      return *this;
    }

  };

  template<size_t I, typename T, typename... Q>
  struct __tuple_tail<I, T, Q...> : public __tuple_head<I, T>, __tuple_tail < I + 1, Q... > {
    using Head = __tuple_head<I, T>;
    using Tail = __tuple_tail < I + 1, Q... >;

    __tuple_tail() : Head(), Tail() { }
    template<typename A, typename... Args>
    __tuple_tail(A&& a, Args&&... args) : Head(/*forward*/a), Tail(/*forward*/args...) { }

    constexpr void swap(__tuple_tail& other) {
      Head::swap(other);
      Tail::swap(other);
    }

    __tuple_tail& operator= (__tuple_tail const& other) {
      Head::operator=(other);
      Tail::operator=(other);
      return *this;
    }
  };

  template<typename T...>
  using tuple = __tuple_tail<0, T...>;

  template<typename... T>
  void swap(tuple<T...>& lhs, tuple<T...>& rhs) {
    lhs.swap(rhs);
  }

  template<size_t, typename>
  struct tuple_element;
  template<size_t I, typename T, typename... Q>
  struct tuple_element<I, T, Q...> : tuple_element < I - 1, Q... > { };
  template<typename T, typename... Q>
  struct tuple_element<0, T, Q...> {
    using type = T;
  };

  template<size_t I, typename T>
  using tuple_element_t = typename tuple_element<I, T>::type;

  template<typename>
  struct tuple_size;
  template<typename... T>
  struct tuple_size<tuple<T...> > : integral_constant<size_t, sizeof...(T)> { };

  template<size_t I, typename T>
  constexpr T& __get_impl(__tuple_head<I, T>& arg) {
    return arg.v;
  }
  template<size_t I, typename T>
  constexpr T const& __get_impl(__tuple_head<I, T> const& arg) {
    return arg.v;
  }

  template<typename T, size_t I>
  constexpr T& __get_impl(__tuple_head<I, T>& arg) {
    return arg.v;
  }
  template<typename T, size_t I>
  constexpr T const& __get_impl(__tuple_head<I, T> const& arg) {
    return arg.v;
  }

  template<size_t I, typename... T>
  constexpr tuple_element_t<I, tuple<T...> >& get(tuple<T...>& arg) {
    return __get_impl<I>(arg);
  }
  template<size_t I, typename... T>
  constexpr tuple_element_t<I, tuple<T...> > const& get(tuple<T...> const& arg) {
    return __get_impl<I>(arg);
  }

  template<typename Ty, typename... T>
  constexpr Ty& get(tuple<T...>& arg) {
    return __get_impl<Ty>(arg);
  }
  template<typename Ty, typename... T>
  constexpr Ty const& get(tuple<T...> const& arg) {
    return __get_impl<Ty>(arg);
  }

}
