#pragma once

#include <cstdio>
#include <cctype>

#ifdef LINUXCOMPAT
#include <linuxcompat>
#else
#include <syscall>
#endif

inline long strtol(const char *nptr, const char **endptr = nullptr, int base = 0) {
  while (isspace(*nptr)) nptr++;
  long sign = +1;
  switch (*nptr) {
    case '-':
      sign = -1;
      __attribute__((fallthrough));
    case '+':
      nptr++;
  }

  if (base == 0) {
    if (nptr[0] == '0') {
      if (nptr[1] == 'x' || nptr[1] == 'X')
        base = 16;
      else
        base = 8;
    } else
      base = 10;
  }

  long n = 0;
  while (*nptr) {
    long digit = base;

    if (isdigit(*nptr))
      digit = (*nptr) - '0';
    else if (islower(*nptr))
      digit = (*nptr) - 'a' + 10;
    else if (isupper(*nptr))
      digit = (*nptr) - 'A' + 10;

    if (digit < base) {
      n *= base;
      n += digit;
    } else
      break;

    nptr++;
  }

  if (endptr)
    *endptr = nptr;

  return sign * n;
}

#ifndef SPEAK_QUENYA

[[noreturn]] inline void abort() {
  uint64_t mask = 1 << Syscall::SIGABRT;
  Syscall::sigprocmask(Syscall::SIG_UNBLOCK, &mask);
  Syscall::raise(Syscall::SIGABRT);
  for (;;); // Never executed
}

[[noreturn]] static inline void exit(int status) {
  Syscall::exit(status);
}

#endif
