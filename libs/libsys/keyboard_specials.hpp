#pragma once

#include <cstdint>

namespace Keyboard {
  enum Mods : int16_t {
    Shift = 1 << 8,
    Ctrl  = 1 << 9,
    Alt   = 1 << 10,
    AltGr   = 1 << 11,
    Mod4  = 1 << 12,
    Ext   = 1 << 14,
  };

  constexpr int16_t K_UP    = 0x48 + Ext;
  constexpr int16_t K_DOWN  = 0x50 + Ext;
  constexpr int16_t K_LEFT  = 0x4b + Ext;
  constexpr int16_t K_RIGHT = 0x4d + Ext;
}
