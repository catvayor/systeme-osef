#include "syscall.hpp"

namespace Syscall {
  [[noreturn]] void sigreturn() {
    syscall_0(sys_sigreturn);
    for (;;) asm("ud2");
  }
}
