#pragma once

#include <keyboard_specials>
#include <stdint.h>

extern char **environ;

namespace Syscall {
  constexpr uint64_t syscall_int = 0x42;
  constexpr unsigned pipe_out = 0;
  constexpr unsigned pipe_in = 1;
  constexpr int64_t et_null = 0;
  constexpr int64_t et_dir = 1;
  constexpr int64_t et_file = 2;
  constexpr uint64_t max_io_size = 0x1000;
  constexpr uint64_t max_fd = 0x100;

  typedef int64_t pid_t;
  typedef int64_t fd_t;
  typedef uint64_t mode_t;

  static inline void syscall_0(uint64_t id) {
    register uint64_t rax asm("rax") = id;
    asm volatile ("int %[nb]" :: "r"(rax), [nb] "i"(syscall_int) : "memory");
    asm volatile("nop");
  }

  static inline void syscall_1(uint64_t id, uint64_t arg1) {
    register uint64_t rax asm("rax") = id;
    register uint64_t rdi asm("rdi") = arg1;
    asm volatile ("int %[nb]" :: "r"(rax), "r"(rdi), [nb] "i"(syscall_int) : "memory");
    asm volatile("nop");
  }

  static inline uint64_t syscall_r_0(uint64_t id) {
    register uint64_t rax asm("rax") = id;
    asm volatile ("int %[nb]" : "=r"(rax) : "0"(rax), [nb] "i"(syscall_int) : "memory");
    asm volatile("nop");
    return rax;
  }

  static inline uint64_t syscall_r_1(uint64_t id, uint64_t arg1) {
    register uint64_t rax asm("rax") = id;
    register uint64_t rdi asm("rdi") = arg1;
    asm volatile ("int %[nb]" : "=r"(rax) : "0"(rax), "r"(rdi), [nb] "i"(syscall_int) : "memory");
    asm volatile("nop");
    return rax;
  }

  static inline uint64_t syscall_r_2(uint64_t id, uint64_t arg1, uint64_t arg2) {
    register uint64_t rax asm("rax") = id;
    register uint64_t rdi asm("rdi") = arg1;
    register uint64_t rsi asm("rsi") = arg2;
    asm volatile ("int %[nb]" : "=r"(rax) : "0"(rax), "r"(rdi), "r"(rsi), [nb] "i"(syscall_int) : "memory");
    asm volatile("nop");
    return rax;
  }

  static inline uint64_t syscall_r_3(uint64_t id, uint64_t arg1, uint64_t arg2, uint64_t arg3) {
    register uint64_t rax asm("rax") = id;
    register uint64_t rdi asm("rdi") = arg1;
    register uint64_t rsi asm("rsi") = arg2;
    register uint64_t rdx asm("rdx") = arg3;
    asm volatile ("int %[nb]" : "=r"(rax) : "0"(rax), "r"(rdi), "r"(rsi), "r"(rdx), [nb] "i"(syscall_int) : "memory");
    asm volatile("nop");
    return rax;
  }

  enum syscall_id : uint64_t {
    sys_exit,
    sys_fork,
    sys_microsleep,
    sys_execve,
    sys_waitpid,
    sys_open,
    sys_close,
    sys_dup2,
    sys_pipe,
    sys_flush_pipe,
    sys_sbrk,
    sys_write,
    sys_read,
    sys_pollin,
    sys_chdir,
    sys_opendir,
    sys_readdir,

    sys_kill,
    sys_raise,
    sys_sigaction,
    sys_sigreturn,
    sys_sigprocmask,
    sys_pause,

    sys_window_get,
    sys_window_refresh,
    sys_get_key,
  };

  enum filenos : fd_t {
    STDIN_FILENO = 0,
    STDOUT_FILENO = 1,
    STDERR_FILENO = 2,
    STDUSRIN_FILENO = 3,
  };

  enum err_id : int64_t {
    err_success  =  0,
    err_error    = -1,
    err_notfound = -2,
    err_eof      = -3,
    err_child    = -4,
    err_fault    = -5,
    err_badfd    = -6,
    err_nomem    = -7,
    err_size     = -8,
    err_nowin    = -9,
    err_intr     = -10,
    err_nohang   = -11,
    err_badpid   = -12,
    err_badsig   = -13,
  };

  enum open_flags : int64_t {
    O_RDONLY  = 1 << 0,
    O_WRONLY  = 1 << 1,
    O_CREAT   = 1 << 2,
    O_APPEND  = 1 << 3,
    O_TRUNC   = 1 << 4,
  };

  enum mode_flags : mode_t {
    S_IRWXU = 0x700,
    S_IRUSR = 0x400,
    S_IWUSR = 0x200,
    S_IXUSR = 0x010,
    S_IRWXG = 0x070,
    S_IRGRP = 0x040,
    S_IWGRP = 0x020,
    S_IXGRP = 0x010,
    S_IRWXO = 0x007,
    S_IROTH = 0x004,
    S_IWOTH = 0x002,
    S_IXOTH = 0x001,
  };

  enum wait_flags : int64_t {
    WNOHANG = 1 << 1,
  };

  constexpr unsigned NBSIG = 34;
  enum sig_type : uint64_t {
    SIGNONE = 0,
    SIGABRT = 1,
    SIGINT  = 2,
    SIGKILL = 3,
  };
  constexpr uint64_t SIG_NO_BLOCK = (1 << SIGNONE) | (1 << SIGKILL);
  enum exit_value : uint64_t {
    SIGINT_EXIT = 130,
    SIGKILL_EXIT = 143,
  };
  enum sig_mask_action : uint64_t {
    SIG_BLOCK,
    SIG_UNBLOCK,
    SIG_SETMASK,
  };

  [[noreturn]] void sigreturn();
  struct sigact {
    void (*sa_handler)(int) = nullptr;
    //void (*sa_sigaction)(int, siginfo_t*, void*);
    uint64_t sa_mask = 0;
    //int sa_flags;
    void (*sa_restorer)(void) = &sigreturn;
  };
#define SIG_DFL ((void(*)(int))(~0ul))
#define SIG_IGN ((void(*)(int))(~0ul-1ul))

  struct WindowData {
    uint32_t w = 0;
    uint32_t h = 0;
    uint16_t *data = nullptr;
  };

  struct WindowSize {
    uint32_t w, h;

    WindowSize(uint64_t v) {
      w = v;
      h = v >> 32;
    };
  };

  [[noreturn]] static inline void exit(int64_t code) {
    syscall_1(sys_exit, code);
    for (;;) asm("ud2");
  }
  static inline int64_t fork() {
    return syscall_r_0(sys_fork);
  }
  static inline int64_t waitpid(pid_t pid, int64_t *status, int64_t options) {
    return syscall_r_3(sys_waitpid, (uint64_t)pid, (uintptr_t)status, (uint64_t)options);
  }
  static inline int64_t wait(int64_t* status) {
    return waitpid(-1, status, 0);
  }
  static inline int64_t microsleep(uint64_t ms) {
    return syscall_r_1(sys_microsleep, ms);
  }
  static inline int64_t execve(char const* path, char const* const* argv, char const* const* envp) {
    return syscall_r_3(sys_execve, (uintptr_t)path, (uintptr_t)argv, (uintptr_t)envp);
  }
  static inline int64_t execv(char const* path, char const* const* argv) {
    return execve(path, argv, environ);
  }
  static inline int64_t exec(char const* path) {
    char const* argv[2] = {path, nullptr};
    return execv(path, argv);
  }
  static inline uintptr_t sbrk(int64_t delta) {
    return syscall_r_1(sys_sbrk, delta);
  }

  static inline int64_t open(const char* path, int64_t flags) {
    return syscall_r_2(sys_open, (uintptr_t)path, (uint64_t)flags);
  }
  static inline int64_t close(fd_t fd) {
    return syscall_r_1(sys_close, (uint64_t)fd);
  }
  static inline int64_t dup2(fd_t oldfd, fd_t newfd) {
    return syscall_r_2(sys_dup2, (uint64_t)oldfd, (uint64_t)newfd);
  }
  static inline int64_t pipe(fd_t fds[2]) {
    return syscall_r_1(sys_pipe, (uintptr_t)fds);
  }
  static inline int64_t flush_pipe(fd_t fds[2]) {
    return syscall_r_1(sys_flush_pipe, (uintptr_t)fds);
  }

  static inline int64_t write(fd_t fd, const uint8_t* buf, uint64_t count) {
    return syscall_r_3(sys_write, (uint64_t)fd, (uintptr_t)buf, count);
  }
  static inline int64_t write(fd_t fd, const char* buf, uint64_t count) {
    return write(fd, (const uint8_t*)buf, count);
  }
  static inline int64_t read(fd_t fd, uint8_t* buf, uint64_t count) {
    return syscall_r_3(sys_read, (uint64_t)fd, (uintptr_t)buf, count);
  }
  static inline int64_t read(fd_t fd, char* buf, uint64_t count) {
    return read(fd, (uint8_t*)buf, count);
  }
  static inline int64_t pollin(fd_t* fds, uint64_t count) {
    return syscall_r_2(sys_pollin, (uintptr_t)fds, count);
  }

  static inline int64_t chdir(const char* path) {
    return syscall_r_1(sys_chdir, (uintptr_t)path);
  }
  static inline int64_t opendir(const char* path = ".") {
    return syscall_r_1(sys_opendir, (uintptr_t)path);
  }
  static inline int64_t readdir(char* name) {
    return syscall_r_1(sys_readdir, (uintptr_t)name);
  }

  static inline int64_t raise(uint64_t sig) {
    return syscall_r_1(sys_raise, sig);
  }
  static inline int64_t kill(pid_t pid, uint64_t sig) {
    return syscall_r_2(sys_kill, pid, sig);
  }
  static inline int64_t sigaction(uint64_t sig, const sigact* new_action, sigact* old_action = 0) {
    return syscall_r_3(sys_sigaction, sig, (uintptr_t)new_action, (uintptr_t)old_action);
  }
  static inline int64_t sigprocmask(sig_mask_action how, uint64_t *set, uint64_t *oldset = nullptr) {
    return syscall_r_3(sys_sigprocmask, how, (uintptr_t)set, (uintptr_t)oldset);
  }
  static inline int64_t pause() {
    return syscall_r_0(sys_pause);
  }

  static inline WindowSize window_get() {
    return WindowSize(syscall_r_0(sys_window_get));
  }
  static inline void window_refresh(WindowData* ptr) {
    return syscall_1(sys_window_refresh, (uintptr_t)ptr);
  }
  static inline int16_t get_key() {
    return syscall_r_0(sys_get_key);
  }
}
