#pragma once

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <utility>

// Linked List implementation of malloc
// Block : {HeapBlockStatus status}{uintptr_t size}<data : size bytes>;{uintptr_t ptr_to_status}

namespace __malloc {

  enum HeapBlockStatus {
    END_HEAP = 0,
    START_HEAP = 1,
    FREE = 2,
    USED = 3
  };

  constexpr uintptr_t block_header_size = sizeof(uintptr_t) + sizeof(HeapBlockStatus);
  constexpr uintptr_t block_footer_size = sizeof(uintptr_t);
  constexpr uintptr_t block_meta_size = block_header_size + block_footer_size;

  inline uintptr_t block_data_start(uintptr_t block) {
    return block + block_header_size;
  }

  inline uintptr_t block_size(uintptr_t block) {
    return *reinterpret_cast<uintptr_t*>(block + sizeof(HeapBlockStatus));
  }

  inline HeapBlockStatus block_status(uintptr_t block) {
    return *reinterpret_cast<HeapBlockStatus*>(block);
  }

  inline uintptr_t prev_block(uintptr_t block, bool ignore_start = false) {
    if (!ignore_start && block_status(block) == START_HEAP)
      return 0;
    return *reinterpret_cast<uintptr_t*>(block - block_footer_size);
  }

  inline uintptr_t next_block(uintptr_t block, bool ignore_end = false) {
    if (!ignore_end && block_status(block) == END_HEAP)
      return 0;
    return block + block_header_size + block_size(block) + block_footer_size;
  }

  inline void set_block_status(uintptr_t block, HeapBlockStatus status) {
    *reinterpret_cast<HeapBlockStatus*>(block) = status;
  }

  inline void set_block_size(uintptr_t block, uintptr_t size) {
    *reinterpret_cast<uintptr_t*>(block + sizeof(HeapBlockStatus)) = size;
  }

  inline void set_end_pointer(uintptr_t block) {
    *reinterpret_cast<uintptr_t*>(block + block_header_size + block_size(block)) = block;
  }

  template<typename Malloc_Data>
  inline void free(void* addr);

  template<typename Malloc_Data>
  inline void init_malloc(void* start, uintptr_t size) {
    Malloc_Data::heap_start = reinterpret_cast<uintptr_t>(start);

    set_block_status(Malloc_Data::heap_start, START_HEAP);
    set_block_size(Malloc_Data::heap_start, 0);
    set_end_pointer(Malloc_Data::heap_start);

    uintptr_t main = next_block(Malloc_Data::heap_start);
    set_block_status(main, FREE);
    set_block_size(main, size - 3 * block_meta_size);
    set_end_pointer(main);

    uintptr_t end = next_block(main);
    set_block_status(end, END_HEAP);
    set_block_size(end, 0);
    set_end_pointer(end);

    Malloc_Data::heap_end = next_block(end, true);
  }

  template<typename Malloc_Data>
  inline void expand_malloc(uintptr_t size_inc) {
    uintptr_t end = prev_block(Malloc_Data::heap_end, true);
    set_block_status(end, USED);
    set_block_size(end, size_inc - block_meta_size);
    set_end_pointer(end);

    uintptr_t new_end = next_block(end);
    set_block_status(new_end, END_HEAP);
    set_block_size(new_end, 0);
    set_end_pointer(new_end);

    Malloc_Data::heap_end = next_block(new_end, true);

    free<Malloc_Data>(reinterpret_cast<void*>(block_data_start(end))); // Fusion éventuelle avec précédent
  }

  template<typename Malloc_Data>
  inline std::pair<uintptr_t, uintptr_t> malloc_info() {
    return std::pair<uintptr_t, uintptr_t>(Malloc_Data::heap_start, Malloc_Data::heap_end);
  }

// Returns 0 if no space was found
  template<typename Malloc_Data>
  inline void* malloc(uintptr_t size, uintptr_t ptr = Malloc_Data::heap_start) {
    while (ptr != 0 && (block_status(ptr) != FREE || size > block_size(ptr)))
      ptr = next_block(ptr);
    if (ptr == 0) {
      uint64_t delta = std::max(Malloc_Data::heap_end - Malloc_Data::heap_start, size + block_meta_size);
      Malloc_Data()((void*)Malloc_Data::heap_end, delta);
      expand_malloc<Malloc_Data>(delta);
      return malloc<Malloc_Data>(size, prev_block(prev_block(Malloc_Data::heap_end, true)));
    }

    uintptr_t ptr_size = block_size(ptr);

    // Split block
    if (ptr_size > size + block_meta_size) {
      set_block_size(ptr, size);
      set_end_pointer(ptr);
      uintptr_t next = next_block(ptr);
      set_block_status(next, FREE);
      set_block_size(next, ptr_size - size - block_meta_size);
      set_end_pointer(next);
    }

    set_block_status(ptr, USED);
    return reinterpret_cast<void*>(block_data_start(ptr));
  }

  inline uint64_t ceil(uint64_t value, uint64_t alignment) {
    uintptr_t align_mask = alignment - 1;
    return (value + align_mask) & ~align_mask;
  }

  template<typename Malloc_Data>
  inline void* aligned_alloc(uint64_t alignment, uint64_t size, uintptr_t bptr = Malloc_Data::heap_start) {
    while (bptr != 0) {
      if (block_status(bptr) == FREE) {
        uintptr_t data_start = block_data_start(bptr);
        uintptr_t data_end = data_start + size;
        uintptr_t block_data_end = block_data_start(bptr) + block_size(bptr);

        if (data_end <= block_data_end) {
          uintptr_t align_mask = alignment - 1;
          if ((data_start & align_mask) == 0) break;

          data_start = ceil(data_start + block_meta_size, alignment);
          data_end = data_start + size;

          if (data_end <= block_data_end) {
            set_block_size(bptr, data_start - block_data_start(bptr) - block_meta_size);
            set_end_pointer(bptr);
            bptr = next_block(bptr);
            set_block_size(bptr, block_data_end - data_start);
            set_end_pointer(bptr);
            break;
          }
        }
      }
      bptr = next_block(bptr);
    }
    if (bptr == 0) {
      uint64_t delta = std::max(Malloc_Data::heap_end - Malloc_Data::heap_start, alignment + size + block_meta_size);
      Malloc_Data()((void*)Malloc_Data::heap_end, delta);
      expand_malloc<Malloc_Data>(delta);
      return aligned_alloc<Malloc_Data>(alignment, size, prev_block(prev_block(Malloc_Data::heap_end, true)));
    }

    // Set status (used for next_block)
    set_block_status(bptr, USED);

    uint64_t end_margin = block_size(bptr) - size;

    // Split after block
    if (end_margin >= block_meta_size) {
      set_block_size(bptr, size);
      set_end_pointer(bptr);
      uintptr_t next = next_block(bptr);
      set_block_status(next, FREE);
      set_block_size(next, end_margin - block_meta_size);
      set_end_pointer(next);
    }

    return reinterpret_cast<void*>(block_data_start(bptr));
  }

  template<typename Malloc_Data>
  inline void free(void* addr) {
    if (addr == nullptr) return;

    uintptr_t block = reinterpret_cast<uintptr_t>(addr) - sizeof(HeapBlockStatus) - sizeof(uintptr_t);
    uintptr_t prev = prev_block(block);

    // Merge blocks
    uintptr_t size_inc = 0;
    if (block_status(prev) == FREE) {
      size_inc += sizeof(HeapBlockStatus) + sizeof(uintptr_t) + block_size(block) + sizeof(uintptr_t);
      block = prev;
    } else
      set_block_status(block, FREE);
    uintptr_t next = next_block(block);
    if (block_status(next) == FREE)
      size_inc += sizeof(HeapBlockStatus) + sizeof(uintptr_t) + block_size(next) + sizeof(uintptr_t);
    uintptr_t new_size = block_size(block) + size_inc;
    set_block_size(block, new_size);
    set_end_pointer(block);
  }

  template<typename Malloc_Data>
  [[gnu::warning("todo")]] inline void* realloc(void* ptr, std::size_t new_size) { //not really deprec, but TODO in it
    //TODO : real impl
    std::size_t old_size = *((uintptr_t*)ptr - 1);
    void* new_reg = malloc<Malloc_Data>(new_size);
    std::memcpy(new_reg, ptr, std::min(old_size, new_size));
    free<Malloc_Data>(ptr);
    return new_reg;
  }

}
