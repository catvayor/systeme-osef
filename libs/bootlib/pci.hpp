#pragma once

#include <cstdint>
#include <vector>

namespace PCI {

  constexpr uint8_t id_off = 0x0;
  constexpr uint8_t status_cmd = 0x4;
  constexpr uint8_t device_class = 0x8;
  constexpr uint8_t bist_head_latency_cache = 0xC;

//type 0
  constexpr uint8_t bar0 = 0x10;
  constexpr uint8_t bar1 = 0x14;
  constexpr uint8_t bar2 = 0x18;
  constexpr uint8_t bar3 = 0x1C;
  constexpr uint8_t bar4 = 0x20;
  constexpr uint8_t bar5 = 0x24;


  uint32_t read(uint32_t addr);
  void write(uint32_t addr, uint32_t val);

  uint32_t addr(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset = 0);
  uint16_t get_vendor_id(uint8_t bus, uint8_t device, uint8_t func);
  uint8_t get_header_type(uint8_t bus, uint8_t device, uint8_t func);

  void* alloc_bar(uint32_t bar_addr);

  template<typename FunT>//FunT of sig void(uint8_t,uint8_t,uint8_t)
  void check_device(uint8_t bus, uint8_t device, FunT checker) {
    uint8_t function = 0;

    uint16_t vendorID = PCI::get_vendor_id(bus, device, function);
    if (vendorID == 0xFFFF)
      return;

    checker(bus, device, function);
    uint8_t headerType = PCI::get_header_type(bus, device, function);

    if ( (headerType & 0x80) != 0)
      for (function = 1; function < 8; function++)
        if (PCI::get_vendor_id(bus, device, function) != 0xFFFF)
          checker(bus, device, function);
  }

  template<typename FunT>
  void check_all_buses(FunT checker) {
    for (uint16_t bus = 0; bus < 256; bus++)
      for (uint8_t device = 0; device < 32; device++)
        check_device<FunT>(bus, device, checker);
  }

  template<typename Alloc>
  void enumerate(std::vector<uint32_t, Alloc>& dest) {
    check_all_buses([&dest](uint8_t bus, uint8_t device, uint8_t func) {
      dest.push_back(addr(bus, device, func));
    });
  }

}

