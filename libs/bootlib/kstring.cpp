#include "kstring.hpp"

#include <kstack>

kstring generic_to_kstring(uint64_t x, bool neg) {
  kstring ret = "";
  kstack<char> st;

  if (neg) ret.push_back('-');

  do {
    st.push("0123456789"[x % 10]);
    x /= 10;
  } while (x);

  while (!st.empty()) ret.push_back(*st.pop());
  return ret;
}

kstring to_kstring(int32_t x) {
  return x < 0 ? generic_to_kstring(-x, true) : generic_to_kstring(x, false);
}

kstring to_kstring(int64_t x) {
  return x < 0 ? generic_to_kstring(-x, true) : generic_to_kstring(x, false);
}

kstring to_kstring(uint32_t x) {
  return generic_to_kstring(x, false);
}

kstring to_kstring(uint64_t x) {
  return generic_to_kstring(x, false);
}