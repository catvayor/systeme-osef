#pragma once

#include <cstring>
#include <terminal>
#include <x86>

#define INTERACT_DEBUG

namespace Interact {
  inline void init() { }

  inline void print_init(const char* name) {
    Terminal::print("Init ");
    Terminal::print(name);
    Terminal::print("...");
  }

  inline void print_done() {
    Terminal::print(" DONE\n", Terminal::fg_dgreen);
  }

  inline void print_failed() {
    Terminal::print(" FAILED\n", Terminal::fg_lred);
  }

  inline void debug_trace([[maybe_unused]] const char* msg) {
#ifdef INTERACT_DEBUG
    int x = Terminal::width - std::strlen(msg);
    Terminal::print_at(" #", x - 2, Terminal::last_line, Terminal::fg_pink);
    Terminal::print_at(msg, x, Terminal::last_line, Terminal::fg_pink);
    Terminal::newline();
#endif
  }

  template<typename T>
  inline void debug_value([[maybe_unused]] const char* name, [[maybe_unused]] T value) {
#ifdef INTERACT_DEBUG
    int x = std::strlen(name);
    Terminal::print_at(name, 0, Terminal::last_line, Terminal::fg_pink);
    Terminal::print_at(": ", x, Terminal::last_line, Terminal::fg_pink);
    Terminal::print_hex_at(value, x + 2, Terminal::last_line, Terminal::fg_pink);
    Terminal::print_at(".", x + 2 + 2 * sizeof(T), Terminal::last_line, Terminal::fg_pink);
    Terminal::newline();
#endif
  }

  inline void warning(const char* msg) {
    Terminal::newline();
    Terminal::print(msg, Terminal::fg_yellow);
    Terminal::newline();
  }

  [[noreturn]] inline void error(const char* msg) {
    x86::cli();
    Terminal::newline();
    Terminal::print(msg, Terminal::fg_dred);
    Terminal::newline();
    x86::ud2_loop();
  }

  inline void display_banner(unsigned screen_x = 0, unsigned screen_y = 0, unsigned screen_w = 80, unsigned screen_h = 25) {
    unsigned x_center = screen_x + screen_w / 2;
    unsigned y_center = screen_y + screen_h / 2;
    unsigned logo_w = 52;
    unsigned logo_h = 6;

    for (unsigned x = screen_x; x < screen_x + screen_w; x++) {
      for (unsigned y = screen_y; y < screen_y + screen_h; y++) {
        if (x < x_center - logo_w / 2 || x >= x_center + logo_w / 2 || y < y_center - logo_h / 2 || y >= y_center + logo_h / 2)
          Terminal::putc_at(' ', x, y, Terminal::fg_dgreen);
      }
    }

    unsigned x = x_center - 52 / 2;
    unsigned y = y_center - 3;
    Terminal::print_at(" _    _             _         ______ _  __          ", x, y++, Terminal::fg_dgreen);
    Terminal::print_at("| |  | |           | |       |  ____| |/ _|         ", x, y++, Terminal::fg_dgreen);
    Terminal::print_at("| |__| | __ _ _   _| |_ ___  | |__  | | |_ ___  ___ ", x, y++, Terminal::fg_dgreen);
    Terminal::print_at("|  __  |/ _` | | | | __/ __| |  __| | |  _/ _ \\/ __|", x, y++, Terminal::fg_dgreen);
    Terminal::print_at("| |  | | (_| | |_| | |_\\__ \\ | |____| | ||  __/\\__ \\", x, y++, Terminal::fg_dgreen);
    Terminal::print_at("|_|  |_|\\__,_|\\__,_|\\__|___/ |______|_|_| \\___||___/", x, y++, Terminal::fg_dgreen);
  }
}
