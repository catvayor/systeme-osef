#pragma once
#include <stdint.h>

namespace Terminal {
  constexpr unsigned width = 80;
  constexpr unsigned height = 25;
  constexpr unsigned last_line = height - 1;

  constexpr uintptr_t vga_buffer_addr = 0xFFFFFF0000000000;
  enum Color : uint8_t {
    fg_black   = 0x00,
    fg_dblue   = 0x01,
    fg_dgreen  = 0x02,
    fg_dcyan   = 0x03,
    fg_dred    = 0x04,
    fg_magenta = 0x05,
    fg_brown   = 0x06,
    fg_lgray   = 0x07,
    fg_dgray   = 0x08,
    fg_lblue   = 0x09,
    fg_lgreen  = 0x0a,
    fg_lcyan   = 0x0b,
    fg_lred    = 0x0c,
    fg_pink    = 0x0d,
    fg_yellow  = 0x0e,
    fg_white   = 0x0f,

    bg_black   = 0x00,
    bg_blue    = 0x10,
    bg_green   = 0x20,
    bg_cyan    = 0x30,
    bg_red     = 0x40,
    bg_magenta = 0x50,
    bg_brown   = 0x60,
    bg_gray    = 0x70,

    bg_light_blink = 0x80,
  };

  uint16_t volatile& char_at(int x, int y);

  void clear();
  void newline();

  void putc(char c, uint8_t color = 0x0f);
  void putc_at(char c, int x, int y, uint8_t color = 0x0f);

  void print(const char* str, uint8_t color = 0x0f);
  void print_at(const char* str, int x, int y, uint8_t color = 0x0f);

  template<typename T>
  void print_hex(T v, uint8_t color = 0x0f) {
    for (int s = 8 * sizeof(T); s; s -= 4) {
      putc("0123456789ABCDEF"[(v >> (s - 4)) & 0xf], color);
    }
  }

  template<typename T>
  void print_hex_at(T v, int x, int y, uint8_t color = 0x0f) {
    for (int s = 8 * sizeof(T); s; s -= 4) {
      putc_at("0123456789ABCDEF"[(v >> (s - 4)) & 0xf], x++, y, color);
    }
  }
}
