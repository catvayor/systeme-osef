#pragma once

namespace x86 {
  static inline void cli() {
    asm volatile("cli");
  }
  static inline void sti() {
    asm volatile("sti");
  }
  static inline void ud2() {
    asm volatile("ud2");
  }
  static inline void hlt() {
    asm volatile("hlt");
  }
  [[noreturn]] static inline void ud2_loop() {
    for (;;) ud2();
  }
  [[noreturn]] static inline void hlt_loop() {
    for (;;) hlt();
  }
  [[noreturn]] static inline void freeze() {
    cli();
    hlt_loop();
  }

  static inline uint64_t get_cr2() {
    uint64_t ret;
    asm volatile("movl %%cr2,%0" : "=r" (ret));
    return ret;
  }

  static inline void softint(uint8_t n) {
    asm volatile("int %0" :: "N"(n));
  }
}
