#include "interrupt_base.hpp"
#include <x86>

namespace Interrupt {

  struct InterruptDescriptor {
    uint16_t offset_1;        // offset bits 0..15
    uint16_t selector;        // a code segment selector in GDT or LDT
    uint8_t  ist;             // bits 0..2 holds Interrupt Stack Table offset, rest of bits zero.
    uint8_t  type_attributes; // gate type, dpl, and p fields
    uint16_t offset_2;        // offset bits 16..31
    uint32_t offset_3;        // offset bits 32..63
    uint32_t zero;            // reserved
  } __attribute__((packed));

  struct IdtRecord {
    uint16_t limit;            // Size of IDT array - 1
    InterruptDescriptor* base; // Pointer to IDT array

    void load() {
      asm ("lidt %0" :: "g"(*this));
    }
  } __attribute__((packed));

  InterruptDescriptor idt[256];
  IdtRecord idtr = {256 * sizeof(InterruptDescriptor) - 1, idt};

  extern "C" void handler_default_wrapper();

  void setup_handler(uint64_t i, void(*handler)(), uint8_t ist, bool user) {
    uintptr_t ptr = reinterpret_cast<uintptr_t>(handler);
    idt[i].offset_1 = (uint16_t) ptr;
    idt[i].offset_2 = (uint16_t)(ptr >> 16);
    idt[i].offset_3 = (uint32_t)(ptr >> 32);
    idt[i].ist      = ist;
    if (user)
      idt[i].type_attributes |= 0x60;
    else
      idt[i].type_attributes &= ~0x60;
  }

  void clear_int() {
    for (int i = 0; i < 256; ++i) {
      constexpr uint16_t interrupt_routine_segment = 0x08;
      idt[i].selector = interrupt_routine_segment;
      idt[i].zero = 0b0;
      idt[i].type_attributes = 0x8f;
      setup_handler(i, handler_default_wrapper);
    }
  }

  void init_base() {
    idtr.load();
    x86::sti();
  }
}
