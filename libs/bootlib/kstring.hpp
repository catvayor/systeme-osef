#pragma once

#include <string>
#include "kallocator.hpp"

using kstring = std::basic_string<char, std::char_traits<char>, kallocator<char> >;
using ku8string = std::basic_string<char8_t, std::char_traits<char8_t>, kallocator<char8_t> >;
using ku16string = std::basic_string<char16_t, std::char_traits<char16_t>, kallocator<char16_t> >;
using ku32string = std::basic_string<char32_t, std::char_traits<char32_t>, kallocator<char32_t> >;
using kwstring = std::basic_string<wchar_t, std::char_traits<wchar_t>, kallocator<wchar_t> >;

inline kstring ku16string_to_kstring(ku16string const& src) {
  kstring dst;
  for (char16_t const& c : src) {
    dst.push_back(c);
  }
  return dst;
}

inline ku16string kstring_to_ku16string(kstring const& src) {
  ku16string dst;
  for (char const& c : src) {
    dst.push_back(c);
  }
  return dst;
}

kstring to_kstring(int32_t);
kstring to_kstring(int64_t);
kstring to_kstring(uint32_t);
kstring to_kstring(uint64_t);