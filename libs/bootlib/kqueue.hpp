#pragma once

#include <queue>
#include "kallocator.hpp"

template<typename T> using kqueue = std::queue<T, kallocator<T> >;
