#pragma once

#include <unordered_map>
#include "kallocator.hpp"

template<typename Key, typename T> using kunordered_map = std::unordered_map<Key, T, std::hash<Key>, std::equal_to<Key>, kallocator<char> >;
