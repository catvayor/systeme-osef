#pragma once

#include <stdint.h>
#include <utility>

// WARNING : undefined behaviour for init_malloc (resp expand_malloc) if size < 9*8o (resp size_inc < 3*8o)
void init_malloc(void* start, uintptr_t size);	// Inits blocks for malloc in [start, start+size)
void expand_malloc(uintptr_t size_inc);			// Expands heap to [heap_start; heap_end+size_inc)
//void* malloc(uintptr_t size);
[[nodiscard]] void* kmalloc(uintptr_t size);
void kfree(void* addr);
[[nodiscard]] void* aligned_kalloc(uint64_t alignment, uint64_t size);
