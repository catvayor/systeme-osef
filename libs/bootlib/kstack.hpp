#pragma once

#include <stack>
#include "kallocator.hpp"

template<typename T> using kstack = std::stack<T, kallocator<T> >;
