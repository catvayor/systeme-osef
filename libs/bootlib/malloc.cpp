#include <__malloc>
#include <virtual_memory_base>

struct KernelMallocData {
  static uintptr_t heap_start;
  static uintptr_t heap_end;
  void operator()(void* start, uint64_t delta) {
    virtual_memory::make_virtual_address(start, delta, false, true, true);
  }
};

uintptr_t KernelMallocData::heap_start;
uintptr_t KernelMallocData::heap_end;

uintptr_t& heap_start() {
  return KernelMallocData::heap_start;
}
uintptr_t& heap_end() {
  return KernelMallocData::heap_end;
}

void init_malloc(void* start, uintptr_t size) {
  __malloc::init_malloc<KernelMallocData>(start, size);
}
void expand_malloc(uintptr_t size_inc) {
  __malloc::expand_malloc<KernelMallocData>(size_inc);
}

[[nodiscard]] void* kmalloc(uint64_t size) {
  return __malloc::malloc<KernelMallocData>(size);
}

[[nodiscard]] void* aligned_kalloc(uint64_t alignment, uint64_t size) {
  return __malloc::aligned_alloc<KernelMallocData>(alignment, size);
}

void kfree(void* addr) {
  __malloc::free<KernelMallocData>(addr);
}

void *operator new (std::size_t size) {
  return kmalloc(size);
}

void *operator new[](std::size_t size) {
  return kmalloc(size);
}

void operator delete (void *p) {
  kfree(p);
}

void operator delete[](void *p) {
  kfree(p);
}

void operator delete (void *p, std::size_t) {
  kfree(p);
}

void operator delete[](void *p, std::size_t) {
  kfree(p);
}

