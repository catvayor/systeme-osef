#include "boot_info.hpp"
#include "virtual_memory_base"

struct boot_info {
  uint64_t bootloader_size;
  decltype(virtual_memory::frame_alloc_data) frame_alloc;
  decltype(virtual_memory::fake_alloc_data) fake_alloc;
  decltype(virtual_memory::special_alloc_data) special_alloc;
  uintptr_t heap_st;
  uintptr_t heap_ed;
};

uint64_t bootloader_size;

extern uintptr_t& heap_start();
extern uintptr_t& heap_end();

void store_boot_info() {
  using namespace virtual_memory;

  volatile boot_info* infos;
  asm("movq $0, %0" : "=g"(infos));
  infos->bootloader_size = bootloader_size;
  infos->frame_alloc =     frame_alloc_data;
  infos->fake_alloc =      fake_alloc_data;
  infos->special_alloc =   special_alloc_data;
  infos->heap_st =   heap_start();
  infos->heap_ed =   heap_end();
}

void load_boot_info() {
  using namespace virtual_memory;

  volatile boot_info* infos;
  asm("movq $0, %0" : "=g"(infos));
  bootloader_size =    infos->bootloader_size;
  frame_alloc_data =   infos->frame_alloc;
  fake_alloc_data =    infos->fake_alloc;
  special_alloc_data = infos->special_alloc;
  heap_start() = infos->heap_st;
  heap_end() = infos->heap_ed;
}

