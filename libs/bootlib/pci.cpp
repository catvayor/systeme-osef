#include "pci.hpp"
#include "io_access.hpp"
#include "virtual_memory_base.hpp"
#include <bit>

namespace PCI {

  constexpr uint32_t addr_port = 0xCF8;
  constexpr uint32_t data_port = 0xCFC;

  uint32_t read(uint32_t addr) {
    outl(addr_port, addr);
    return inl(data_port);
  }

  void write(uint32_t addr, uint32_t val) {
    outl(addr_port, addr);
    outl(data_port, val);
  }

  uint32_t addr(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
    uint32_t lbus  = (uint32_t)bus;
    uint32_t lslot = (uint32_t)slot;
    uint32_t lfunc = (uint32_t)func;

    return (lbus << 16) | (lslot << 11) |
           (lfunc << 8) | (offset & 0xFC) | 0x80000000u;
  }

  uint16_t get_vendor_id(uint8_t bus, uint8_t device, uint8_t func) {
    return 0xFFFF & read(addr(bus, device, func, id_off));
  }

  uint8_t get_header_type(uint8_t bus, uint8_t device, uint8_t func) {
    return 0xFF & (read(addr(bus, device, func, bist_head_latency_cache)) >> 16);
  }

  void* alloc_bar(uint32_t bar_addr) {
    outl(addr_port, bar_addr);
    uint32_t base_bar = inl(data_port);
    uint32_t mask = base_bar & 1 ? 0x3 : 0xF;
    outl(data_port, ~mask | base_bar);
    uint32_t written = inl(data_port);
    uint64_t size = (mask | ~written) + 1;
    if (size == 1ull << 32 && !(base_bar & 1) && ((base_bar & 6) == 4)) {
      //64 bits bar with align > 31 : 64 bit alloc
      outl(addr_port, bar_addr + 4);
      outl(data_port, ~0);
      size += uint64_t(~inl(data_port)) << 32;
      outl(addr_port, bar_addr);
    }
    void* bar_region = virtual_memory::fake_alloc(size, std::bit_width(size) - 1);
    uint64_t bar = uint64_t(bar_region) | (mask & base_bar);
    outl(data_port, bar & 0xFFFFFFFF);

    if (!(base_bar & 1) && ((base_bar & 6) == 4))
      write(bar_addr + 4, bar >> 32);
    else if (bar >> 32)
      asm("ud2");

    return bar_region;
  }

}
