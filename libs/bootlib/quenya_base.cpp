#include "quenya_base.hpp"
#include "virtual_memory_base.hpp"
#include <bit>
#include <cstring>

namespace quenya {

  bool check_magicNumber(Header_t const* header) {
    return header->e_ident[Header_t::ident_mag0] == Header_t::mag0
           && header->e_ident[Header_t::ident_mag1] == Header_t::mag1
           && header->e_ident[Header_t::ident_mag2] == Header_t::mag2
           && header->e_ident[Header_t::ident_mag3] == Header_t::mag3;
  }

  bool supported(Header_t const* header) {
    return header->e_ident[Header_t::ident_class] == Header_t::class64
           && header->e_ident[Header_t::ident_data] == Header_t::dataLittleEndian
           && header->e_machine == HM_X86_64
           && header->e_version == Elfic_version;
  }

  void loadPrgmHeaders_risky(Prgm_Header_t const* headers, void const* file_base,
                             short hdc, bool user, bool global) {
    for (short i = 0; i < hdc; ++i) {
      bool load = false;
      switch (headers[i].p_type) {
        case PHT_NULL:
          break;
        case PHT_LOAD:
          load = true;
          break;
        case PHT_DYNAMIC:
          break;
        case PHT_INTERP:
          break;
        case PHT_NOTE:
          break;
        case PHT_SHLIB:
          break;
        case PHT_PHDR:
          break;
        case PHT_TLS:
          break;
        default:
          break;
      }

      bool writable = headers[i].p_flags & Writable;
      void* vaddr = reinterpret_cast<void*>(headers[i].p_vaddr);
      uint64_t to_ld = headers[i].p_filesz;
      uint64_t sz = headers[i].p_memsz;
      void* base = (void*)((uintptr_t)file_base + headers[i].p_offset);
      if (!load)
        continue;
      virtual_memory::make_virtual_address(vaddr, sz, user, true, global);
      std::memset(vaddr, 0, sz);
      std::memcpy(vaddr, base, to_ld);
      virtual_memory::make_virtual_address(vaddr, sz, user, writable, global);
    }
  }

  call_point_t loadElfHeader_risky(void const* file, bool user, bool global) {
    Header_t const* header = reinterpret_cast<Header_t const*>(file);
    if (!check_magicNumber(header))
      return nullptr;
    if (!supported(header))
      return nullptr;
    switch (header->e_type) {
      case HT_none:
        return nullptr;
      case HT_rel:
        return nullptr;
      case HT_exec:
        break;
      case HT_dyn:
        return nullptr;
      case HT_core:
        return nullptr;
      default:
        return nullptr;
    }
    uintptr_t ph_start = reinterpret_cast<uintptr_t>(file) + header->e_phoff;
    loadPrgmHeaders_risky(reinterpret_cast<Prgm_Header_t const*>(ph_start), file,
                          header->e_phnum, user, global);
    return reinterpret_cast<call_point_t>(header->e_entry);
  }
}
