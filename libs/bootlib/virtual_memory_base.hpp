#pragma once
#include <stdint.h>
#include <cstddef>
#include <array>

namespace virtual_memory {

  ///////////////////////////////////Memory map

  struct memory_map_entry_t {
    memory_map_entry_t() = default;
    memory_map_entry_t(memory_map_entry_t volatile& other)
      : Base(other.Base), Length(other.Length), Type(other.Type), ACPI(other.ACPI) {}
    memory_map_entry_t& operator=(memory_map_entry_t const volatile& other) {
      Base = other.Base;
      Length = other.Length;
      Type = other.Type;
      ACPI = other.ACPI;
      return *this;
    }
    memory_map_entry_t& operator=(memory_map_entry_t const& other) {
      Base = other.Base;
      Length = other.Length;
      Type = other.Type;
      ACPI = other.ACPI;
      return *this;
    }

    memory_map_entry_t(uint64_t base, uint64_t len, uint32_t type, uint32_t acpi = 1):
      Base(base), Length(len), Type(type), ACPI(acpi) {}

    uint64_t Base;
    uint64_t Length;
    uint32_t Type;
    uint32_t ACPI;

    uint64_t end() const {
      return Base + Length;
    }
  };

  void normalize_mem_map();

  /////////////////////////////////// gdt

  enum GDT_flags : uint64_t {
    gdt_present = 1ull << 47,
    gdt_privilege = 1ull << 45,
    gdt_S = 1ull << 44,
    gdt_executable = 1ull << 43,
    gdt_DC = 1ull << 42,
    gdt_RW = 1ull << 41,
    gdt_acc = 1ull << 40,
    gdt_gran = 1ull << 55,
    gdt_Sz = 1ull << 54,
    gdt_L = 1ull << 53,
  };

  ///////////////////////////////////Paging

  typedef void* physical_ptr;

  constexpr physical_ptr nullphysptr = nullptr;

  /* Paging structure :
   *   PML4 recursive (entry 0x1ff)
   */
  typedef uint64_t page_entry;

  enum page_entry_flags : uint64_t {
    Present = 1 << 0,
    Writable = 1 << 1,
    User = 1 << 2,
    WriteThroughCaching = 1 << 3,
    DisableCache = 1 << 4,
    Accessed = 1 << 5,
    Dirty = 1 << 6,
    Huge = 1 << 7,
    Global = 1 << 8,
    Available = (0x7ffull << 52) | (7 << 9),
    Address = 0x7fffffffff000,
    //no exec = 1 << 63
  };

  void loadPaging(physical_ptr main_page);
  void invlpg(void* vaddr);
  void reloadPaging();

  page_entry* ptr_to_PML4();
  page_entry& PML3_entry(void* ptr);
  page_entry* ptr_to_PML3(void* ptr);
  page_entry& PML2_entry(void* ptr);
  page_entry* ptr_to_PML2(void* ptr);
  page_entry& PML1_entry(void* ptr);
  page_entry* ptr_to_PML1(void* ptr);
  page_entry& PML0_entry(void* ptr);
  physical_ptr getPhysicalAddress(void* ptr);

  void set_PML3(void* vaddr, bool global, bool user);
  void set_PML2(void* vaddr, bool global, bool user);
  void set_PML1(void* vaddr, bool global, bool user);
  void set_PML0(void* vaddr, bool global, bool user, bool writable);
  void make_virtual_address(void* new_addr, uint64_t size,
                            bool user = false, bool writable = true, bool global = false);
  void map_virtual_address(void* vaddr, physical_ptr paddr, uint64_t size,
                           bool user = false, bool writable = true, bool global = true);

  ///////////////////////////////////Frame Alloc

  struct alloc_data_t {
    uintptr_t base;
    uint64_t size;
    std::size_t nxtBlk;//0 if end

    uintptr_t end() const {
      return base + size;
    }
    uintptr_t aligned_base(uint64_t bitalign);
    uint64_t size_with_align(uint64_t bitalign);
  };

  template<std::size_t max_blk>//size == 0x20*max_blk
  struct alloc_structure {
    std::array<alloc_data_t, max_blk> alloc_memory alignas(0x1000);

    struct {
      std::array<std::size_t, max_blk> m_data;//use 0 as top of stack
      void push(std::size_t v) {
        m_data[m_data[0]] = v;
        ++m_data[0];
      }

      std::size_t pop() {
        if (m_data[0] == 1) asm("ud2");
        --m_data[0];
        return m_data[m_data[0]];
      }
    } idx_stack;
  };

  constexpr std::size_t frame_alloc_max_blk   = 0x400;
  constexpr std::size_t fake_alloc_max_blk    = 0x80;
  constexpr std::size_t special_alloc_max_blk = 0x80;

  extern alloc_structure<frame_alloc_max_blk>* frame_alloc_data;
  extern alloc_structure<fake_alloc_max_blk>* fake_alloc_data;
  extern alloc_structure<special_alloc_max_blk>* special_alloc_data;

  void init_allocs(uint64_t bootloader_size);

  physical_ptr phys_alloc(uint64_t size, uint64_t bitalign = 0);
  void phys_free(physical_ptr base, uint64_t size);

  physical_ptr fake_alloc(uint64_t size, uint64_t bitalign = 0);
  void fake_free(physical_ptr base, uint64_t size);

  void* special_alloc(uint64_t size, uint64_t bitalign = 12);
  void special_free(void* base, uint64_t size);

  physical_ptr frame_alloc(uint64_t nb, uint64_t bitalign = 12);
  void frame_free(physical_ptr base, uint64_t nb);

///////////////////////////// boot pass

  void free_bootloader(uint64_t bootloader_size);
}

