#pragma once //lecture de fichier elf
#include <cstddef>
#include <stdint.h>

namespace quenya {

  using Addr_t   = uint64_t;
  using Off_t    = uint64_t;
  using Half_t   = uint16_t;
  using Word_t   = uint32_t;
  using SWord_t  = int32_t;
  using XWord_t  = uint64_t;
  using SXWord_t = int64_t;
  using uchar    = uint8_t;

  using call_point_t = void(*)();

  constexpr Word_t Elfic_version = 1;

  enum Head_Type_t : Half_t {
    HT_none = 0,
    HT_rel = 1,
    HT_exec = 2,
    HT_dyn = 3,
    HT_core = 4,
    HT_loos = 0xfe00,
    HT_hios = 0xfeff,
    HT_loproc = 0xff00,
    HT_hiproc = 0xffff,
  };

  enum Head_Machine_t : Half_t {
    HM_none = 0,
    HM_M32 = 1,
    HM_SPARC = 2,
    HM_386 = 3,
    HM_68K = 4,
    HM_88K = 5,
    HM_860 = 7,
    HM_MIPS = 8,
    HM_S370 = 9,
    HM_MIPS_RS3_LE = 10,
    HM_PARISC = 15,
    HM_VPP500 = 17,
    HM_SPARC32PLUS = 18,
    HM_960 = 19,
    HM_PPC = 20,
    HM_PPC64 = 21,
    HM_S390 = 22,
    HM_V800 = 36,
    HM_FR20 = 37,
    HM_RH32 = 38,
    HM_RCE = 39,
    HM_ARM = 40,
    HM_ALPHA = 41,
    HM_SH = 42,
    HM_SPARCV9 = 43,
    HM_TRICORE = 44,
    HM_ARC = 45,
    HM_H8_300 = 46,
    HM_H8_300H = 47,
    HM_H8S = 48,
    HM_H8_500 = 49,
    HM_IA_64 = 50,
    HM_MIPS_X = 51,
    HM_COLDFIRE = 52,
    HM_68HC12 = 53,
    HM_MMA = 54,
    HM_PCP = 55,
    HM_NCPU = 56,
    HM_NDR1 = 57,
    HM_STARCORE = 58,
    HM_ME16 = 59,
    HM_ST100 = 60,
    HM_TINYJ = 61,
    HM_X86_64 = 62,
    HM_PDSP = 63,
    HM_PDP10 = 64,
    HM_PDP11 = 65,
    HM_FX66 = 66,
    HM_ST9PLUS = 67,
    HM_ST7 = 68,
    HM_68HC16 = 69,
    HM_68HC11 = 70,
    HM_68HC08 = 71,
    HM_68HC05 = 72,
    HM_SVX = 73,
    HM_ST19 = 74,
    HM_VAX = 75,
    HM_CRIS = 76,
    HM_JAVELIN = 77,
    HM_FIREPATH = 78,
    HM_ZSP = 79,
    HM_MMIX = 80,
    HM_HUANY = 81,
    HM_PRISM = 82,
    HM_AVR = 83,
    HM_FR30 = 84,
    HM_D10V = 85,
    HM_D30V = 86,
    HM_V850 = 87,
    HM_M32R = 88,
    HM_MN10300 = 89,
    HM_MN10200 = 90,
    HM_PJ = 91,
    HM_OPENRISC = 92,
    HM_ARC_A5 = 93,
    HM_XTENSA = 94,
    HM_VIDEOCORE = 95,
    HM_TMM_GPP = 96,
    HM_NS32K = 97,
    HM_TPC = 98,
    HM_SNP1K = 99,
    HM_ST200 = 100
  };

  struct Header_t {
    static constexpr std::size_t ident_mag0 = 0;
    static constexpr std::size_t ident_mag1 = 1;
    static constexpr std::size_t ident_mag2 = 2;
    static constexpr std::size_t ident_mag3 = 3;
    static constexpr std::size_t ident_class = 4;
    static constexpr std::size_t ident_data = 5;
    static constexpr std::size_t ident_version = 6;
    static constexpr std::size_t ident_osABI = 7;
    static constexpr std::size_t ident_ABIversion = 8;
    static constexpr std::size_t ident_pad = 9;
    static constexpr std::size_t ident_size = 16;

    static constexpr uchar mag0 = 0x7f;
    static constexpr uchar mag1 = 'E';
    static constexpr uchar mag2 = 'L';
    static constexpr uchar mag3 = 'F';
    //class
    static constexpr uchar classNone = 0;
    static constexpr uchar class32 = 1;
    static constexpr uchar class64 = 2;
    //data
    static constexpr uchar dataNone = 0;
    static constexpr uchar dataLittleEndian = 1;
    static constexpr uchar dataBigEndian = 2;
    //osABI
    static constexpr uchar abiNone = 0;
    static constexpr uchar abiHPUX = 1;
    static constexpr uchar abiNETBSD = 2;
    static constexpr uchar abiLINUX = 3;
    static constexpr uchar abiSOLARIS = 6;
    static constexpr uchar abiAIX = 7;
    static constexpr uchar abiIRIX = 8;
    static constexpr uchar abiFREEBSD = 9;
    static constexpr uchar abiTRU64 = 10;
    static constexpr uchar abiMODESTO = 11;
    static constexpr uchar abiBSD = 12;
    static constexpr uchar abiVMS = 13;
    static constexpr uchar abiNSK = 14;
    uchar e_ident[ident_size];
    Half_t e_type;
    Head_Machine_t e_machine;
    Word_t e_version;
    Addr_t e_entry;
    Off_t e_phoff;
    Off_t e_shoff;
    Word_t e_flags;
    Half_t e_ehsize;
    Half_t e_phentsize;
    Half_t e_phnum;
    Half_t e_shentsize;
    Half_t e_shnum;
    Half_t e_shstrndx;
  };

  enum Section_Type_t : Word_t {
    SHT_NULL = 0,
    SHT_PROGBITS = 1,
    SHT_SYMTAB = 2,
    SHT_STRTAB = 3,
    SHT_RELA = 4,
    SHT_HASH = 5,
    SHT_DYNAMIC = 6,
    SHT_NOTE = 7,
    SHT_NOBITS = 8,
    SHT_REL = 9,
    SHT_SHLIB = 10,
    SHT_DYNSYM = 11,
    SHT_LOPROC = 0x70000000,
    SHT_HIPROC = 0x7fffffff,
    SHT_LOUSER = 0x80000000,
    SHT_HIUSER = 0xffffffff,
  };

  struct Section_Header_t {
    Word_t sh_name;
    Word_t sh_type;
    Word_t sh_flags;
    Addr_t sh_addr;
    Off_t sh_offset;
    Word_t sh_size;
    Word_t sh_link;
    Word_t sh_info;
    Word_t sh_addralign;
    Word_t sh_entsize;
  };

  enum Prgm_Header_Type_t : Word_t {
    PHT_NULL = 0,
    PHT_LOAD = 1,
    PHT_DYNAMIC = 2,
    PHT_INTERP = 3,
    PHT_NOTE = 4,
    PHT_SHLIB = 5,
    PHT_PHDR = 6,
    PHT_TLS = 7,
    PHT_LOOS = 0x60000000,
    PHT_HIOS = 0x6FFFFFFF,
    PHT_LOPROC = 0x70000000,
    PHT_HIPROC = 0x7FFFFFFF,
  };

  enum Prgm_Flags_t : Word_t {
    Executable = 1 << 0,
    Writable = 1 << 1,
    Readable = 1 << 2,
  };

  struct Prgm_Header_t {
    Word_t p_type;
    Word_t p_flags;
    Off_t p_offset;
    Addr_t p_vaddr;
    Addr_t p_paddr;
    XWord_t p_filesz;
    XWord_t p_memsz;
    XWord_t p_align;
  };

  bool check_magicNumber(Header_t const* header);
  bool supported(Header_t const* header);
  void loadPrgmHeaders_risky(Prgm_Header_t const* headers, short hdc, bool user, bool global);
  call_point_t loadElfHeader_risky(void const* file, bool user, bool global);
}
