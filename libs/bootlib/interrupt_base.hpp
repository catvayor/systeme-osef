#pragma once
#include <cstdint>

namespace Interrupt {
  void clear_int();
  void init_base();
  void setup_handler(uint64_t i, void (*handler)(), uint8_t ist = 0, bool user = false);
}
