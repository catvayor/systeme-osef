#pragma once
#include <cstdint>

namespace USB {

  bool is_supported_controller(uint32_t pci_addr);

}
