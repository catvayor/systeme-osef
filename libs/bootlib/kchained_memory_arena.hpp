#pragma once

#include <chained_memory_arena>
#include "kallocator.hpp"

template<typename T> using kchained_memory_arena = std::chained_memory_arena<T, kallocator>;
