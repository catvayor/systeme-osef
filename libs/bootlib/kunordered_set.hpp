#pragma once

#include <unordered_set>
#include "kallocator.hpp"

template<typename Key> using kunordered_set = std::unordered_set<Key, std::hash<Key>, std::equal_to<Key>, kallocator<char> >;
