#pragma once

#include "malloc.hpp"

template<typename T>
struct kallocator {
  typedef T value_type;

  kallocator() = default;
  template<typename U>
  kallocator(const kallocator<U>&) { }
  template<typename U>
  kallocator(kallocator<U>&&) { }

  [[nodiscard]] T* allocate(std::size_t nb) {
    return (T*)kmalloc(nb * sizeof(T));
  }

  void deallocate(T* ptr, std::size_t) {
    kfree(ptr);
  }
};
