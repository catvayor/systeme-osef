#pragma once
#include <cstdint>

extern uint64_t bootloader_size;

void store_boot_info();
void load_boot_info();

