#pragma once

#include <chains>
#include "kallocator.hpp"

template<typename index_t> using kchains = std::chains<index_t, kallocator>;
