#include "fs_struct.hpp"
#include <cstring>

alloc_entry_t::alloc_entry_t(dir_entry_t ent) {
  std::memcpy(this, &ent, sizeof(dir_entry_t));
}
cap_table_entry_t::cap_table_entry_t(dir_entry_t ent) {
  std::memcpy(this, &ent, sizeof(dir_entry_t));
}
vol_lab_entry_t::vol_lab_entry_t(dir_entry_t ent) {
  std::memcpy(this, &ent, sizeof(dir_entry_t));
}
file_entry_t::file_entry_t(dir_entry_t ent) {
  std::memcpy(this, &ent, sizeof(dir_entry_t));
}
stream_extension_entry_t::stream_extension_entry_t(dir_entry_t ent) {
  std::memcpy(this, &ent, sizeof(dir_entry_t));
}
filename_entry_t::filename_entry_t(dir_entry_t ent) {
  std::memcpy(this, &ent, sizeof(dir_entry_t));
}

filename_entry_t::filename_entry_t(char16_t* name, uint8_t nb) {
  std::memset(filename, 0, 30);
  std::memcpy(filename, name, nb * 2);
}

dir_entry_t::dir_entry_t() {
  std::memset(this, 0, sizeof(dir_entry_t));
}
dir_entry_t::dir_entry_t(const alloc_entry_t& entry) {
  std::memcpy(this, &entry, sizeof(dir_entry_t));
}
dir_entry_t::dir_entry_t(const cap_table_entry_t& entry) {
  std::memcpy(this, &entry, sizeof(dir_entry_t));
}
dir_entry_t::dir_entry_t(const vol_lab_entry_t& entry) {
  std::memcpy(this, &entry, sizeof(dir_entry_t));
}
dir_entry_t::dir_entry_t(const file_entry_t& entry) {
  std::memcpy(this, &entry, sizeof(dir_entry_t));
}
dir_entry_t::dir_entry_t(const stream_extension_entry_t& entry) {
  std::memcpy(this, &entry, sizeof(dir_entry_t));
}
dir_entry_t::dir_entry_t(const filename_entry_t& entry) {
  std::memcpy(this, &entry, sizeof(dir_entry_t));
}

bool dir_entry_t::is_active() const {
  return type & 0x80;
}

bool dir_entry_t::is_null() const {
  return type == 0;
}
