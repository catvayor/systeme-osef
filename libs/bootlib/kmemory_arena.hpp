#pragma once

#include <memory_arena>
#include "kallocator.hpp"

template<typename T> using kmemory_arena = std::memory_arena<T, kallocator>;
