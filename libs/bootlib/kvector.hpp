#pragma once

#include <vector>
#include "kallocator.hpp"

template<typename T> using kvector = std::vector<T, kallocator<T> >;
