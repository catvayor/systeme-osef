#include "virtual_memory_base.hpp"
#include <algorithm>
#include <terminal>

namespace virtual_memory {

  memory_map_entry_t* mem_map = (memory_map_entry_t*)0x1000;

  void normalize_mem_map() {
    uint32_t mem_map_size = 0;
    for (; mem_map[mem_map_size].Type != 0; ++mem_map_size);
    std::sort(mem_map, mem_map + mem_map_size,
              [](memory_map_entry_t const & e1, memory_map_entry_t const & e2)
              -> bool{ return e1.Base < e2.Base; });
    memory_map_entry_t tmp_map[0x800];
    tmp_map[0] = mem_map[0];
    uint32_t curr_size = 1;
    for (uint32_t i = 1; i < mem_map_size; ++i) {
      //bouche les trou et gere overlap
      if (mem_map[i].Base < tmp_map[curr_size - 1].end()) {
        tmp_map[curr_size - 1].Length = mem_map[i].Base - tmp_map[curr_size - 1].Base;
        //ACPI ??
        //most restrictive
        //order less to most
        //1
        //3
        //4
        //2
        //5
        tmp_map[curr_size - 1].Type = std::max(tmp_map[curr_size - 1].Type, mem_map[i].Type,
        [](uint32_t v1, uint32_t v2) -> bool{
          constexpr std::array<int, 5> prio = {1, 4, 2, 3, 5};
          return prio[v1 - 1] < prio[v2 - 1];
        });
      } else if (mem_map[i].Base > tmp_map[curr_size - 1].end()) {
        tmp_map[curr_size] = memory_map_entry_t(
                               tmp_map[curr_size - 1].end(),
                               mem_map[i].Base - tmp_map[curr_size - 1].end(),
                               2);
        ++curr_size;
      }
      tmp_map[curr_size++] = mem_map[i];
    }
    mem_map[0] = tmp_map[0];
    uint32_t mem_map_idx = 0;
    for (auto i = 1u; i < curr_size; ++i)
      if (mem_map[mem_map_idx].Type == tmp_map[i].Type)
        mem_map[mem_map_idx].Length += tmp_map[i].Length;
      else
        mem_map[++mem_map_idx] = tmp_map[i];
    mem_map_size = mem_map_idx + 1;
    mem_map[mem_map_size].Type = 0;
  }

///////////////////////////////////////////paging

  void loadPaging(physical_ptr main_page) {
    asm volatile ("movq %%rax, %%cr3" :: "a"(main_page) : "memory");
  }

  void invlpg(void* vaddr) {
    asm volatile("invlpg (%0)" :: "r"(vaddr) : "memory");
  }

  void reloadPaging() {
    asm volatile ("movq %%cr3, %%rax; movq %%rax, %%cr3" : : : "rax", "memory");
  }

  template<uint64_t Nb>
  inline constexpr uint64_t mask = (1ull << Nb) - 1;

  page_entry* ptr_to_PML4() {
    return reinterpret_cast<page_entry*>((~0ull) << 12);
  }

  page_entry& PML3_entry(void* ptr) {
    uint64_t raw_ptr = reinterpret_cast<uint64_t>(ptr);
    //bits 48 to 39
    uintptr_t PML4_offset = (raw_ptr >> 39)&mask<9>;
    return ptr_to_PML4()[PML4_offset];
  }

  page_entry* ptr_to_PML3(void* ptr) {
    uint64_t raw_ptr = reinterpret_cast<uint64_t>(ptr);
    uintptr_t PML4_offset = (raw_ptr >> 39)&mask<9>;
    return reinterpret_cast<page_entry*>( ((~0ull) << 21) | ( PML4_offset << 12 ) );
  }

  page_entry& PML2_entry(void* ptr) {
    uint64_t raw_ptr = reinterpret_cast<uint64_t>(ptr);
    uintptr_t PML3_offset = (raw_ptr >> 30)&mask<9>;
    return ptr_to_PML3(ptr)[PML3_offset];
  }

  page_entry* ptr_to_PML2(void* ptr) {
    uint64_t raw_ptr = reinterpret_cast<uint64_t>(ptr);
    uintptr_t PML3_offset = (raw_ptr >> 30)&mask<18>;
    return reinterpret_cast<page_entry*>( ((~0ull) << 30) | ( PML3_offset << 12 ) );
  }

  page_entry& PML1_entry(void* ptr) {
    uint64_t raw_ptr = reinterpret_cast<uint64_t>(ptr);
    //if(PML2_entry(ptr) & Huge)
    //    error;
    uintptr_t PML2_offset = (raw_ptr >> 21)&mask<9>;
    return ptr_to_PML2(ptr)[PML2_offset];
  }

  page_entry* ptr_to_PML1(void* ptr) {
    uint64_t raw_ptr = reinterpret_cast<uint64_t>(ptr);
    //if(PML2_entry(ptr) & Huge)
    //    error;
    uintptr_t PML2_offset = (raw_ptr >> 21)&mask<27>;
    return reinterpret_cast<page_entry*>( ((~0ull) << 39) | ( PML2_offset << 12 ) );
  }

  page_entry& PML0_entry(void* ptr) {
    uint64_t raw_ptr = reinterpret_cast<uint64_t>(ptr);
    //if(PML1_entry(ptr) & Huge)
    //    error;
    uintptr_t PML1_offset = (raw_ptr >> 12)&mask<9>;
    return ptr_to_PML1(ptr)[PML1_offset];
  }

  physical_ptr getPhysicalAddress(void* ptr) {
    uint64_t raw_ptr = reinterpret_cast<uint64_t>(ptr);


    if (PML2_entry(ptr) & Huge) //1G page
      return reinterpret_cast<physical_ptr>((PML2_entry(ptr)&Address) + (raw_ptr & mask<30>));

    if (PML1_entry(ptr) & Huge) //2M page
      return reinterpret_cast<physical_ptr>((PML1_entry(ptr)&Address) + (raw_ptr & mask<21>));

    return reinterpret_cast<physical_ptr>((PML0_entry(ptr)&Address) + (raw_ptr & mask<12>));
  }

  void set_PML3(void* vaddr, bool global, bool user) {
    bool exists = PML3_entry(vaddr) & Present;
    set_PML0(ptr_to_PML3(vaddr), global, user, true);
    if (!exists) {
      page_entry* PML3 = ptr_to_PML3(vaddr);
      for (std::size_t i = 0; i < 512; ++i)
        PML3[i] = 0;
    }
  }

  void set_PML2(void* vaddr, bool global, bool user) {
    bool exists = PML2_entry(vaddr) & Present;
    set_PML0(ptr_to_PML2(vaddr), global, user, true);
    if (!exists) {
      page_entry* PML2 = ptr_to_PML2(vaddr);
      for (std::size_t i = 0; i < 512; ++i)
        PML2[i] = 0;
    }
  }

  void set_PML1(void* vaddr, bool global, bool user) {
    bool exists = PML1_entry(vaddr) & Present;
    set_PML0(ptr_to_PML1(vaddr), global, user, true);
    if (!exists) {
      page_entry* PML1 = ptr_to_PML1(vaddr);
      for (std::size_t i = 0; i < 512; ++i)
        PML1[i] = 0;
    }
  }

  void set_PML0(void* vaddr, bool global, bool user, bool writable) {
    page_entry& e_PML0 = PML0_entry(vaddr);
    page_entry old = e_PML0;
    if (!(e_PML0 & Present)) {
      physical_ptr pg = frame_alloc(1);
      e_PML0 = reinterpret_cast<uint64_t>(pg) | Present;
    }
    if (global)
      e_PML0 |= Global;
    else
      e_PML0 &= ~Global;
    if (writable)
      e_PML0 |= Writable;
    else
      e_PML0 &= ~Writable;
    if (user)
      e_PML0 |= User;
    //This needs to be commented: otherwise one might revoke user access
    //else
    //    e_PML0 &= ~User;
    if (old != e_PML0)
      invlpg(vaddr);
  }

  void make_mappable(uintptr_t raw_addr, uint64_t size, bool user, bool global) {
    //args aligned
    for (uint64_t off = 0; off < size; off += 0x1000) {
      void* vaddr = reinterpret_cast<void*>(raw_addr + off);
      set_PML3(vaddr, global, user);
      set_PML2(vaddr, global, user);
      set_PML1(vaddr, global, user);
    }
  }

  void make_virtual_address(void* new_addr, uint64_t size, bool user, bool writable,
                            bool global) {
    uintptr_t raw_addr = reinterpret_cast<uintptr_t>(new_addr);
    //frame align args
    size += raw_addr & mask<12>;
    raw_addr &= ~mask<12>;
    size = (size + mask<12>) & ~mask<12>;

    make_mappable(raw_addr, size, user, global);

    for (uint64_t off = 0; off < size; off += 0x1000)
      set_PML0((void*)(raw_addr + off), global, user, writable);
  }

  void map_virtual_address(void* vaddr, physical_ptr paddr, uint64_t size,
                           bool user, bool writable, bool global) {
    uintptr_t raw_vaddr = reinterpret_cast<uintptr_t>(vaddr);
    //frame align args
    size += raw_vaddr & mask<12>;
    raw_vaddr &= ~mask<12>;
    size = (size + mask<12>) & ~mask<12>;

    uintptr_t raw_paddr = reinterpret_cast<uintptr_t>(paddr);
    raw_paddr &= ~mask<12>;

    make_mappable(raw_vaddr, size, user, global);

    for (uint64_t off = 0; off < size; off += 0x1000) {
      vaddr = (void*)(raw_vaddr + off);
      page_entry& e_PML0 = PML0_entry(vaddr);
      if (e_PML0 & 1)
        frame_free(physical_ptr(e_PML0 & Address), 1);
      e_PML0 = (raw_paddr + off) | Present;
      if (global)
        e_PML0 |= Global;
      if (writable)
        e_PML0 |= Writable;
      if (user)
        e_PML0 |= User;
      invlpg(vaddr);
    }
  }

////////////////////////////frame alloc

  alloc_structure<frame_alloc_max_blk>* frame_alloc_data     = nullptr;
  alloc_structure<fake_alloc_max_blk>* fake_alloc_data       = nullptr;
  alloc_structure<special_alloc_max_blk>* special_alloc_data = nullptr;

  template<std::size_t max_blk>
  void mark_as_used(uintptr_t base, uint64_t len, alloc_structure<max_blk>* alstr) {
    //supposed in one block
    auto& mem = alstr->alloc_memory;
    std::size_t i = 0;
    std::size_t prec_blk = 0;

    while (mem[i].end() <= base) {
      prec_blk = i;
      i = mem[i].nxtBlk;
      if (i == 0)
        asm("ud2");//no block containing this : error !
    }

    if (mem[i].base == base) { //move the base of block
      mem[i].base = base + len;
      mem[i].size -= len;
    } else {//split block
      uint64_t curr_new_size = base - mem[i].base;
      uint64_t new_size = mem[i].size - curr_new_size - len;

      std::size_t newBlki = alstr->idx_stack.pop();
      mem[newBlki] = {
        .base = base + len,
        .size = new_size,
        .nxtBlk = mem[i].nxtBlk
      };
      mem[i].nxtBlk = newBlki;
      mem[i].size = curr_new_size;
      prec_blk = i;
      i = newBlki;
    }

    if (mem[i].size == 0) { //emptied this block
      if (i != 0) {
        mem[prec_blk].nxtBlk = mem[i].nxtBlk;
        alstr->idx_stack.push(i);
      }
    }
  }

  template<std::size_t max_blk>
  void mark_as_free(uintptr_t base, uint64_t len, alloc_structure<max_blk>* alstr) {
    auto& mem = alstr->alloc_memory;

    std::size_t i = 0;

    for (;;) {
      // insert
      if (mem[i].base >= base) {
        std::size_t newBlki = alstr->idx_stack.pop();
        std::memcpy(&mem[newBlki], &mem[i], sizeof(alloc_data_t));

        mem[i] = {
          .base = base,
          .size = len,
          .nxtBlk = newBlki
        };
        break;
      }

      // extend
      if (mem[i].end() >= base) {
        mem[i].size = std::max(mem[i].size, base - mem[i].base + len);
        break;
      }

      // push_back
      if (mem[i].nxtBlk == 0) {
        std::size_t newBlki = alstr->idx_stack.pop();
        mem[newBlki] = {
          .base = base,
          .size = len,
          .nxtBlk = 0
        };
        mem[i].nxtBlk = newBlki;
        i = newBlki;
        break;
      }
      i = mem[i].nxtBlk;
    }

    std::size_t nxt = mem[i].nxtBlk;
    while (nxt != 0 && mem[i].end() >= mem[nxt].base) {
      alstr->idx_stack.push(nxt);//avant car nxt modifié

      mem[i].size = std::max(mem[nxt].end(), mem[i].end()) - mem[i].base;
      mem[i].nxtBlk = nxt = mem[nxt].nxtBlk;
    }
  }

  template<std::size_t max_blk>
  void init(alloc_structure<max_blk>* alstr) {
    alstr->idx_stack.m_data[0] = 1;
    for (std::size_t i = 1; i < max_blk; ++i)
      alstr->idx_stack.push(i);

    alstr->alloc_memory[0] = {.base = 0, .size = 0, .nxtBlk = 0};
  }

  template<std::size_t max_blk>
  uintptr_t al_alloc(uint64_t size, uint64_t bitalign, alloc_structure<max_blk>* alstr) {
    std::size_t i = 0;
    while (alstr->alloc_memory[i].size_with_align(bitalign) < size) {
      i = alstr->alloc_memory[i].nxtBlk;
      if (i == 0)
        return 0;
    }
    uintptr_t ret = alstr->alloc_memory[i].aligned_base(bitalign);
    mark_as_used(ret, size, alstr);
    return ret;
  }

  void init_allocs(uint64_t bootloader_size) {
    //init
    init(frame_alloc_data);
    init(fake_alloc_data);
    init(special_alloc_data);


    //play region
    mark_as_free(0xFFFFFF0000000000, 0x0000004000000000, special_alloc_data);
    for (auto i = mem_map; i->Type != 0; ++i)
      if (i->Type == 1)
        mark_as_free(i->Base, i->Length, frame_alloc_data);
      else if (i->Type == 2)
        mark_as_free(i->Base, i->Length, fake_alloc_data);

    //already allocated
    mark_as_used(0, bootloader_size, frame_alloc_data);
    mark_as_used(0xB8000, 0x1000, fake_alloc_data);
    mark_as_used(Terminal::vga_buffer_addr, 0x1000, special_alloc_data);

    //reserving spec_addr (init allocs called in bootloader, address are physical)
    void* vaddr = special_alloc(sizeof(*frame_alloc_data));
    map_virtual_address(vaddr, frame_alloc_data, sizeof(*frame_alloc_data));
    frame_alloc_data = (decltype(frame_alloc_data))vaddr;

    vaddr = special_alloc(sizeof(*fake_alloc_data));
    map_virtual_address(vaddr, fake_alloc_data, sizeof(*fake_alloc_data));
    fake_alloc_data = (decltype(fake_alloc_data))vaddr;

    vaddr = special_alloc(sizeof(*special_alloc_data));
    map_virtual_address(vaddr, special_alloc_data, sizeof(*special_alloc_data));
    special_alloc_data = (decltype(special_alloc_data))vaddr;
  }

  uintptr_t alloc_data_t::aligned_base(uint64_t bitalign) {
    uint64_t mask = (1ull << bitalign) - 1;
    return (base + mask) & ~mask;
  }

  uint64_t alloc_data_t::size_with_align(uint64_t bitalign) {
    if (end() < aligned_base(bitalign))
      return 0;
    else
      return end() - aligned_base(bitalign);
  }

  physical_ptr phys_alloc(uint64_t size, uint64_t bitalign) {
    return (physical_ptr)al_alloc(size, bitalign, frame_alloc_data);
  }
  void phys_free(physical_ptr base, uint64_t size) {
    mark_as_free((uintptr_t)base, size, frame_alloc_data);
  }

  physical_ptr fake_alloc(uint64_t size, uint64_t bitalign) {
    return (physical_ptr)al_alloc(size, bitalign, fake_alloc_data);
  }
  void fake_free(physical_ptr base, uint64_t size) {
    mark_as_free((uintptr_t)base, size, fake_alloc_data);
  }

  void* special_alloc(uint64_t size, uint64_t bitalign) {
    return (void*)al_alloc(size, bitalign, special_alloc_data);
  }
  void special_free(void* base, uint64_t size) {
    mark_as_free((uintptr_t)base, size, special_alloc_data);
  }

  physical_ptr frame_alloc(uint64_t nb, uint64_t bitalign) {
    return phys_alloc(nb * 0x1000, bitalign);
  }
  void frame_free(physical_ptr base, uint64_t nb) {
    phys_free(base, nb * 0x1000);
  }

  void free_bootloader(uint64_t bootloader_size) {
    mark_as_free(0, bootloader_size, frame_alloc_data);

    mark_as_used((uintptr_t)getPhysicalAddress(frame_alloc_data),
                 sizeof(*frame_alloc_data), frame_alloc_data);

    mark_as_used((uintptr_t)getPhysicalAddress(fake_alloc_data),
                 sizeof(*fake_alloc_data), frame_alloc_data);

    mark_as_used((uintptr_t)getPhysicalAddress(special_alloc_data),
                 sizeof(*special_alloc_data), frame_alloc_data);

    mark_as_used((uintptr_t)getPhysicalAddress(ptr_to_PML4()),
                 0x1000, frame_alloc_data);
  }

}
