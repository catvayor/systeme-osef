#include "usb.hpp"
#include "pci.hpp"

namespace USB {

  constexpr uint8_t serial_controller_class = 0x0C;
  constexpr uint8_t USB_subclass = 0x03;

  constexpr uint8_t UHCI_IF = 0x00;
  constexpr uint8_t OHCI_IF = 0x10;
  constexpr uint8_t EHCI_IF = 0x20;
  constexpr uint8_t XHCI_IF = 0x30;

  bool is_supported_controller(uint32_t pci_addr) {
    uint32_t dev_class = PCI::read(pci_addr | PCI::device_class);
    if (((dev_class >> 24) & 0xFF) != serial_controller_class ||
        ((dev_class >> 16) & 0xFF) != USB_subclass)
      return false;
    uint32_t IF = (dev_class >> 8) & 0xFF;
    return false
           //|| IF == UHCI_IF
           //|| IF == OHCI_IF
           || IF == EHCI_IF
           //|| IF == XHCI_IF
           ;
  }

}
