#include "terminal.hpp"
#include <functional>

namespace Terminal {
  constexpr int row_nb = 25;
  constexpr int col_nb = 80;

  int curr_col = 0;

  uint16_t volatile& char_at(int x, int y) {
    return *( ((uint16_t volatile*)vga_buffer_addr) + y * col_nb + x );
  }

  void clear() {
    for (int y = 0; y < row_nb; ++y)
      for (int x = 0; x < col_nb; ++x)
        char_at(x, y) = 0;
    curr_col = 0;
  }

  void newline() {
    for (int y = 0; y < row_nb - 1; ++y)
      for (int x = 0; x < col_nb; ++x)
        char_at(x, y) = char_at(x, y + 1);
    for (int x = 0; x < col_nb; ++x)
      char_at(x, row_nb - 1) = 0;
    curr_col = 0;
  }

  void putc(char c, uint8_t color) {
    if (c == '\n') {
      newline();
      return;
    }
    uint16_t code = (((uint16_t)color) << 8) | c;
    if (curr_col == col_nb)
      newline();
    char_at(curr_col++, row_nb - 1) = code;
  }

  void putc_at(char c, int x, int y, uint8_t color) {
    uint16_t code = (((uint16_t)color) << 8) | c;
    char_at(x, y) = code;
  }

  void print(const char* str, uint8_t color) {
    for (; *str != 0; ++str)
      putc(*str, color);
  }

  void print_at(const char* str, int x, int y, uint8_t color) {
    for (; *str != 0; ++str)
      putc_at(*str, x++, y, color);
  }
}
