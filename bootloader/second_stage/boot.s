    .code16
     #sector 1

    movl (0x7c00+80), %eax
    movl %eax, fatoffset
    movl (0x7c00+84), %eax
    movl %eax, fatlen
    movl (0x7c00+88), %eax
    movl %eax, heapoff
    movb (0x7c00+109), %al
    movb %al, sect_clus_shift
    addb $9, %al
    movb %al, byte_clus_shift
    movl $1,%esi
    movb %al,%cl
    sall %cl,%esi
    movl %esi, byte_per_clus


    movl $0,%edi
    movl (0x7c00+96), %esi
    call read_fat_chain

    movl $sysScan, %esi
    call call_overfat

    jmp sysnFound

real_lock:
    cli
    hlt
    jmp real_lock


.long 0
.align 512 #sector 2

.long 0
.align 512 #sector 3

.long 0
.align 512 #sector 4

.long 0
.align 512 #sector 5

.long 0
.align 512 #sector 6

load:
    pushw %cx
    call loadclus
    popw %cx
    addw %ax, %cx
    ret

bootfileName:
    .byte 'b',0,'o',0,'o',0,'t',0

bootScan:
    movw $0x7C0, %cx
    call loadclus
    movl $0x7c00, %ebx
1:
    testb $0xFF, (%ebx)
    jz 3f
    cmpb $0x85, (%ebx)
    jne 2f
    cmpw $0x20, 4(%ebx)
    jne 2f

    addl $32, %ebx #stream extension entry

    cmpb $0xC0,(%ebx)
    jne 2f
    cmpb $4,3(%ebx)
    jne 2f

    addl $32, %ebx #filename entry

    cmpb $0xC1,(%ebx)
    jne 2f

    xorl %ecx, %ecx
    movl $bootfileName, %ebp
4:
    movb 2(%ebx,%ecx), %al
    cmpb %al, (%ebp,%ecx)
    jne 2f

    incl %ecx
    cmpl $8,%ecx
    jne 4b

    subl $32, %ebx
    xorl %edi, %edi
    testb $2, 1(%ebx)
    jz 4f
    movl 24(%ebx), %edi
4:
    movl 20(%ebx), %esi
    call read_fat_chain

    movw byte_per_clus, %ax
    sarw $4, %ax
    movw $0x7C0, %cx
    movl $load, %esi
    call call_overfat

    movw $0x7C00, %ax
    call *%ax

2:
    addl $32,%ebx
    jmp 1b
3:
    ret

sysdirName:
    .byte 's',0,'y',0,'s',0

sysScan:
    movw $0x7C0, %cx
    call loadclus
    movl $0x7c00, %ebx
1:
    testb $0xFF, (%ebx)
    jz 3f
    cmpb $0x85, (%ebx)
    jne 2f
    cmpw $0x10, 4(%ebx)
    jne 2f

    addl $32, %ebx #stream extension entry

    cmpb $0xC0,(%ebx)
    jne 2f
    cmpb $3,3(%ebx)
    jne 2f

    addl $32, %ebx #filename entry

    cmpb $0xC1,(%ebx)
    jne 2f

    xorl %ecx, %ecx
    movl $sysdirName, %ebp
4:
    movb 2(%ebx,%ecx), %al
    cmpb %al, (%ebp,%ecx)
    jne 2f

    incl %ecx
    cmpl $6,%ecx
    jne 4b

    subl $32, %ebx
    xorl %edi, %edi
    testb $2, 1(%ebx)
    jz 4f
    movl 24(%ebx), %edi
4:
    movl 20(%ebx), %esi
    call read_fat_chain

    movl $bootScan, %esi
    call call_overfat

    jmp bootnFound

2:
    addl $32,%ebx
    jmp 1b
3:
    ret

.long 0
.align 512 #sector 7

fatoffset:
    .long 0
fatlen:
    .long 0 #only the 2 low bytes read
heapoff:
    .long 0
sect_clus_shift:
    .byte 0
byte_clus_shift:
    .byte 0
byte_per_clus:
    .long 0

read_fat_chain:#esi -> clus_num, edi -> len, will load fat
    testl %edi,%edi
    jnz 3f

    pushl %esi
    pushw %ds
    subw $16,%sp
    movb $16,%ss:(%esp)
    movb $0,%ss:1(%esp)
    movw fatlen,%si
    movw %si,%ss:2(%esp)
    movl $0x07C00000,%ss:4(%esp)
    movl fatoffset,%esi
    movl %esi,%ss:8(%esp)
    movl $0,%ss:12(%esp)

    movw %ss,%si
    movw %si,%ds
    movw %sp,%si
    movb $0x42,%ah
    int $0x13
    jc read_error
    addw $16,%sp
    popw %ds
    popl %esi
    pushw %fs
    movw $0x7C0,%cx
    movw %cx,%fs
    movl $0x7000,%ecx
    jmp 2f
1:
    imull $4,%esi
    movl %fs:(%esi),%esi
2:
    movl %esi, (%ecx)
    addl $4,%ecx
    cmpl $0xFFFFFFF8,%esi
    jb 1b

    popw %fs
    ret
3:
    movl $0x7000,%ecx
1:
    movl %esi,(%ecx)
    addl $4,%ecx
    incl %esi
    subl byte_per_clus,%edi
    jg 1b
    movl $0xFFFFFFFF,(%ecx)

    ret

loadclus:#esi -> clus_nb, cx -> dest segment
    pushw %ds
    pushw %ax
    subw $16,%sp

    movb $16, %ss:(%esp)
    movb $0, %ss:1(%esp)
    movw $0, %ss:4(%esp)
    movl $0, %ss:12(%esp)
    movw %cx, %ss:6(%esp)

    subl $2, %esi
    movb sect_clus_shift, %cl
    sall %cl, %esi
    addl heapoff, %esi
    movl %esi, %ss:8(%esp)

    movw $1, %si
    salw %cl, %si
    movw %si, %ss:2(%esp)

    movw %ss,%si
    movw %si,%ds
    movw %sp,%si
    movb $0x42,%ah
    int $0x13
    jc read_error

    addw $16,%sp
    popw %ax
    popw %ds

    ret

call_overfat:#call *%esi with arg %esi -> clus_nb, no clobber
    pushl %esi
    pushl $0x7000
1:
    movl %ss:(%esp), %esi
    movl (%esi),%esi
    addl $4, %ss:(%esp)
    cmpl $0xFFFFFFF8, %esi
    jae 1f

    call *%ss:4(%esp)

    jmp 1b
1:
    popl %esi
    popl %esi
    ret


.long 0
.align 512 #sector 8

1: .string "'sys' directory not found\n"
sysnFound:
    movl $1b, %esi
    call printstr
    jmp real_lock

1: .string "'boot' file not found\n"
bootnFound:
    movl $1b, %esi
    call printstr
    jmp real_lock

1: .string "error reading at : "
2: .string "len : "
3: .string "error code : "
read_error:
    pushw %ax
    movl %ds:8(%si), %ebx
    movl %ebx, fatoffset
    movw %ds:2(%si), %bx
    movw %bx, fatlen

    xorw %si,%si
    movw %si,%ds
    movw $1b,%si
    call printstr
    movl fatoffset, %esi
    call printreg32
    movw $2b,%si
    call printstr
    movw fatoffset, %si
    call printreg16
    movw $3b,%si
    call printstr
    popw %si
    rolw $8,%si
    call printreg8

    jmp real_lock

printstr:
    movb $0xE, %ah
    movb $0, %bh
1:    lodsb
    testb %al, %al
    jz 2f
    cmpb $0xA, %al
    jne 3f
    int $0x10
    movb $0xD, %al
3:    int $0x10
    jmp 1b
2:    ret

hexstr:  .byte '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'

printreg8:
    movw $2, %cx
    roll $24, %esi
    jmp 1f

printreg16:
    movw $4, %cx
    roll $16, %esi
    jmp 1f

printreg32:
    movw $8, %cx
1:
    movw $hexstr, %bp
    movb $0xE, %ah
    movb $0, %bh
1:
    roll $4, %esi
    movw %si, %di
    andw $0x0f, %di
    addw %bp, %di
    movb (%di), %al
    int $0x10

    decw %cx
    jnz 1b

printNewline:
    movb $0, %bh
    movw $0x0E0D, %ax
    int $0x10
    movb $0xA, %al
    int $0x10
    ret

.long 0
