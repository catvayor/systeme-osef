#include "fat_wrapper.hpp"
#include "file_wrapper.hpp"
#include <fs_struct>
#include <terminal>
#include <kallocator>
#include <quenya_base>
#include <virtual_memory_base>
#include <boot_info>

extern "C" const uint8_t driveCode;

quenya::call_point_t kernel_load() {
  uint8_t ata_drive = driveCode & ~0x80;

  bootsector_t bootsector;
  Drive::ide_read_sectors(ata_drive, 0, 1, (uint16_t*)&bootsector);
  std::vector<uint32_t, kallocator<uint32_t> > fat_chain;
  fat_wrapper<kallocator<uint32_t> > fat(
    ata_drive,
    bootsector.fat_off,
    bootsector.fatLength,
    bootsector.cluster_count,
    512 << bootsector.cluster_shift);

  file_wrapper<kallocator<uint32_t>,
               kallocator<char> > root_dir(
                 ata_drive,
                 bootsector.clusterHead_off,
                 512 << bootsector.cluster_shift);
  root_dir.load_clus_chain(&fat, bootsector.root, 0);
  root_dir.read_data();

  stream_extension_entry_t* str_sys;

  for (auto ent = (dir_entry_t*)root_dir.data(); !ent->is_null(); ++ent) {
    if (ent->type != File || ((file_entry_t*)ent)->attrib != 0x10)
      continue;
    str_sys = (stream_extension_entry_t*)++ent;
    if (str_sys->nameLen != 3)
      continue;
    ++ent;
    if (std::memcmp(u"sys", ((filename_entry_t*)ent)->filename, 6) == 0)
      goto sysFound;
  }

  Terminal::print("'sys' dir not found !", Terminal::fg_dred);
  for (;;) asm("hlt");
sysFound:
  Terminal::print("'sys' dir found\n", Terminal::fg_dgreen);

  file_wrapper<kallocator<uint32_t>,
               kallocator<char> > sys_dir(
                 ata_drive,
                 bootsector.clusterHead_off,
                 512 << bootsector.cluster_shift);
  sys_dir.load_clus_chain(&fat,
                          str_sys->first_clus,
                          ((str_sys->seconFlags >> 1) & 1)*str_sys->len);
  sys_dir.read_data();

  stream_extension_entry_t* str_kernel;
  for (auto ent = (dir_entry_t*)sys_dir.data(); !ent->is_null(); ++ent) {
    if (ent->type != File || ((file_entry_t*)ent)->attrib != 0x20)
      continue;
    str_kernel = (stream_extension_entry_t*)++ent;
    if (str_kernel->nameLen != 10)
      continue;
    ++ent;
    if (std::memcmp(u"kernel.elf", ((filename_entry_t*)ent)->filename, 20) == 0)
      goto kernelFound;
  }

  Terminal::print("'kernel.elf' file not found !", Terminal::fg_dred);
  for (;;) asm("hlt");

kernelFound:
  Terminal::print("'kernel.elf' file found\n", Terminal::fg_dgreen);

  file_wrapper<kallocator<uint32_t>,
               kallocator<char> > kernel_file(
                 ata_drive,
                 bootsector.clusterHead_off,
                 512 << bootsector.cluster_shift);
  kernel_file.load_clus_chain(&fat,
                              str_kernel->first_clus,
                              ((str_kernel->seconFlags >> 1) & 1)*str_kernel->len);
  kernel_file.read_data();

  return quenya::loadElfHeader_risky(kernel_file.data(), false, true);
}

void launch_kernel() {
  auto kernel_start_point = kernel_load();

  uintptr_t stack = 0xFFFF808000000000;
  virtual_memory::make_virtual_address((void*)(stack - (1 << 20)), 1 << 20);

  store_boot_info();

  kernel_start_point();
}

