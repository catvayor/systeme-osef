    .section .begin

    .code16

    jmp _start
# -------------------- utils
printstr:
    movb $0xE, %ah
    movb $0, %bh
1:    lodsb
    testb %al, %al
    jz 2f
    cmpb $0xA, %al
    jne 3f
    int $0x10
    movb $0xD, %al
3:    int $0x10
    jmp 1b
2:    ret

hexstr:  .byte '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
printreg16:
    movw $hexstr, %dx
    movw $4, %cx
    movb $0xE, %ah
    movb $0, %bh
1:
    rolw $4, %si
    movw %si, %di
    andw $0x0f, %di
    addw %dx, %di
    movb (%di), %al
    int $0x10

    decw %cx
    jnz 1b

printNewline:
    movb $0, %bh
    movw $0x0E0D, %ax
    int $0x10
    movb $0xA, %al
    int $0x10
    ret


# ------------------------------------

_start:
    movw $0, %ax
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    movw %ax, %ss
    movw $0x7c00, %sp
    movb %dl, driveCode #save drive

    #show it
    movw $driveMSG, %si
    call printstr

    movb $0, %dh
    movb driveCode, %dl
    movw %dx, %si
    call printreg16

    call detectMemory 

# disable bios blinking cursor
    mov $32, %ch
    mov $1, %ah
    int $0x10

# notify BIOS that we're going to do 64bit
    mov $0xEC00, %ax
    mov $2, %bl
    int $0x15
    cli

# switch to protected mode
    lgdt gdtinfo
    movl %cr0, %eax
    orb $1, %al
    movl %eax, %cr0
    jmp $0x08, $main_prot

real_lock:
    cli
    hlt
    jmp real_lock

//data
driveMSG:   .string "Booting from : "
sectTold:   .string "Sectors to load : "
rstRet:     .string "Reset disk returns : "
ldMSG:      .string "Loading sectors returns : "

//bss
    .globl driveCode
driveCode:  .byte 0
    .globl mem_detect_size
mem_detect_size: .long 0

# not in boot sector
    .section .rodata

noa20:      .string "a20 disabled\n"
oka20:      .string "a20 enabled\nSwitching to protected mode..."

gdt:
    .byte  0,0,0,0, 0,0,0,0
    .byte  0xff,0xff,0,0, 0,0b10011010,0b11001111,0
    .byte  0xff,0xff,0,0, 0,0b10010010,0b11001111,0
gdtinfo:
    .short gdtinfo - gdt - 1
    .long  gdt

    .bss
    .zero 0x20000
    stack_top:

    .code32

    .text
    
main_prot:
    movw $0x10, %ax
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    movw %ax, %ss
    movl $stack_top, %esp

    call term_clear
    .section .rodata
1:  .string "Switched to protected mode\n"
    .text
    pushl $1b
    call term_print
    popl %edi
    
# checking cpuid
    .section .rodata
1:  .string "Checking cpuid : "
    .text
    pushl $1b
    call term_print
    popl %edi

    pushfl
    popl %eax
    movl %eax, %ecx
    xorl $(1 << 21), %eax
    pushl %eax
    popfl
    pushfl
    popl %eax
    pushl %ecx
    popfl
    xorl %ecx, %eax
    jz NoCPUID
    .section .rodata
1:  .string "ok\nChecking long mode : "
    .text
    pushl $1b
    call term_print
    popl %edi

# long mode check
    movl $0x80000000, %eax
    cpuid
    cmpl $0x80000001, %eax
    jb NoLongMode
    movl $0x80000001, %eax
    cpuid
    testl $(1 << 29), %edx
    jz NoLongMode
    .section .rodata
1:  .string "ok\n"
    .text
    pushl $1b
    call term_print
    popl %edi

    call prot_boot_main

    cli
lock:
    hlt
    jmp lock

NoCPUID:
    .section .rodata
1:  .string "no"
    .text

    pushl $1b
    call term_print
    popl %edi
    jmp lock

NoLongMode:
    .section .rodata
1:  .string "no"
    .text

    pushl $1b
    call term_print
    popl %edi
    jmp lock

    .section .rodata
LMgdt:
    .quad 0

    .long 0xffff
    .byte 0,0b10011010,0b10101111,0
    
    .long 0xffff
    .byte 0,0b10010010,0b11001111,0

    .long 0x00000068
    .long 0x00CF8900
LMgdtinfo:
    .short LMgdtinfo - LMgdt - 1
    .long  LMgdt

    .text
    .globl switch_to_longMode
switch_to_longMode:

    # setup CR0
    xorl %eax, %eax
    btsl $0, %eax # protected
    btsl $31, %eax # paging
    btsl $16, %eax # write-protect
    movl %eax, %cr0

    # CR3 & CR4 already setup

    lgdt LMgdtinfo
    jmp $0x8, $main_LM

    .code64
main_LM:
    cli
    movw $0x10, %ax
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    movw %ax, %ss
    movq $stack_top, %rsp

# print 'done'
    movq $0x0F650F6E0F6F0F64, %rax
    movq %rax, (0xB8000)
   
    call LM_boot_main

1:  hlt 
    jmp 1b
