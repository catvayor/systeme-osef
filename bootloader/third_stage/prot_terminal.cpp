asm(".code32");
#include "prot_terminal.hpp"

constexpr int term_row = 25;
constexpr int term_col = 80;

int term_curr_col;

uint16_t volatile& term_at(int x, int y) {
  return *(((uint16_t volatile*)0xb8000) + term_col * y + x);
}

extern "C" {

  void term_clear() {
    for (int y = 0; y < term_row; ++y)
      for (int x = 0; x < term_col; ++x)
        term_at(x, y) = 0;
    term_curr_col = 0;
  }

  void term_newline() {
    for (int y = 0; y < term_row - 1; ++y)
      for (int x = 0; x < term_col; ++x)
        term_at(x, y) = term_at(x, y + 1);
    for (int x = 0; x < term_col; ++x)
      term_at(x, term_row - 1) = 0;
    term_curr_col = 0;
  }

  void term_putc(char c, char color) {
    if (c == '\n') {
      term_newline();
      return;
    }
    uint16_t code = (color << 8) | c;
    if (term_curr_col == term_col)
      term_newline();
    term_at(term_curr_col++, term_row - 1) = code;
  }

  void term_print(const char* str) {
    for (; *str != 0; ++str)
      term_putc(*str);
  }

  void term_printHex32(uint32_t v) {
    for (int i = 0; i < 8; ++i) {
      asm volatile("roll $4,%0" :"=r"(v) : "0"(v) );
      term_putc("0123456789ABCDEF"[v & 0x0f]);
    }
  }

}
