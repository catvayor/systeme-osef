#pragma once
#include <vector>
#include <cstddef>
#include <ata>
#include "fat_wrapper.hpp"

template<typename Clus_alloc, typename Data_alloc>
struct file_wrapper {

  file_wrapper(uint8_t fs_drive, uint32_t clus_off, uint64_t byte_per_clus, Clus_alloc cal = Clus_alloc(), Data_alloc dal = Data_alloc())
    : m_drive(fs_drive), m_clus_off(clus_off), m_byte_per_clus(byte_per_clus), m_clus_list(0, cal), m_data(0, dal) { }

  void read_data() {
    std::size_t nb_read = 0;
    m_data.resize(m_clus_list.size()*m_byte_per_clus);
    for (auto clus : m_clus_list) {
      uint8_t reading = std::min<uint32_t>(m_byte_per_clus / 512, 256) & 0xFF;
      Drive::ide_read_sectors(m_drive, // drive
                              m_clus_off + (clus - 2)*m_byte_per_clus / 512, // lba
                              reading, // numsects
                              (uint16_t*)(m_data.data() + nb_read) );
      nb_read += reading * 512;
    }
  }

  void* data() {
    return m_data.data();
  }

  template<typename fat_alloc>
  void load_clus_chain(fat_wrapper<fat_alloc>* fat, uint32_t first_clus, uint64_t len = 0) {
    fat->unroll_fatchain(m_clus_list, first_clus, len);
  }

 private:
  uint8_t m_drive;
  uint32_t m_clus_off;
  uint64_t m_byte_per_clus;
  std::vector<uint32_t, Clus_alloc> m_clus_list;
  std::vector<char, Data_alloc> m_data;
};
