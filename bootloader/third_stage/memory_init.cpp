#include <terminal>
#include <virtual_memory_base>
#include <type_traits>
#include <boot_info>

std::remove_reference_t<decltype(*virtual_memory::frame_alloc_data)>
fr_alloc alignas(0x1000);
std::remove_reference_t<decltype(*virtual_memory::fake_alloc_data)>
fa_alloc alignas(0x1000);
std::remove_reference_t<decltype(*virtual_memory::special_alloc_data)>
sp_alloc alignas(0x1000);

extern "C" const char __end_of_bootloader__;//linker var

void init_terminal() {
  void* vga_buf_virt = (void*)Terminal::vga_buffer_addr;
  constexpr uint64_t vga_buf_phys = 0xB8000;
  virtual_memory::map_virtual_address(
    vga_buf_virt,
    virtual_memory::physical_ptr(vga_buf_phys),
    0x1000,
    true);

  Terminal::clear();
  Terminal::print("Init Terminal:");
  Terminal::print(" DONE\n", Terminal::fg_dgreen);
}

void print_mem_map() {
  using namespace Terminal;
  using namespace virtual_memory;
  memory_map_entry_t* mem_map = (memory_map_entry_t*)0x1000;

  print("Memory map : ");

  print("\nBase            | Length         | Type\n");
  for (auto i = mem_map; i->Type != 0; ++i) {
    print_hex(i->Base);
    putc(' ');
    print_hex(i->Length);
    putc(' ');
    print_hex(i->Type);
    newline();
  }
}

void mem_init() {
  bootloader_size = (uint64_t)&__end_of_bootloader__;

  virtual_memory::frame_alloc_data = &fr_alloc;
  virtual_memory::fake_alloc_data = &fa_alloc;
  virtual_memory::special_alloc_data = &sp_alloc;

  virtual_memory::normalize_mem_map();
  virtual_memory::init_allocs(bootloader_size);

  init_terminal();
  print_mem_map();
}
