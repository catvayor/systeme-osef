asm(".code32");
#include "memory.hpp"
#include "prot_terminal.hpp"

std::array<uint64_t, 512> PML4_table alignas(0x1000);
std::array<uint64_t, 512> PML3_table alignas(0x1000);
std::array<uint64_t, 512> PML2_table alignas(0x1000);

void init_paging() {
  term_print("Setting up 1G LM page...");

  for (int i = 1; i < 511; i++)
    PML4_table[i] = 0;
  for (int i = 1; i < 512; i++)
    PML3_table[i] = 0;
  for (int i = 0; i < 512; i++)
    PML2_table[i] = i * 0x200000 | Huge | Writable | Present;
  PML3_table[0] = reinterpret_cast<uintptr_t>(PML2_table.data()) | Writable | Present;
  PML4_table[0] = reinterpret_cast<uintptr_t>(PML3_table.data()) | Writable | Present;
  PML4_table[511] = reinterpret_cast<uintptr_t>(PML4_table.data()) | Writable | Present;

  setPAE(true);
  loadPaging(PML4_table.data());
  setLM(true);
  setPaging(true);
  term_print(" done\n");
}

void loadPaging(void* main_page) {
  asm volatile ("movl %0, %%cr3" :: "r"(main_page));
}

void setPAE(bool PAE) {
  if (PAE)
    asm volatile ("movl %%cr4,%%eax;  btsl $5, %%eax; movl %%eax, %%cr4" : : : "eax");
  else
    asm volatile ("movl %%cr4,%%eax;  btcl $5, %%eax; movl %%eax, %%cr4" : : : "eax");
}

void setLM(bool LM) {
  if (LM)
    asm volatile ("movl $0xC0000080, %%ecx; rdmsr; btsl $8, %%eax; wrmsr" : : : "eax", "ecx");
  else
    asm volatile ("movl $0xC0000080, %%ecx; rdmsr; btcl $8, %%eax; wrmsr" : : : "eax", "ecx");
}

void setPaging(bool paging) {
  if (paging)
    asm volatile ("movl %%cr0, %%eax; btsl $31, %%eax; movl %%eax, %%cr0" ::: "eax");
  else
    asm volatile ("movl %%cr0, %%eax; btcl $31, %%eax; movl %%eax, %%cr0" ::: "eax");
}

