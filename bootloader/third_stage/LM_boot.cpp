#include <ata>
#include <terminal>
#include <kallocator>
#include <pci>
#include <usb>
#include <kmalloc>
#include <interact>
#include <virtual_memory_base>

extern void launch_kernel();
extern void mem_init();

extern "C" [[noreturn]] void LM_boot_main() {
  mem_init();

  Interact::print_init("Heap/Malloc");

  void* heap_st = (void*)0xFFFFFF4000000000;
  virtual_memory::make_virtual_address(heap_st, 0x1000, false, true, true);
  init_malloc(heap_st, 0x1000);

  Interact::print_done();

  {
    //scope to free vector memory
    std::vector<uint32_t, kallocator<uint32_t> > PCIs;
    PCI::enumerate(PCIs);
    for (uint32_t pci : PCIs) {
      Terminal::print("PCI FOUND : ");
      Terminal::print_hex(PCI::read(pci | PCI::device_class));
      if (USB::is_supported_controller(pci))
        Terminal::print(" usb");
      Terminal::newline();
    }
  }

  Drive::init();

  launch_kernel();

  asm("cli");
  for (;;) asm("hlt");
}

