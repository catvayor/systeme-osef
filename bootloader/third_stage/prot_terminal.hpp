#pragma once
#include <stdint.h>

uint16_t volatile& term_at(int x, int y);
extern "C" void term_clear();
extern "C" void term_newline();
extern "C" void term_putc(char c, char color = 0x0f);
extern "C" void term_print(const char* str);
extern "C" void term_printHex32(uint32_t v);
