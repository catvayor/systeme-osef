#pragma once
#include <vector>
#include <cstddef>
#include <ata>

template<typename Alloc>
struct fat_wrapper {

  constexpr static uint32_t clus_lim = 0xFFFFFFF8u;

  fat_wrapper(uint8_t fs_drive, uint32_t fat_off, uint64_t fat_len, uint32_t nb_clus, uint64_t byte_per_clus, Alloc al = Alloc())
    : m_drive(fs_drive), m_fat_off(fat_off), m_fat_len(fat_len), m_nb_clus(nb_clus), m_byte_per_clus(byte_per_clus), m_fat_data(0, al) { }

  template<typename Ret_alloc>
  void unroll_fatchain(std::vector<uint32_t, Ret_alloc>& dest, uint32_t first_clus, uint64_t len = 0) { //clear dest
    dest.clear();
    while (len != 0) { //nofat
      dest.push_back(first_clus);
      ++first_clus;
      if (len <= m_byte_per_clus)
        return;
      len -= m_byte_per_clus;
    }
    while (first_clus < clus_lim) {
      dest.push_back(first_clus);
      if (first_clus > m_nb_clus + 3)
        asm("ud2");
      grow_fat(first_clus);
      first_clus = m_fat_data[first_clus];
    }
  }

 private:
  uint8_t m_drive;
  uint32_t m_fat_off;
  uint32_t m_fat_len;
  uint32_t m_nb_clus;
  uint64_t m_byte_per_clus;
  std::vector<uint32_t, Alloc> m_fat_data;

  void grow_fat(uint32_t inc_clus) {
    constexpr uint32_t ent_per_sect = (512 / sizeof(uint32_t));
    if (inc_clus < m_fat_data.size())
      return;
    uint32_t curr_nb_ent = m_fat_data.size();
    uint32_t nb_read = m_fat_data.size() / ent_per_sect;
    uint32_t nb_to_read = (inc_clus - m_fat_data.size() + ent_per_sect - 1) / ent_per_sect;
    m_fat_data.resize(ent_per_sect * (nb_read + nb_to_read));
    while (nb_to_read != 0) {
      uint8_t reading = std::min<uint32_t>(nb_to_read, 256) & 0xFF;
      Drive::ide_read_sectors(m_drive, // drive
                              m_fat_off + nb_read, // lba
                              reading, // numsects
                              (uint16_t*)(m_fat_data.data() + curr_nb_ent) );
      nb_to_read -= reading;
      nb_read += reading;
    }
  }
};
