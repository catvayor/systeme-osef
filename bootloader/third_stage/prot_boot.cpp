asm(".code32");

#include "memory.hpp"
#include "prot_terminal.hpp"

void term_mem_map() {
  SMAP_entry_t* mem_map = (SMAP_entry_t*)0x1000;

  term_print("Memory map : ");

  term_print("\nBase            | Length         | Type\n");
  for (auto i = mem_map; i->Type != 0; ++i) {
    term_printHex32(i->BaseH);
    term_printHex32(i->BaseL);
    term_putc(' ');
    term_printHex32(i->LengthH);
    term_printHex32(i->LengthL);
    term_putc(' ');
    term_printHex32(i->Type);
    term_newline();
  }
}

extern "C" [[noreturn]] void prot_boot_main() {
  term_mem_map();
  init_paging();
  switch_to_longMode();
}
