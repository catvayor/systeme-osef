#include <array>
#include <stdint.h>

enum GDT_Flags : uint64_t {
  GRAN_4k = 1 << 7,
  SZ_32 = 1 << 6,
  LongMode = 1 << 5
};

enum GDT_Acc : uint64_t {
  PRESENT = 1 << 7,
  NOT_SYS = 1 << 4,
  EXEC = 1 << 3,
  DC = 1 << 2,
  RW = 1 << 1,
  ACCESSED = 1 << 0
};

using SMAP_entry_t = struct {
  uint32_t BaseL;
  uint32_t BaseH;
  uint32_t LengthL;
  uint32_t LengthH;
  uint32_t Type;
  uint32_t ACPI;
} __attribute((packed));

extern SMAP_entry_t* mem_map;

#ifndef REAL

enum Entry_flags : uint64_t {
  Present = 1 << 0,
  Writable = 1 << 1,
  User = 1 << 2,
  WriteThroughCaching = 1 << 3,
  DisableCache = 1 << 4,
  Accessed = 1 << 5,
  Dirty = 1 << 6,
  Huge = 1 << 7,
  Global = 1 << 8,
};

void init_paging();
extern "C" [[noreturn]] void switch_to_longMode();
void loadPaging(void*);
void setPAE(bool);
void setLM(bool);
void setPaging(bool);

#endif
