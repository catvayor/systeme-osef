asm(".code16");
#define REAL
#include "memory.hpp"

SMAP_entry_t* mem_map = (SMAP_entry_t*)0x1000;

// load memory map to buffer - note: regparm(3) avoids stack issues with gcc in real mode
extern "C" void __attribute__((noinline)) __attribute__((regparm(3))) detectMemory() {
  SMAP_entry_t* buffer = mem_map;
  uint32_t maxentries = 0x3ff;

  uint32_t contID = 0;
  uint32_t entries = 0;
  int signature;
  int bytes;
  do {
    asm volatile ("int  $0x15"
                  : "=a"(signature), "=c"(bytes), "=b"(contID)
                  : "a"(0xE820), "b"(contID), "c"(24), "d"(0x534D4150), "D"(buffer));
    if (signature != 0x534D4150)
      return; // error
    if (bytes > 20 && (buffer->ACPI & 0x0001) == 0) {
      // ignore this entry
    } else {
      if (bytes == 20)
        buffer->ACPI = 1;
      buffer++;
      entries++;
    }
  } while (contID != 0 && entries < maxentries);
  buffer->Type = 0;
}

