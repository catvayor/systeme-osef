    .code16

    xorw %ax, %ax
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    movw %ax, %ss

    movw $0x7000, %sp

    movb $0, %dh
    movb $0, %ah
    int $0x13

    movb $0x02, %ah
    movb $8, %al
    movw $0x0002, %cx
    movb $0, %dh
    movw $0x1000, %bx
    int $0x13

    pushf
    movw %ax,%si
    call printreg16
    popf

    jc real_lock

1:
    call checka20
    jnc 1f

    call enable_A20
    jmp 1b
1:

    movw $0x1000,%ax

    jmp *%ax

real_lock:
    cli
    hlt
    jmp real_lock

checka20:
    pushw %ds
    pushw %es

    xorw %ax, %ax
    movw %ax, %ds
    movw $0xFFFF, %ax
    movw %ax, %es

    movw %ds:(0x7DFE), %di
    movw %es:(0x7E0E), %si
    cmpw %di,%si
    jne 1f

    notw %di
    movw %di, %ds:(0x7DFE)
    movw %es:(0x7E0E), %si
    notw %si
    notw %di
    movw %di, %ds:(0x7DFE)
    cmpw %di, %si
    jne 1f

    popw %ds
    popw %es
    stc
    ret

1:
    popw %ds
    popw %es
    clc
    ret

enable_A20:
    cli

    call 1f
    mov $0xAD, %al
    outb %al, $0x64

    call 1f
    mov $0xD0, %al
    outb %al, $0x64

    call 2f
    inb $0x60, %al
    pushl %eax

    call 1f
    mov $0xD1, %al
    outb %al, $0x64

    call 1f
    popl %eax
    orb $2,%al
    outb %al, $0x60

    call 1f
    movb $0xAE, %al
    outb %al, $0x64

    call 1f
    sti
    ret

1:
    inb $0x64, %al
    testb $2, %al
    jnz 1b
    ret

2:
    inb $0x64, %al
    testb $1, %al
    jz 2b
    ret

hexstr:  .byte '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
printreg16:
    movw $hexstr, %bp
    movw $4, %cx
    movb $0xE, %ah
    movb $0, %bh
1:
    rolw $4, %si
    movw %si, %di
    andw $0x0f, %di
    addw %bp, %di
    movb (%di), %al
    int $0x10

    decw %cx
    jnz 1b

printNewline:
    movb $0, %bh
    movw $0x0E0D, %ax
    int $0x10
    movb $0xA, %al
    int $0x10
    ret
